<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

Route::middleware('auth', 'auth.public')->group(function () {
	Route::get('/dashboard', [
		'as' => 'main_dashboard',
		'uses' => 'DashboardController@index'
	]);

	Route::group(['prefix' => 'profile'], function () {
		Route::get('/', [
			'as' => 'my_profile_view',
			'uses' => 'AdminUsersController@viewProfile'
		]);

		Route::post('/save', [
			'as' => 'my_profile_save',
			'uses' => 'AdminUsersController@saveProfile'
		]);
	});
});

Route::middleware('auth', 'role')->group(function () {
	/**
	 * Routes for Venue Skills Page
	 */
	Route::group(['prefix' => 'venue-skills'], function () {
		Route::get('/', [
			'as' => 'venue_skills_index',
			'uses' => 'VenueSkillsController@index'
		]);

		Route::get('/clear-venues', [
			'as' => 'clear_venue_skills',
			'uses' => 'VenueSkillsController@clearVenueSkills'
		]);

		Route::get('user-skills/view', [
			'as' => 'user_skills'
		]);

		Route::group(['prefix' => 'add-venues'], function () {

			Route::get('/view', [
				'as' => 'add_venue_skills_view',
				'uses' => 'VenueSkillsController@viewAddUserVenueSkills'
			]);

			Route::post('/save', [
				'as' => 'save_add_venue_skills',
				'uses' => 'VenueSkillsController@saveAddUserVenueSkills'
			]);

		});

		Route::group(['prefix' => 'tlc-codes'], function () {

			Route::get('/add/view', [
				'as' => 'add_tlc_codes_view',
				'uses' => 'VenueSkillsController@viewAddTLCCodes'
			]);

			Route::post('/add/save', [
				'as' => 'save_tlc_codes',
				'uses' => 'VenueSkillsController@saveTLCCodes'
			]);

			Route::get('/add-skills/view', [
				'as' => 'add_tlc_code_skills_view',
				'uses' => 'VenueSkillsController@viewAddTLCCodeSkills'
			]);

			Route::post('/skills/load', [
				'as' => 'load_tlc_skills',
				'uses' => 'VenueSkillsController@loadTLCSkills'
			]);

			Route::post('/skills/save', [
				'as' => 'save_tlc_skills',
				'uses' => 'VenueSkillsController@saveTLCSkills'
			]);

			Route::post('/skills/remove', [
				'as' => 'remove_tlc_skills',
				'uses' => 'VenueSkillsController@removeTLCSkills'
			]);

			Route::get('/employee/add/view', [
				'as' => 'add_employee_tlc_codes_view',
				'uses' => 'VenueSkillsController@viewAddEmployeeTLCCodes'
			]);

			Route::post('/employee/add/save', [
				'as' => 'save_employee_tlc_codes',
				'uses' => 'VenueSkillsController@saveEmpolyeeTLCCodes'
			]);

			Route::group(['prefix' => 'job-int'], function () {

				Route::get('/add/view', [
					'as' => 'add_tlc_job_ints',
					'uses' => 'VenueSkillsController@viewAddTLCJobInts'
				]);

				Route::post('/add/load', [
					'as' => 'load_tlc_jobs',
					'uses' => 'VenueSkillsController@loadTLCJobs'
				]);

				Route::post('/save', [
					'as' => 'save_tlc_jobs',
					'uses' => 'VenueSkillsController@saveTLCJobs'
				]);

				Route::post('/remove', [
					'as' => 'remove_tlc_jobs',
					'uses' => 'VenueSkillsController@removeTLCJobs'
				]);

			});

		});

		Route::group(['prefix' => 'user-skills'], function () {

			Route::get('/view', [
				'as' => 'update_user_skills_view',
				'uses' => 'VenueSkillsController@viewUpdateUserSkills'
			]);

			Route::post('/validate-job-int-code', [
				'as' => 'validate_job_int_code',
				'uses' => 'VenueSkillsController@validateJobIntCode'
			]);

			Route::post('/total-results', [
				'as' => 'get_users_by_ship_and_job_code',
				'uses' => 'VenueSkillsController@getUsersByShipAndjobCode'
			]);

			Route::post('/update', [
				'as' => 'update_user_skill_set_joins',
				'uses' => 'VenueSkillsController@updateUserSkillSetJoins'
			]);

			Route::post('/skill-combo-details', [
				'as' => 'get_skill_combo_details',
				'uses' => 'VenueSkillsController@getSkillComboDetails'
			]);

		});
	});

	/**
	 * Route for Background check
	 */
	Route::group(['prefix' => 'background-checks'], function () {

		Route::get('/', [
			'as' => 'background_check_index',
			'uses' => 'BackgroundChecksController@index'
		]);

		Route::post('submit-form', [
			'as' => 'background_check_submit',
			'uses' => 'BackgroundChecksController@backgroundCheck'
		]);

		Route::post('/hireright-get-job-int-packages', [
			'as' => 'background_check_hireright_get_job_package',
			'uses' => 'BackgroundChecksController@getHirerightJobPackageByJobIntId'
		]);

		Route::post('/update-document-checklist-form-upload-status', [
			'as' => 'update_document_checklist_form_upload_status',
			'uses' => 'BackgroundChecksController@updateDocumentChecklistFormUploadsStatus'
		]);

		Route::post('/save-document-checklist-form-upload', [
			'as' => 'save_document_checklist_form_upload',
			'uses' => 'BackgroundChecksController@saveDocumentChecklistFormUploads'
		]);

		Route::post('/save-document-checklist-upload', [
			'as' => 'save_document_checklist_upload',
			'uses' => 'BackgroundChecksController@saveDocumentChecklistUploads'
		]);

		Route::post('/update-created-on-document-checklist-upload', [
			'as' => 'update_created_on_document_checklist_upload',
			'uses' => 'BackgroundChecksController@updateCreatedOnDocumentChecklistUploads'
		]);

		Route::post('/update-form-candidate-data', [
			'as' => 'update_form_candidate_data',
			'uses' => 'BackgroundChecksController@updateFormCandidateData'
		]);

		Route::post('/update-form-candidate-data-manual-trigger', [
			'as' => 'update_form_candidate_data_manual_trigger',
			'uses' => 'BackgroundChecksController@updateFormCandidateDataManualTrigger'
		]);

	});

	Route::group(['prefix' => 'crisis-management'], function () {

		Route::get('/', [
			'as' => 'crisis_management_index',
			'uses' => 'CrisisManagement@index'
		]);

		Route::post('/update-all', [
			'as' => 'update_crisis_management',
			'uses' => 'CrisisManagement@updateCrisisManagement'
		]);

		Route::post('/update', [
			'as' => 'update_crisis_management_pdf_only',
			'uses' => 'CrisisManagement@updateCrisisManagementPdfOnly'
		]);

		Route::post('/add-new', [
			'as' => 'add_new_crisis_management_pdf_only',
			'uses' => 'CrisisManagement@addNewCrisisManagementPdfOnly'
		]);

		Route::post('', [
			'as' => 'cleanup_crisis_pdf',
			'uses' => 'CrisisManagement@crisisdataCleanUp'
		]);

		Route::post('', [
			'as' => 'cleanup_crisis_pd',
			'uses' => 'CrisisManagement@crisisdataCleanUpPersonal'
		]);

	});

	Route::group(['prefix' => 'ctrac-reports'], function () {

		Route::get('talent-pools', [
			'as' => 'ctrac_reports_talent_pool',
			'uses' => 'CtracReportsController@viewTalentPoolReports'
		]);

	});

	Route::group(['prefix' => 'ctrac-gateways'], function () {

		Route::get('/', [
			'as' => 'gateways_index_view',
			'uses' => 'GatewaysController@index',
		]);

		Route::post('/update-gateways', [
			'as' => 'gateway_update',
			'uses' => 'GatewaysController@updateBulkGateway'
		]);

	});

	Route::group(['prefix' => 'archive-documents'], function () {

		Route::get('/', [
			'as' => 'archive_document_index',
			'uses' => 'ArchiveDocumentsController@index',
		]);

		Route::post('/submit-search', [
			'as' => 'archive_document_submit_search',
			'uses' => 'ArchiveDocumentsController@submitSearch',
		]);

		Route::get('/download-document', [
			'as' => 'archive_document_download_document',
			'uses' => 'ArchiveDocumentsController@downloadDocument',
		]);

		Route::post('/view-document-details', [
			'as' => 'archive_document_view_document_details',
			'uses' => 'ArchiveDocumentsController@viewDocumentDetails',
		]);

		Route::get('/get-all-users', [
			'as' => 'get_all_users',
			'uses' => 'ArchiveDocumentsController@getAllUsers',
		]);

	});

	Route::group(['prefix' => 'admin-users'], function () {

		Route::get('/list', [
			'as' => 'admin_users_list',
			'uses' => 'AdminUsersController@index'
		]);

		Route::post('/save', [
			'as' => 'admin_users_save',
			'uses' => 'AdminUsersController@saveUser'
		]);

		Route::post('/delete', [
			'as' => 'admin_users_delete',
			'uses' => 'AdminUsersController@deleteUser'
		]);

	});

	Route::group(['prefix' => 'pages'], function () {

		Route::get('/list', [
			'as' => 'admin_page_list',
			'uses' => 'PagesController@index'
		]);

		Route::post('/save', [
			'as' => 'admin_save_page',
			'uses' => 'PagesController@savePage'
		]);

		Route::post('/delete', [
			'as' => 'admin_delete_page',
			'uses' => 'PagesController@deletePage'
		]);

	});

	Route::group(['prefix' => 'requisitions'], function () {

		Route::get('/upload/id-list', [
			'as' => 'requisitions_upload_id_list',
			'uses' => 'RequisitionsController@index'
		]);

		Route::post('/upload/save-id-list', [
			'as' => 'requisitions_upload_save_id_list',
			'uses' => 'RequisitionsController@uploadReq'
		]);

		Route::get('/upload/hiring-managers', [
			'as' => 'requisitions_upload_hiring_managers',
			'uses' => 'RequisitionsController@viewUploadHiringManagers'
		]);

		Route::post('/upload/save-hiring-managers', [
			'as' => 'requisitions_upload_save_hiring_managers',
			'uses' => 'RequisitionsController@uploadHiringManagers'
		]);

		Route::get('/upload/department-list', [
			'as' => 'requisitions_upload_department_list',
			'uses' => 'RequisitionsController@viewUploadDepartmentList'
		]);

		Route::post('/upload/save-department-list', [
			'as' => 'requisitions_upload_save_department_list',
			'uses' => 'RequisitionsController@uploadDepartmentList'
		]);

		Route::get('/upload/job-code-list', [
			'as' => 'requisitions_upload_job_code_list',
			'uses' => 'RequisitionsController@viewUploadJobCodeList'
		]);

		Route::post('/upload/save-job-code-list', [
			'as' => 'requisitions_upload_save_job_code_list',
			'uses' => 'RequisitionsController@uploadJobCodeList'
		]);

		Route::get('/upload/positions', [
			'as' => 'requisitions_upload_positions',
			'uses' => 'RequisitionsController@viewUploadPositions'
		]);

		Route::post('/upload/save-positions', [
			'as' => 'requisitions_upload_save_positions',
			'uses' => 'RequisitionsController@uploadPositions'
		]);

		Route::get('/upload/daily', [
			'as' => 'requisitions_upload_daily_req',
			'uses' => 'RequisitionsController@viewUploadDailyRequisitions'
		]);

		Route::post('/upload/save-daily', [
			'as' => 'requisitions_upload_save_daily_req',
			'uses' => 'RequisitionsController@uploadDailyReq'
		]);

		Route::get('/upload/closed', [
			'as' => 'requisitions_upload_closed_req',
			'uses' => 'RequisitionsController@viewUploadClosedReq'
		]);

		Route::post('/upload/save-closed', [
			'as' => 'requisitions_upload_save_closed_req',
			'uses' => 'RequisitionsController@uploadClosedReq'
		]);
	});

	Route::group(['prefix' => 'roles'], function () {

		Route::get('/list', [
			'as' => 'admin_role_list',
			'uses' => 'RolesController@index'
		]);

		Route::post('/save-role', [
			'as' => 'admin_role_save',
			'uses' => 'RolesController@saveRole'
		]);

		Route::post('/delete', [
			'as' => 'admin_delete_role',
			'uses' => 'RolesController@deleteRole'
		]);

	});

	Route::group(['prefix' => 'support'], function () {

		/*
		 * Rehire Support Items 
		 */
		Route::group(['prefix' => 'rehires'], function () {
			Route::get('/with-account', [
				'as' => 'support_rehire_with_account',
				'uses' => 'SupportController@rehireWithAccount'
			]);

			Route::post('/save-with-account', [
				'as' => 'support_rehire_with_account_save',
				'uses' => 'SupportController@saveRehireWithAccount'
			]);
		});

		Route::group(['prefix' => 'block-countries'], function () {

			Route::get('/view', [
				'as' => 'support_revert_block_countries',
				'uses' => 'SupportController@revertBlockCountries'
			]);

			Route::post('/submit-block-countries', [
				'as' => 'support_revert_block_countries_submit',
				'uses' => 'SupportController@revertBlockCountriesSubmit'
			]); 

			Route::post('/update-block-countries', [
				'as' => 'support_update_job_app_block',
				'uses' => 'SupportController@revertBlockCountriesUpdate'
			]); 
		});

		Route::group(['prefix' => 'hrsd-type'], function () {
			Route::get('/view', [
				'as' => 'support_hrsd_type_view',
				'uses' => 'SupportController@addHRSDTypeView'
			]);
		});

		Route::group(['prefix' => 'personal-info'], function () {

			Route::get('/view', [
				'as' => 'support_hrsd_personal_info_view',
				'uses' => 'SupportController@viewPersonalInfo'
			]);

			Route::post('/search', [
				'as' => 'support_hrsd_personal_info_search',
				'uses' => 'SupportController@searchPersonalInfo'
			]);
		});

		Route::group(['prefix' => 'bypass-medical-exam'], function () {

			Route::get('/view', [
				'as' => 'support_bypass_medical_exam',
				'uses' => 'SupportController@bypassMedicalExam'
			]);

			Route::post('/submit-bypass-medical-exam', [
				'as' => 'support_bypass_medical_exam_submit',
				'uses' => 'SupportController@bypassMedicalExamSubmit'
			]);

			Route::post('/update-bypass-medical-exam', [
				'as' => 'support_bypass_medical_exam_update',
				'uses' => 'SupportController@bypassMedicalExamUpdate'
			]); 

		});

	});

	Route::group(['prefix' => 'allotee'], function () {
		/*
		 * Allotee
		 */
		Route::get('/view', [
			'as' => 'allotee_view',
			'uses' => 'AlloteeController@view'
		]);

		Route::post('/get-allotee-details', [
			'as' => 'allotee_check_details',
			'uses' => 'AlloteeController@getAlloteeDetails'
		]);

		Route::post('/check-allotee-details', [
			'as' => 'allotee_fetch_details',
			'uses' => 'AlloteeController@checkAlloteeDetails'
		]);

		Route::post('/save-allotee-details', [
			'as' => 'allotee_save_details',
			'uses' => 'AlloteeController@saveAlloteeDetails'
		]);

		Route::post('/delete-allotee-details', [
			'as' => 'allotee_delete_details',
			'uses' => 'AlloteeController@deleteAlloteeDetails'
		]);
	});

	Route::group(['prefix' => 'ship-salary-matrix'], function () {
		/*
		 * Ship Salary Matrix Uploader
		 */
		Route::get('/view', [
			'as' => 'ship_salary_matrix_view',
			'uses' => 'ShipSalaryMatrixController@view'
		]);

		Route::post('/submit', [
			'as' => 'ship_salary_matrix_submit',
			'uses' => 'ShipSalaryMatrixController@uploadMatrix'
		]);
	});

	Route::group(['prefix' => 'ctrac-reports'], function () {

		Route::get('/crew-pending-completed-documents', [
			'as' => 'reports_crew_pending_completed_doc_view',
			'uses' => 'CtracReportsController@viewPendingCompletedDocuments'
		]);

		Route::post('/upload-crew-pending-completed-documents', [
			'as' => 'reports_crew_pending_completed_doc_upload',
			'uses' => 'CtracReportsController@uploadPendingCompletedDocuments'
		]); 

		Route::get('/rejected-pdf-offer-due-to-automation', [
			'as' => 'reports_rejected_pdf_offer_automation_view',
			'uses' => 'CtracReportsController@viewRejectedPdfAndOfferLetterAutomation'
		]);

		Route::post('/rejected-pdf-offer-due-to-automation/submit', [
			'as' => 'reports_rejected_pdf_offer_automation_submit',
			'uses' => 'CtracReportsController@getRejectedPdfAndOfferLetterAutomation'
		]);

		Route::get('/new-hires-travel-information', [
			'as' => 'document_travel_info_view',
			'uses' => 'CtracReportsController@viewDocumentTravelInfo'
		]);

		Route::post('/new-hires-travel-information/submit', [
			'as' => 'document_travel_info_submit',
			'uses' => 'CtracReportsController@getDocumentTravelInfo'
		]);

		Route::get('/pdf-report-list', [
			'as' => 'reports_pdf_with_job_code_view',
			'uses' => 'CtracReportsController@viewReportOnPDF'
		]);

		Route::post('/pdf-report-list/submit', [
			'as' => 'reports_pdf_with_job_code_submit',
			'uses' => 'CtracReportsController@getReportOnPDF'
		]);

		Route::get('/crew-header-pending-completed-documents', [
			'as' => 'reports_crew_header_pending_completed_doc_view',
			'uses' => 'CtracReportsController@viewHeaderPendingCompletedDocuments'
		]);

		Route::post('/upload-crew-header-pending-completed-documents', [
			'as' => 'reports_crew_header_pending_completed_doc_upload',
			'uses' => 'CtracReportsController@uploadHeaderPendingCompletedDocuments'
		]); 

	});

	Route::group(['prefix' => 'ilo'], function () {

		Route::get('/tna-violation-dashboard', [
			'as' => 'ilo_tna_violation_dashboard_view',
			'uses' => 'ILOController@viewDashboard'
		]);

		Route::get('/tna-violation-data-uploader', [
			'as' => 'ilo_tna_violation_data_uploader_view',
			'uses' => 'ILOController@viewTAndAViolation'
		]);

		Route::post('/tna-violation-data-submit', [
			'as' => 'ilo_tna_violation_data_uploader_submit',
			'uses' => 'ILOController@uploadTAndAViolation'
		]); 

	});

	Route::group(['prefix' => 'skills'], function () {

		Route::get('/uploader', [
			'as' => 'skills_view',
			'uses' => 'SkillsController@viewUploader'
		]);

		Route::post('/uploader/submit', [
			'as' => 'skills_upload_submit',
			'uses' => 'SkillsController@saveUploader'
		]); 

	});

	Route::group(['prefix' => 'ph-government-document-uploader'], function () {

		Route::get('/', [
			'as' => 'ph_government_uploader_view',
			'uses' => 'PHGovernmentUploadersController@viewUploader'
		]);

		Route::post('/submit', [
			'as' => 'ph_government_uploader_submit',
			'uses' => 'PHGovernmentUploadersController@uploadGovernmentDocuments'
		]);

		Route::get('/download-sucess-result-logs', [
			'as' => 'ph_government_uploader_success_result_logs_download',
			'uses' => 'PHGovernmentUploadersController@downloadSuccessResultLogs'
		]);

		Route::get('/download-fail-result-logs', [
			'as' => 'ph_government_uploader_fail_result_logs_download',
			'uses' => 'PHGovernmentUploadersController@downloadFailResultLogs'
		]); 

	});

	Route::group(['prefix' => 'adhocs'], function () {

		Route::group(['prefix' => 'medical-certificates'], function () {

			Route::get('/view', [
				'as' => 'adhoc_medical_cert_view',
				'uses' => 'AdhocController@viewMedicalCertificateUpload'
			]);
	
			Route::post('/submit', [
				'as' => 'adhoc_medical_cert_submit',
				'uses' => 'AdhocController@saveMedicalCertificateUpload'
			]);
	
			Route::get('/download-sucess-result-logs', [
				'as' => 'adhoc_medical_cert_success_result_logs_download',
				'uses' => 'AdhocController@downloadMedicalCertificateSuccessResultLogs'
			]);
	
			Route::get('/download-fail-result-logs', [
				'as' => 'adhoc_medical_cert_fail_result_logs_download',
				'uses' => 'AdhocController@downloadMedicalCertificateFailResultLogs'
			]); 

		});

	});

	Route::group(['prefix' => 'stcw-matrix'], function () {
		/*
		 * STCW Matrix Uploader
		 */
		Route::get('/view', [
			'as' => 'stcw_matrix_view',
			'uses' => 'STCWMatrixController@view'
		]);

		Route::post('/submit', [
			'as' => 'stcw_update',
			'uses' => 'STCWMatrixController@uploadMatrix'
		]);
		Route::get('/download-sucess-result-logs', [
				'as' => 'stcw_matrix_result_logs_download',
				'uses' => 'STCWMatrixController@downloadSTCWMatrixSuccessResultLogs'
		]);
		Route::get('/download-deleted-job-logs', [
				'as' => 'stcw_matrix_deleted_job_logs_download',
				'uses' => 'STCWMatrixController@downloadSTCWMatrixDeletedJobLogs'
		]); 
		Route::get('/download-inserted-job-logs', [
				'as' => 'stcw_matrix_inserted_job_logs_download',
				'uses' => 'STCWMatrixController@downloadSTCWMatrixInsertedJobLogs'
		]);
	});

});
