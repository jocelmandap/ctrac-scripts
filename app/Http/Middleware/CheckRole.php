<?php

namespace CTRAC\Http\Middleware;

use Closure;
use Route;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request_uri = url('/') . '/' . $request->path();
        // dd($type);
        if ($request->method() == 'GET')
        {
            $user = $request->user();
            $user_role = $user->roles->first();

            /**
             * 1 - Super Admin
             */
            if ($user_role->getId() == 1)
            {
                return $next($request);
            }

            
            $pages = $user_role->pages->sortBy('order');

            if (!$pages->isEmpty()) 
            {
                foreach ($pages as $page) 
                {
                    if(Route::has($page->getRouteName()))
                    {
                        if ($request_uri == route($page->getRouteName()))
                        {
                            return $next($request);
                        }
                    }
                }
            }
            abort(401, 'This action is unauthorized.');
        }
        
        return $next($request);
    }
}
