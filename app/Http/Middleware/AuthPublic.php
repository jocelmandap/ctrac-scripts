<?php

namespace CTRAC\Http\Middleware;

use Closure;

class AuthPublic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type = 'public')
    {
        if ($type == 'public')
        {
            return $next($request);
        }
    }
}
