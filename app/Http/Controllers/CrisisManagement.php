<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\DashboardLink;
use CTRAC\Helpers\StringUtility;
use CTRAC\Model\Users\User;
use CTRAC\Model\Forms\FormCandidateData;
use CTRAC\Helpers\DateUtility;
use CTRAC\Model\Gateways\Gateway;
use CTRAC\Model\Schools\SchoolDegreeType;
use CTRAC\Model\Users\PersonalData;
use Illuminate\Support\Facades\Auth;

class CrisisManagement extends Controller
{
    public function index()
    {
        // $data = [
        //     'first_name' => 'YOGESH KUMAR',
        //     'middle_name' => 'KUMAR',
        //     'last_name' => 'LNU',
        //     'fathers_name' => 'HARI SINGH'
        // ];
        // $personal_data_class = new PersonalData();
        // $personal_data_class->setDBConnection('ctrac_prod_db');
        // $personal_details = $personal_data_class->getPersonalDataByFullName($data);

    	$active_page = DashboardLink::CRISIS_MANAGEMENT_PAGE;

    	return view('crisis-management.view', 
    		compact(
    			'active_page'
    		)
    	);
    }

    public function updateCrisisManagement(
        Request $request, 
        Gateway $gateway_class, 
        SchoolDegreeType $school_degree_types_class
    )
    {
    	$upload_csv = $request->upload_csv;
    	$raw_data = [];

    	$request->validate([
    		'upload_csv' => 'required'
    	]);

    	$file_name = time() . date('Y-m-d') . '-crisis-management' . '.csv';
    	$request->upload_csv->storeAs('CrisisManagement', $file_name);

    	if ($request->hasFile('upload_csv')) 
    	{
    		$file_path = storage_path('app/CrisisManagement/' . $file_name);
    		$csv_data = StringUtility::crisisCsvToArray($file_path);

    		if (!empty($csv_data))
    		{
    			$raw_data = $csv_data;
    		}
    	}
    	
        $pdf_failed_data = [];
    	$pd_failed_data = [];
        $pdf_success_data = [];
        $pd_success_data = [];
        // dd($raw_data);
    	if (!empty($raw_data))
    	{
    		foreach ($raw_data as $row) 
    		{

    			$user_class = new User();
                $user_class->setDBConnection('ctrac_prod_db');
    			$user_details = $user_class->getByJDEId($row['jde_id']);

    			if (!is_null($user_details))
    			{
                    /**
                     * Update Crew PDF or Personal Data Form
                     */
    				$form_candidate_data_class = new FormCandidateData();
                    $form_candidate_data_class->setDBConnection('ctrac_prod_db');
    				$form_candidate_details = $form_candidate_data_class->getByUserId($user_details->getId());
                    /**
                     * Get Gateway Details
                     */
                    $gateway_code = (isset($row['gateway_id']) && !empty($row['gateway_id'])) ? $row['gateway_id'] : 0;
                    $gateway_details = $gateway_class->getByGatewayCode($gateway_code);

                    /*
                     * Get School Degree Type
                     */
                    $degree_type_code = (isset($row['highest_education_id']) && !empty($row['highest_education_id'])) ? $row['highest_education_id'] : 0;
                    $degree_type_details = $school_degree_types_class->getByDegreeTypeCode($degree_type_code);

                    /**
                     * Dates Details
                     */
                    $CDF_passp_issDate = $row['passport_expiry_date'];
                    $CDF_passp_expiryDate = $row['passport_expiry_date'];
                    $CDF_birthdate = $row['birthdate'];

                    if (!is_null(DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_passp_issDate = DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y');
                    }

                    if (!is_null(DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_passp_expiryDate = DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y');
                    }

                    if (!is_null(DateUtility::changeDateFormat($row['birthdate'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_birthdate = DateUtility::changeDateFormat($row['birthdate'], 'Y-m-d', 'm/d/Y');
                    }

                    /*
                     * Will occur when the user don't have a PDF
                     */
                    if (!is_null($form_candidate_details))
                    {
                        $form_candidate_data = [
                            'id' => $form_candidate_details->getId(),
                            'user_id' => $user_details->getId(),
                            'CDF_firstName' => (isset($row['first_name']) && !empty($row['first_name'])) ? $row['first_name'] : $form_candidate_details->CDF_firstName,
                            'CDF_middleName' => (isset($row['middle_name']) && !empty($row['middle_name'])) ? $row['middle_name'] : $form_candidate_details->CDF_middleName,
                            'CDF_lastName' => (isset($row['last_name']) && !empty($row['last_name'])) ? $row['last_name'] : $form_candidate_details->CDF_lastName,
                            'CDF_otherSurname' => (isset($row['other_last_name']) && !empty($row['other_last_name'])) ? $row['other_last_name'] : $form_candidate_details->CDF_otherSurname,
                            'CDF_add' => (isset($row['per_street_address']) && !empty($row['per_street_address'])) ? $row['per_street_address'] : $form_candidate_details->CDF_add,
                            'CDF_city' => (isset($row['per_city']) && !empty($row['per_city'])) ? $row['per_city'] : $form_candidate_details->CDF_city,
                            'CDF_state' => (isset($row['per_state']) && !empty($row['per_state'])) ? $row['per_state'] : $form_candidate_details->CDF_state,
                            'CDF_zip' => (isset($row['per_zip_code']) && !empty($row['per_zip_code'])) ? $row['per_zip_code'] : $form_candidate_details->CDF_zip,
                            'CDF_country_code' => (isset($row['per_country_code']) && !empty($row['per_country_code'])) ? $row['per_country_code'] : $form_candidate_details->CDF_country_code,
                            'CDF_primaryType' => 1,
                            'CDF_primaryNumCt' => (isset($row['Country Calling code']) && !empty($row['Country Calling code'])) ? $row['Country Calling code'] : $form_candidate_details->CDF_primaryNumCt,
                            'CDF_primaryNum' => (isset($row['mobile number']) && !empty($row['mobile number'])) ? $row['mobile number'] : $form_candidate_details->CDF_primaryNum,
                            'CDF_altType' => (isset($row['Alternate Number']) && !empty($row['Alternate Number'])) ? 1 : 0,
                            'CDF_altNumCt' => (isset($row['Alternate Calling Country Code']) && !empty($row['Alternate Calling Country Code'])) ? $row['Alternate Calling Country Code'] : $form_candidate_details->CDF_altNumCt,
                            'CDF_altNum' => (isset($row['Alternate Number']) && !empty($row['Alternate Number'])) ? $row['Alternate Number'] : $form_candidate_details->CDF_altNum,
                            'CDF_birthdate' => (!empty($CDF_birthdate)) ? $CDF_birthdate : null,
                            'CDF_status' => (isset($row['marital_status']) && !empty($row['marital_status'])) ? $row['marital_status'] : $form_candidate_details->CDF_status,
                            'CDF_gender' => (isset($row['sex']) && $row['sex'] == 0) ? 'female' : 'male',
                            'CDF_sss' => (isset($row['sss']) && !empty($row['sss'])) ? $row['sss'] : $form_candidate_details->CDF_sss,
                            'CDF_gateway' => (!is_null($gateway_details)) ? $gateway_details->getId() : $form_candidate_details->CDF_gateway,
                            'CDF_dateavail' => $form_candidate_details->CDF_dateavail,
                            'CDF_emailAdd' => (isset($row['email']) && !empty($row['email'])) ? $row['email'] : $form_candidate_details->CDF_emailAdd,
                            'CDF_bcity' => (isset($row['birthplace_city']) && !empty($row['birthplace_city'])) ? $row['birthplace_city'] : $form_candidate_details->CDF_bcity,
                            'CDF_bstate' => (isset($row['birthplace_state']) && !empty($row['birthplace_state'])) ? $row['birthplace_state'] : $form_candidate_details->CDF_bstate,
                            'CDF_bcountry' => (isset($row['birthplace_country_code']) && !empty($row['birthplace_country_code'])) ? $row['birthplace_country_code'] : $form_candidate_details->CDF_bcountry,
                            'CDF_education' => (!is_null($degree_type_details)) ? $degree_type_details->getId() : $form_candidate_details->CDF_education,
                            'CDF_school' => $form_candidate_details->CDF_school,
                            'CDF_ffirstName' => (isset($row['father_fname']) && !empty($row['father_fname'])) ? $row['father_fname'] : $form_candidate_details->CDF_ffirstName,
                            'CDF_fmiddleName' => (isset($row['father_mname']) && !empty($row['father_mname'])) ? $row['father_mname'] : $form_candidate_details->CDF_fmiddleName,
                            'CDF_flastName' => (isset($row['father_lname']) && !empty($row['father_lname'])) ? $row['father_lname'] : $form_candidate_details->CDF_flastName,
                            'CDF_fcountry' => (isset($row['father_country_code']) && !empty($row['father_country_code'])) ? $row['father_country_code'] : $form_candidate_details->CDF_fcountry,
                            'CDF_mfirstName' => (isset($row['mother_fname']) && !empty($row['mother_fname'])) ? $row['mother_fname'] : $form_candidate_details->CDF_mfirstName,
                            'CDF_mmiddleName' => (isset($row['mother_mname']) && !empty($row['mother_mname'])) ? $row['mother_mname'] : $form_candidate_details->CDF_mmiddleName,
                            'CDF_mlastName' => (isset($row['mother_lname']) && !empty($row['mother_lname'])) ? $row['mother_lname'] : $form_candidate_details->CDF_mlastName,
                            'CDF_mcountry' => (isset($row['mother_country_code']) && !empty($row['mother_country_code'])) ? $row['mother_country_code'] : $form_candidate_details->CDF_mcountry,
                            'CDF_passport_file' => $form_candidate_details->CDF_passport_file,
                            'CDF_passp_Num' => (isset($row['passport_num']) && !empty($row['passport_num'])) ? $row['passport_num'] : $form_candidate_details->CDF_passp_Num,
                            'CDF_passp_issCountry' => (isset($row['passport_country']) && !empty($row['passport_country'])) ? $row['passport_country'] : $form_candidate_details->CDF_passp_issCountry,
                            'CDF_passp_issDate' => (!empty($CDF_passp_issDate)) ? $CDF_passp_issDate : null,
                            'CDF_passp_expiryDate' => (!empty($CDF_passp_expiryDate)) ? $CDF_passp_expiryDate : null,
                            'CDF_EM_name' => (isset($row['EC_fullname']) && !empty($row['EC_fullname'])) ? $row['EC_fullname'] : $form_candidate_details->CDF_EM_name,
                            'CDF_EM_relationship' => (isset($row['EC_relation']) && !empty($row['EC_relation'])) ? $row['EC_relation'] : $form_candidate_details->CDF_EM_relationship,
                            'CDF_EM_phoneNum' => (isset($row['EC_phone_number']) && !empty($row['EC_phone_number'])) ? $row['EC_phone_number'] : $form_candidate_details->CDF_EM_phoneNum,
                            'CDF_EM_cel' => (isset($row['EC_cell_number']) && !empty($row['EC_cell_number'])) ? $row['EC_cell_number'] : $form_candidate_details->CDF_EM_cel,
                            'CDF_EM_email1' => (isset($row['EC_email']) && !empty($row['EC_email'])) ? $row['EC_email'] : $form_candidate_details->CDF_EM_email1,
                            'CDF_EM_add' => (isset($row['EC_street_address']) && !empty($row['EC_street_address'])) ? $row['EC_street_address'] : $form_candidate_details->CDF_EM_add,
                            'CDF_EM_city' => (isset($row['EC_city']) && !empty($row['EC_city'])) ? $row['EC_city'] : $form_candidate_details->CDF_EM_city,
                            'CDF_EM_zip' => (isset($row['EC_zip_code']) && !empty($row['EC_zip_code'])) ? $row['EC_zip_code'] : $form_candidate_details->CDF_EM_zip,
                            'CDF_EM_state' => (isset($row['EC_state']) && !empty($row['EC_state'])) ? $row['EC_state'] : $form_candidate_details->CDF_EM_state,
                            'CDF_EM_country_code' => (isset($row['EC_country_code']) && !empty($row['EC_country_code'])) ? $row['EC_country_code'] : $form_candidate_details->CDF_EM_country_code,
                            'CDF_B_same_inc' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? 1 : $form_candidate_details->CDF_B_same_inc,
                            'CDF_B_name' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? $row['BF_fullname'] : $form_candidate_details->CDF_B_name,
                            'CDF_B_phoneNum' => (isset($row['BF_phone_number']) && !empty($row['BF_phone_number'])) ? $row['BF_phone_number'] : $form_candidate_details->CDF_B_phoneNum,
                            'CDF_B_cel' => (isset($row['BF_cell_number']) && !empty($row['BF_cell_number'])) ? $row['BF_cell_number'] : $form_candidate_details->CDF_B_cel,
                            'CDF_B_add' => (isset($row['BF_street_address']) && !empty($row['BF_street_address'])) ? $row['BF_street_address'] : $form_candidate_details->CDF_B_add,
                            'CDF_B_city' => (isset($row['BF_city']) && !empty($row['BF_city'])) ? $row['BF_city'] : $form_candidate_details->CDF_B_city,
                            'CDF_B_zip' => (isset($row['BF_zip_code']) && !empty($row['BF_zip_code'])) ? $row['BF_zip_code'] : $form_candidate_details->CDF_B_zip,
                            'CDF_B_state' => (isset($row['BF_state']) && !empty($row['BF_state'])) ? $row['BF_state'] : $form_candidate_details->CDF_B_state,
                            'CDF_B_country_code' => (isset($row['BF_country_code']) && !empty($row['BF_country_code'])) ? $row['BF_country_code'] : $form_candidate_details->CDF_B_country_code,
                            'CDF_B_relationship' => (isset($row['BF_relation']) && !empty($row['BF_relation'])) ? $row['BF_relation'] : $form_candidate_details->CDF_B_relationship,
                            'CDF_B_email' => (isset($row['BF_email']) && !empty($row['BF_email'])) ? $row['BF_email'] : $form_candidate_details->CDF_B_email,
                            'CDF_B_per' => (isset($row['BF_percent']) && !empty($row['BF_percent'])) ? $row['BF_percent'] : $form_candidate_details->CDF_B_per,
                            'CDF_B_name1' => (isset($row['BF_fullname1']) && !empty($row['BF_fullname1'])) ? $row['BF_fullname1'] : $form_candidate_details->CDF_B_name1,
                            'CDF_B_phoneNum1' => (isset($row['BF_phone_number1']) && !empty($row['BF_phone_number1'])) ? $row['BF_phone_number1'] : $form_candidate_details->CDF_B_phoneNum1,
                            'CDF_B_cel1' => (isset($row['BF_cell_number1']) && !empty($row['BF_cell_number1'])) ? $row['BF_cell_number1'] : $form_candidate_details->CDF_B_cel1,
                            'CDF_B_add1' => (isset($row['BF_street_address1']) && !empty($row['BF_street_address1'])) ? $row['BF_street_address1'] : $form_candidate_details->CDF_B_add1,
                            'CDF_B_city1' => (isset($row['BF_city1']) && !empty($row['BF_city1'])) ? $row['BF_city1'] : $form_candidate_details->CDF_B_city1,
                            'CDF_B_zip1' => (isset($row['BF_zip_code1']) && !empty($row['BF_zip_code1'])) ? $row['BF_zip_code1'] : $form_candidate_details->CDF_B_zip1,
                            'CDF_B_state1' => (isset($row['BF_state1']) && !empty($row['BF_state1'])) ? $row['BF_state1'] : $form_candidate_details->CDF_B_state1,
                            'CDF_B_country_code1' => (isset($row['BF_country_code1']) && !empty($row['BF_country_code1'])) ? $row['BF_country_code1'] : $form_candidate_details->CDF_B_country_code1,
                            'CDF_B_relationship1' => (isset($row['BF_relation1']) && !empty($row['BF_relation1'])) ? $row['BF_relation1'] : $form_candidate_details->CDF_B_relationship1,
                            'CDF_B_email1' => (isset($row['BF_email1']) && !empty($row['BF_email1'])) ? $row['BF_email1'] : $form_candidate_details->CDF_B_email1,
                            'CDF_B_per1' => (isset($row['BF_percent1']) && !empty($row['BF_percent1'])) ? $row['BF_percent1'] : $form_candidate_details->CDF_B_per1,
                            'CDF_brand' => (isset($row['brand']) && !empty($row['brand'])) ? $row['brand'] : $user_personal_data->CDF_brand,
                            'CDF_recruiter' => (isset($row['recruiter']) && !empty($row['recruiter'])) ? $row['recruiter'] : $user_personal_data->CDF_recruiter,
                            'CDF_jobcode' => (isset($row['job_code']) && !empty($row['job_code'])) ? $row['job_code'] : $user_personal_data->CDF_jobcode,
                            'last_updated_on' => date('Y-m-d H:i:s'),
                            'CDF_e1' => (isset($row['jde_id']) && !empty($row['jde_id'])) ? $row['jde_id'] : $user_personal_data->CDF_e1,
                            'updated_by' => 999999999999
                        ];


                        $form_candidate_data_class_2 = new FormCandidateData();
                        $form_candidate_data_class_2->setDBConnection('ctrac_prod_db');
                        // $pdf_success_data[] = $form_candidate_data;
                        $pdf_success_data[] = $form_candidate_data_class_2->savePersonalDataForm($form_candidate_data);
                    }

                    /**
                     * Update Crew Personal Data
                     */
                    $personal_data_class = new PersonalData();
                    $personal_data_class->setDBConnection('ctrac_prod_db');
                    $user_personal_data = $user_details->personalData()->first();
                    $telephone_data = [];

                    $telephone_data = [
                        'country_code' => (isset($row['country_code']) && !empty($row['country_code'])) ? $row['country_code'] : '',
                        'phone_number' => (isset($row['mobile number']) && !empty($row['mobile number'])) ? $row['mobile number'] : '',
                        'cellphone' => '1',
                    ];

                    /*
                     * Marital Status
                     */
                    $marital_status = null;
                    if (strtolower($row['marital_status']) == 'single')
                    {
                        $marital_status = 1;
                    }
                    else if (strtolower($row['marital_status']) == 'married')
                    {
                        $marital_status = 2;
                    }
                    else if (strtolower($row['marital_status']) == 'separated')
                    {
                        $marital_status = 3;
                    }
                    else if (strtolower($row['marital_status']) == 'divorced')
                    {
                        $marital_status = 4;
                    }
                    else if (strtolower($row['marital_status']) == 'widowed')
                    {
                        $marital_status = 5;
                    }

                    $address_from_date = $row['address_from_date'];
                    $address_to_date = $row['address_from_date'];
                    $passport_issued_date = $row['passport_issued_date'];
                    $passport_expiry_date = $row['passport_expiry_date'];
                    $passport_expiry_date = $row['passport_expiry_date'];
                    $birthdate = $row['birthdate'];
                    $per_address_from_date = $row['per_address_from_date'];
                    $per_address_to_date = $row['per_address_to_date'];

                    if (!is_null(DateUtility::changeDateFormat($address_from_date)))
                    {
                        $address_from_date = DateUtility::changeDateFormat($address_from_date);
                    }

                    if (!is_null(DateUtility::changeDateFormat($address_to_date)))
                    {
                        $address_to_date = DateUtility::changeDateFormat($address_to_date);
                    }

                    if (!is_null(DateUtility::changeDateFormat($passport_issued_date)))
                    {
                        $passport_issued_date = DateUtility::changeDateFormat($passport_issued_date);
                    }

                    if (!is_null(DateUtility::changeDateFormat($passport_expiry_date)))
                    {
                        $passport_expiry_date = DateUtility::changeDateFormat($passport_expiry_date);
                    }

                    if (!is_null(DateUtility::changeDateFormat($birthdate)))
                    {
                        $birthdate = DateUtility::changeDateFormat($birthdate);
                    }

                    if (!is_null(DateUtility::changeDateFormat($per_address_from_date)))
                    {
                        $per_address_from_date = DateUtility::changeDateFormat($per_address_from_date);
                    }

                    if (!is_null(DateUtility::changeDateFormat($per_address_to_date)))
                    {
                        $per_address_to_date = DateUtility::changeDateFormat($per_address_to_date);
                    }

                    if (is_null($user_personal_data))
                    {
                        $user_personal_data = new PersonalData();
                    }

                    $personal_data = [
                        'personal_data_id' => $user_personal_data->getId(),
                        'first_name' => (isset($row['first_name']) && !empty($row['first_name'])) ? $row['first_name'] : $user_personal_data->first_name,
                        'middle_name' => (isset($row['middle_name']) && !empty($row['middle_name'])) ? $row['middle_name'] : $user_personal_data->middle_name,
                        'last_name' => (isset($row['last_name']) && !empty($row['last_name'])) ? $row['last_name'] : $user_personal_data->last_name,
                        'title_name' => (isset($row['title_name']) && !empty($row['title_name'])) ? $row['title_name'] : $user_personal_data->title_name,
                        'other_first_name' => (isset($row['other_first_name']) && !empty($row['other_first_name'])) ? $row['other_first_name'] : $user_personal_data->other_first_name,
                        'other_middle_name' => (isset($row['other_middle_name']) && !empty($row['other_middle_name'])) ? $row['other_middle_name'] : $user_personal_data->other_middle_name,
                        'other_last_name' => (isset($row['other_last_name']) && !empty($row['other_last_name'])) ? $row['other_last_name'] : $user_personal_data->other_last_name,
                        'fathers_name' => (isset($row['fathers_name']) && !empty($row['fathers_name'])) ? $row['fathers_name'] : $user_personal_data->fathers_name,
                        'mothers_name' => (isset($row['mothers_name']) && !empty($row['mothers_name'])) ? $row['mothers_name'] : $user_personal_data->mothers_name,
                        'sex' => (isset($row['sex']) && !empty($row['sex'])) ? $row['sex'] : 0,
                        'telephone_data' => json_encode($telephone_data),
                        'street_address' => (isset($row['street_address']) && !empty($row['street_address'])) ? $row['street_address'] : $user_personal_data->street_address,
                        'city' => (isset($row['city']) && !empty($row['city'])) ? $row['city'] : $user_personal_data->city,
                        'state' => (isset($row['state']) && !empty($row['state'])) ? $row['state'] : $user_personal_data->state,
                        'country_code' => (isset($row['country_code']) && !empty($row['country_code'])) ? $row['country_code'] : $user_personal_data->country_code,
                        'zip_code' => (isset($row['zip_code']) && !empty($row['zip_code'])) ? $row['zip_code'] : $user_personal_data->zip_code,
                        'address_from_date' => (!empty($address_from_date)) ? $address_from_date : null,
                        'address_to_date' => (!empty($address_to_date)) ? $address_to_date : null,
                        'skype' => (isset($row['skype']) && !empty($row['skype'])) ? $row['skype'] : $user_personal_data->skype,
                        'gateway_id' => (!is_null($gateway_details)) ? $gateway_details->getId() : $user_personal_data->gateway_id,
                        'contact_id' => (isset($row['contact_id']) && !empty($row['contact_id'])) ? $row['contact_id'] : $user_personal_data->contact_id,
                        'sched_id' => (isset($row['sched_id']) && !empty($row['sched_id'])) ? $row['sched_id'] : $user_personal_data->sched_id,
                        'medical_id' => (isset($row['medical_id']) && !empty($row['medical_id'])) ? $row['medical_id'] : $user_personal_data->medical_id,
                        'per_street_address' => (isset($row['per_street_address']) && !empty($row['per_street_address'])) ? $row['per_street_address'] : $user_personal_data->per_street_address,
                        'per_city' => (isset($row['per_city']) && !empty($row['per_city'])) ? $row['per_city'] : $user_personal_data->per_city,
                        'per_state' => (isset($row['per_state']) && !empty($row['per_state'])) ? $row['per_state'] : $user_personal_data->per_state,
                        'per_zip_code' => (isset($row['per_zip_code']) && !empty($row['per_zip_code'])) ? $row['per_zip_code'] : $user_personal_data->per_zip_code,
                        'per_country_code' => (isset($row['per_country_code']) && !empty($row['per_country_code'])) ? $row['per_country_code'] : $user_personal_data->per_country_code,
                        'per_address_from_date' => (!empty($per_address_from_date)) ? $per_address_from_date : null,
                        'per_address_to_date' => (!empty($per_address_to_date)) ? $per_address_to_date : null,
                        'permanent_indicator' => (isset($row['permanent_indicator']) && !empty($row['permanent_indicator'])) ? $row['permanent_indicator'] : 0,
                        'mailing_street_address' => (isset($row['mailing_street_address']) && !empty($row['mailing_street_address'])) ? $row['mailing_street_address'] : $user_personal_data->mailing_street_address,
                        'mailing_city' => (isset($row['mailing_city']) && !empty($row['mailing_city'])) ? $row['mailing_city'] : $user_personal_data->mailing_city,
                        'mailing_state' => (isset($row['mailing_state']) && !empty($row['mailing_state'])) ? $row['mailing_state'] : $user_personal_data->mailing_state,
                        'mailing_zip_code' => (isset($row['mailing_zip_code']) && !empty($row['mailing_zip_code'])) ? $row['mailing_zip_code'] : $user_personal_data->mailing_zip_code,
                        'mailing_country_code' => (isset($row['mailing_country_code']) && !empty($row['mailing_country_code'])) ? $row['mailing_country_code'] : $user_personal_data->mailing_country_code,
                        'father_fname' => (isset($row['father_fname']) && !empty($row['father_fname'])) ? $row['father_fname'] : $user_personal_data->father_fname,
                        'father_mname' => (isset($row['father_mname']) && !empty($row['father_mname'])) ? $row['father_mname'] : $user_personal_data->father_mname,
                        'father_lname' => (isset($row['father_lname']) && !empty($row['father_lname'])) ? $row['father_lname'] : $user_personal_data->father_lname,
                        'father_country_code' => (isset($row['father_country_code']) && !empty($row['father_country_code'])) ? $row['father_country_code'] : $user_personal_data->father_country_code,
                        'mother_fname' => (isset($row['mother_fname']) && !empty($row['mother_fname'])) ? $row['mother_fname'] : $user_personal_data->mother_fname,
                        'mother_mname' => (isset($row['mother_mname']) && !empty($row['mother_mname'])) ? $row['mother_mname'] : $user_personal_data->mother_mname,
                        'mother_lname' => (isset($row['mother_lname']) && !empty($row['mother_lname'])) ? $row['mother_lname'] : $user_personal_data->mother_lname,
                        'mother_country_code' => (isset($row['mother_country_code']) && !empty($row['mother_country_code'])) ? $row['mother_country_code'] : $user_personal_data->mother_country_code,
                        'EC_fullname' => (isset($row['EC_fullname']) && !empty($row['EC_fullname'])) ? $row['EC_fullname'] : $user_personal_data->EC_fullname,
                        'EC_relation' => (isset($row['EC_relation']) && !empty($row['EC_relation'])) ? $row['EC_relation'] : $user_personal_data->EC_relation,
                        'EC_email' => (isset($row['EC_email']) && !empty($row['EC_email'])) ? $row['EC_email'] : $user_personal_data->EC_email,
                        'EC_phone_number' => (isset($row['EC_phone_number']) && !empty($row['EC_phone_number'])) ? $row['EC_phone_number'] : $user_personal_data->EC_phone_number,
                        'EC_cell_number' => (isset($row['EC_cell_number']) && !empty($row['EC_cell_number'])) ? $row['EC_cell_number'] : $user_personal_data->EC_cell_number,
                        'EC_street_address' => (isset($row['EC_street_address']) && !empty($row['EC_street_address'])) ? $row['EC_street_address'] : $user_personal_data->EC_street_address,
                        'EC_city' => (isset($row['EC_city']) && !empty($row['EC_city'])) ? $row['EC_city'] : $user_personal_data->EC_city,
                        'EC_state' => (isset($row['EC_state']) && !empty($row['EC_state'])) ? $row['EC_state'] : $user_personal_data->EC_state,
                        'EC_zip_code' => (isset($row['EC_zip_code']) && !empty($row['EC_zip_code'])) ? $row['EC_zip_code'] : $user_personal_data->EC_zip_code,
                        'EC_country_code' => (isset($row['EC_country_code']) && !empty($row['EC_country_code'])) ? $row['EC_country_code'] : $user_personal_data->EC_country_code,
                        'BF_fullname' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? $row['BF_fullname'] : $user_personal_data->BF_fullname,
                        'BF_relation' => (isset($row['BF_relation']) && !empty($row['BF_relation'])) ? $row['BF_relation'] : $user_personal_data->BF_relation,
                        'BF_email' => (isset($row['BF_email']) && !empty($row['BF_email'])) ? $row['BF_email'] : $user_personal_data->BF_email,
                        'BF_phone_number' => (isset($row['BF_phone_number']) && !empty($row['BF_phone_number'])) ? $row['BF_phone_number'] : $user_personal_data->BF_phone_number,
                        'BF_cell_number' => (isset($row['BF_cell_number']) && !empty($row['BF_cell_number'])) ? $row['BF_cell_number'] : $user_personal_data->BF_cell_number,
                        'BF_percent' => (isset($row['BF_percent']) && !empty($row['BF_percent'])) ? $row['BF_percent'] : $user_personal_data->BF_percent,
                        'BF_street_address' => (isset($row['BF_street_address']) && !empty($row['BF_street_address'])) ? $row['BF_street_address'] : $user_personal_data->BF_street_address,
                        'BF_city' => (isset($row['BF_city']) && !empty($row['BF_city'])) ? $row['BF_city'] : $user_personal_data->BF_city,
                        'BF_state' => (isset($row['BF_state']) && !empty($row['BF_state'])) ? $row['BF_state'] : $user_personal_data->BF_state,
                        'BF_zip_code' => (isset($row['BF_zip_code']) && !empty($row['BF_zip_code'])) ? $row['BF_zip_code'] : $user_personal_data->BF_zip_code,
                        'BF_country_code' => (isset($row['BF_country_code']) && !empty($row['BF_country_code'])) ? $row['BF_country_code'] : $user_personal_data->BF_country_code,
                        'BF_fullname1' => (isset($row['BF_fullname1']) && !empty($row['BF_fullname1'])) ? $row['BF_fullname1'] : $user_personal_data->BF_fullname1,
                        'BF_relation1' => (isset($row['BF_relation1']) && !empty($row['BF_relation1'])) ? $row['BF_relation1'] : $user_personal_data->BF_relation1,
                        'BF_email1' => (isset($row['BF_email1']) && !empty($row['BF_email1'])) ? $row['BF_email1'] : $user_personal_data->BF_email1,
                        'BF_phone_number1' => (isset($row['BF_phone_number1']) && !empty($row['BF_phone_number1'])) ? $row['BF_phone_number1'] : $user_personal_data->BF_phone_number1,
                        'BF_cell_number1' => (isset($row['BF_cell_number1']) && !empty($row['BF_cell_number1'])) ? $row['BF_cell_number1'] : $user_personal_data->BF_cell_number1,
                        'BF_percent1' => (isset($row['BF_percent1']) && !empty($row['BF_percent1'])) ? $row['BF_percent1'] : $user_personal_data->BF_percent1,
                        'BF_street_address1' => (isset($row['BF_street_address1']) && !empty($row['BF_street_address1'])) ? $row['BF_street_address1'] : $user_personal_data->BF_street_address1,
                        'BF_city1' => (isset($row['BF_city1']) && !empty($row['BF_city1'])) ? $row['BF_city1'] : $user_personal_data->BF_city1,
                        'BF_state1' => (isset($row['BF_state1']) && !empty($row['BF_state1'])) ? $row['BF_state1'] : $user_personal_data->BF_state1,
                        'BF_zip_code1' => (isset($row['BF_zip_code1']) && !empty($row['BF_zip_code1'])) ? $row['BF_zip_code1'] : $user_personal_data->BF_zip_code1,
                        'BF_country_code1' => (isset($row['BF_country_code1']) && !empty($row['BF_country_code1'])) ? $row['BF_country_code1'] : $user_personal_data->BF_country_code1,
                        'passport_num' => (isset($row['passport_num']) && !empty($row['passport_num'])) ? $row['passport_num'] : $user_personal_data->passport_num,
                        'passport_country' => (isset($row['passport_country']) && !empty($row['passport_country'])) ? $row['passport_country'] : $user_personal_data->passport_country,
                        'passport_issued_date' => (!empty($passport_issued_date)) ? $passport_issued_date : null,
                        'passport_expiry_date' => (!empty($passport_expiry_date)) ? $passport_expiry_date : null,
                        'worked_on_ship' => (isset($row['worked_on_ship']) && !empty($row['worked_on_ship'])) ? $row['worked_on_ship'] : $user_personal_data->worked_on_ship,
                        'areas_of_interest' => (isset($row['areas_of_interest']) && !empty($row['areas_of_interest'])) ? $row['areas_of_interest'] : $user_personal_data->areas_of_interest,
                        // 'worked_for_royal' => (isset($row['worked_for_royal']) && !empty($row['worked_for_royal'])) ? $row['worked_for_royal'] : 0,
                        // 'knows_royal' => (isset($row['knows_royal']) && !empty($row['knows_royal'])) ? $row['knows_royal'] : 0,
                        'royal_employees' => (isset($row['royal_employees']) && !empty($row['royal_employees'])) ? $row['royal_employees'] : $user_personal_data->royal_employees,
                        'worked_brand' => (isset($row['worked_brand']) && !empty($row['worked_brand'])) ? $row['worked_brand'] : $user_personal_data->worked_brand,
                        'marital_status' => $marital_status,
                        'birthdate' => (!empty($birthdate)) ? $birthdate : null,
                        'birthplace_city' => (isset($row['birthplace_city']) && !empty($row['birthplace_city'])) ? $row['birthplace_city'] : $user_personal_data->birthplace_city,
                        'birthplace_state' => (isset($row['birthplace_state']) && !empty($row['birthplace_state'])) ? $row['birthplace_state'] : $user_personal_data->birthplace_state,
                        'birthplace_country_code' => (isset($row['birthplace_country_code']) && !empty($row['birthplace_country_code'])) ? $row['birthplace_country_code'] : $user_personal_data->birthplace_country_code,
                        'sss' => (isset($row['sss']) && !empty($row['sss'])) ? $row['sss'] : $user_personal_data->sss,
                        'social_media_pages' => (isset($row['social_media_pages']) && !empty($row['social_media_pages'])) ? $row['social_media_pages'] : $user_personal_data->social_media_pages,
                        'philhealth_number' => (isset($row['philhealth_number']) && !empty($row['philhealth_number'])) ? $row['philhealth_number'] : $user_personal_data->philhealth_number,
                        'hdmf_number' => (isset($row['hdmf_number']) && !empty($row['hdmf_number'])) ? $row['hdmf_number'] : $user_personal_data->hdmf_number,
                        'license_number' => (isset($row['license_number']) && !empty($row['license_number'])) ? $row['license_number'] : $user_personal_data->license_number,
                        'spouse_ind' => 0,
                        'spouse_first_name' => (isset($row['spouse_first_name']) && !empty($row['spouse_first_name'])) ? $row['spouse_first_name'] : $user_personal_data->spouse_first_name,
                        'spouse_middle_name' => (isset($row['spouse_middle_name']) && !empty($row['spouse_middle_name'])) ? $row['spouse_middle_name'] : $user_personal_data->spouse_middle_name,
                        'spouse_last_name' => (isset($row['spouse_last_name']) && !empty($row['spouse_last_name'])) ? $row['spouse_last_name'] : $user_personal_data->spouse_last_name,
                        'mothers_maiden_name' => (isset($row['mothers_maiden_name']) && !empty($row['mothers_maiden_name'])) ? $row['mothers_maiden_name'] : $user_personal_data->mothers_maiden_name,
                        'last_updated_date' => date('Y-m-d H:i:s')
                    ];

                    $personal_data_id = $personal_data_class->savePersonalData($personal_data);
                    $pd_success_data[] = $personal_data_id;
                    /**
                     * Update User Data
                     */
                    $user_details->source_id = 54;
                    $user_details->hp_register_company = $row['Source'];
                    if (is_null($user_personal_data->getId()))
                    {
                        $user_details->personal_data_id = $personal_data_id;
                    }
                    $user_details->save();
    			}
    			else
    			{
    				$pdf_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                    $pd_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
    			}
    		}
            // dd($pdf_success_data);
    		session()->flash('success_flash_data', true);
            session()->flash('pdf_success_data', $pdf_success_data);
            session()->flash('pd_success_data', $pd_success_data);
            session()->flash('pdf_failed_data', $pdf_failed_data);
    		session()->flash('pd_failed_data', $pd_failed_data);

    		return redirect()->back();
    	}

    	session()->flash('success_flash_data', false);

    	return redirect()->back();
    }

    public function updateCrisisManagementPdfOnly(
        Request $request, 
        Gateway $gateway_class, 
        SchoolDegreeType $school_degree_types_class
    )
    {
        $upload_csv = $request->upload_csv;
        $raw_data = [];

        $request->validate([
            'upload_csv' => 'required'
        ]);

        $file_name = time() . date('Y-m-d') . '-crisis-management' . '.csv';
        $request->upload_csv->storeAs('CrisisManagement', $file_name);

        if ($request->hasFile('upload_csv')) 
        {
            $file_path = storage_path('app/CrisisManagement/' . $file_name);
            $csv_data = StringUtility::crisisCsvToArray($file_path);

            if (!empty($csv_data))
            {
                $raw_data = $csv_data;
            }
        }
        
        $pdf_failed_data = [];
        $pd_failed_data = [];
        $pdf_success_data = [];
        $pd_success_data = [];
        // dd($raw_data);
        if (!empty($raw_data))
        {
            foreach ($raw_data as $row) 
            {

                $user_class = new User();
                $user_class->setDBConnection('ctrac_prod_db');
                $user_details = $user_class->getByJDEId($row['jde_id']);

                if (!is_null($user_details))
                {
                    /**
                     * Update Crew PDF or Personal Data Form
                     */
                    $form_candidate_data_class = new FormCandidateData();
                    $form_candidate_data_class->setDBConnection('ctrac_prod_db');
                    $form_candidate_details = $form_candidate_data_class->getByUserId($user_details->getId());
                    /**
                     * Get Gateway Details
                     */
                    $gateway_code = (isset($row['gateway_id']) && !empty($row['gateway_id'])) ? $row['gateway_id'] : 0;
                    $gateway_details = $gateway_class->getByGatewayCode($gateway_code);

                    /*
                     * Get School Degree Type
                     */
                    $degree_type_code = (isset($row['highest_education_id']) && !empty($row['highest_education_id'])) ? $row['highest_education_id'] : 0;
                    $degree_type_details = $school_degree_types_class->getByDegreeTypeCode($degree_type_code);

                    /**
                     * Dates Details
                     */
                    $CDF_passp_issDate = $row['passport_expiry_date'];
                    $CDF_passp_expiryDate = $row['passport_expiry_date'];
                    $CDF_birthdate = $row['birthdate'];

                    if (!is_null(DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_passp_issDate = DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y');
                    }

                    if (!is_null(DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_passp_expiryDate = DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y');
                    }

                    if (!is_null(DateUtility::changeDateFormat($row['birthdate'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_birthdate = DateUtility::changeDateFormat($row['birthdate'], 'Y-m-d', 'm/d/Y');
                    }

                    /*
                     * Will occur when the user don't have a PDF
                     */
                    if (!is_null($form_candidate_details))
                    {
                        $form_candidate_data = [
                            'id' => $form_candidate_details->getId(),
                            'user_id' => $user_details->getId(),
                            'CDF_firstName' => (isset($row['first_name']) && !empty($row['first_name'])) ? $row['first_name'] : $form_candidate_details->CDF_firstName,
                            'CDF_middleName' => (isset($row['middle_name']) && !empty($row['middle_name'])) ? $row['middle_name'] : $form_candidate_details->CDF_middleName,
                            'CDF_lastName' => (isset($row['last_name']) && !empty($row['last_name'])) ? $row['last_name'] : $form_candidate_details->CDF_lastName,
                            'CDF_otherSurname' => (isset($row['other_last_name']) && !empty($row['other_last_name'])) ? $row['other_last_name'] : $form_candidate_details->CDF_otherSurname,
                            'CDF_add' => (isset($row['per_street_address']) && !empty($row['per_street_address'])) ? $row['per_street_address'] : $form_candidate_details->CDF_add,
                            'CDF_city' => (isset($row['per_city']) && !empty($row['per_city'])) ? $row['per_city'] : $form_candidate_details->CDF_city,
                            'CDF_state' => (isset($row['per_state']) && !empty($row['per_state'])) ? $row['per_state'] : $form_candidate_details->CDF_state,
                            'CDF_zip' => (isset($row['per_zip_code']) && !empty($row['per_zip_code'])) ? $row['per_zip_code'] : $form_candidate_details->CDF_zip,
                            'CDF_country_code' => (isset($row['per_country_code']) && !empty($row['per_country_code'])) ? $row['per_country_code'] : $form_candidate_details->CDF_country_code,
                            'CDF_primaryType' => 1,
                            'CDF_primaryNumCt' => (isset($row['Country Calling code']) && !empty($row['Country Calling code'])) ? $row['Country Calling code'] : $form_candidate_details->CDF_primaryNumCt,
                            'CDF_primaryNum' => (isset($row['mobile number']) && !empty($row['mobile number'])) ? $row['mobile number'] : $form_candidate_details->CDF_primaryNum,
                            'CDF_altType' => (isset($row['Alternate Number']) && !empty($row['Alternate Number'])) ? 1 : 0,
                            'CDF_altNumCt' => (isset($row['Alternate Calling Country Code']) && !empty($row['Alternate Calling Country Code'])) ? $row['Alternate Calling Country Code'] : $form_candidate_details->CDF_altNumCt,
                            'CDF_altNum' => (isset($row['Alternate Number']) && !empty($row['Alternate Number'])) ? $row['Alternate Number'] : $form_candidate_details->CDF_altNum,
                            'CDF_birthdate' => (!empty($CDF_birthdate)) ? $CDF_birthdate : null,
                            'CDF_status' => (isset($row['marital_status']) && !empty($row['marital_status'])) ? $row['marital_status'] : $form_candidate_details->CDF_status,
                            'CDF_gender' => (isset($row['sex']) && $row['sex'] == 0) ? 'female' : 'male',
                            'CDF_sss' => (isset($row['sss']) && !empty($row['sss'])) ? $row['sss'] : $form_candidate_details->CDF_sss,
                            'CDF_gateway' => (!is_null($gateway_details)) ? $gateway_details->getId() : $form_candidate_details->CDF_gateway,
                            'CDF_dateavail' => $form_candidate_details->CDF_dateavail,
                            'CDF_emailAdd' => (isset($row['email']) && !empty($row['email'])) ? $row['email'] : $form_candidate_details->CDF_emailAdd,
                            'CDF_bcity' => (isset($row['birthplace_city']) && !empty($row['birthplace_city'])) ? $row['birthplace_city'] : $form_candidate_details->CDF_bcity,
                            'CDF_bstate' => (isset($row['birthplace_state']) && !empty($row['birthplace_state'])) ? $row['birthplace_state'] : $form_candidate_details->CDF_bstate,
                            'CDF_bcountry' => (isset($row['birthplace_country_code']) && !empty($row['birthplace_country_code'])) ? $row['birthplace_country_code'] : $form_candidate_details->CDF_bcountry,
                            'CDF_education' => (!is_null($degree_type_details)) ? $degree_type_details->getId() : $form_candidate_details->CDF_education,
                            'CDF_school' => $form_candidate_details->CDF_school,
                            'CDF_ffirstName' => (isset($row['father_fname']) && !empty($row['father_fname'])) ? $row['father_fname'] : $form_candidate_details->CDF_ffirstName,
                            'CDF_fmiddleName' => (isset($row['father_mname']) && !empty($row['father_mname'])) ? $row['father_mname'] : $form_candidate_details->CDF_fmiddleName,
                            'CDF_flastName' => (isset($row['father_lname']) && !empty($row['father_lname'])) ? $row['father_lname'] : $form_candidate_details->CDF_flastName,
                            'CDF_fcountry' => (isset($row['father_country_code']) && !empty($row['father_country_code'])) ? $row['father_country_code'] : $form_candidate_details->CDF_fcountry,
                            'CDF_mfirstName' => (isset($row['mother_fname']) && !empty($row['mother_fname'])) ? $row['mother_fname'] : $form_candidate_details->CDF_mfirstName,
                            'CDF_mmiddleName' => (isset($row['mother_mname']) && !empty($row['mother_mname'])) ? $row['mother_mname'] : $form_candidate_details->CDF_mmiddleName,
                            'CDF_mlastName' => (isset($row['mother_lname']) && !empty($row['mother_lname'])) ? $row['mother_lname'] : $form_candidate_details->CDF_mlastName,
                            'CDF_mcountry' => (isset($row['mother_country_code']) && !empty($row['mother_country_code'])) ? $row['mother_country_code'] : $form_candidate_details->CDF_mcountry,
                            'CDF_passport_file' => $form_candidate_details->CDF_passport_file,
                            'CDF_passp_Num' => (isset($row['passport_num']) && !empty($row['passport_num'])) ? $row['passport_num'] : $form_candidate_details->CDF_passp_Num,
                            'CDF_passp_issCountry' => (isset($row['passport_country']) && !empty($row['passport_country'])) ? $row['passport_country'] : $form_candidate_details->CDF_passp_issCountry,
                            'CDF_passp_issDate' => (!empty($CDF_passp_issDate)) ? $CDF_passp_issDate : null,
                            'CDF_passp_expiryDate' => (!empty($CDF_passp_expiryDate)) ? $CDF_passp_expiryDate : null,
                            'CDF_EM_name' => (isset($row['EC_fullname']) && !empty($row['EC_fullname'])) ? $row['EC_fullname'] : $form_candidate_details->CDF_EM_name,
                            'CDF_EM_relationship' => (isset($row['EC_relation']) && !empty($row['EC_relation'])) ? $row['EC_relation'] : $form_candidate_details->CDF_EM_relationship,
                            'CDF_EM_phoneNum' => (isset($row['EC_phone_number']) && !empty($row['EC_phone_number'])) ? $row['EC_phone_number'] : $form_candidate_details->CDF_EM_phoneNum,
                            'CDF_EM_cel' => (isset($row['EC_cell_number']) && !empty($row['EC_cell_number'])) ? $row['EC_cell_number'] : $form_candidate_details->CDF_EM_cel,
                            'CDF_EM_email1' => (isset($row['EC_email']) && !empty($row['EC_email'])) ? $row['EC_email'] : $form_candidate_details->CDF_EM_email1,
                            'CDF_EM_add' => (isset($row['EC_street_address']) && !empty($row['EC_street_address'])) ? $row['EC_street_address'] : $form_candidate_details->CDF_EM_add,
                            'CDF_EM_city' => (isset($row['EC_city']) && !empty($row['EC_city'])) ? $row['EC_city'] : $form_candidate_details->CDF_EM_city,
                            'CDF_EM_zip' => (isset($row['EC_zip_code']) && !empty($row['EC_zip_code'])) ? $row['EC_zip_code'] : $form_candidate_details->CDF_EM_zip,
                            'CDF_EM_state' => (isset($row['EC_state']) && !empty($row['EC_state'])) ? $row['EC_state'] : $form_candidate_details->CDF_EM_state,
                            'CDF_EM_country_code' => (isset($row['EC_country_code']) && !empty($row['EC_country_code'])) ? $row['EC_country_code'] : $form_candidate_details->CDF_EM_country_code,
                            'CDF_B_same_inc' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? 1 : $form_candidate_details->CDF_B_same_inc,
                            'CDF_B_name' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? $row['BF_fullname'] : $form_candidate_details->CDF_B_name,
                            'CDF_B_phoneNum' => (isset($row['BF_phone_number']) && !empty($row['BF_phone_number'])) ? $row['BF_phone_number'] : $form_candidate_details->CDF_B_phoneNum,
                            'CDF_B_cel' => (isset($row['BF_cell_number']) && !empty($row['BF_cell_number'])) ? $row['BF_cell_number'] : $form_candidate_details->CDF_B_cel,
                            'CDF_B_add' => (isset($row['BF_street_address']) && !empty($row['BF_street_address'])) ? $row['BF_street_address'] : $form_candidate_details->CDF_B_add,
                            'CDF_B_city' => (isset($row['BF_city']) && !empty($row['BF_city'])) ? $row['BF_city'] : $form_candidate_details->CDF_B_city,
                            'CDF_B_zip' => (isset($row['BF_zip_code']) && !empty($row['BF_zip_code'])) ? $row['BF_zip_code'] : $form_candidate_details->CDF_B_zip,
                            'CDF_B_state' => (isset($row['BF_state']) && !empty($row['BF_state'])) ? $row['BF_state'] : $form_candidate_details->CDF_B_state,
                            'CDF_B_country_code' => (isset($row['BF_country_code']) && !empty($row['BF_country_code'])) ? $row['BF_country_code'] : $form_candidate_details->CDF_B_country_code,
                            'CDF_B_relationship' => (isset($row['BF_relation']) && !empty($row['BF_relation'])) ? $row['BF_relation'] : $form_candidate_details->CDF_B_relationship,
                            'CDF_B_email' => (isset($row['BF_email']) && !empty($row['BF_email'])) ? $row['BF_email'] : $form_candidate_details->CDF_B_email,
                            'CDF_B_per' => (isset($row['BF_percent']) && !empty($row['BF_percent'])) ? $row['BF_percent'] : $form_candidate_details->CDF_B_per,
                            'CDF_B_name1' => (isset($row['BF_fullname1']) && !empty($row['BF_fullname1'])) ? $row['BF_fullname1'] : $form_candidate_details->CDF_B_name1,
                            'CDF_B_phoneNum1' => (isset($row['BF_phone_number1']) && !empty($row['BF_phone_number1'])) ? $row['BF_phone_number1'] : $form_candidate_details->CDF_B_phoneNum1,
                            'CDF_B_cel1' => (isset($row['BF_cell_number1']) && !empty($row['BF_cell_number1'])) ? $row['BF_cell_number1'] : $form_candidate_details->CDF_B_cel1,
                            'CDF_B_add1' => (isset($row['BF_street_address1']) && !empty($row['BF_street_address1'])) ? $row['BF_street_address1'] : $form_candidate_details->CDF_B_add1,
                            'CDF_B_city1' => (isset($row['BF_city1']) && !empty($row['BF_city1'])) ? $row['BF_city1'] : $form_candidate_details->CDF_B_city1,
                            'CDF_B_zip1' => (isset($row['BF_zip_code1']) && !empty($row['BF_zip_code1'])) ? $row['BF_zip_code1'] : $form_candidate_details->CDF_B_zip1,
                            'CDF_B_state1' => (isset($row['BF_state1']) && !empty($row['BF_state1'])) ? $row['BF_state1'] : $form_candidate_details->CDF_B_state1,
                            'CDF_B_country_code1' => (isset($row['BF_country_code1']) && !empty($row['BF_country_code1'])) ? $row['BF_country_code1'] : $form_candidate_details->CDF_B_country_code1,
                            'CDF_B_relationship1' => (isset($row['BF_relation1']) && !empty($row['BF_relation1'])) ? $row['BF_relation1'] : $form_candidate_details->CDF_B_relationship1,
                            'CDF_B_email1' => (isset($row['BF_email1']) && !empty($row['BF_email1'])) ? $row['BF_email1'] : $form_candidate_details->CDF_B_email1,
                            'CDF_B_per1' => (isset($row['BF_percent1']) && !empty($row['BF_percent1'])) ? $row['BF_percent1'] : $form_candidate_details->CDF_B_per1,
                            'CDF_brand' => (isset($row['brand']) && !empty($row['brand'])) ? $row['brand'] : $user_personal_data->CDF_brand,
                            'CDF_recruiter' => (isset($row['recruiter']) && !empty($row['recruiter'])) ? $row['recruiter'] : $user_personal_data->CDF_recruiter,
                            'CDF_jobcode' => (isset($row['job_code']) && !empty($row['job_code'])) ? $row['job_code'] : $user_personal_data->CDF_jobcode,
                            'last_updated_on' => date('Y-m-d H:i:s'),
                            'updated_by' => 999999999999
                        ];

                        $form_candidate_data_class_2 = new FormCandidateData();
                        $form_candidate_data_class_2->setDBConnection('ctrac_prod_db');
                        // $pdf_success_data[] = $form_candidate_data;
                        $pdf_success_data[] = $form_candidate_data_class_2->savePersonalDataForm($form_candidate_data);
                    }

                    /**
                     * Update User Data
                     */
                    $user_details->source_id = 54;
                    $user_details->hp_register_company = $row['Source'];
                    $user_details->save();
                }
                else
                {
                    $pdf_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                    $pd_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                }
            }
            // dd($pdf_success_data);
            session()->flash('success_flash_data', true);
            session()->flash('pdf_success_data', $pdf_success_data);
            session()->flash('pd_success_data', $pd_success_data);
            session()->flash('pdf_failed_data', $pdf_failed_data);
            session()->flash('pd_failed_data', $pd_failed_data);

            return redirect()->back();
        }

        session()->flash('success_flash_data', false);

        return redirect()->back();
    }

    public function addNewCrisisManagementPdfOnly(
        Request $request, 
        Gateway $gateway_class, 
        SchoolDegreeType $school_degree_types_class
    )
    {
        $upload_csv = $request->upload_csv;
        $raw_data = [];

        $request->validate([
            'upload_csv' => 'required'
        ]);

        $file_name = time() . date('Y-m-d') . '-crisis-management' . '.csv';
        $request->upload_csv->storeAs('CrisisManagement', $file_name);

        if ($request->hasFile('upload_csv')) 
        {
            $file_path = storage_path('app/CrisisManagement/' . $file_name);
            $csv_data = StringUtility::crisisCsvToArray($file_path);

            if (!empty($csv_data))
            {
                $raw_data = $csv_data;
            }
        }
        
        $pdf_failed_data = [];
        $pd_failed_data = [];
        $pdf_success_data = [];
        $pd_success_data = [];
        // dd($raw_data);
        if (!empty($raw_data))
        {
            foreach ($raw_data as $row) 
            {

                $user_class = new User();
                $user_class->setDBConnection('ctrac_prod_db');
                $user_details = $user_class->getByJDEId($row['jde_id']);

                if (!is_null($user_details))
                {
                    /**
                     * Update Crew PDF or Personal Data Form
                     */
                    $form_candidate_data_class = new FormCandidateData();
                    $form_candidate_data_class->setDBConnection('ctrac_prod_db');
                    $form_candidate_details = $form_candidate_data_class->getByUserId($user_details->getId());
                    /**
                     * Get Gateway Details
                     */
                    $gateway_code = (isset($row['gateway_id']) && !empty($row['gateway_id'])) ? $row['gateway_id'] : 0;
                    $gateway_details = $gateway_class->getByGatewayCode($gateway_code);

                    /*
                     * Get School Degree Type
                     */
                    $degree_type_code = (isset($row['highest_education_id']) && !empty($row['highest_education_id'])) ? $row['highest_education_id'] : 0;
                    $degree_type_details = $school_degree_types_class->getByDegreeTypeCode($degree_type_code);

                    /**
                     * Dates Details
                     */
                    $CDF_passp_issDate = $row['passport_expiry_date'];
                    $CDF_passp_expiryDate = $row['passport_expiry_date'];
                    $CDF_birthdate = $row['birthdate'];

                    if (!is_null(DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_passp_issDate = DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y');
                    }

                    if (!is_null(DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_passp_expiryDate = DateUtility::changeDateFormat($row['passport_expiry_date'], 'Y-m-d', 'm/d/Y');
                    }

                    if (!is_null(DateUtility::changeDateFormat($row['birthdate'], 'Y-m-d', 'm/d/Y')))
                    {
                        $CDF_birthdate = DateUtility::changeDateFormat($row['birthdate'], 'Y-m-d', 'm/d/Y');
                    }

                    /*
                     * Will occur when the user don't have a PDF
                     */
                    if (is_null($form_candidate_details))
                    {
                        $form_candidate_details = new FormCandidateData();
                        $form_candidate_data = [
                            'id' => $form_candidate_details->getId(),
                            'user_id' => $user_details->getId(),
                            'CDF_firstName' => (isset($row['first_name']) && !empty($row['first_name'])) ? $row['first_name'] : $form_candidate_details->CDF_firstName,
                            'CDF_middleName' => (isset($row['middle_name']) && !empty($row['middle_name'])) ? $row['middle_name'] : $form_candidate_details->CDF_middleName,
                            'CDF_lastName' => (isset($row['last_name']) && !empty($row['last_name'])) ? $row['last_name'] : $form_candidate_details->CDF_lastName,
                            'CDF_otherSurname' => (isset($row['other_last_name']) && !empty($row['other_last_name'])) ? $row['other_last_name'] : $form_candidate_details->CDF_otherSurname,
                            'CDF_add' => (isset($row['per_street_address']) && !empty($row['per_street_address'])) ? $row['per_street_address'] : $form_candidate_details->CDF_add,
                            'CDF_city' => (isset($row['per_city']) && !empty($row['per_city'])) ? $row['per_city'] : $form_candidate_details->CDF_city,
                            'CDF_state' => (isset($row['per_state']) && !empty($row['per_state'])) ? $row['per_state'] : $form_candidate_details->CDF_state,
                            'CDF_zip' => (isset($row['per_zip_code']) && !empty($row['per_zip_code'])) ? $row['per_zip_code'] : $form_candidate_details->CDF_zip,
                            'CDF_country_code' => (isset($row['per_country_code']) && !empty($row['per_country_code'])) ? $row['per_country_code'] : $form_candidate_details->CDF_country_code,
                            'CDF_primaryType' => 1,
                            'CDF_primaryNumCt' => (isset($row['Country Calling code']) && !empty($row['Country Calling code'])) ? $row['Country Calling code'] : $form_candidate_details->CDF_primaryNumCt,
                            'CDF_primaryNum' => (isset($row['mobile number']) && !empty($row['mobile number'])) ? $row['mobile number'] : $form_candidate_details->CDF_primaryNum,
                            'CDF_altType' => (isset($row['Alternate Number']) && !empty($row['Alternate Number'])) ? 1 : 0,
                            'CDF_altNumCt' => (isset($row['Alternate Calling Country Code']) && !empty($row['Alternate Calling Country Code'])) ? $row['Alternate Calling Country Code'] : $form_candidate_details->CDF_altNumCt,
                            'CDF_altNum' => (isset($row['Alternate Number']) && !empty($row['Alternate Number'])) ? $row['Alternate Number'] : $form_candidate_details->CDF_altNum,
                            'CDF_birthdate' => (!empty($CDF_birthdate)) ? $CDF_birthdate : null,
                            'CDF_status' => (isset($row['marital_status']) && !empty($row['marital_status'])) ? $row['marital_status'] : $form_candidate_details->CDF_status,
                            'CDF_gender' => (isset($row['sex']) && $row['sex'] == 0) ? 'female' : 'male',
                            'CDF_sss' => (isset($row['sss']) && !empty($row['sss'])) ? $row['sss'] : $form_candidate_details->CDF_sss,
                            'CDF_gateway' => (!is_null($gateway_details)) ? $gateway_details->getId() : $form_candidate_details->CDF_gateway,
                            'CDF_dateavail' => $form_candidate_details->CDF_dateavail,
                            'CDF_emailAdd' => (isset($row['email']) && !empty($row['email'])) ? $row['email'] : $form_candidate_details->CDF_emailAdd,
                            'CDF_bcity' => (isset($row['birthplace_city']) && !empty($row['birthplace_city'])) ? $row['birthplace_city'] : $form_candidate_details->CDF_bcity,
                            'CDF_bstate' => (isset($row['birthplace_state']) && !empty($row['birthplace_state'])) ? $row['birthplace_state'] : $form_candidate_details->CDF_bstate,
                            'CDF_bcountry' => (isset($row['birthplace_country_code']) && !empty($row['birthplace_country_code'])) ? $row['birthplace_country_code'] : $form_candidate_details->CDF_bcountry,
                            'CDF_education' => (!is_null($degree_type_details)) ? $degree_type_details->getId() : $form_candidate_details->CDF_education,
                            'CDF_school' => $form_candidate_details->CDF_school,
                            'CDF_ffirstName' => (isset($row['father_fname']) && !empty($row['father_fname'])) ? $row['father_fname'] : $form_candidate_details->CDF_ffirstName,
                            'CDF_fmiddleName' => (isset($row['father_mname']) && !empty($row['father_mname'])) ? $row['father_mname'] : $form_candidate_details->CDF_fmiddleName,
                            'CDF_flastName' => (isset($row['father_lname']) && !empty($row['father_lname'])) ? $row['father_lname'] : $form_candidate_details->CDF_flastName,
                            'CDF_fcountry' => (isset($row['father_country_code']) && !empty($row['father_country_code'])) ? $row['father_country_code'] : $form_candidate_details->CDF_fcountry,
                            'CDF_mfirstName' => (isset($row['mother_fname']) && !empty($row['mother_fname'])) ? $row['mother_fname'] : $form_candidate_details->CDF_mfirstName,
                            'CDF_mmiddleName' => (isset($row['mother_mname']) && !empty($row['mother_mname'])) ? $row['mother_mname'] : $form_candidate_details->CDF_mmiddleName,
                            'CDF_mlastName' => (isset($row['mother_lname']) && !empty($row['mother_lname'])) ? $row['mother_lname'] : $form_candidate_details->CDF_mlastName,
                            'CDF_mcountry' => (isset($row['mother_country_code']) && !empty($row['mother_country_code'])) ? $row['mother_country_code'] : $form_candidate_details->CDF_mcountry,
                            'CDF_passport_file' => $form_candidate_details->CDF_passport_file,
                            'CDF_passp_Num' => (isset($row['passport_num']) && !empty($row['passport_num'])) ? $row['passport_num'] : $form_candidate_details->CDF_passp_Num,
                            'CDF_passp_issCountry' => (isset($row['passport_country']) && !empty($row['passport_country'])) ? $row['passport_country'] : $form_candidate_details->CDF_passp_issCountry,
                            'CDF_passp_issDate' => (!empty($CDF_passp_issDate)) ? $CDF_passp_issDate : null,
                            'CDF_passp_expiryDate' => (!empty($CDF_passp_expiryDate)) ? $CDF_passp_expiryDate : null,
                            'CDF_EM_name' => (isset($row['EC_fullname']) && !empty($row['EC_fullname'])) ? $row['EC_fullname'] : $form_candidate_details->CDF_EM_name,
                            'CDF_EM_relationship' => (isset($row['EC_relation']) && !empty($row['EC_relation'])) ? $row['EC_relation'] : $form_candidate_details->CDF_EM_relationship,
                            'CDF_EM_phoneNum' => (isset($row['EC_phone_number']) && !empty($row['EC_phone_number'])) ? $row['EC_phone_number'] : $form_candidate_details->CDF_EM_phoneNum,
                            'CDF_EM_cel' => (isset($row['EC_cell_number']) && !empty($row['EC_cell_number'])) ? $row['EC_cell_number'] : $form_candidate_details->CDF_EM_cel,
                            'CDF_EM_email1' => (isset($row['EC_email']) && !empty($row['EC_email'])) ? $row['EC_email'] : $form_candidate_details->CDF_EM_email1,
                            'CDF_EM_add' => (isset($row['EC_street_address']) && !empty($row['EC_street_address'])) ? $row['EC_street_address'] : $form_candidate_details->CDF_EM_add,
                            'CDF_EM_city' => (isset($row['EC_city']) && !empty($row['EC_city'])) ? $row['EC_city'] : $form_candidate_details->CDF_EM_city,
                            'CDF_EM_zip' => (isset($row['EC_zip_code']) && !empty($row['EC_zip_code'])) ? $row['EC_zip_code'] : $form_candidate_details->CDF_EM_zip,
                            'CDF_EM_state' => (isset($row['EC_state']) && !empty($row['EC_state'])) ? $row['EC_state'] : $form_candidate_details->CDF_EM_state,
                            'CDF_EM_country_code' => (isset($row['EC_country_code']) && !empty($row['EC_country_code'])) ? $row['EC_country_code'] : $form_candidate_details->CDF_EM_country_code,
                            'CDF_B_same_inc' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? 1 : $form_candidate_details->CDF_B_same_inc,
                            'CDF_B_name' => (isset($row['BF_fullname']) && !empty($row['BF_fullname'])) ? $row['BF_fullname'] : $form_candidate_details->CDF_B_name,
                            'CDF_B_phoneNum' => (isset($row['BF_phone_number']) && !empty($row['BF_phone_number'])) ? $row['BF_phone_number'] : $form_candidate_details->CDF_B_phoneNum,
                            'CDF_B_cel' => (isset($row['BF_cell_number']) && !empty($row['BF_cell_number'])) ? $row['BF_cell_number'] : $form_candidate_details->CDF_B_cel,
                            'CDF_B_add' => (isset($row['BF_street_address']) && !empty($row['BF_street_address'])) ? $row['BF_street_address'] : $form_candidate_details->CDF_B_add,
                            'CDF_B_city' => (isset($row['BF_city']) && !empty($row['BF_city'])) ? $row['BF_city'] : $form_candidate_details->CDF_B_city,
                            'CDF_B_zip' => (isset($row['BF_zip_code']) && !empty($row['BF_zip_code'])) ? $row['BF_zip_code'] : $form_candidate_details->CDF_B_zip,
                            'CDF_B_state' => (isset($row['BF_state']) && !empty($row['BF_state'])) ? $row['BF_state'] : $form_candidate_details->CDF_B_state,
                            'CDF_B_country_code' => (isset($row['BF_country_code']) && !empty($row['BF_country_code'])) ? $row['BF_country_code'] : $form_candidate_details->CDF_B_country_code,
                            'CDF_B_relationship' => (isset($row['BF_relation']) && !empty($row['BF_relation'])) ? $row['BF_relation'] : $form_candidate_details->CDF_B_relationship,
                            'CDF_B_email' => (isset($row['BF_email']) && !empty($row['BF_email'])) ? $row['BF_email'] : $form_candidate_details->CDF_B_email,
                            'CDF_B_per' => (isset($row['BF_percent']) && !empty($row['BF_percent'])) ? $row['BF_percent'] : $form_candidate_details->CDF_B_per,
                            'CDF_B_name1' => (isset($row['BF_fullname1']) && !empty($row['BF_fullname1'])) ? $row['BF_fullname1'] : $form_candidate_details->CDF_B_name1,
                            'CDF_B_phoneNum1' => (isset($row['BF_phone_number1']) && !empty($row['BF_phone_number1'])) ? $row['BF_phone_number1'] : $form_candidate_details->CDF_B_phoneNum1,
                            'CDF_B_cel1' => (isset($row['BF_cell_number1']) && !empty($row['BF_cell_number1'])) ? $row['BF_cell_number1'] : $form_candidate_details->CDF_B_cel1,
                            'CDF_B_add1' => (isset($row['BF_street_address1']) && !empty($row['BF_street_address1'])) ? $row['BF_street_address1'] : $form_candidate_details->CDF_B_add1,
                            'CDF_B_city1' => (isset($row['BF_city1']) && !empty($row['BF_city1'])) ? $row['BF_city1'] : $form_candidate_details->CDF_B_city1,
                            'CDF_B_zip1' => (isset($row['BF_zip_code1']) && !empty($row['BF_zip_code1'])) ? $row['BF_zip_code1'] : $form_candidate_details->CDF_B_zip1,
                            'CDF_B_state1' => (isset($row['BF_state1']) && !empty($row['BF_state1'])) ? $row['BF_state1'] : $form_candidate_details->CDF_B_state1,
                            'CDF_B_country_code1' => (isset($row['BF_country_code1']) && !empty($row['BF_country_code1'])) ? $row['BF_country_code1'] : $form_candidate_details->CDF_B_country_code1,
                            'CDF_B_relationship1' => (isset($row['BF_relation1']) && !empty($row['BF_relation1'])) ? $row['BF_relation1'] : $form_candidate_details->CDF_B_relationship1,
                            'CDF_B_email1' => (isset($row['BF_email1']) && !empty($row['BF_email1'])) ? $row['BF_email1'] : $form_candidate_details->CDF_B_email1,
                            'CDF_B_per1' => (isset($row['BF_percent1']) && !empty($row['BF_percent1'])) ? $row['BF_percent1'] : $form_candidate_details->CDF_B_per1,
                            'CDF_brand' => (isset($row['brand']) && !empty($row['brand'])) ? $row['brand'] : $user_personal_data->CDF_brand,
                            'CDF_recruiter' => (isset($row['recruiter']) && !empty($row['recruiter'])) ? $row['recruiter'] : $user_personal_data->CDF_recruiter,
                            'CDF_jobcode' => (isset($row['job_code']) && !empty($row['job_code'])) ? $row['job_code'] : $user_personal_data->CDF_jobcode,
                            'last_updated_on' => date('Y-m-d H:i:s'),
                            'updated_by' => 999999999999
                        ];

                        $form_candidate_data_class_2 = new FormCandidateData();
                        $form_candidate_data_class_2->setDBConnection('ctrac_prod_db');
                        // $pdf_success_data[] = $form_candidate_data;
                        $pdf_success_data[] = $form_candidate_data_class_2->savePersonalDataForm($form_candidate_data);
                    }

                    /**
                     * Update User Data
                     */
                    $user_details->source_id = 54;
                    $user_details->hp_register_company = $row['Source'];
                    $user_details->save();
                }
                else
                {
                    $pdf_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                    $pd_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                }
            }
            // dd($pdf_success_data);
            session()->flash('success_flash_data', true);
            session()->flash('pdf_success_data', $pdf_success_data);
            session()->flash('pd_success_data', $pd_success_data);
            session()->flash('pdf_failed_data', $pdf_failed_data);
            session()->flash('pd_failed_data', $pd_failed_data);

            return redirect()->back();
        }

        session()->flash('success_flash_data', false);

        return redirect()->back();
    }

    public function crisisdataCleanUp(
        Request $request, 
        Gateway $gateway_class, 
        SchoolDegreeType $school_degree_types_class
    )
    {
        $upload_csv = $request->upload_csv;
        $raw_data = [];

        $request->validate([
            'upload_csv' => 'required'
        ]);

        $file_name = time() . date('Y-m-d') . '-crisis-management' . '.csv';
        $request->upload_csv->storeAs('CrisisManagement', $file_name);

        if ($request->hasFile('upload_csv')) 
        {
            $file_path = storage_path('app/CrisisManagement/' . $file_name);
            $csv_data = StringUtility::crisisCsvToArray($file_path);

            if (!empty($csv_data))
            {
                $raw_data = $csv_data;
            }
        }
        
        $pdf_failed_data = [];
        $pd_failed_data = [];
        $pdf_success_data = [];
        $pd_success_data = [];
        $delete = 0;
        // dd($raw_data);
        if (!empty($raw_data))
        {
            foreach ($raw_data as $row) 
            {

                $user_class = new User();
                $user_class->setDBConnection('ctrac_prod_db');
                $user_details = $user_class->getByJDEId($row['jde_id']);

                if (!is_null($user_details))
                {
                    /**
                     * Update Crew PDF or Personal Data Form
                     */
                    $form_candidate_data_class = new FormCandidateData();
                    $form_candidate_data_class->setDBConnection('ctrac_prod_db');
                    $form_candidate_details = $form_candidate_data_class->getByUserIdAll($user_details->getId());

                    if (!$form_candidate_details->isEmpty())
                    {
                        foreach ($form_candidate_details as $value) 
                        {
                            $value->delete();
                        }
                        $delete++;
                    }
                }
                else
                {
                    $pdf_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                    $pd_failed_data[] = [
                        'jde_id' => $row['jde_id'],
                        'email' => $row['email']
                    ];
                }
            }
            dd($delete);
            // dd($pdf_success_data);
            session()->flash('success_flash_data', true);
            session()->flash('pdf_success_data', $pdf_success_data);
            session()->flash('pd_success_data', $pd_success_data);
            session()->flash('pdf_failed_data', $pdf_failed_data);
            session()->flash('pd_failed_data', $pd_failed_data);

            return redirect()->back();
        }

        session()->flash('success_flash_data', false);

        return redirect()->back();
    }

    public function crisisdataCleanUpPersonal(Request $request)
    {
        $upload_csv = $request->upload_csv;
        $raw_data = [];

        $request->validate([
            'upload_csv' => 'required'
        ]);

        $file_name = time() . date('Y-m-d') . '-crisis-management' . '.csv';
        $request->upload_csv->storeAs('CrisisManagement', $file_name);

        if ($request->hasFile('upload_csv')) 
        {
            $file_path = storage_path('app/CrisisManagement/' . $file_name);
            $csv_data = StringUtility::crisisCsvToArray($file_path);

            if (!empty($csv_data))
            {
                $raw_data = $csv_data;
            }
        }
        
        $pdf_failed_data = [];
        $pd_failed_data = [];
        $pdf_success_data = [];
        $pd_success_data = [];
        $delete = [];
        $count = 0;
        // dd($raw_data);
        if (!empty($raw_data))
        {
            foreach ($raw_data as $row) 
            {
                $data = [
                    'first_name' => (!is_null($row['first_name']) && !empty($row['first_name'])) ? $row['first_name'] : null,
                    'middle_name' => (!is_null($row['middle_name']) && !empty($row['middle_name'])) ? $row['middle_name'] : null,
                    'last_name' => (!is_null($row['last_name']) && !empty($row['last_name'])) ? $row['last_name'] : null,
                    'fathers_name' => (!is_null($row['fathers_name']) && !empty($row['fathers_name'])) ? $row['fathers_name'] : null,
                ];
                $personal_data_class = new PersonalData();
                $personal_data_class->setDBConnection('ctrac_prod_db');
                $personal_details = $personal_data_class->getPersonalDataByFullName($data);
                // dd($data);
                // $delete[] = !$personal_details->isEmpty();
                if (!$personal_details->isEmpty())
                {
                    foreach ($personal_details as $value) 
                    {
                        $value->delete();
                    }
                    $delete++;
                    // $count++;
                }
            }
            dd($delete);
            // dd($pdf_success_data);
            // session()->flash('success_flash_data', true);
            // session()->flash('pdf_success_data', $pdf_success_data);
            // session()->flash('pd_success_data', $pd_success_data);
            // session()->flash('pdf_failed_data', $pdf_failed_data);
            // session()->flash('pd_failed_data', $pd_failed_data);

            return redirect()->back();
        }

        session()->flash('success_flash_data', false);

        return redirect()->back();
    }
}
