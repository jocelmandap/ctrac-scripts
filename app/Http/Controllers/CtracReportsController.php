<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Model\Users\User;
use CTRAC\Model\Documents\DocumentChecklist;
use CTRAC\Model\Documents\DocumentChecklistUpload;
use CTRAC\Model\Documents\DocumentChecklistFormUpload;
use CTRAC\Model\Documents\DocumentChecklistGroup;
use CTRAC\Exports\CrewPendingAndCompletedDocumentsExport;
use CTRAC\Helpers\DateUtility;
use CTRAC\Helpers\StringUtility;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Helpers\clientHttpHelper;
use Excel;
use CTRAC\Model\Statuses\StatusCode;
use CTRAC\Model\Histories\HistoryRecord;
use CTRAC\Model\Jobs\JobApplication;
use CTRAC\Model\Jobs\JobInt;
use CTRAC\Model\Jobs\JobRequest;
use CTRAC\Model\Countries\Country;
use Illuminate\Support\Facades\DB;

class CtracReportsController extends Controller
{
	public function viewPendingCompletedDocuments ()
	{
	    return view('reports.pending-completed-docs');
	}

	public function uploadPendingCompletedDocuments (Request $request)
	{
		$extracted_data = [];
		$doc_status = $request->doc_status;

		if ($request->action == 'upload_csv')
		{
			$request->validate([
				'upload_csv' => 'required',
				'from_date' => 'required',
				'to_date' => 'required'
			]);

			$file_name_status = '-new-hire-document-statuses';

			$file_name = time() . date('Y-m-d') . $file_name_status . '-uploads' . '.csv';
			$request->upload_csv->storeAs('Reports', $file_name);

			if ($request->hasFile('upload_csv')) 
			{
				$file_path = storage_path('app/Reports/' . $file_name);
				$csv_data = StringUtility::csvToArray($file_path);

				if (!empty($csv_data))
				{
					$extracted_data = $csv_data;
				}
			}
		}
		else
		{
			$request->validate([
				'employee_id' => 'required',
				'from_date' => 'required',
				'to_date' => 'required'
			]);

			$extracted_data = [
				[
					'Employee ID' => $request->employee_id
				]
			];
		}

		if (!empty($extracted_data))
		{
			foreach ($extracted_data as $row) 
			{
				$jde_ids[] = str_replace(',', '', $row['Employee ID']);
			}

			// $jde_ids = [426832, 429333, 420335, 428816, 411677, 425657];
			$from_date_modified = DateUtility::changeDateFormat($request->from_date, 'F d, Y', 'Y-m-d');
			$to_date_modified = DateUtility::changeDateFormat($request->to_date, 'F d, Y', 'Y-m-d');

			$doc_checklist_upload_class = new DocumentChecklistUpload();
			$doc_group_class = new DocumentChecklistGroup();

			/**
			 * Set DB Connection to Production
			 */
			$db_connection = DatabaseConnection::CTRAC_QA;
			if (config('app.env') != 'local')
			{
			    $db_connection = DatabaseConnection::CTRAC_PROD;
			}

			$doc_checklist_upload_class->setDBConnection($db_connection);
	    	$doc_group_class->setDBConnection($db_connection);

			$document_types = [];
			$document_group_types = $doc_group_class->whereIn('doc_group_id', [1,3,7])->get();
			if (!$document_group_types->isEmpty())
			{
			    foreach ($document_group_types as $document_group_type) 
			    {
			        $document_type_data = $document_group_type->docType()->get();

			        if (!$document_type_data->isEmpty())
			        {
			            foreach ($document_type_data as $document_type_detail) 
			            {
			                $document_types[] = $document_type_detail->getId();
			            }
			        }
			    }
			    $document_types[] = 42; // New Hire Checklist
			    $document_types[] = 117; // New Hire Verification
			    $document_types[] = 124; // Background Check Int
			    $document_types[] = 125; // Background Check Domestic (US)
			}

			$user_document_data = $doc_checklist_upload_class->getNewHireDocumentStatusList (
		        $document_types,
		        $doc_status,
		        $from_date_modified,
		        $to_date_modified,
		        $jde_ids
		    );

		    if (!$user_document_data->isEmpty())
		    {
		    	session()->flash('success_flash', true);
		    	session()->flash('message_flash', 'Successfully retrieve data! Total of Data: ' . $user_document_data->count());
		    	session()->flash('user_document_data', $user_document_data);
		    	return redirect()->back();
		    }
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'An error occured while processing data, please try again later.');
		return redirect()->back();
	}

	public function viewRejectedPdfAndOfferLetterAutomation ()
	{
		$status_code_class = new StatusCode();

		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
		    $db_connection = DatabaseConnection::CTRAC_PROD;
		}

		$status_code_class->setDBConnection($db_connection);

		$rejected_status_codes = $status_code_class->getByIds([6900, 6904]);

		return view('reports.rejected-pdf-offer-automation', 
			compact('rejected_status_codes')
		);
	}

	public function getRejectedPdfAndOfferLetterAutomation (
		Request $request,
		JobApplication $job_application_class,
		DocumentChecklist $document_type_class
	)
	{
		$request->validate([
			'rejected_status' => 'required',
			'from_date' => 'required',
			'to_date' => 'required'
		]);

		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
		    $db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$job_application_class->setDBConnection($db_connection);
		$document_type_class->setDBConnection($db_connection);
		$from_date_modified = DateUtility::changeDateFormat($request->from_date, 'F d, Y H:i A', 'Y-m-d H:i:s');
		$to_date_modified = DateUtility::changeDateFormat($request->to_date, 'F d, Y H:i A', 'Y-m-d H:i:s');
		$status_code[] = $request->rejected_status;
		$admin_id = 99999999999;

		if ($request->rejected_status == 'All') 
		{
			$status_code = [6900, 6904];
		}

		$offer_letter_ids = [];
		$offer_letters = $document_type_class->getAllOFferLetter();
		if (!$offer_letters->isEmpty())
		{
			foreach ($offer_letters as $offer_letter) 
			{
				$offer_letter_ids[] = $offer_letter->getId();
			}
		}
		$passport_document_id = 37;
		$pdf_document_id = 109;

		$records = $job_application_class->getAutoFailPDFandOfferLetterReport (
	    	$status_code,
	    	$from_date_modified,
	    	$to_date_modified,
	    	$offer_letter_ids
		);

		if (!$records->isEmpty()) {
			session()->flash('records', $records);
			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully retrieve records! (Total: ' . $records->count() . ')');
		} else {
			session()->flash('success_flash', false);
			session()->flash('message_flash', 'No records found!');
		}

		return redirect()->back();
	}

	public function viewDocumentTravelInfo (Country $country_class)
	{
		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
		    $db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$country_class->setDBConnection($db_connection);

		$countries = $country_class->getAll();

		return view('reports.document-travel-info', 
			compact(
				'countries'
			)
		);
	}

	public function getDocumentTravelInfo (Request $request)
	{
		$extracted_jde_data = [];

		if ($request->action == 'bulk')
		{
			if ($request->hasFile('upload_csv')) 
			{
				$file_name = time() . date('Y-m-d') . '-document-travel-info' . '.csv';
				$request->upload_csv->storeAs('Reports', $file_name);

				$file_path = storage_path('app/Reports/' . $file_name);
				$csv_data = StringUtility::csvToArray($file_path);

				if (!empty($csv_data))
				{
					$tmp_data = [];
					foreach ($csv_data as $datum) 
					{
						$tmp_data[] = $datum['Employee ID'];
					}
					$extracted_jde_data = implode(',', $tmp_data);
				}
			}
		}
		else
		{
			if (!empty( trim($request->employee_ids) ))
			{
				$extracted_jde_data = $request->employee_ids;
			}
		}

		$form_params = [];

		// Employee ID - Sample: 949399,952880,964574,964831
		if ( !empty( $extracted_jde_data ) )
		{
			$form_params['jde_id'] = $extracted_jde_data;
		}

		// Departure Country
		if (!empty( $request->departure_country ))
		{
			$form_params['country_from'] = $request->departure_country;
		}

		// Arrival Country
		if (!empty( $request->arrival_country ))
		{
			$form_params['country_to'] = $request->arrival_country;
		}

		// Departure Date
		if ( !empty( $request->departure_date ) )
		{
			$form_params['departure_date'] = DateUtility::changeDateFormat($request->departure_date, 'F d, Y', 'Y-m-d');
		}

		// Arrival Date
		if ( !empty( $request->arrival_date ) )
		{
			$form_params['arrival_date'] = DateUtility::changeDateFormat($request->arrival_date, 'F d, Y', 'Y-m-d');
		}

		$url = env('API_CTRAC_BASE_URI', '') . env('API_FOR_DOCUMENT_TRAVEL_INFO', '');
		$client = new clientHttpHelper( $url );
		$result = $client->post( $form_params );

		if ($result)
		{
			if ($result['status'] && !empty($result['data']))
			{
				session()->flash('records', $result['data']);
				session()->flash('success_flash', true);
				session()->flash('message_flash', $result['message'] . ' - (Total: ' . count( $result['data'] ) . ')');
				return redirect()->back();
			}

			session()->flash('success_flash', false);
			session()->flash('message_flash', $result['message']);
			return redirect()->back();
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'An error occured while processing data, please try again later.');
		return redirect()->back();
	}

	public function viewReportOnPDF (
		StatusCode $status_code_class,
		JobInt $job_int_class
	)
	{
		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
		    $db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$status_code_class->setDBConnection($db_connection);
		$job_int_class->setDBConnection($db_connection);

		$status_codes = $status_code_class->getByIds([2420 , 3420, 5000, 17000]);
		$job_codes = $job_int_class->getAllByStatus(1);

		return view('reports.pdf-report-list',
			compact(
				'status_codes',
				'job_codes'
			)
		);
	}

	public function getReportOnPDF (
		Request $request,
		JobRequest $job_request_class
	)
	{
		$request->validate([
			'pdf_document_status' => 'required',
			'job_code_ids' => 'required'
		]);

		$application_status = ($request->application_status == 'all') ? [2420 , 3420, 5000, 17000] : [ $request->application_status ];
		$pdf_document_status = $request->pdf_document_status;
		$job_codes = (in_array('all', $request->job_code_ids)) ? [] : $request->job_code_ids;
		$from_offer_date = DateUtility::changeDateFormat($request->from_offer_date, 'F d, Y', 'Y-m-d');
		$to_offer_date = DateUtility::changeDateFormat($request->to_offer_date, 'F d, Y', 'Y-m-d');

		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
		    $db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$job_request_class->setDBConnection($db_connection);

		$job_request_class->setDBConnection($db_connection);

		$result = $job_request_class->getPDFListReport (
	        $application_status,
	        $pdf_document_status,
	        $job_codes,
	        $from_offer_date,
	        $to_offer_date
	    );

		if (!$result->isEmpty())
		{
			session()->flash('records', $result);

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully fetch data! Total ( ' . $result->count() . ' )');

			return redirect()->back();
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'No data found!');

		return redirect()->back();
	}

	public function viewHeaderPendingCompletedDocuments ()
	{
	    return view('reports.header-pending-completed-docs');
	}

	public function uploadHeaderPendingCompletedDocuments (Request $request)
	{
		$extracted_data = [];
		$doc_status = $request->doc_status;

		if ($request->action == 'upload_csv')
		{
			$request->validate([
				'upload_csv' => 'required',
				'from_date' => 'required',
				'to_date' => 'required'
			]);

			$file_name_status = '-new-hire-document-statuses';

			$file_name = time() . date('Y-m-d') . $file_name_status . '-uploads' . '.csv';
			$request->upload_csv->storeAs('Reports', $file_name);

			if ($request->hasFile('upload_csv')) 
			{
				$file_path = storage_path('app/Reports/' . $file_name);
				$csv_data = StringUtility::csvToArray($file_path);

				if (!empty($csv_data))
				{
					$extracted_data = $csv_data;
				}
			}
		}
		else
		{
			$request->validate([
				'employee_id' => 'required',
				'from_date' => 'required',
				'to_date' => 'required'
			]);

			$extracted_data = [
				[
					'Employee ID' => $request->employee_id
				]
			];
		}

		if (!empty($extracted_data))
		{
			foreach ($extracted_data as $row) 
			{
				$jde_ids[] = str_replace(',', '', $row['Employee ID']);
			}

			// $jde_ids = [426832, 429333, 420335, 428816, 411677, 425657];
			$from_date_modified = DateUtility::changeDateFormat($request->from_date, 'F d, Y', 'Y-m-d');
			$to_date_modified = DateUtility::changeDateFormat($request->to_date, 'F d, Y', 'Y-m-d');

			$doc_checklist_form_upload_class = new DocumentChecklistFormUpload();
			$doc_group_class = new DocumentChecklistGroup();

			/**
			 * Set DB Connection to Production
			 */
			$db_connection = DatabaseConnection::CTRAC_QA;
			if (config('app.env') != 'local')
			{
			    $db_connection = DatabaseConnection::CTRAC_PROD;
			}

			$doc_checklist_form_upload_class->setDBConnection($db_connection);
	    	$doc_group_class->setDBConnection($db_connection);

			$document_types = [];
			$document_group_types = $doc_group_class->whereIn('doc_group_id', [1,3,7])->get();
			if (!$document_group_types->isEmpty())
			{
			    foreach ($document_group_types as $document_group_type) 
			    {
			        $document_type_data = $document_group_type->docType()->get();

			        if (!$document_type_data->isEmpty())
			        {
			            foreach ($document_type_data as $document_type_detail) 
			            {
			                $document_types[] = $document_type_detail->getId();
			            }
			        }
			    }
			    $document_types[] = 42; // New Hire Checklist
			    $document_types[] = 117; // New Hire Verification
			    $document_types[] = 124; // Background Check Int
			    $document_types[] = 125; // Background Check Domestic (US)
			}

			$user_document_data = $doc_checklist_form_upload_class->getNewHireMainDocumentStatusList (
		        $document_types,
		        $doc_status,
		        $from_date_modified,
		        $to_date_modified,
		        $jde_ids
		    );

		    if (!$user_document_data->isEmpty())
		    {
		    	session()->flash('success_flash', true);
		    	session()->flash('message_flash', 'Successfully retrieve data! Total of Data: ' . $user_document_data->count());
		    	session()->flash('user_document_data', $user_document_data);
		    	return redirect()->back();
		    }
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'An error occured while processing data, please try again later.');
		return redirect()->back();
	}
}
