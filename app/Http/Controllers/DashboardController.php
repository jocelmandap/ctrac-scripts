<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\DashboardLink;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	$role = $user->roles->first();
    	$parent_pages = [];
    	$child_pages = [];

    	$pages = $role->pages->where('is_active', '=', 1)->sortBy('order');

    	if (!$pages->isEmpty()) 
    	{
    	    foreach ($pages as $parent_page) 
    	    {
    	        if ($parent_page->getParentId() == 0) {
    	            $parent_pages[] = $parent_page;
    	        }
    	    }

    	    foreach ($pages as $child_page) 
    	    {
    	        if ($child_page->getParentId() != 0) {
    	            $child_pages[] = $child_page;
    	        }
    	    }
    	}

    	$active_page = DashboardLink::MAIN_DASHBOARD;

    	return view('dashboard', 
    		compact(
    			'active_page',
    			'role',
    			'parent_pages',
    			'child_pages',
    			'pages'
    		)
    	);
    }
}
