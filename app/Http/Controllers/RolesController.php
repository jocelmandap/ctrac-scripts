<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Model\Scripts\CtracRole;

class RolesController extends Controller
{
    public function index(CtracRole $role_class)
    {
    	$role_list = $role_class->getAll();
    	return view('roles.view', 
    		compact(
    			'role_list'
    		)
    	);
    }

    public function saveRole(Request $request, CtracRole $role_class)
    {
    	$request->validate([
    	    'name' => 'required',
    	    'desc' => 'required'
    	]);

    	$data = [
    		'id' => $request->role_id,
    		'name' => $request->name,
    		'description' => $request->desc
    	];

    	$result = $role_class->saveRole($data, false);

    	if ($result) 
    	{
    		$request->session()->flash('success_flash', true);
    		$request->session()->flash('message_flash', $request->name . ' role was successfully save!');
    		return redirect()->back();
    	}
    	$request->session()->flash('success_flash', false);
    	$request->session()->flash('message_flash', 'An error occur while saving it, please try again later!');

    	return redirect()->back();
    }

    public function deleteRole(Request $request, CtracRole $role_class)
    {
    	if (!is_null($request->role_id))
    	{
    		$role = $role_class->getById($request->role_id);

    		if (!is_null($role))
    		{
    			if ($role->delete())
    			{
    				$request->session()->flash('success_flash', true);
    				$request->session()->flash('message_flash', $request->name . ' was successfully deleted!');

    				return response()->json([
    					'status' => true,
    					'message' => 'Role was successfully deleted!'
    				]);
    			}
    		}

    		return response()->json([
    			'status' => false,
    			'message' => 'Role not available!'
    		]);
    	}

    	return response()->json([
    		'status' => false,
    		'message' => 'Empty Role Id!'
    	]);
    }
}
