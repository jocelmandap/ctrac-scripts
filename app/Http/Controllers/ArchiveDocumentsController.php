<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Model\Scripts\CtracUser;
use Auth;
use View;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\Users\User;
use CTRAC\Model\Documents\DocumentChecklist;
use CTRAC\Model\Documents\DocumentChecklistFormUpload;
use CTRAC\Model\Documents\DocumentChecklistUpload;
use CTRAC\Model\Documents\DocumentFormUploadsStatus;
use CTRAC\Model\Documents\DocumentChecklistUploadsArchive;
use Illuminate\Support\Facades\DB;

class ArchiveDocumentsController extends Controller
{
    public function index ()
    {
    	return view('archive-documents.view');
    }

    public function submitSearch(
        Request $request,
        User $user_class,
        DocumentChecklistUploadsArchive $document_checklist_upload_archive_class
    )
    {
        $ck_list_id = $request->document_id;
        $search_by = $request->user_search_by;
        $user_detail = $request->user_detail;
        $app_env = $request->app_env;

        if (!is_null($ck_list_id) && !is_null($search_by) && !is_null($user_detail))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if ($app_env == 'prod')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $user_class->setDBConnection($db_connection);
            $document_checklist_upload_archive_class->setDBConnection($db_connection);

            $query = $document_checklist_upload_archive_class->leftJoin('users', 'document_checklist_uploads_archives.user_id', '=', 'users.user_id')
                        ->where('users.' . $search_by, '=', $user_detail);

            if ($ck_list_id != 0) {
                $query->where('document_checklist_uploads_archives.ck_list_id', '=', $ck_list_id);
            }

            $results = $query->get();

            if (!$results->isEmpty())
            {
                /**
                 * Results Data
                 * Render a HTML form
                 */
                $results_content = View::make('partials.archive-documents.search-results', [
                    'results' => $results
                ])->render();

                return response()->json([
                    'status' => true,
                    'message' => 'Successfully fetch all the Archive list',
                    'search_result' => $results_content
                ]);
            }

            return response()->json([
                'status' => false,
                'message' => 'No Archive Documents for '. $user_detail
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => 'An error occured, Please try again later'
        ]);
    }

    public function downloadDocument (Request $request)
    {
        if (!is_null($request->file_path))
        {
            $host = config('panda-media-service.host');
            $token = config('panda-media-service.access-token');
            $pandaPath = base64_encode($host.'/'.$request->file_path);
            $pandaLink = base64_decode($pandaPath, true);
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . basename($pandaLink) . "\"");
            readfile($pandaLink .'?at='.$token);
            exit();
        }
    }

    public function viewDocumentDetails (
        Request $request, 
        DocumentChecklistUploadsArchive $document_checklist_upload_archive_class
    )
    {
        if (!is_null($request->archive_id) && !is_null($request->file_path) && !is_null($request->doc_ext))
        {
            $app_env = $request->app_env;
            $archive_id = $request->archive_id;
            $host = config('panda-media-service.host');
            $token = config('panda-media-service.access-token');
            $file_ext = $request->doc_ext;
            $type = 'image';

            if ($file_ext === 'doc' || $file_ext === 'docx' || $file_ext === 'pdf')
            {
                $type = 'application';

                if($file_ext === 'doc' || $file_ext === 'docx')
                {
                    $file_ext = 'msword';
                }
            }

            $panda = preg_replace('/\s+/', '%20', $request->file_path);
            $url = $host . '/' . $panda . '?at=' . $token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
            header("Content-Type: ". $type ."/" . $file_ext);
            $rawdata = curl_exec($ch);
            curl_close ($ch);

            $file = 'data:' . $type . '/' .$file_ext . ';base64,'.base64_encode($rawdata);

            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if ($app_env == 'prod')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $document_checklist_upload_archive_class->setDBConnection($db_connection);
            $results = $document_checklist_upload_archive_class->getById($archive_id);
            /**
             * Results Data
             * Render a HTML form
             */
            $results_content = View::make('partials.archive-documents.view-document-details', [
                'results' => $results,
                'file_type' => $type,
                'file' => $file
            ])->render();

            return response()->json([
                'status' => true,
                'message' => 'Successfully retrive file',
                'content' => $results_content
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => 'Empty file'
        ]);
    }

    public function getAllUsers(
        Request $request,
        User $user_class,
        DocumentChecklist $document_checklist_class,
        DocumentChecklistUpload $document_checklist_upload_class
    )
    {
    	
    }

}
