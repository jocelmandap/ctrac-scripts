<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\ILO\Ship;

class ILOController extends Controller
{
	public function viewDashboard ()
	{
		return view('ilo.view');
	}

    public function viewTAndAViolation (Ship $ship_class)
    {
    	/**
    	 * Set DB Connection to Production
    	 */
    	$db_connection = DatabaseConnection::CTRAC_QA;
    	if (config('app.env') != 'local')
    	{
    	    $db_connection = DatabaseConnection::CTRAC_PROD;
    	}
    	$ship_class->setDBConnection($db_connection);
    	$ships = $ship_class->getAll();

    	return view('ilo.view-uploader', compact('ships'));
    }

    public function uploadTAndAViolation (Request $request)
    {
    	$request->validate([
    		'period_of' => 'required',
    		'year_of' => 'required',
    		'month_of' => 'required',
    		'ship' => 'required',
    		'upload_csv' => 'required'
    	]);

    	$period_of = $request->period_of;
    	$year_of = $request->year_of;
    	$month_of = $request->month_of;
    	$ship_id = $request->ship;

    	/**
    	 * Store CSV File
    	 */
    	$month_name = date("F", mktime(0, 0, 0, $month_of, 10));
    	$file_name = $month_name . '-' . $period_of . '-of-3-ilo-violation-data' . '.csv';
    	$request->upload_csv->storeAs('ilo-violation-data-' . $year_of , $file_name);
    }
}
