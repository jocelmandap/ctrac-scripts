<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Helpers\DashboardLink;
use CTRAC\Model\Users\User;
use CTRAC\Model\Forms\FormCandidateData;
use CTRAC\Model\Documents\DocumentChecklistFormUpload;
use CTRAC\Model\Documents\DocumentChecklistUpload;
use CTRAC\Model\Documents\DocumentFormUploadsStatus;
use CTRAC\Model\Hirerights\HirerightCrewRequest;
use CTRAC\Model\Hirerights\HirerightJobintPackage;
use CTRAC\Model\Hirerights\HirerightAllowedStatus;
use CTRAC\Model\Hirerights\HirerightPackage;
use CTRAC\Model\Jobs\JobApplication;
use View;
use CTRAC\Helpers\MailUtility;
use CTRAC\Helpers\DateUtility;
use Illuminate\Support\Facades\Auth;
use CTRAC\Model\Scripts\CtracSupportHistory;

class BackgroundChecksController extends Controller
{
    public function index (DocumentFormUploadsStatus $document_form_upload_status_class)
    {
    	$active_page = DashboardLink::BACKGROUND_CHECKS_PAGE;
    	return view('background-checks.view', 
    		compact(
    			'active_page'
    		)
    	);
    }

    public function backgroundCheck (
    	Request $request,
    	User $user_class,
    	FormCandidateData $form_candidate_class, 
    	DocumentChecklistFormUpload $document_checklist_form_upload_class,
        DocumentChecklistUpload $document_checklist_upload_class,
    	DocumentFormUploadsStatus $document_form_upload_status_class,
    	HirerightCrewRequest $hireright_crew_request_class,
    	HirerightJobintPackage $hireright_jobint_package_class,
    	HirerightAllowedStatus $hireright_allowed_status_class,
    	HirerightPackage $hireright_package_class,
        JobApplication $job_application_class
    )
    {
        /**
         * Set DB Connection to Production
         */
        $db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local')
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }
        $user_class->setDBConnection($db_connection);
        $form_candidate_class->setDBConnection($db_connection);
        $document_checklist_form_upload_class->setDBConnection($db_connection);
        $document_checklist_upload_class->setDBConnection($db_connection);
        $document_form_upload_status_class->setDBConnection($db_connection);
        $hireright_crew_request_class->setDBConnection($db_connection);
        $hireright_jobint_package_class->setDBConnection($db_connection);
        $hireright_allowed_status_class->setDBConnection($db_connection);
        $hireright_package_class->setDBConnection($db_connection);
        $job_application_class->setDBConnection($db_connection);

    	$jde_id = $request->jde_id;
    	$user_details = $user_class->getByJDEId($jde_id);

    	if (!is_null($user_details))
    	{
            /**
             * Get All Highright Allowed Status
             */
            $hireright_statuses = $hireright_allowed_status_class->getAll();

            /**
             * Get All Document Upload Status
             */
            $document_form_upload_statuses = $document_form_upload_status_class->getAll();

    		/**
    		 * Form Candidate Data
    		 * Render a HTML form
    		 */
    		$form_candidate_data = $form_candidate_class->getByUserId($user_details->getId());
    		$form_candidate_content = View::make('partials.background-checks.form-candidate-data', [
                'form_candidate_data' => $form_candidate_data
            ])->render();

    		/**
    		 * Document Checklist Form Uploads
    		 * Render a HTML form
    		 */
            $document_checlist_form_upload_data = $document_checklist_form_upload_class->getByUserId($user_details->getId());
            $document_checlist_form_upload_content = View::make('partials.background-checks.document-checklist-form-uploads', [
                'document_checlist_form_upload_data' => $document_checlist_form_upload_data,
                'document_form_upload_statuses' => $document_form_upload_statuses
            ])->render();

            /**
    		 * Document Checklist Uploads
    		 * Render a HTML form
    		 */
            $document_checlist_upload_data = $document_checklist_upload_class->getByUserId($user_details->getId());
            $document_checlist_upload_content = View::make('partials.background-checks.document-checklist-uploads', [
                'document_checlist_upload_data' => $document_checlist_upload_data
            ])->render();

            /**
             * Highright Crew Requests
             * Render a HTML form
             */
            $hireright_crew_request_data = $hireright_crew_request_class->getByUserId($user_details->getId());
            $hireright_crew_request_content = View::make('partials.background-checks.highright-crew-requests', [
                'hireright_crew_request_data' => $hireright_crew_request_data
            ])->render();

            /**
             * Job Applications
             * Render a HTML form
             */
            $job_application_data = $job_application_class->getByUserId($user_details->getId());
            $job_application_content = View::make('partials.background-checks.job-applications', [
                'job_application_data' => $job_application_data,
                'hireright_statuses' => $hireright_statuses
            ])->render();

            /**
             * Posible Queries
             * Render a HTML form
             */
            $posible_queries = View::make('partials.background-checks.posible-queries', [
                'user_id' => $user_details->getId(),
                'form_candidate_data' => $form_candidate_data,
                'document_checlist_form_upload_data' => $document_checlist_form_upload_data,
                'document_checlist_upload_data' => $document_checlist_upload_data,
                'hireright_crew_request_data' => $hireright_crew_request_data,
                'job_application_data' => $job_application_data
            ])->render();

            /**
             * User Details
             * Render a HTML form
             */
            $user_details_content = View::make('partials.background-checks.user-details', [
                'user_details' => $user_details
            ])->render();

    		return response()->json([
    			'status' => true,
    			'user_details_content' => $user_details_content,
    			'form_candidate_content' => $form_candidate_content,
    			'document_checlist_form_upload_content' => $document_checlist_form_upload_content,
    			'document_checlist_upload_content' => $document_checlist_upload_content,
                'hireright_crew_request_content' => $hireright_crew_request_content,
                'job_application_content' => $job_application_content,
                'posible_queries' => $posible_queries
    		]);
    	}
    	
    	return response()->json([
    		'status' => false
    	]);
    }

    public function getHirerightJobPackageByJobIntId (
        Request $request,
        HirerightPackage $hireright_package_class, 
        HirerightJobintPackage $hireright_jobint_package_class
    )
    {
        $job_int_id = $request->job_int_id;

        if (!is_null($job_int_id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $hireright_package_class->setDBConnection($db_connection);
            $hireright_jobint_package_class->setDBConnection($db_connection);

            $hireright_packages = $hireright_package_class->getAll();
            $job_int_packages = $hireright_jobint_package_class->getByJobIntId($job_int_id);

            if (!$job_int_packages->isEmpty())
            {
                /**
                 * Job Packages
                 * Render a HTML form
                 */
                $content = View::make('partials.background-checks.hireright-job-int-package', [
                    'job_int_packages' => $job_int_packages,
                    'hireright_packages' => $hireright_packages
                ])->render();

                return response()->json([
                    'status' => true,
                    'content' => $content
                ]);
            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * Update Document Checklist Form Upload Status
     * @return boolean In json format use for AJAX
     */
    public function updateDocumentChecklistFormUploadsStatus(
        Request $request, 
        DocumentChecklistFormUpload $document_checklist_form_upload_class,
        CtracSupportHistory $support_history_class
    )
    {
        $document_checklist_form_upload_id = $request->document_checklist_form_upload_id;
        $status_key = $request->status_key;
        $date_modified = $request->date_modified;

        if (!is_null($document_checklist_form_upload_id) && !is_null($status_key) && !is_null($date_modified))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $document_checklist_form_upload_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);
            $document_checklist_form_upload_details = $document_checklist_form_upload_class->getById($document_checklist_form_upload_id);

            if (!is_null($document_checklist_form_upload_details))
            {
                $data = [
                    'id' => $document_checklist_form_upload_id,
                    'is_completed' => $status_key,
                    'date_modified' => DateUtility::changeDateFormat($date_modified, 'F d, Y H:i A', 'Y-m-d H:i:s')
                ];

                $result = $document_checklist_form_upload_class->saveChanges($data, true);

                if ($result)
                {
                    /**
                     * Insert Support History Record
                     */
                    $support_history_data = [
                        'user_id' => $document_checklist_form_upload_details->getUserId(),
                        'ctrac_support_history_type_id' => 4,
                        'note' => 'Background Check: Set Document as Completed',
                        'ctrac_user_email' => Auth::user()->getEmail(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->getId()
                    ];

                    $support_history_class->saveSupportHistory($support_history_data);

                    return response()->json([
                        'status' => true
                    ]);
                }
            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * Save / Insert Document Checklist Form Upload
     * @return boolean In json format use for AJAX
     */
    public function saveDocumentChecklistFormUploads(
        Request $request, 
        DocumentChecklistFormUpload $document_checklist_form_upload_class,
        CtracSupportHistory $support_history_class
    )
    {
        $user_id = $request->user_id;

        if (!is_null($user_id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $document_checklist_form_upload_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);

            $data = [
                'id' => '',
                'user_id' => $user_id,
                'ck_list_id' => 124,
                'is_completed' => 1,
                'document_child_status_id' => 0,
                'doc_prev_status' => 0,
                'date_created' => date('Y-m-d H:i:s'),
                'date_modified' => date('Y-m-d H:i:s'),
                'created_by' => '',
                'modified_by' => '',
                'job_application_id' => 661361,
                'reviewed_ind' => 0,
                'triton_ind' => 0,
                'locked_agreement_ind' => 0
            ];

            $result = $document_checklist_form_upload_class->saveNew($data, true);

            if ($result)
            {
                /**
                 * Insert Support History Record
                 */
                $support_history_data = [
                    'user_id' => $user_id,
                    'ctrac_support_history_type_id' => 4,
                    'note' => 'Background Check: Insert Document Checklist Form Uploads (Header)',
                    'ctrac_user_email' => Auth::user()->getEmail(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Auth::user()->getId()
                ];

                $support_history_class->saveSupportHistory($support_history_data);

                return response()->json([
                    'status' => true
                ]);
            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * Save / Insert Document Checklist Uploads
     * @return boolean In json format use for AJAX
     */
    public function saveDocumentChecklistUploads(
        Request $request, 
        DocumentChecklistUpload $document_checklist_upload_class,
        CtracSupportHistory $support_history_class
    )
    {
        $user_id = $request->user_id;

        if (!is_null($user_id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $document_checklist_upload_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);

            $data = [
                'user_id' => $user_id,
                'ck_list_id' => 124,
                'filename' => 'hireright_consent',
                'status' => 'submitted',
                'last_updated_by' => 999999999999,
                'last_updated_on' => date('Y-m-d H:i:s'),
                'created_on' => date('Y-m-d H:i:s'),
                'issued_date' => date('Y-m-d'),
                'expiration_date' => date('Y-m-d'),
                'medical_facility_id' => 0,
                'training_type_id' => 0,
                'training_facility' => 0,
                'locked_ind' => 0,
                'newly_added' => 0,
                'newly_updated' => 0,
                'e1_migration' => 0,
                'e1_date_updated',
                'is_primary' => 0,
                'triton_ind' => 0,
                'document_status_id' => 1
            ];

            $result = $document_checklist_upload_class->saveNew($data, true);

            if ($result)
            {
                /**
                 * Insert Support History Record
                 */
                $support_history_data = [
                    'user_id' => $user_id,
                    'ctrac_support_history_type_id' => 4,
                    'note' => 'Background Check: Insert Document Checklist Uploads',
                    'ctrac_user_email' => Auth::user()->getEmail(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Auth::user()->getId()
                ];

                $support_history_class->saveSupportHistory($support_history_data);

                return response()->json([
                    'status' => true
                ]);
            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * Update Created On Document Checklist Uploads
     * @return boolean In json format use for AJAX
     */
    public function updateCreatedOnDocumentChecklistUploads(
        Request $request, 
        DocumentChecklistUpload $document_checklist_upload_class,
        CtracSupportHistory $support_history_class
    )
    {
        $id = $request->id;

        if (!is_null($id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $document_checklist_upload_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);
            $document_checklist_upload_details = $document_checklist_upload_class->getById($id);
            if (!is_null($document_checklist_upload_details))
            {
                $data = [
                    'id' => $id,
                    'created_on' => date('Y-m-d H:i:s')
                ];

                $result = $document_checklist_upload_class->saveChanges($data, true);

                if ($result)
                {
                    /**
                     * Insert Support History Record
                     */
                    $support_history_data = [
                        'user_id' => $document_checklist_upload_details->getUserId(),
                        'ctrac_support_history_type_id' => 4,
                        'note' => 'Background Check: Updated Created On Date of the Document Checklist Uploads',
                        'ctrac_user_email' => Auth::user()->getEmail(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->getId()
                    ];

                    $support_history_class->saveSupportHistory($support_history_data);

                    return response()->json([
                        'status' => true
                    ]);
                }
            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * Update Created On Document Checklist Uploads
     * @return boolean In json format use for AJAX
     */
    public function updateFormCandidateData(
        Request $request, 
        FormCandidateData $form_candidate_class,
        CtracSupportHistory $support_history_class
    )
    {
        $user_id = $request->user_id;

        if (!is_null($user_id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $form_candidate_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);
            $form_candidate_details = $form_candidate_class->getByUserId($user_id);

            if (!is_null($form_candidate_details))
            {
                $data = [
                    'id' => $form_candidate_details->getId(),
                    'hireright_submit_ind' => 1, 
                    'hireright_data_constructed_ind' => 0, 
                    'hireright_submit_executed_ind' => 0, 
                    'hireright_submit_attempt_ind' => 0
                ];

                $result = $form_candidate_class->saveChanges($data, true);

                if ($result)
                {
                    /**
                     * Insert Support History Record
                     */
                    $support_history_data = [
                        'user_id' => $form_candidate_details->getUserId(),
                        'ctrac_support_history_type_id' => 4,
                        'note' => 'Background Check: Manually Trigger Background Check to   (1 0 0 0)',
                        'ctrac_user_email' => Auth::user()->getEmail(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->getId()
                    ];


                    $support_history_class->saveSupportHistory($support_history_data);

                    return response()->json([
                        'status' => true
                    ]);
                }
            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    /**
     * Update Form Candidate Data to 1 0 0 0
     * For the BGC Team to manually Trigger it on HireRight 
     * To stop C-TRAC from executing the BGC of the Applicant
     * @return boolean In json format use for AJAX
     */
    public function updateFormCandidateDataManualTrigger(
        Request $request, 
        FormCandidateData $form_candidate_class,
        CtracSupportHistory $support_history_class
    )
    {
        $form_candidate_id = $request->form_candidate_id;

        if (!is_null($form_candidate_id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $form_candidate_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);
            $form_candidate_details = $form_candidate_class->getById($form_candidate_id);

            if (!is_null($form_candidate_details))
            {
                $data = [
                    'id' => $form_candidate_details->getId(),
                    'hireright_submit_ind' => 1, 
                    'hireright_data_constructed_ind' => 1, 
                    'hireright_submit_executed_ind' => 1, 
                    'hireright_submit_attempt_ind' => 1
                ];

                $result = $form_candidate_class->saveChanges($data, true);

                if ($result)
                {
                    /**
                     * Insert Support History Record
                     */
                    $support_history_data = [
                        'user_id' => $form_candidate_details->getUserId(),
                        'ctrac_support_history_type_id' => 4,
                        'note' => 'Background Check: Manually Trigger Background Check to   (1 1 1 1)',
                        'ctrac_user_email' => Auth::user()->getEmail(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->getId()
                    ];


                    $support_history_class->saveSupportHistory($support_history_data);

                    return response()->json([
                        'status' => true
                    ]);
                }
            }
        }

        return response()->json([
            'status' => false
        ]);
    }
}
