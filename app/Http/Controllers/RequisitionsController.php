<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\StringUtility;
use CTRAC\Helpers\DateUtility;
use Auth;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\Requisitions\PeoplesoftRequisitionIdList;
use CTRAC\Model\Requisitions\PeoplesoftHiringManager;
use CTRAC\Model\Requisitions\PeoplesoftDepartmentList;
use CTRAC\Model\Requisitions\PeoplesoftJobCodeList;
use CTRAC\Model\Requisitions\PeoplesoftPosition;
use CTRAC\Model\Jobs\JobRequest;
use CTRAC\Model\Jobs\JobRequestFlsaStatus;
use CTRAC\Model\Jobs\JobInt;
use CTRAC\Model\Scripts\CtracUser;
use CTRAC\Model\Scripts\CtracRole;

class RequisitionsController extends Controller
{
	/**
	 * @return void
	 */
	protected $db_connection; 

	public function __construct()
	{
	    /**
	     * Set DB Connection to Production
	     * CTRAC_PROD -> Prod DB
	     * CTRAC_QA -> QA DB
	     */
	    $this->db_connection = DatabaseConnection::CTRAC_QA;
	    if (config('app.env') != 'local')
	    {
	        $this->db_connection = DatabaseConnection::CTRAC_PROD;
	    }
	}

	public function index ()
	{
		return view('requisitions.view');
	}

	public function uploadReq (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			foreach ($req_data as $row) 
			{

				$datum = [];
				$requisition_class = new PeoplesoftRequisitionIdList();
				$requisition_class->setDBConnection($this->db_connection);
				$requisition_detail = $requisition_class->getById($row[0]);

				if (is_null($requisition_detail))
				{
					$created = (!is_null(DateUtility::changeDateFormat($row[4]))) ? DateUtility::changeDateFormat($row[4]) : '0000-00-00';
					$stat_date = (!is_null(DateUtility::changeDateFormat($row[6]))) ? DateUtility::changeDateFormat($row[6]) : '0000-00-00';
					$datum = [
						'job_opening' => $row[0],
						'job_code' => $row[1],
						'position' => $row[2],
						'created' => $created,
						'status' => $row[5],
						'stat_dt' => $stat_date,
						'openings' => $row[7],
						'target_openings' => $row[8],
						'unit' => $row[10],
						'dept_id' => trim($row[11], '0'),
						'co' => $row[13],
						'location' => $row[14],
						'posting_title' => $row[15],
						'justification' => $row[16],
						'req_sent' => $row[17],
						'comments' => $row[18],
						'rclform' => $row[19],
						'rclhrform' => $row[20],
						'int_ext' => $row[21],
						'req_type' => $row[22],
						'job_type' => $row[23],
						'id_replacement' => $row[24],
						'name' => $row[25],
						'mgrid' => $row[26],
						'name_2' => $row[27],
						'recruiter' => $row[28],
						'name3' => $row[29],
						'status2' => $row[30],
						'descr' => $row[31],
						'country' => $row[32]
					];

					if ($requisition_class->saveNew($datum))
					{
						$total_added++;
						$history_logs[] = [
							'success' => true,
							'job_opening' => $row[0],
							'job_code' => $row[1]
						];
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added new Requisition! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);

			return redirect()->back();
		}

		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No new requisition!');

		return redirect()->back();
	}

	public function viewUploadHiringManagers ()
	{
		return view('requisitions.view-hiring-managers');
	}

	public function uploadHiringManagers (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-hiring-managers-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			foreach ($req_data as $row) 
			{

				$datum = [];
				$requisition_class = new PeoplesoftHiringManager();
				$requisition_class->setDBConnection($this->db_connection);
				$requisition_detail = $requisition_class->getByManagerId($row[0]);

				if (is_null($requisition_detail))
				{
					if ($row[4] == 'USA')
					{
						$datum = [
							'peoplesoft_mgr_id' => $row[0],
							'peoplesoft_role_id' => 1,
							'hiring_manager_name' => $row[3],
							'country_code' => 'US',
							'pay_status' => 1
						];

						if ($requisition_class->saveNew($datum))
						{
							$total_added++;
							$history_logs[] = [
								'success' => true,
								'manager_id' => $row[0],
								'manager_name' => $row[3]
							];
						}
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added new Hiring Manager Requisition! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);

			return redirect()->back();
		}

		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No new hiring manager requisition!');

		return redirect()->back();
	}

	public function viewUploadDepartmentList ()
	{
		return view('requisitions.view-department-list');
	}

	public function uploadDepartmentList (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-department-list-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			foreach ($req_data as $row) 
			{

				$datum = [];
				$requisition_class = new PeoplesoftDepartmentList();
				$requisition_class->setDBConnection($this->db_connection);
				$requisition_detail = $requisition_class->getByDeptId(trim($row[1], '0'));

				if (is_null($requisition_detail))
				{
					$eff_date = (!is_null(DateUtility::changeDateFormat($row[2]))) ? DateUtility::changeDateFormat($row[2]) : '0000-00-00';
					$company_code = $row[5];
					if ($company_code == 'ACC')
					{
						$company_code = 'AZA';
					}
					if ($company_code == 'CCI')
					{
						$company_code = 'CEL';
					}

					$datum = [
						'set_id' => $row[0],
						'dept_id' => trim($row[1], '0'),
						'eff_date' => $eff_date,
						'status' => 1,
						'description' => $row[4],
						'company_code' => $company_code,
						'location' => $row[6]
					];

					if ($requisition_class->saveNew($datum))
					{
						$total_added++;
						$history_logs[] = [
							'success' => true,
							'dept_id' => trim($row[1], '0'),
							'desc' => $row[4],
							'company_code' => $company_code
						];
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added new Department List Requisition! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);

			return redirect()->back();
		}

		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No new department list requisition!');

		return redirect()->back();
	}

	public function viewUploadJobCodeList ()
	{
		return view('requisitions.view-job-code-list');
	}

	public function uploadJobCodeList (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-job-code-list-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			foreach ($req_data as $row) 
			{

				$datum = [];
				$requisition_class = new PeoplesoftJobCodeList();
				$requisition_class->setDBConnection($this->db_connection);
				$requisition_detail = $requisition_class->getByJobCode($row[0]);

				if (is_null($requisition_detail))
				{
					$datum = [
						'job_code' => $row[0],
						'job_description' => $row[1]
					];

					if ($requisition_class->saveNew($datum))
					{
						$total_added++;
						$history_logs[] = [
							'success' => true,
							'job_code' => $row[0],
							'job_description' => $row[1]
						];
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added new Job Code Requisition! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);

			return redirect()->back();
		}

		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No new job code requisition!');

		return redirect()->back();
	}

	public function viewUploadPositions ()
	{
		return view('requisitions.view-positions');
	}

	public function uploadPositions (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-positions-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			foreach ($req_data as $row) 
			{

				$datum = [];
				$requisition_class = new PeoplesoftPosition();
				$requisition_class->setDBConnection($this->db_connection);
				$requisition_detail = $requisition_class->getByPositionCode(trim($row[2], '0'));

				if (is_null($requisition_detail))
				{
					$eff_date = (!is_null(DateUtility::changeDateFormat($row[3]))) ? DateUtility::changeDateFormat($row[3]) : '0000-00-00';
					$company_code = $row[1];
					if ($company_code == 'ACC')
					{
						$company_code = 'AZA';
					}
					if ($company_code == 'CCI')
					{
						$company_code = 'CEL';
					}

					$datum = [
						'status' => 1,
						'company_code' => $company_code,
						'eff_date' => $eff_date,
						'position_code' => trim($row[2], '0'),
						'description' => $row[4],
						'short_desc' => $row[5],
						'unit' => $row[6],
						'dept_id' => trim($row[7], '0'),
						'job_code' => $row[8],
						'reports_to' => trim($row[9], '0'),
						'location' => $row[10]
					];

					if ($requisition_class->saveNew($datum))
					{
						$total_added++;
						$history_logs[] = [
							'success' => true,
							'position_code' => trim($row[2], '0'),
							'description' => $row[4]
						];
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added new Position Requisition! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);

			return redirect()->back();
		}

		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No new position requisition!');

		return redirect()->back();
	}

	public function viewUploadDailyRequisitions ()
	{
		return view('requisitions.view-daily');
	}

	public function uploadDailyReq (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$job_int_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-daily-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionDailyCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			foreach ($req_data as $row) 
			{
				/**
				 * Check if Requisition ID List is Available
				 */
				if (isset($row[1]) && !empty($row[1]))
				{
					$requisition_id_list_class = new PeoplesoftRequisitionIdList();
					$requisition_id_list_class->setDBConnection($this->db_connection);
					$requisition_id_list_detail = $requisition_id_list_class->getById($row[1]);

					if (is_null($requisition_id_list_detail))
					{
						$history_logs[] = [
							'success' => false,
							'requisition_id' => $row[1],
							'message_log' => 'Requisition ID ' . $row[1] . ' is missing, please upload the updated CTRAC_REQS_HOLD_OPEN <a href="' . route('requisitions_upload_id_list') . '" target="_blank" rel="noopener noreferrer">here</a>, then try again.'
						];

						continue;
					}
				}
				else
				{
					$history_logs[] = [
						'success' => false,
						'requisition_id' => 'None',
						'message_log' => 'Missing requisition id on CSV file provided, please upload a correct CSV file'
					];
					continue;
				}

				/**
				 * Check if Hiring Manager Requisition ID is Available
				 */
				if (isset($row[2]) && !empty($row[2]))
				{
					$requisition_hiring_manager_class = new PeoplesoftHiringManager();
					$requisition_hiring_manager_class->setDBConnection($this->db_connection);
					$requisition_hiring_manager_detail = $requisition_hiring_manager_class->getByManagerId($row[2]);

					if (is_null($requisition_hiring_manager_detail))
					{
						$history_logs[] = [
							'success' => false,
							'requisition_id' => $row[1],
							'message_log' => 'Requisition Hiring Manager ID ' . $row[2] . ' is missing, please upload the updated RCL_RL_ACTIVES_HIRING_MGR_US <a href="' . route('requisitions_upload_hiring_managers') . '" target="_blank" rel="noopener noreferrer">here</a>, then try again.'
						];

						continue;
					}
				}
				else
				{
					$history_logs[] = [
						'success' => false,
						'requisition_id' => $row[1],
						'message_log' => 'Missing hiring manager requisition id on CSV file provided, please upload a correct CSV file'
					];
					continue;
				}

				/**
				 * Check if Department Requisition ID is Available
				 */
				if (isset($row[28]) && !empty($row[28]))
				{
					$requisition_dept_class = new PeoplesoftDepartmentList();
					$requisition_dept_class->setDBConnection($this->db_connection);
					$requisition_dept_detail = $requisition_dept_class->getByDeptId(trim($row[28], '0'));

					if (is_null($requisition_dept_detail))
					{
						$history_logs[] = [
							'success' => false,
							'requisition_id' => $row[1],
							'message_log' => 'Requisition Department ID ' . trim($row[28], '0') . ' is missing, please upload the updated RCL_RL_ACTIVES_HIRING_MGR_US <a href="' . route('requisitions_upload_department_list') . '" target="_blank" rel="noopener noreferrer">here</a>, then try again.'
						];

						continue;
					}
				}
				else
				{
					$history_logs[] = [
						'success' => false,
						'requisition_id' => $row[1],
						'message_log' => 'Missing department requisition id on CSV file provided, please upload a correct CSV file'
					];
					continue;
				}

				/**
				 * Check if Position Requisition ID is Available
				 */
				if (isset($row[44]) && !empty($row[44]))
				{
					$requisition_position_class = new PeoplesoftPosition();
					$requisition_position_class->setDBConnection($this->db_connection);
					$requisition_position_detail = $requisition_position_class->getByPositionCode(trim($row[44], '0'));

					if (is_null($requisition_position_detail))
					{
						$history_logs[] = [
							'success' => false,
							'requisition_id' => $row[1],
							'message_log' => 'Requisition Position Code ' . trim($row[44], '0') . ' is missing, please upload the updated RCL_ACTIVE_POSITIONS_US_RL <a href="' . route('requisitions_upload_positions') . '" target="_blank" rel="noopener noreferrer">here</a>, then try again.'
						];

						continue;
					}
				}
				else
				{
					$history_logs[] = [
						'success' => false,
						'requisition_id' => $row[1],
						'message_log' => 'Missing position requisition id on CSV file provided, please upload a correct CSV file'
					];
					continue;
				}

				/**
				 * Check if Job Code Requisition ID is Available
				 */
				if (isset($row[36]) && !empty($row[36]))
				{
					$requisition_job_code_class = new PeoplesoftJobCodeList();
					$requisition_job_code_class->setDBConnection($this->db_connection);
					$requisition_job_code_detail = $requisition_job_code_class->getByJobCode($row[36]);

					if (is_null($requisition_job_code_detail))
					{
						$history_logs[] = [
							'success' => false,
							'requisition_id' => $row[1],
							'message_log' => 'Requisition Job Code ' . $row[36] . ' is missing, please upload the updated CTRAC_NIC_JOBCODE_US <a href="' . route('requisitions_upload_job_code_list') . '" target="_blank" rel="noopener noreferrer">here</a>, then try again.'
						];

						continue;
					}
					else
					{
						$job_int_class = new JobInt();
						$job_int_class->setDBConnection($this->db_connection);
						$job_int_detail = $job_int_class->getByJobIntCodeAndJobIntStatus($row[36], 0);
						$job_int_datum = [];
						if (is_null($job_int_detail))
						{
							$job_int_datum = [
								'job_int_code' => $requisition_job_code_detail->getJobCode(),
								'job_int_name' => $requisition_job_code_detail->getJobDescription(),
								'job_int_desc' => $requisition_job_code_detail->getJobDescription(),
								'job_int_max' => 0,
								'job_int_min_range' => 0,
								'job_int_max_range' => 100,
								'job_request_id' => 0,
								'job_int_status' => 0,
								'job_int_brand' => 'RCI',
								'pipeline_id' => NULL,
								'pipeline_code' => NULL,
								'allow_shorten' => 1,
								'career_id' => 2
							];
							if ($job_int_class->saveNew($job_int_datum))
							{
								$job_int_logs[] = [
									'success' => true,
									'job_code' => $requisition_job_code_detail->getJobCode(),
									'job_desc' => $requisition_job_code_detail->getJobDescription()
								];
							}
						}
					}
				}
				else
				{
					$history_logs[] = [
						'success' => false,
						'requisition_id' => $row[1],
						'message_log' => 'Missing position requisition id on CSV file provided, please upload a correct CSV file'
					];
					continue;
				}

				$datum = [];
				$job_request_class = new JobRequest();
				$job_request_class->setDBConnection($this->db_connection);
				$job_request_detail = $job_request_class->getByPeoplesoftRequisitionId($row[1]);

				if (is_null($job_request_detail))
				{
					$brand_id = 'RCI';
					$user_id = 13000000002;

					$job_request_status_class = new JobRequestFlsaStatus();
					$job_request_status_class->setDBConnection($this->db_connection);
					$job_request_status = $job_request_status_class->getByName($row[40]);

					$datum = [
						'job_group_code' => trim($row[28], '0'),
						'brand_id' => $brand_id,
						'user_id' => $user_id,
						'job_request_title' => $row[22],
						'job_request_description' => addslashes($row[24]),
						'job_specific_questions_timer' => '3',
						'job_specific_questions_timer_msr' => 'm',
						'request_create_date' => date('Y-m-d H:i:s'),
						'request_start_date' => date('Y-m-d H:i:s'),
						'request_end_date' => '2020-12-31',
						'max_allowed' => $row[14],
						'peoplesoft_requisition_id' => $row[1],
						'peoplesoft_position_id' => trim($row[44], '0'),
						'hiring_manager_id' => $row[2],
						'created_by' => $user_id,
						'required_views' => 0,
						'request_status' => 3,
						'wizard_views_data' => 'personal_data,address,phone,education_summary,work_experience,references,language,skills,questions',
						'flsa_status_id' => (!is_null($job_request_status)) ? $job_request_status->getId() : 0
					];

					if ($job_request_class->saveNew($datum))
					{
						$total_added++;
						$history_logs[] = [
							'success' => true,
							'requisition_id' => $row[1],
							'message_log' => 'Daily Requisition ' . $row[1] . ' was successfully added!'
						];
					}
				}
				else
				{
					$history_logs[] = [
						'success' => true,
						'requisition_id' => $row[1],
						'message_log' => 'Daily Requisition ' . $row[1] . ' already exist!'
					];
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added new Daily Requisition! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);
			if (!empty($job_int_logs))
			{
				session()->flash('job_int_logs', $job_int_logs);
			}

			/**
			 * Send Email Notification
			 */
			$user_uploader = Auth::user();
			$role = new CtracRole;
			$role_detail = $role->getByName('HRIS Requisition Admin');
			
			if (!$role_detail->users->isEmpty())
			{
				foreach ($role_detail->users as $user) 
				{
					if (config('app.env') != 'local')
					{
						$user->emailRequisitionResults(
							$user_uploader, 
							[
								'history_logs' => $history_logs, 
								'job_int_logs' => $job_int_logs
							]
						);
					}		
				}
			}

			return redirect()->back();
		}

		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No new daily requisition!');

		return redirect()->back();
	}

	public function viewUploadClosedReq ()
	{
		return view('requisitions.view-closed');
	}

	public function uploadClosedReq (Request $request)
	{
		$req_data = [];
		$history_logs = [];
		$total_added = 0;

		$request->validate([
			'upload_req_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-closed-requisition-uploads' . '.csv';
		$request->upload_req_csv->storeAs('Requisitions', $file_name);

		if ($request->hasFile('upload_req_csv')) 
		{
			$file_path = storage_path('app/Requisitions/' . $file_name);
			$csv_data = StringUtility::requisitionCsvToArray($file_path);

			if (!empty($csv_data))
			{
				$req_data = $csv_data;
			}
		}

		if (!empty($req_data))
		{
			$requisition_status = [
				'closed',
				'external',
				'private',
				'pending'
			];

			foreach ($req_data as $row) 
			{

				$datum = [];
				$job_request_class = new JobRequest();
				$job_request_class->setDBConnection($this->db_connection);
				$job_request_detail = $job_request_class->getByPeoplesoftRequisitionId($row[0]);

				if (!is_null($job_request_detail))
				{
					$closed_status = 0;
					if ($job_request_detail->getRequestStatus() != 0)
					{
						$old_status = $job_request_detail->getRequestStatus();
						$admin_user_id = 13000000002;
						$datum = [
							'job_request_id' => $job_request_detail->getId(),
							'request_status' => $closed_status,
							'request_end_date' => date('Y-m-d H:i:s'),
							'last_updated_on' => date('Y-m-d H:i:s'),
							'last_updated_by' => $admin_user_id
						];

						if ($job_request_class->saveChanges($datum))
						{
							$total_added++;
							$history_logs[] = [
								'success' => true,
								'requisition_id' => $row[0],
								'old_status' => $requisition_status[$old_status],
								'new_status' => $requisition_status[$closed_status],
							];
						}
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully updated Requisition to closed! Total: ' . $total_added);
			session()->flash('history_logs', $history_logs);

			return redirect()->back();
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'Nothing to closed requisition!');

		return redirect()->back();
	}
}
