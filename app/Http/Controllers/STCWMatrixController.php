<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Excel;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Helpers\StringUtility;
use CTRAC\Helpers\DateUtility;
use CTRAC\Model\Trainings\DocumentTrainingCertificateType;
use CTRAC\Model\Trainings\DocumentTrainingCertificateJobCode;
use CTRAC\Model\Jobs\JobInt;
use CTRAC\Exports\STCWMatrixSuccessResultLogs;
use CTRAC\Exports\STCWMatrixDeletedJobLogs;
use CTRAC\Exports\STCWMatrixInsertedJobLogs;

class STCWMatrixController extends Controller
{
	public function view()
	{
		return view('stcw-matrix.view');
	}

	public function uploadMatrix (Request $request)
	{
		set_time_limit(0);
		$stcw_data = [];
		$history_logs = [];
		$total_updates = 0;
		$admin_id = 99999999999;
		$total_deleted = 0;

		$request->validate([
			'upload_stcw_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-stcw-updates' . '.csv';
		$request->upload_stcw_csv->storeAs('STCW', $file_name);

		if ($request->hasFile('upload_stcw_csv')) 
		{
			$file_path = storage_path('app/STCW/' . $file_name);
			$csv_data = StringUtility::csvToArray($file_path);

			if (!empty($csv_data))
			{
				$stcw_data = $csv_data;
			}
		}

		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local') 
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}
		
		if ($request->update_by == 'certificates')
		{
			if (!empty($stcw_data))
			{

				$active_certificates = [];

				foreach ($stcw_data as $row) 
				{

					$datum = [];
					$stcw_class = new DocumentTrainingCertificateType();
					$stcw_class->setDBConnection($db_connection);
					$stcw_detail = $stcw_class->getByCertificateCode($row['cert_type_code']);

					/**
					 * Update STCW Certificate
					 */
					if (!is_null($stcw_detail))
					{

						$stcw_status = 'UPDATED';
						$datum = [
							'cert_type_code' => $row['cert_type_code'],
							'cert_type_name' => $row['cert_type_name'],
							'cert_order' => $row['cert_order'],
							'use_country_ind' => $row['use_country_ind'],
							'use_jobcode_ind' => $row['use_jobcode_ind'],
							'bypass_validation_ind' => $row['bypass_validation_ind'],
							'show_ind' => $row['show_ind'],
							'last_updated_on' => date('Y-m-d H:i:s'),
							'last_updated_by' => $admin_id,
							'use_job_code_ind' => $row['use_job_code_ind']
						];

						if($stcw_detail->getName() == $row['cert_type_name'] && $stcw_detail->getOrder() == $row['cert_order'] && $stcw_detail->getShowInd() == $row['show_ind'] && $stcw_detail->getCountryInd() == $row['use_country_ind'] && $stcw_detail->getJobCodeInd() == $row['use_jobcode_ind'] && $stcw_detail->getBypassValidationInd() == $row['bypass_validation_ind'] )
						{
							$stcw_status = 'No Update';
							$history_logs[] = [
								'success' => false,
								'cert_type_id' => $stcw_detail->getId(),
								'cert_type_code' => $stcw_detail->getCode(),
								'cert_type_name' => $stcw_detail->getName(),
								'show_ind' => $stcw_detail->getShowInd(),
								'stcw_status' => $stcw_status
							];
						}

						else
						{
							$total_updates++;

							if($stcw_detail->saveCertificate($datum))
							{
								$history_logs[] = [
									'success' => true,
									'cert_type_id' => $stcw_detail->getId(),
									'cert_type_code' => $stcw_detail->getCode(),
									'cert_type_name' => $stcw_detail->getName(),
									'show_ind' => $stcw_detail->getShowInd(),
									'stcw_status' => $stcw_status
								];
							}

							else 
							{
								$stcw_status = 'No Update';
								$history_logs[] = [
									'success' => false,
									'cert_type_id' => $stcw_detail->getId(),
									'cert_type_code' => $stcw_detail->getCode(),
									'cert_type_name' => $stcw_detail->getName(),
									'show_ind' => $stcw_detail->getShowInd(),
									'stcw_status' => $stcw_status
								];
							}

						}

					}
					else
					{
						/**
						 * Insert new STCW Matrix 
						 * If Data not available in the table
						 */
						$datum = [
							'cert_type_code' => $row['cert_type_code'],
							'cert_type_name' => $row['cert_type_name'],
							'cert_order' => $row['cert_order'],
							'use_country_ind' => $row['use_country_ind'],
							'use_jobcode_ind' => $row['use_jobcode_ind'],
							'bypass_validation_ind' => $row['bypass_validation_ind'],
							'show_ind' => $row['show_ind'],
							'created_on' => date('Y-m-d H:i:s'),
							'created_by' => $admin_id,
							'use_job_code_ind' => $row['use_job_code_ind']
						];

						$new_stcw_class = new DocumentTrainingCertificateType();
						$new_stcw_class->setDBConnection($db_connection);

						$total_updates++;
						$stcw_status = 'INSERTED';

						if($new_stcw_class->saveCertificate($datum))
						{
							$history_logs[] = [
								'success' => true,
								'cert_type_id' => $new_stcw_class->getId(),
								'cert_type_code' => $new_stcw_class->getCode(),
								'cert_type_name' => $new_stcw_class->getName(),
								'show_ind' => $new_stcw_class->getShowInd(),
								'stcw_status' => $stcw_status
							];
							$stcw_status = 'No Update';
							
						}

						else
						{
							$stcw_status = 'No Update';
							$history_logs[] = [
								'success' => false,
								'cert_type_id' => $new_stcw_class->getId(),
								'cert_type_code' => $new_stcw_class->getCode(),
								'cert_type_name' => $new_stcw_class->getName(),
								'show_ind' => $new_stcw_class->getShowInd(),
								'stcw_status' => $stcw_status
							];
						}
					}
					array_push($active_certificates, $row['cert_type_code']);
				}

				$stcw_certificates = new DocumentTrainingCertificateType();
				$stcw_certificates->setDBConnection($db_connection);
				$stcw_certificates_details = $stcw_certificates->getAllCertificateTypeCodes();
				$inactive_certificates = array_diff($stcw_certificates_details , $active_certificates);

				foreach ($inactive_certificates as $row){
					$stcw_inactive = new DocumentTrainingCertificateType();
					$stcw_inactive->setDBConnection($db_connection);
					$stcw_inactive_details = $stcw_inactive->getByCertificateCode($row);
					$datum = [];
					$datum = [
						'cert_type_code' => $row,
						'show_ind' => 0,
						'last_updated_on' => date('Y-m-d H:i:s'),
						'last_updated_by' => $admin_id
					];
					$stcw_inactive_details->saveCertificate($datum);
				}

				session()->flash('success_flash', true);
				session()->flash('update_by', $request->update_by);
				session()->flash('message_flash', 'Successfully updated STCW Matrix! Total: ' . $total_updates);
				session()->flash('history_logs', $history_logs);
				session()->flash('total_updates', $total_updates); 
				session()->put('history_logs_download', $history_logs);

				return redirect()->back();
			}
		}

		if ($request->update_by == 'jobcode')
		{
			
			if (!empty($stcw_data))
			{
				/*
				* Get list of Certificates
				*/
				$insert =[];
				$insert_logs =[];
				$delete =[];

				foreach ($stcw_data as $key => $result) {

					/*
					 * Get List of all Job Int Id by Job Code
					*/
  
					$job_list = $this->getAllJobInt($result['job_code']);

					/*
					 * Get List of Certificates based on the CSV
					*/
					$cert_list = [];

					foreach ($result as $key => $values) {
						
						if($values == 1)
						{
							$cert_type_code = $this->getCertificate($key);

							if($cert_type_code != '' || !empty($cert_type_code) || !is_null($cert_type_code)){
									array_push($cert_list, $cert_type_code->cert_type_id);
							}
							 
						}
					}

					foreach ($job_list as $job_int_id) {
						
						/*
						 * Get List of Old Certficates based on Job Int Id
						*/
						$old_certificate = $this->getCertificateByJobIntId($job_int_id);

						/*
						 * Get List of all Certificates need to be inserted
						*/
						$new_certificate = array_diff($cert_list, $old_certificate);

						/*
						 * Get List of all Certificates need to be deleted
						*/
						$delete_certificate = array_diff($old_certificate, $cert_list);

						/*
						 * Insert new certificate based on Job Int Id
						*/
						foreach ($new_certificate as $new_cert_type_id)
						{
							$total_updates++;
							$stcw_status = 'Insert';
							$insert = [
								'cert_type_id' => $new_cert_type_id,
								'job_int_id' => $job_int_id,
								'created_on' => date('Y-m-d H:i:s'),
								'created_by' => $admin_id
							];

							$insert_stcw_class = new DocumentTrainingCertificateJobCode();
							$insert_stcw_class->setDBConnection($db_connection);

							if($insert_stcw_class->saveCertificate($insert))
							{
								$cert_code_class = new DocumentTrainingCertificateType();
								$cert_code_class->setDBConnection($db_connection);
								$cert_code = $cert_code_class->getByCertificateId($insert_stcw_class->getCertTypeId());

								$history_logs[] = [
									'success' => true,
									'id' => $insert_stcw_class->getId(),
									'job_int_id' => $insert_stcw_class->getJobIntId(),
									'cert_type_id' => $insert_stcw_class->getCertTypeId(),
									'created_on' => $insert_stcw_class->getCreatedOn(),
									'created_by' => $insert_stcw_class->getCreatedBy(),
									'job_code' => $result['job_code'],
									'cert_type_code' => $cert_code->getCode(),
									'stcw_status' => $stcw_status
								];
							}


						}

						/*
						 * Delete Certificate Job that was not on the package
						*/
						if($delete_certificate != '' || !empty($delete_certificate) || !is_null($delete_certificate)){

							foreach ($delete_certificate as $delete_cert_id)
							{
								$stcw_status = 'Delete';
								$delete_details_class = $this->getCertificateToDelete($job_int_id,$delete_cert_id);

								foreach ($delete_details_class as $delete_details) 
								{
									$cert_code_class = new DocumentTrainingCertificateType();
									$cert_code_class->setDBConnection($db_connection);
									$cert_code = $cert_code_class->getByCertificateId($delete_details->getCertTypeId());

									$total_deleted++;
									$delete[] = [
										'id' => $delete_details->getId(),
										'job_int_id' => $delete_details->getJobIntId(),
										'cert_type_id' => $delete_details->getCertTypeId(),
										'job_code' => $result['job_code'],
										'created_on' => $delete_details->getCreatedOn(),
										'created_by' => $delete_details->getCreatedBy(),
										'cert_type_code' => $cert_code->getCode(),
										'stcw_status' => $stcw_status

									];

									$delete_details->delete();

								}
								
							}
						}
						
					}
					
				}
			
			}

			session()->flash('success_flash', true);
			session()->flash('update_by', $request->update_by);
			session()->flash('message_flash', 'Successfully inserted STCW Matrix! Total: ' . $total_updates . ' Deleted STCW Matrix! Total: ' . $total_deleted);
			session()->flash('delete', $delete); 
			session()->put('delete_logs', $delete);
			session()->flash('history_logs', $history_logs);
			session()->put('history_logs_download', $history_logs);
			return redirect()->back();

		}
		session()->flash('success_flash', true);
		session()->flash('message_flash', 'No updates are made on STCW Matrix');

		return redirect()->back();
	}

	public function downloadSTCWMatrixSuccessResultLogs ()
	{
		$history_logs = [];
		if (session()->has('history_logs_download'))
		{
			$history_logs = session('history_logs_download');
			session()->forget('history_logs_download');
		}

		return Excel::download(
			new STCWMatrixSuccessResultLogs(
				$history_logs
			), 
			'stcw-matrix-success-result-logs-' . date('Y-m-d') . time() . '.xlsx'
		);
	}

	private function getAllJobInt ($job_code)
	{
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local') 
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$results = [];
		$result = new JobInt();
		$result->setDBConnection($db_connection);
		$result_detail = $result->getAllByJobIntCode($job_code);
		foreach ($result_detail as $key => $value) {
			array_push($results, $value->job_int_id);
		}

		return $results;
	}

	private function getCertificateByJobIntId ($job_int_id)
	{
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local') 
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$results = [];
		$result = new DocumentTrainingCertificateJobCode();
		$result->setDBConnection($db_connection);
		$result_detail = $result->getByJobIntId($job_int_id);
		foreach ($result_detail as $key => $value) {
			array_push($results, $value->cert_type_id);
		}

		return $results;
	}

	private function getCertificateToDelete ($job_int_id,$cert_type_id)
	{
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local') 
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}
		$results = [];
		$result = new DocumentTrainingCertificateJobCode();
		$result->setDBConnection($db_connection);
		$result_detail = $result->getByJobIntIdCertTypeId($job_int_id,$cert_type_id);
		foreach ($result_detail as $key => $value) {
			array_push($results, $value);
		}

		return $results;
	}

	private function getCertificate ($cert_type_code)
	{
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local') 
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}

		$result = new DocumentTrainingCertificateType();
		$result->setDBConnection($db_connection);
		$result_detail = $result->getByCertificateCode($cert_type_code);

		return $result_detail;
	}

	public function downloadSTCWMatrixDeletedJobLogs ()
	{
		$delete_cert_logs = [];
		if (session()->has('delete_logs'))
		{
			$delete_cert_logs = session('delete_logs');
			session()->forget('delete_logs');
		}

		return Excel::download(
			new STCWMatrixDeletedJobLogs(
				$delete_cert_logs
			), 
			'stcw-matrix-deleted-job-logs-' . date('Y-m-d') . time() . '.xlsx'
		);
	}

	public function downloadSTCWMatrixInsertedJobLogs ()
	{
		$history_logs = [];
		if (session()->has('history_logs_download'))
		{
			$history_logs = session('history_logs_download');
			session()->forget('history_logs_download');
		}

		return Excel::download(
			new STCWMatrixInsertedJobLogs(
				$history_logs
			), 
			'stcw-matrix-inserted-job-logs-' . date('Y-m-d') . time() . '.xlsx'
		);
	}
}