<?php

namespace CTRAC\Http\Controllers;

use Storage;
use View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use CTRAC\Model\Users\User;
use CTRAC\Model\Users\UserVenue;
use CTRAC\Helpers\DashboardLink;
use CTRAC\Helpers\StringUtility;
use CTRAC\Model\Venues\Venue;
use CTRAC\Helpers\DateUtility;
use CTRAC\Model\Statuses\StatusCode;
use CTRAC\Model\Brands\Brand;
use CTRAC\Model\Ships\Ship;
use CTRAC\Model\Jobs\JobGroup;
use CTRAC\Model\Jobs\JobInt;
use CTRAC\Model\Skills\SkillCombo;
use CTRAC\Model\Skills\SkillComboDetail;
use CTRAC\Model\Skills\SkillSetJoin;
use CTRAC\Model\Skills\SkillSet;
use CTRAC\Model\Skills\SkillLevel;
use CTRAC\Model\TravelMart\CrewCurrentStatus;
use CTRAC\Model\Trainings\TrainingCourse;
use CTRAC\Model\Trainings\TrainingCourseSkill;
use CTRAC\Model\Trainings\TrainingCourseJob;
use CTRAC\Model\Users\EmployeeTraining;

class VenueSkillsController extends Controller
{
	public function index (UserVenue $user_venue_class, User $user_class)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$total_user_venues = $user_venue_class->getAll()->count();

		return view('venue-skills.view', 
			compact(
				'active_page',
				'total_user_venues'
			)
		);
	}

	public function viewAddUserVenueSkills (Venue $venue_class)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$venues = $venue_class->getAll();

		return view('venue-skills.add-view', 
			compact(
				'active_page',
				'venues'
			)
		);
	}

	public function saveAddUserVenueSkills (Request $request)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$upload_type = $request->upload_type;
		$user_type_id = $request->user_type_id;
		$venue_id = $request->venue;
		$start_date = $request->start_date;
		$end_date = $request->end_date;
		$user_ids = $request->user_ids;
		$upload_user_csv = $request->upload_user_csv;
		$user_data = [];

		if ($upload_type == 'user_id_upload')
		{
			$request->validate([
				'user_ids' => 'required'
			]);

			$user_data_ids = explode(',', $user_ids);

			if (!empty($user_data_ids))
			{
				foreach ($user_data_ids as $id) 
				{
					$user_data[] = [
						'User Id' => $id,
						'Venue Id' => $venue_id,
						'Start Date' => $start_date,
						'End Date' => $end_date
					];
				}
			}
		}

		if ($upload_type == 'csv_id_upload')
		{
			$request->validate([
				'upload_user_csv' => 'required'
			]);

			$file_name = time() . date('Y-m-d') . '-venue-skills' . '.csv';
			$request->upload_user_csv->storeAs('VenueSkills', $file_name);

			if ($request->hasFile('upload_user_csv')) 
			{
				$file_path = storage_path('app/VenueSkills/' . $file_name);
				$csv_data = StringUtility::csvToArray($file_path);

				if (!empty($csv_data))
				{
					$user_data = $csv_data;
				}
			}
		}
		
		$user_venue_ids = [];

		if (!empty($user_data))
		{
			foreach ($user_data as $row) 
			{
				$user_class = new User();
				$user_venue_class = new UserVenue();

				if ($user_type_id == 'user_id')
				{
					$user_details = $user_class->getById($row['User Id']);
				}
				else
				{
					$user_details = $user_class->getByJDEId($row['User Id']);
				}
				
				if (!is_null($user_details))
				{
					$data = [
						'id' => '',
						'user_id' => $user_details->getId(),
						'venue_id' => $row['Venue Id'],
						'start_date' => DateUtility::changeDateFormat($row['Start Date']),
						'end_date' => DateUtility::changeDateFormat($row['End Date']),
						'created_by' => 99999,
						'updated_by' => 99999
					];
					
					$user_venue_id = $user_venue_class->saveUserVenue($data);

					if ($user_venue_id != false)
					{
						$user_venue_ids[] = $user_venue_id;
					}
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added User Venues! Total: ' . count($user_venue_ids));

			return redirect()->back();
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'Something went wrong, please try again later');

		return redirect()->back();
	}

	public function clearVenueSkills (UserVenue $user_venue_class)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$all_user_venues = $user_venue_class->getAll();
		$total_user_venues = $all_user_venues->count();
		$total_cleared = 0;

		//Uncomment to activate
		// if ($total_user_venues > 0)
		// {
		// 	foreach ($all_user_venues as $user_venue) 
		// 	{
		// 		if ($user_venue->delete())
		// 		{
		// 			$total_cleared++;
		// 		}
		// 	}
		// }

		return view('venue-skills.remove-venue-skills', 
			compact(
				'active_page',
				'total_user_venues',
				'total_cleared'
			)
		);
	}

	public function viewUpdateUserSkills (
		User $user_class,
		StatusCode $status_code_class, 
		Brand $brand_class,
		Ship $ship_class,
		SkillCombo $skill_combo_class,
		SkillComboDetail $skill_combo_detail_class,
		SkillSetJoin $skill_set_join_class,
		CrewCurrentStatus $crew_current_Status_class
	)
	{	
		// dd($skill_set_join_class->getByUserId(16000010195)->isEmpty());	
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$default_brand = 'RCI';
		$default_ship = 44; // Enchantment of the Seas
		$default_cost_center = 471; // Restaurant
		$default_job_int_code = 'WA'; // Assistant Waiter

		$brands = $brand_class->getAll();
		$ships = $ship_class->getAll();
		$skill_combos = $skill_combo_class->getAll();

		return view('venue-skills.update-user-skills', 
			compact(
				'active_page',
				'brands',
				'ships',
				'skill_combos',
				'default_brand',
				'default_ship',
				'default_cost_center',
				'default_job_int_code'
			)
		);
	}

	public function validateJobIntCode (Request $request, JobInt $job_int_class)
	{
		$job_int_code = $request->job_int_code;
		$job_int_details = $job_int_class->getByJobIntCode($job_int_code);

		if (!is_null($job_int_details))
		{
			return response()->json([
				'status' => true,
				'is_available' => true
			]);
		}

		return response()->json([
			'status' => false
		]);
	}

	public function getUsersByShipAndjobCode (
		Request $request, 
		CrewCurrentStatus $crew_current_Status_class,
		User $user_class
	)
	{
		$status_code_id = $request->status_code_id;
		$brand_id = $request->brand_id;
		$ship_code = $request->ship_code;
		$cost_center = $request->cost_center;
		$job_int_code = $request->job_int_code;

		$results = $crew_current_Status_class->getUserIdsByShipCodeBrandCodeCostCenterAndJobCode(
						$ship_code, 
						$brand_id, 
						$cost_center, 
						$job_int_code
					);

		if (!$results->isEmpty())
		{
			$user_ids = [];
			$user_details = [];

			foreach ($results as $value) 
			{
				$user_ids[] = $value->user_id;
			}

			foreach ($user_ids as $user_id) 
			{
				$user_details[] = $user_class->getById($user_id);
			}

			/**
			 * Total Results
			 * Render a HTML form
			 */
			$content = View::make('partials.venue-skills.total-users', [
			    'results' => $results,
			    'user_ids' => json_encode($user_ids)
			])->render();

			/**
			 * Users List tables
			 * Render a HTML form
			 */
			$user_content = View::make('partials.venue-skills.total-user-list', [
			    'user_details' => $user_details
			])->render();

			return response()->json([
				'status' => true,
				'content' => $content,
				'total' => $results->count(),
				'user_content' => $user_content
			]);
		}

		return response()->json([
			'status' => false
		]);
	}

	public function updateUserSkillSetJoins (
		Request $request, 
		SkillSetJoin $skill_set_join_class, 
		SkillCombo $skill_combo_class
	)
	{
		$skill_combo_id = $request->skill_combo_id;
		$user_ids = $request->user_ids;

		if (!is_null($user_ids))
		{
			$user_ids = json_decode($user_ids);
			$skill_combos = $skill_combo_class->getById($skill_combo_id);

			
			$results = [];
			$result_user_list = [];

			if (!is_null($skill_combos))
			{
				$skill_combo_details = $skill_combos->skills()->get();

				if (!$skill_combo_details->isEmpty())
				{
					foreach ($user_ids as $user_id) 
					{
						$skill_set_join_class = new SkillSetJoin();
						$user_skill_sets = $skill_set_join_class->getByUserId($user_id);

						if (!$user_skill_sets->isEmpty())
						{
							foreach ($skill_combo_details as $skill_combo_detail) 
							{
								$have_skill = false;
								$data = [];

								foreach ($user_skill_sets as $user_skill_set) 
								{
									if ($user_skill_set->getSkillSetId() == $skill_combo_detail->getSkillSetId())
									{
										/*
										 * Update only Skill Set Join
										 */
										$have_skill = true;

										$data = [
											'skill_set_join_id' => $user_skill_set->getId(),
											'skill_set_id' => $user_skill_set->getSkillSetId(),
											'foreign_id' => $user_skill_set->getUserId(),
											'foreign_table' => 'persons',
											'is_a_must' => 0,
											'skill_level_id' => $skill_combo_detail->getSkillLevelId(),
											'read' => 0,
											'write' => 0,
											'speak' => 0,
											'comprehend' => 0,
											'last_update' => date('Y-m-d H:i:s')
										];

										$skill_set_join_class = new SkillSetJoin();

										$results[$user_skill_set->getUserId()][] = $skill_set_join_class->saveSkillSetJoins($data) . ' => Updated';

										break;
									}
								}

								if (!$have_skill)
								{
									/*
									 * Insert New Skill Set Join
									 */
									
									$data = [
										'skill_set_join_id' => '',
										'skill_set_id' => $skill_combo_detail->getSkillSetId(),
										'foreign_id' => $user_skill_set->getUserId(),
										'foreign_table' => 'persons',
										'is_a_must' => 0,
										'skill_level_id' => $skill_combo_detail->getSkillLevelId(),
										'read' => 0,
										'write' => 0,
										'speak' => 0,
										'comprehend' => 0,
										'last_update' => date('Y-m-d H:i:s')
									];

									$skill_set_join_class = new SkillSetJoin();

									$results[$user_skill_set->getUserId()][] = $skill_set_join_class->saveSkillSetJoins($data) . ' => New';
								}
							}
						}
						else
						{
							$result_user_list[] = $user_id;
							foreach ($skill_combo_details as $skill_combo_detail) 
							{
								/*
								 * Insert New Skill Set Join
								 */
								
								$data = [
									'skill_set_join_id' => '',
									'skill_set_id' => $skill_combo_detail->getSkillSetId(),
									'foreign_id' => $user_id,
									'foreign_table' => 'persons',
									'is_a_must' => 0,
									'skill_level_id' => $skill_combo_detail->getSkillLevelId(),
									'read' => 0,
									'write' => 0,
									'speak' => 0,
									'comprehend' => 0,
									'last_update' => date('Y-m-d H:i:s')
								];

								$skill_set_join_class = new SkillSetJoin();

								$results[$user_id][] = $skill_set_join_class->saveSkillSetJoins($data) . ' => New';
							}
						}
					}

					return response()->json([
						'status' => true,
						'total_results' => count($results),
						'total_results_list' => $results,
						'user_list_updated' => $result_user_list
					]);
				}
			}
		}

		return response()->json([
			'status' => false
		]);
	}

	public function getSkillComboDetails (Request $request, SkillCombo $skill_combo_class)
	{
		$skill_combo_id = $request->skill_combo_id;
		$skill_combos = $skill_combo_class->getById($skill_combo_id);

		if (!is_null($skill_combos))
		{
			$skill_combo_details = $skill_combos->skills()->get();

			if (!$skill_combo_details->isEmpty())
			{
				/**
				 * Skill Combo Details
				 * Render a HTML form
				 */
				$content = View::make('partials.venue-skills.skill-combo-details', [
					'skill_combos' => $skill_combos,
				    'skill_combo_details' => $skill_combo_details
				])->render();

				return response()->json([
					'status' => true,
					'content' => $content
				]);
			}
		}

		return response()->json([
			'status' => false
		]);
	}

	public function viewAddTLCCodes()
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;

		// $trainig_course_class = new TrainingCourse();

		// $data = [
		// 	'id' => '',
		// 	'training_code' => 'ECDISP',
		// 	'training_name' => 'TYPE SPECIFIC NACOS PLATINUM',
		// 	'training_desc' => 'TYPE SPECIFIC NACOS PLATINUM',
		// 	'training_type_desc' => 'Certificate',
		// 	'is_active' => 1,
		// 	'regulation_type' => 'O',
		// 	'owner' => 'HR',
		// 	'expiration_date' => 1,
		// 	'expire_at_sign_off' => 1,
		// 	'issue_date_required' => 1,
		// 	'stwc_regulation' => null,
		// 	'num_months_expiration' => 0,
		// 	'expire_at_eplan_change' => 1,
		// 	'print_certificate' => 1,
		// 	'active_flag' => 'A',
		// 	'location' => 'SB'
		// ];
		
		// dd($trainig_course_class->saveTrainingCodes($data));

		return view('venue-skills.add-tlc-view', 
			compact(
				'active_page'
			)
		);
	}

	public function saveTLCCodes(Request $request)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$upload_tlc_csv = $request->upload_tlc_csv;
		$tlc_data = [];

		$request->validate([
			'upload_tlc_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-tlc-codes' . '.csv';
		$request->upload_tlc_csv->storeAs('VenueSkills', $file_name);

		if ($request->hasFile('upload_tlc_csv')) 
		{
			$file_path = storage_path('app/VenueSkills/' . $file_name);
			$csv_data = StringUtility::csvToArray($file_path);

			if (!empty($csv_data))
			{
				$tlc_data = $csv_data;
			}
		}
		
		$tlc_data_ids = [];

		if (!empty($tlc_data))
		{
			foreach ($tlc_data as $row) 
			{
				$trainig_course_class = new TrainingCourse();

				$trainig_course = $trainig_course_class->getByTrainingCode($row['TLC Code']);

				$data = [
					'id' => (!is_null($trainig_course)) ? $trainig_course->getId() : '',
					'training_code' => $row['TLC Code'],
					'training_name' => htmlentities($row['TLC Description']),
					'training_desc' => htmlentities($row['TLC Description']),
					'training_type_desc' => $row['TLC type Description'],
					'is_active' => 1,
					'regulation_type' => $row['Regulation Type'],
					'owner' => $row['Owner'],
					'expiration_date' => (isset($row['Expiration Date Req.']) && $row['Expiration Date Req.'] = 'Y') ? 1 : 0,
					'expire_at_sign_off' => (isset($row['Expire at Sign Off']) && $row['Expire at Sign Off'] = 'Y') ? 1 : 0,
					'issue_date_required' => (isset($row['Issue Date Required.']) && $row['Issue Date Required.'] = 'Y') ? 1 : 0,
					'stwc_regulation' => (isset($row['STWC  Regulation']) && $row['STWC  Regulation'] = 'N') ? 0 : null,
					'num_months_expiration' => $row['Nbr. of Months Until Expiration'],
					'expire_at_eplan_change' => (isset($row['Expire At Eplan Change']) && $row['Expire At Eplan Change'] = 'Y') ? 1 : 0,
					'print_certificate' => (isset($row['Print Certificate']) && $row['Print Certificate'] = 'Y') ? 1 : 0,
					'active_flag' => (isset($row['Active Flag']) && !empty($row['Active Flag'])) ? $row['Active Flag'] : null,
					'location' => (isset($row['Location']) && !empty($row['Location'])) ? $row['Location'] : null
				];
				
				$tlc_code_id = $trainig_course_class->saveTrainingCodes($data);

				if ($tlc_code_id != false)
				{
					$tlc_data_ids[] = $tlc_code_id;
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added TLC Codes! Total: ' . count($tlc_data_ids));

			return redirect()->back();
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'Something went wrong, please try again later');

		return redirect()->back();
	}
	
	public function viewAddTLCCodeSkills(
		TrainingCourse $training_course_class, 
		SkillSet $skill_set_class, 
		SkillLevel $skill_level_class
	)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$training_codes = $training_course_class->getAll();
		$skill_sets = $skill_set_class->getAll();
		$skill_levels = $skill_level_class->getAll();

		return view('venue-skills.add-tlc-skills-view', 
			compact(
				'active_page',
				'training_codes',
				'skill_sets',
				'skill_levels'
			)
		);
	}

	public function loadTLCSkills(Request $request, TrainingCourse $training_course_class)
	{
		$training_course_id = $request->training_course_id;

		if (!is_null($training_course_id))
		{
			$training_course = $training_course_class->getById($training_course_id);

			if (!is_null($training_course))
			{
				$training_course_skills = $training_course->TrainingSkills()->get();

				/**
				 * TLC Skills
				 * Render a HTML form
				 */
				$content = View::make('partials.venue-skills.tlc-skill-contents', [
					'training_course_skills' => $training_course_skills
				])->render();

				return response()->json([
					'status' => true,
					'training_course_name' => $training_course->getTrainingCode() . ' - ' . $training_course->getTrainingName(),
					'training_course_skills' => $content
				]);
			}
		}

		return response()->json([
			'status' => false
		]);
	}

	public function saveTLCSkills(Request $request, TrainingCourseSkill $training_course_skill_class)
	{
		$training_course_id = $request->training_course_id;
		$training_course_skill_id = $request->training_course_skill_id;
		$skill_set_id = $request->skill_set_id;
		$skill_level_id = $request->skill_level_id;
		$form_action_type = $request->form_action_type;

		if (!is_null($training_course_id) && !is_null($skill_set_id) && !is_null($skill_level_id))
		{
			$data = [
				'id' => (!is_null($training_course_skill_id)) ? $training_course_skill_id : '',
				'training_course_id' => $training_course_id,
				'skill_set_id' => $skill_set_id,
				'skill_level_id' => $skill_level_id,
				'is_active' => 1
			];

			$result = $training_course_skill_class->saveTrainingCodeSkills($data);

			if ($result)
			{
				$training_course_skill = $training_course_skill_class->getById($result);

				if (!is_null($training_course_skill))
				{
					$skill = $training_course_skill->skill()->first();
					$level = $training_course_skill->level()->first();

					if (!is_null($skill) && !is_null($level))
					{
						$content = '
							<td>'. $skill->getName() .'</td>
							<td>'. $level->getName() .'</td>
							<td>
								<button 
									class="update-tlc-skills btn btn-xs btn-info" 
									data-id="' . $training_course_skill->getId() . '"
									data-skill="'. $skill->getId() .'"
									data-level="'. $level->getId() .'"
									title="Update"
									data-toggle="tooltip" 
									><i class="fa fa-pencil" aria-hidden="true"></i></button>
								<button 
									class="delete-tlc-skills btn btn-xs btn-danger" 
									data-id="' . $training_course_skill->getId() . '"
									data-skill="'. $skill->getId() .'"
									data-level="'. $level->getId() .'"
									data-toggle="tooltip"
									title="Delete" 
									><i class="fa fa-trash" aria-hidden="true"></i></button>
							</td>
							';

						// Form Action is Update
						$main_content = $content;

						// Form Action is Add
						if ($form_action_type == 'add')
						{
							$main_content = '
								<tr id="training_skill_set_'. $training_course_skill->getId() .'">
								'. $content .'
								</tr>
							';
						}

						return response()->json([
							'status' => true,
							'content' => $main_content
						]);
					}
				}
			}
		}

		return response()->json([
			'status' => false
		]);
	}

	public function removeTLCSkills(Request $request, TrainingCourseSkill $training_course_skill_class)
	{
		$training_course_skill_id = $request->training_course_skill_id;

		if (!is_null($training_course_skill_id))
		{
			return response()->json([
				'status' => $training_course_skill_class->removeTrainingCodeSkills($training_course_skill_id)
			]);
		}

		return response()->json([
			'status' => false
		]);
	}

	public function viewAddEmployeeTLCCodes()
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;

		return view('venue-skills.add-employee-tlc-view', 
			compact(
				'active_page'
			)
		);
	}

	public function saveEmpolyeeTLCCodes(Request $request)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$upload_tlc_csv = $request->upload_tlc_csv;
		$employee_tlc_data = [];
		$failed_data = [];

		$request->validate([
			'upload_tlc_csv' => 'required'
		]);

		$file_name = time() . date('Y-m-d') . '-employee-tlc-codes' . '.csv';
		$request->upload_tlc_csv->storeAs('VenueSkills', $file_name);
		if ($request->hasFile('upload_tlc_csv')) 
		{
			$file_path = storage_path('app/VenueSkills/' . $file_name);
			$csv_data = StringUtility::csvToArray($file_path);

			if (!empty($csv_data))
			{
				$employee_tlc_data = $csv_data;
			}
		}
		
		$employee_tlc_data_ids = [];

		if (!empty($employee_tlc_data))
		{
			session()->forget('failed_employee_training_uploads');

			foreach ($employee_tlc_data as $row) 
			{
				$training_course_class = new TrainingCourse();
				$training_course = $training_course_class->getByTrainingCode($row['TLCcode']);
				$issue_date = DateUtility::changeDateFormat($row['IssueDate']);
				$expire_date = DateUtility::changeDateFormat($row['IssueDate']);
				$date_updated = DateUtility::changeDateFormat($row['IssueDate']);

				if (!is_null($training_course))
				{
					$employee_training_class = new EmployeeTraining();

					$data = [
						'jde_id' => $row['EmployeeID'],
						'training_course_id' => $training_course->getId(),
						'issue_date' => $issue_date,
						'expire_date' => $expire_date,
						'date_updated' => $date_updated,
						'certificate_number' => (isset($row['CertificateNumber']) && !empty($row['CertificateNumber'])) ? strval($row['CertificateNumber']) : null,
						'issue_country' => (isset($row['IssueCountry']) && !empty($row['IssueCountry'])) ? $row['IssueCountry'] : null,
						'trainer_name' => (isset($row['TrainerName']) && !empty($row['TrainerName'])) ? htmlentities($row['TrainerName']) : null
					];
					
					$employee_training_id = $employee_training_class->saveEmployeeTrainings($data);

					if ($employee_training_id != false)
					{
						$employee_tlc_data_ids[] = $employee_training_id;
					}
					else
					{
						$failed_data[] = $row;
					}
				}
				else
				{
					$failed_data[] = $row;
				}
			}

			session()->flash('success_flash', true);
			session()->flash('message_flash', 'Successfully added Employee Trainings! Total: ' . count($employee_tlc_data_ids));
			session(['failed_employee_training_uploads' => $failed_data]);

			return redirect()->back();
		}

		session()->flash('success_flash', false);
		session()->flash('message_flash', 'Something went wrong, please try again later');

		return redirect()->back();
	}

	public function viewAddTLCJobInts(JobInt $job_int_class, TrainingCourse $training_course_class)
	{
		$active_page = DashboardLink::VENUE_SKILLS_PAGE;
		$job_ints = $job_int_class->getAll();
		$training_courses = $training_course_class->getAll();

		return view('venue-skills.add-tlc-jobint-view', 
			compact(
				'active_page',
				'job_ints',
				'training_courses'
			)
		);
	}

	public function loadTLCJobs(
		Request $request, 
		TrainingCourseJob $training_course_job_class, 
		JobInt $job_int_class
	)
	{
		$job_int_id = $request->job_int_id;

		if (!is_null($job_int_id))
		{
			$training_course_jobs = $training_course_job_class->getByJobIntId($job_int_id);
			$job_int = $job_int_class->getById($job_int_id);

			/**
			 * TLC Jobs
			 * Render a HTML form
			 */
			$content = View::make('partials.venue-skills.tlc-jobs-contents', [
				'training_course_jobs' => $training_course_jobs
			])->render();

			return response()->json([
				'status' => true,
				'job_int_name' => $job_int->getId() . ' | ' . $job_int->getCode() . ' | ' . ucfirst(strtolower($job_int->getName())),
				'training_course_jobs' => $content
			]);
		}

		return response()->json([
			'status' => false
		]);
	}

	public function saveTLCJobs(Request $request, TrainingCourseJob $training_course_job_class)
	{
		$training_course_id = $request->training_course_id;
		$job_int_id = $request->job_int_id;
		$training_course_job_id = $request->training_course_job_id;
		$form_action_type = $request->form_action_type;

		if (!is_null($training_course_id) && !is_null($job_int_id))
		{
			$data = [
				'id' => (!is_null($training_course_job_id)) ? $training_course_job_id : '',
				'training_course_id' => $training_course_id,
				'job_int_id' => $job_int_id
			];

			$result = $training_course_job_class->saveTrainingCourseJobs($data);

			if ($result)
			{
				$training_course_job = $training_course_job_class->getById($result);

				if (!is_null($training_course_job))
				{
					$training_course = $training_course_job->trainingCourse()->first();

					if (!is_null($training_course))
					{
						$content = '
							<td>'. $training_course->getTrainingCode() . '-' . $training_course->getTrainingName() .'</td>
							<td>
								<button 
									class="update-tlc-jobs btn btn-xs btn-info" 
									data-id="' . $training_course_job->getId() . '"
									data-course="'. $training_course->getId() .'"
									title="Update"
									data-toggle="tooltip" 
									><i class="fa fa-pencil" aria-hidden="true"></i></button>
								<button 
									class="delete-tlc-jobs btn btn-xs btn-danger" 
									data-id="' . $training_course_job->getId() . '"
									data-course="'. $training_course->getId() .'"
									data-toggle="tooltip"
									title="Delete" 
									><i class="fa fa-trash" aria-hidden="true"></i></button>
							</td>
							';

						// Form Action is Update
						$main_content = $content;

						// Form Action is Add
						if ($form_action_type == 'add')
						{
							$main_content = '
								<tr id="training_course_job_'. $training_course_job->getId() .'">
								'. $content .'
								</tr>
							';
						}

						return response()->json([
							'status' => true,
							'content' => $main_content
						]);
					}
				}
			}
		}

		return response()->json([
			'status' => false
		]);
	}

	public function removeTLCJobs(Request $request, TrainingCourseJob $training_course_job_class)
	{
		$training_course_job_id = $request->training_course_job_id;

		if (!is_null($training_course_job_id))
		{
			return response()->json([
				'status' => $training_course_job_class->removeTrainingCourseJob($training_course_job_id)
			]);
		}

		return response()->json([
			'status' => false
		]);
	}
}
