<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\Users\User;
use CTRAC\Model\Others\Bank;
use CTRAC\Model\Users\PersonalDataRelationship;
use CTRAC\Model\Countries\Country;
use CTRAC\Model\Users\PersonalDataAllotee;
use CTRAC\Model\Scripts\CtracSupportHistory;
use View;
use Auth;

class AlloteeController extends Controller
{
    public function view(
    	Bank $bank_class, 
    	PersonalDataRelationship $relationship_class,
    	Country $country_class
    )
    {
    	/**
    	 * Set DB Connection to Production
    	 */
    	$db_connection = DatabaseConnection::CTRAC_QA;
    	if (config('app.env') != 'local')
    	{
    	    $db_connection = DatabaseConnection::CTRAC_PROD;
    	}
    	$bank_class->setDBConnection($db_connection);
    	$relationship_class->setDBConnection($db_connection);
    	$country_class->setDBConnection($db_connection);

    	/**
    	 * Get Bank List
    	 * Get Relationship List
    	 * Get Country List
    	 */
    	$bank_list = $bank_class->getAll();
    	$relationship_list = $relationship_class->getAll();
    	$country_list = $country_class->getAll();

    	return view('allotees.view', 
    		compact(
    			'bank_list',
    			'relationship_list',
    			'country_list'
    		)
    	);
    }

    public function getAlloteeDetails(
    	Request $request,
    	User $user_class
    )
    {
    	/**
    	 * Set DB Connection to Production
    	 */
    	$db_connection = DatabaseConnection::CTRAC_QA;
    	if (config('app.env') != 'local')
    	{
    	    $db_connection = DatabaseConnection::CTRAC_PROD;
    	}

    	$jde_id = $request->jde_id;
    	$user_class->setDBConnection($db_connection);
    	$user_details = $user_class->getByJDEId($jde_id);
        $total_allotee_percentage = 0;

    	if (!is_null($user_details))
    	{
            $personal_details = $user_details->personalData;
            /**
             * Personal Details
             * Render a HTML form
             */
            $personal_details_content = View::make('partials.allotee.personal-details', [
                'user_details' => $user_details,
                'personal_details' => $personal_details
            ])->render();

    		if (!$user_details->allotees->isEmpty())
    		{
                $total_allotee_percentage = $user_details->allotees->sum(function ($allotee) {
                    return $allotee->getPercentage();
                });

    			/**
    			 * Allotee Details
    			 * Render a HTML form
    			 */
    			$content = View::make('partials.allotee.details', [
    			    'allotee_details' => $user_details->allotees,
                    'total_allotee_percentage' => $total_allotee_percentage,
                    'user_id' => $user_details->getId()
    			])->render();

    			return response()->json([
    				'status' => true,
    				'allotee_content' => $content,
                    'user_id' => $user_details->getId(),
                    'total_allotee_percentage' => $total_allotee_percentage,
                    'personal_details_content' => $personal_details_content,
    				'message' => 'Successfully get allotee details!'
    			]);
    		} 
            else 
            {
                /**
                 * Allotee Details
                 * Render a HTML form
                 */
                $content = View::make('partials.allotee.details', [
                    'allotee_details' => null,
                    'total_allotee_percentage' => $total_allotee_percentage,
                    'user_id' => $user_details->getId()
                ])->render();

                return response()->json([
                    'status' => true,
                    'allotee_content' => $content,
                    'user_id' => $user_details->getId(),
                    'total_allotee_percentage' => $total_allotee_percentage,
                    'personal_details_content' => $personal_details_content,
                    'message' => 'Successfully get allotee details!'
                ]);
            }

    		return response()->json([
    			'status' => false,
    			'message' => 'User does not have allotees!'
    		]);
    	}

    	return response()->json([
    		'status' => false,
    		'message' => 'Employee ID does not exist in C-TRAC'
    	]);
    }

    public function checkAlloteeDetails(
        Request $request, 
        PersonalDataAllotee $allotee_class
    )
    {
        /**
         * Set DB Connection to Production
         */
        $db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local')
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }

        $allotee_class->setDBConnection($db_connection);
        $allotee_id = $request->allotee_id;

        if (!empty($allotee_id))
        {
            $allotee_details = $allotee_class->getById($allotee_id);

            if (!is_null($allotee_details))
            {
                return response()->json([
                    'status' => true,
                    'allotee_details' => $allotee_details,
                    'message' => 'Successfully fetch allotee details!'
                ]);
            }
        }

        return response()->json([
            'status' => false,
            'message' => 'Allotee ID does not exist in C-TRAC'
        ]);
    }

    public function saveAlloteeDetails(
        Request $request,
        User $user_class,
        PersonalDataAllotee $allotee_class,
        CtracSupportHistory $support_history_class
    )
    {
        if (!empty($request->user_id))
        {
            /**
             * Set DB Connection to Production
             */
            $db_connection = DatabaseConnection::CTRAC_QA;
            if (config('app.env') != 'local')
            {
                $db_connection = DatabaseConnection::CTRAC_PROD;
            }
            $user_class->setDBConnection($db_connection);
            $allotee_class->setDBConnection($db_connection);
            $support_history_class->setDBConnection($db_connection);
            $user_details = $user_class->getById($request->user_id);
            $total_allotee_percentage = 0;

            if (!is_null($user_details))
            {
                if (!$user_details->allotees->isEmpty())
                {
                    $total_allotee_percentage = $user_details->allotees->sum(function ($allotee) {
                        return $allotee->getPercentage();
                    });
                }

                $data = [
                    'allotee_id' => '',
                    'user_id' => $request->user_id,
                    'allotee_firstname' => $request->first_name,
                    'allotee_middlename' => $request->middle_name,
                    'allotee_lastname' => $request->last_name,
                    'allotee_bank' => $request->bank_id,
                    'allotee_account_no' => $request->account_no,
                    'relationship_id' => $request->relationship_id,
                    'allotee_telephone_no' => $request->tel,
                    'allotee_mobile_no' => $request->mobile,
                    'allotee_street_address' => $request->street,
                    'allotee_city' => $request->city,
                    'allotee_state' => $request->state,
                    'allotee_zip_code' => $request->postal_code,
                    'allotee_country_code' => $request->country
                ];

                if ($request->action == 'add')
                {
                    $data['created_by'] = 13000000002;
                    $remaining_percentage = 100 - $total_allotee_percentage;
                    if ($request->percent > $remaining_percentage)
                    {
                        return response()->json([
                            'status' => false,
                            'message' => 'The Sum of all Allotee Percentage must not exceed 100%'
                        ]);
                    }
                    else
                    {
                        $data['allotee_percentage'] = $request->percent;
                    }
                }

                if ($request->action == 'edit')
                {
                    $data['allotee_id'] = $request->allotee_id;
                    $data['last_updated_by'] = 13000000002;
                    $remaining_percentage = (100 - $total_allotee_percentage) + $request->active_allotee_current_percentage;
                    if ($request->percent > $remaining_percentage)
                    {
                        return response()->json([
                            'status' => false,
                            'message' => 'You only have ' . $remaining_percentage . '% remaining Allotee'
                        ]);
                    }
                    else
                    {
                        $data['allotee_percentage'] = $request->percent;
                    }
                }

                $result = $allotee_class->saveAlloteeData($data);

                if ($result)
                {
                    /**
                     * Insert Support History Record
                     */
                    $support_history_data = [
                        'user_id' => $user_details->getId(),
                        'ctrac_support_history_type_id' => 3, // Allotee Type
                        'note' => 'Allotee: ' . $request->action . ' allotee details action (Allotee ID# ' . $result . ')',
                        'ctrac_user_email' => Auth::user()->getEmail(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => Auth::user()->getId()
                    ];

                    if ($support_history_class->saveSupportHistory($support_history_data) !== false)
                    {
                        if (config('app.env') != 'local')
                        {
                            /**
                             * Send Admin Notification E-mail
                             */
                            Auth::user()->emailAlloteeUpdate($user_details);
                        }

                        return response()->json([
                            'status' => true,
                            'message' => 'Successfully saved allotee details!'
                        ]);
                    }
                }
            }
        }

        return response()->json([
            'status' => false,
            'message' => 'An error occured while saving allotee details'
        ]);
    }

    public function deleteAlloteeDetails(
        Request $request, 
        PersonalDataAllotee $allotee_class,
        CtracSupportHistory $support_history_class
    )
    {
        /**
         * Set DB Connection to Production
         */
        $db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local')
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }

        $allotee_class->setDBConnection($db_connection);
        $support_history_class->setDBConnection($db_connection);
        $allotee_id = $request->allotee_id;
        $user_id = $request->user_id;

        if (!empty($allotee_id))
        {
            $allotee_details = $allotee_class->getById($allotee_id);

            if ($allotee_details->delete())
            {
                /**
                 * Insert Support History Record
                 */
                $support_history_data = [
                    'user_id' => $user_id,
                    'ctrac_support_history_type_id' => 3, // Allotee Type
                    'note' => 'Allotee: Delete allotee details (Allotee ID# ' . $allotee_id . ')',
                    'ctrac_user_email' => Auth::user()->getEmail(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => Auth::user()->getId()
                ];

                if ($support_history_class->saveSupportHistory($support_history_data) !== false)
                {
                    if (config('app.env') != 'local')
                    {
                        /**
                         * Send Admin Notification E-mail
                         */
                        Auth::user()->emailAlloteeUpdate($user_details);
                    }
                    return response()->json([
                        'status' => true,
                        'message' => 'Successfully deleted allotee details!'
                    ]);
                }
            }

            return response()->json([
                'status' => false,
                'message' => 'An error occured while deleting the allotee data, please try again later'
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => 'Allotee ID does not exist in C-TRAC'
        ]);
    }
}
