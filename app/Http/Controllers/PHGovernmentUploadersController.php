<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Helpers\StringUtility;
use CTRAC\Helpers\DateUtility;
use CTRAC\Model\Users\User;
use CTRAC\Model\Documents\DocumentChecklistFormUpload;
use CTRAC\Model\Documents\DocumentChecklistUpload;
use CTRAC\Model\Documents\DocumentFormUploadsStatus;
use CTRAC\Model\Users\PersonalData;
use CTRAC\Exports\GovernmentDocumentUploaderSuccessResultLogs;
use CTRAC\Exports\GovernmentDocumentUploaderFailResultLogs;
use Excel;

class PHGovernmentUploadersController extends Controller
{
    public function viewUploader ()
    {
    	return view('ph-government-uploader.view');
    }

    public function uploadGovernmentDocuments ( Request $request )
    {
    	$req_data = [];
    	$history_logs = [];
    	$history_logs_not_updated = [];
    	$total_updated = 0;
    	$total_not_updated = 0;

    	$request->validate([
    		'upload_csv' => 'required'
    	]);

    	$file_name = time() . date('Y-m-d') . '-ph-government-document-uploads' . '.csv';
    	$request->upload_csv->storeAs('ph-government-document', $file_name);

    	if ($request->hasFile('upload_csv')) 
    	{
    		$file_path = storage_path('app/ph-government-document/' . $file_name);
    		$csv_data = StringUtility::csvToArray($file_path);

    		if (!empty($csv_data))
    		{
    			$req_data = $csv_data;
    		}
    	}

    	if (!empty($req_data))
    	{
    		/**
    		 * Set DB Connection to Production
    		 */
    		$db_connection = DatabaseConnection::CTRAC_QA;
    		if (config('app.env') != 'local')
    		{
    		    $db_connection = DatabaseConnection::CTRAC_PROD;
    		}

    		$admin_id = 99999999999;
    		$ck_list_id = $request->ck_list_id;
    		$filename = 'auto_approved';
    		$document_name = '';

    		foreach ($req_data as $row) 
    		{

    			$datum = [];
    			$user_class = new User;
    			$user_class->setDBConnection($db_connection);
    			$user_details = $user_class->getByJDEId($row['employee_id']);

    			if (!is_null($user_details)) 
    			{

    				if ($ck_list_id = 178)
    				{
    					$government_name = 'sss';
    					$document_name = 'Social Security System';
    					$file_path = 'files/applicants/2015/11/15000164267/documents/sss-document_15000164267_20190521163609_6784.JPG';
    				}

    				if ($ck_list_id = 179)
    				{
    					$government_name = 'philhealth_number';
    					$document_name = 'Philhealth';
    					$file_path = 'files/applicants/2015/11/15000164267/documents/philhealth-document_15000164267_20190523150648_36.jpg';
    				}

    				if ($ck_list_id = 180)
    				{
    					$government_name = 'hdmf_number';
    					$document_name = 'Pag-Ibig';
    					$file_path = 'files/applicants/2015/11/15000164267/documents/pag-ibig-document_15000164267_20190528125229_9737.JPG';
    				}

    				if (!is_null( $user_details->personalData ))
    				{
    					$user_id = $user_details->getId();

    					$document_checklist_form_upload_class = new DocumentChecklistFormUpload;
    					$document_checklist_form_upload_class->setDBConnection($db_connection);
    					$document_checklist_form_upload_data = $document_checklist_form_upload_class->getByUserIdAndCkListId($user_id, $ck_list_id);

    					$document_checklist_upload_class = new DocumentChecklistUpload;
    					$document_checklist_upload_class->setDBConnection($db_connection);
    					$document_checklist_upload_data = $document_checklist_upload_class->getByUserIdAndCkListId($user_id, $ck_list_id);
    					

    					$document_checklist_form_upload_datum = [
    						'user_id' => $user_id,
    						'ck_list_id' => $ck_list_id,
    						'is_completed' => 2,
    						'date_created' => date('Y-m-d H:i:s'),
    						'date_modified' => date('Y-m-d H:i:s'),
    						'created_by' => $admin_id,
    						'modified_by' => $admin_id
    					];

    					if (!is_null( $document_checklist_form_upload_data )) {
    						/**
    						 * Skip all process if Document was already Completed
    						 */
    						if ($document_checklist_form_upload_data->getIsCompleted() == 2) {
    							$total_not_updated++;
    							$history_logs_not_updated[] = [
    								'success' => true,
    								'jde_id' => $row['employee_id'],
    								'remarks' => 'Crew document has been Completed'
    							];
    							continue;
    						}
    						unset($document_checklist_form_upload_datum['date_created']);
    						$document_checklist_form_upload_datum['id'] = $document_checklist_form_upload_data->getId();
    					}

    					/**
    					 * Insert / Update document_checklist_form_uploads
    					 */
    					$document_checklist_form_upload_status = $document_checklist_form_upload_class->saveDocumentChecklistFormUploadData($document_checklist_form_upload_datum);

    					/**
    					 * Insert new entry on document_checklist_uploads
    					 */
    					if ($document_checklist_upload_data->isEmpty())
    					{
    						$document_checklist_upload_datum = [
    						    'user_id' => $user_id,
    						    'ck_list_id' => $ck_list_id,
    						    'filename' => $filename,
    						    'file_path' => $file_path,
    						    'document_number' => $row['number'],
    						    'status' => 'submitted',
    						    'last_updated_by' => $admin_id,
    						    'last_updated_on' => date('Y-m-d H:i:s'),
    						    'created_on' => date('Y-m-d H:i:s'),
    						    'issued_date' => date('Y-m-d'),
    						    'expiration_date' => date('Y-m-d'),
    						    'medical_facility_id' => 0,
    						    'training_type_id' => 0,
    						    'training_facility' => 0,
    						    'locked_ind' => 1,
    						    'newly_added' => 0,
    						    'newly_updated' => 0,
    						    'e1_migration' => 0,
    						    'e1_date_updated',
    						    'is_primary' => 0,
    						    'triton_ind' => 0,
    						    'document_status_id' => 2
    						];
    					}
    					else
    					{
    						/**
    						 * Update entry on document_checklist_uploads
    						 */
    						foreach ($document_checklist_upload_data as $document_checklist_upload_details) {
    							if ($document_checklist_upload_details->isLock() == 0 
    								&& $document_checklist_upload_details->docStatus->getDocStatusName() != 'Completed') {
    								$document_checklist_upload_datum = [
    									'id' => $document_checklist_upload_details->getId(),
    									'last_updated_by' => $admin_id,
    									'last_updated_on' => date('Y-m-d H:i:s'),
    									'filename' => $filename,
    									'file_path' => $file_path,
    									'document_number' => $row['number'],
    									'status' => 'submitted',
    									'locked_ind' => 1,
    									'document_status_id' => 2
    								];
    							}
    						}
    					}

    					if (!empty( $document_checklist_upload_datum )) {
    						
    						$document_checklist_upload_status = $document_checklist_upload_class->saveDocumentChecklistUploadData($document_checklist_upload_datum);
    					}

    					$personal_data = [
    						'personal_data_id' => $user_details->personalData->getId(),
    						$government_name => $row['number']
    					];

    					$personal_data_class = new PersonalData;
    					$personal_data_class->setDBConnection($db_connection);
    					$personal_data_status = $personal_data_class->savePersonalData($personal_data);

    					$total_updated++;
    					$history_logs[] = [
    						'success' => true,
    						'jde_id' => $row['employee_id'],
    						'document_entry' => $document_checklist_upload_status,
    						'document_header' => $document_checklist_form_upload_status,
    						'personal_data' => $personal_data_status
    					];
    				}
    				else
    				{
    					$total_not_updated++;
    					$history_logs_not_updated[] = [
    						'success' => true,
    						'jde_id' => $row['employee_id'],
    						'remarks' => 'Crew personal details missing'
    					];
    				}
    			}
    			else
    			{
    				$total_not_updated++;
    				$history_logs_not_updated[] = [
    					'success' => true,
    					'jde_id' => $row['employee_id'],
    					'remarks' => 'Crew does not exist on the System'
    				];
    			}
    		}

    		session()->flash('success_flash', true);
    		session()->flash('message_flash', 'Successfully added/updated ' . $document_name . ' Total: ' . $total_updated . ' ,and this are the list of not updated: ' . $total_not_updated . ' | Total of all uploaded: ' . ($total_updated + $total_not_updated));
    		session()->put('government_name', $government_name);
    		session()->flash('history_logs', $history_logs);
    		session()->put('history_logs_download', $history_logs);
    		session()->flash('history_logs_not_updated', $history_logs_not_updated);
    		session()->put('history_logs_not_updated_download', $history_logs_not_updated);
    		session()->flash('total_updated', $total_updated);
    		session()->flash('total_not_updated', $total_not_updated);

    		return redirect()->back();
    	}

    	session()->flash('success_flash', true);
    	session()->flash('message_flash', 'No new/updates for PH government documents!');

    	return redirect()->back();
    }

    public function downloadSuccessResultLogs ()
   	{
   		$history_logs = [];
   		$government_name = '';
   		if (session()->has('history_logs_download'))
   		{
   			$history_logs = session('history_logs_download');
   			$government_name = session('government_name');
   			session()->forget('history_logs_download');
   		}

   		return Excel::download(
   			new GovernmentDocumentUploaderSuccessResultLogs(
   				$history_logs
   			), 
   			$government_name . '-success-result-logs-' . date('Y-m-d') . time() . '.xlsx'
   		);
   	}

    public function downloadFailResultLogs ()
   	{
   		$history_logs_not_updated = [];
   		$government_name = '';
   		if (session()->has('history_logs_not_updated_download'))
   		{
   			$history_logs_not_updated = session('history_logs_not_updated_download');
   			$government_name = session('government_name');
   			session()->forget('history_logs_not_updated_download');
   		}

   		return Excel::download(
   			new GovernmentDocumentUploaderFailResultLogs(
   				$history_logs_not_updated
   			), 
   			$government_name . '-fail-result-logs-' . date('Y-m-d') . time() . '.xlsx'
   		);
   	}
}
