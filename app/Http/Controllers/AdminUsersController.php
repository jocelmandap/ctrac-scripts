<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Model\Scripts\CtracUser;
use CTRAC\Model\Scripts\CtracRole;
use Auth;
use File;

class AdminUsersController extends Controller
{
    public function index (CtracUser $user_class)
    {
        $user_list = $user_class->getAll();
        $user_roles = CtracRole::all();

    	return view('admin-users.view', 
    		compact(
    			'user_list',
                'user_roles'
    		)
    	);
    }

    public function saveUser (Request $request, CtracUser $user_class)
    {
        $form_type = $request->form_type;
        $error_message = 'An error occur while saving it, please try again later!';

        if ($form_type == 'edit')
        {
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255'
            ]);

            $data = [
                'id' => $request->user_id,
                'name' => $request->name,
                'email' => $request->email,
                'is_active' => $request->is_active
            ];

            if ($user_class->checkEmailIfValid($request->email, $request->user_id))
            {
                $result = $user_class->saveUser($data, false);

                if ($result) 
                {
                    
                    if (isset($request->role_id) && !empty($request->role_id))
                    {
                        if (!is_null($request->user_id))
                        {
                            // Update User Role
                            // Clear First all the Roles then add new roles
                            if (!$result->roles->isEmpty())
                            {
                                $result->roles()->detach();
                            }
                            $result->roles()->attach($request->role_id);
                        }
                        else
                        {
                            // Add New User Role
                            $result->roles()->attach($request->role_id);
                        }
                    }

                    if (!is_null($request->deactivate_status))
                    {
                        if (config('app.env') != 'local')
                        {
                            /**
                             * Send de-activate user E-mail
                             */
                            $result->emailDeactivateAccount();
                        }
                    }

                    $request->session()->flash('success_flash', true);
                    $request->session()->flash('message_flash', $request->name . ' details was successfully updated!');
                    return redirect()->back();
                }
            }

            $error_message = 'Email: ' . $request->email . ' is already been used.';
        }
        else if ($form_type == 'add')
        {
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:ctrac_users',
                'password' => 'required|string|min:6|confirmed'
            ]);

            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'is_active' => $request->is_active
            ];

            $result = $user_class->saveUser($data, false);

            if ($result) 
            {
                
                if (isset($request->role_id) && !empty($request->role_id))
                {
                    if (!is_null($request->user_id))
                    {
                        // Update User Role
                        // Clear First all the Roles then add new roles
                        if (!$result->roles->isEmpty())
                        {
                            $result->roles()->detach();
                        }
                        $result->roles()->attach($request->role_id);
                    }
                    else
                    {
                        // Add New User Role
                        $result->roles()->attach($request->role_id);

                        if (config('app.env') != 'local')
                        {
                            /**
                             * Send Welcome Message to User
                             * Send Login Credentials
                             */
                            $result->emailWelcomeMessage();
                            $result->emailLoginCredentials($request->password);
                        }
                    }
                }

                $request->session()->flash('success_flash', true);
                $request->session()->flash('message_flash', $request->name . ' user was successfully save!');
                return redirect()->back();
            }
        }
        else if ($form_type == 'change-password')
        {
            $request->validate([
                'password' => 'required|string|min:6|confirmed'
            ]);

            $data = [
                'id' => $request->user_id,
                'password' => bcrypt($request->password)
            ];

            $result = $user_class->saveUser($data, false);

            if ($result) 
            {
                
                if (!is_null($request->change_password))
                {
                    if (config('app.env') != 'local')
                    {
                        /**
                         * Send Login Credentials
                         */
                        $result->emailLoginCredentials($request->password);
                    }
                }

                $request->session()->flash('success_flash', true);
                $request->session()->flash('message_flash', $request->name . ' password was successfully updated!');
                return redirect()->back();
            }
        }

        $request->session()->flash('success_flash', false);
        $request->session()->flash('message_flash', $error_message);

        return redirect()->back();
    }

    public function deleteUser (Request $request, CtracUser $user_class)
    {
    	if (!is_null($request->user_id))
    	{
    		$user = $user_class->getById($request->user_id);

    		if (!is_null($user))
    		{
    			$user->roles()->detach();

    			if ($user->delete())
    			{
    				$request->session()->flash('success_flash', true);
    				$request->session()->flash('message_flash', $request->name . ' was successfully deleted!');

    				return response()->json([
    					'status' => true,
    					'message' => 'User was successfully deleted!'
    				]);
    			}
    		}

    		return response()->json([
    			'status' => false,
    			'message' => 'User not available!'
    		]);
    	}

    	return response()->json([
    		'status' => false,
    		'message' => 'Empty User Id!'
    	]);
    }

    public function viewProfile (CtracRole $role_class)
    {
        $user_details = Auth::user();
        $user_role = $user_details->roles->first();
        $user_members = $role_class->getByName($user_role->getName())->users;
        return view('profile.view', 
            compact(
                'user_details',
                'user_role',
                'user_members'
            )
        );
    }

    public function saveProfile (Request $request, CtracUser $user_class)
    {
        $form_type = $request->form_type;
        $user = Auth::user();
        $user_id = $user->getId();
        $error_message = 'An error occur while saving it, please try again later!';

        if ($form_type == 'update-profile')
        {
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
                'desc' => 'string|max:100'
            ]);

            $file_name = '';

            if (!is_null($request->profile))
            {
                $image_types = ['image/gif', 'image/jpeg', 'image/png'];
                if (in_array($request->profile->getMimeType(), $image_types))
                {
                    if (!is_null($user->profile_photo) && !empty($user->profile_photo))
                    {
                        if (\File::exists(public_path('uploads/Profiles/' . $user->profile_photo)))
                        {
                          \File::delete(public_path('uploads/Profiles/' . $user->profile_photo));
                        }
                    }

                    $file_name = time() . date('Y-m-d') . '-profile-photo.' . $request->profile->getClientOriginalExtension();
                    $request->profile->storeAs('Profiles', $file_name, 'profiles');
                }
            }

            $data = [
                'id' => $user_id,
                'name' => $request->name,
                'email' => $request->email,
                'description' => $request->desc
            ];

            if (!empty($file_name))
            {
                $data['profile_photo'] = $file_name;
            }

            if ($user_class->checkEmailIfValid($request->email, $user_id))
            {
                $result = $user_class->saveUser($data, false);

                if ($result) 
                {
                    $request->session()->flash('success_flash', true);
                    $request->session()->flash('message_flash', $request->name . ' details was successfully updated!');
                    return redirect()->back();
                }
            }

            $error_message = 'Email: ' . $request->email . ' is already been used.';
        }
        else if ($form_type == 'change-password')
        {
            $request->validate([
                'password' => 'required|string|min:6|confirmed'
            ]);

            $data = [
                'id' => $user_id,
                'password' => bcrypt($request->password)
            ];

            $result = $user_class->saveUser($data, false);

            if ($result) 
            {
                Auth::login($result);
                $request->session()->flash('success_flash', true);
                $request->session()->flash('message_flash', $request->name . ' password was successfully updated!');
                return redirect()->back();
            }
        }

        $request->session()->flash('success_flash', false);
        $request->session()->flash('message_flash', $error_message);

        return redirect()->back();
    }
}
