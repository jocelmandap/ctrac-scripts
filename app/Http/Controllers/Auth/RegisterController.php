<?php

namespace CTRAC\Http\Controllers\Auth;

use CTRAC\Model\Scripts\CtracUser;
use CTRAC\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use CTRAC\Model\Scripts\CtracRole;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:ctrac_users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \CTRAC\User
     */
    protected function create(array $data)
    {
        $user_class = new CtracUser();
        $role_class = new CtracRole();

        $user_class->name = $data['name'];
        $user_class->email = $data['email'];
        $user_class->password = bcrypt($data['password']);
        $user_class->is_active = $data['is_active'];
        $user_class->save();

        $role = $role_class->getById($data['role_id']);

        if (!is_null($role))
        {
            $user_class->roles()->attach($role);
            error_log('User: ' . $data['name'] . ' was successfully added a role for ' . $role->getName());
        }
        
        error_log('User: ' . $data['name'] . ' was successfully added.');

        return $user_class;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        /**
         * Send Welcome Message to User
         * Send Login Credentials
         */
        $user->emailWelcomeMessage();
        $user->emailLoginCredentials($request->password);

        $request->session()->flash('success_flash', true);
        $request->session()->flash('message_flash', $request->name . ' was successfully added!');

        return redirect()->back();   
    }

    public function showRegistrationForm()
    {
        $user = Auth::user();
        $user->authorizeRoles(['Super Admin']);
        $user_roles = CtracRole::all();

        return view('auth.register', 
            compact(
                'user_roles'
            )
        );
    }
}
