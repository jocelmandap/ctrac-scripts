<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Excel;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\Documents\DocumentChecklistFormUpload;
use CTRAC\Model\Documents\DocumentChecklistUpload;
use CTRAC\Exports\MedicalCertificateAdhocSuccessResultLogs;
use CTRAC\Exports\MedicalCertificateAdhocFailedResultLogs;

class AdhocController extends Controller
{
    const MED_CERTIFICATE_DOC_ID = 172;

    public function viewMedicalCertificateUpload (DocumentChecklistUpload $document_checklist_upload_class)
    {
        /**
         * Set DB Connection to Production
         */
        $db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local')
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }

        $document_checklist_upload_class->setDBConnection($db_connection);
        $medical_certificates = $document_checklist_upload_class->getByCkListId(self::MED_CERTIFICATE_DOC_ID);

        $acls = 0;
        $bls = 0;
        $pals = 0;
        $atls = 0;
        $acls_uploaded = 0;
        $bls_uploaded = 0;
        $pals_uploaded = 0;
        $atls_uploaded = 0;
        if (!$medical_certificates->isEmpty())
        {
            foreach ($medical_certificates as $medical_certificate) 
            {
                $document_checklist_upload_class = new DocumentChecklistUpload();
                $document_checklist_upload_class->setDBConnection($db_connection);
                if ($medical_certificate->training_type_id == 1) 
                {
                    $acls++;
                    $document_checklist_upload_detail = $document_checklist_upload_class->getByFilePathAndCkListId($medical_certificate->file_path, 189);
                    if (!is_null($document_checklist_upload_detail))
                    {
                        $acls_uploaded++;
                    }
                }
                if ($medical_certificate->training_type_id == 2) 
                {
                    $bls++;
                    $document_checklist_upload_detail = $document_checklist_upload_class->getByFilePathAndCkListId($medical_certificate->file_path, 190);
                    if (!is_null($document_checklist_upload_detail))
                    {
                        $bls_uploaded++;
                    }
                }
                if ($medical_certificate->training_type_id == 3) 
                {
                    $pals++;
                    $document_checklist_upload_detail = $document_checklist_upload_class->getByFilePathAndCkListId($medical_certificate->file_path, 191);
                    if (!is_null($document_checklist_upload_detail))
                    {
                        $pals_uploaded++;
                    }
                }
                if ($medical_certificate->training_type_id == 4) 
                {
                    $atls++;
                    $document_checklist_upload_detail = $document_checklist_upload_class->getByFilePathAndCkListId($medical_certificate->file_path, 192);
                    if (!is_null($document_checklist_upload_detail))
                    {
                        $atls_uploaded++;
                    }
                }
            }
        }

        $medical_document_list_count = [
            'acls' => $acls,
            'bls' => $bls,
            'pals' => $pals,
            'atls' => $atls,
            'acls_uploaded' => $acls_uploaded,
            'bls_uploaded' => $bls_uploaded,
            'pals_uploaded' => $pals_uploaded,
            'atls_uploaded' => $atls_uploaded,
            'total' => $medical_certificates->count()
        ];
        
        return view('adhocs.view-medical-cert', 
            compact(
                'medical_document_list_count'
            )
        );
    }

    public function saveMedicalCertificateUpload (
        Request $request,
        DocumentChecklistUpload $document_checklist_upload_class
    )
    {
        /**
         * Set DB Connection to Production
         */
        $db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local')
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }
        
        if ($request->start && $request->limit)
        {
            $document_checklist_upload_class->setDBConnection($db_connection);
            $medical_certificates = $document_checklist_upload_class->getByCkListId(self::MED_CERTIFICATE_DOC_ID);
            $total_limit = $request->limit;

            if (!$medical_certificates->isEmpty()) 
            {
                $filename = 'auto upload medical certificate';
                $admin_id = 99999999999;
                $acls = 0;
                $bls = 0;
                $pals = 0;
                $atls = 0;
                $total_updated = 0;
                $total_not_updated = 0;
                $history_logs = [];
                $history_logs_not_updated = [];
                $limit_count = 0;
                foreach ($medical_certificates as $medical_certificate)
                {
                    if ($limit_count == $total_limit)
                    {
                        break;
                    }

                    $ck_list_id = 0;
                    if ($medical_certificate->training_type_id == 1)
                    {
                        /*  
                         * Advance Cardiovascular Life Support
                        */
                        $ck_list_id = 189;
                        $acls++;
                    }
                    if ($medical_certificate->training_type_id == 2)
                    {
                        /*  
                         * Basic Life Saving
                        */
                        $ck_list_id = 190;
                        $bls++;
                    }
                    if ($medical_certificate->training_type_id == 3)
                    {
                        /*  
                         * Pediatric Advanced Life Support
                        */
                        $ck_list_id = 191;
                        $pals++;
                    }
                    if ($medical_certificate->training_type_id == 4)
                    {
                        /*  
                         * Advanced Trauma Life Support
                        */
                        $ck_list_id = 192;
                        $atls++;
                    }

                    $document_checklist_upload_class = new DocumentChecklistUpload();
                    $document_checklist_upload_class->setDBConnection($db_connection);
                    $document_checklist_upload_detail = $document_checklist_upload_class->getByFilePathAndCkListId($medical_certificate->file_path, $ck_list_id);

                    if (is_null($document_checklist_upload_detail))
                    {
                        $document_checklist_upload_data = [
                            'user_id' => $medical_certificate->user_id,
                            'ck_list_id' => $ck_list_id,
                            'filename' => $filename,
                            'file_path' => $medical_certificate->file_path,
                            'status' => 'submitted',
                            'last_updated_by' => $admin_id,
                            'last_updated_on' => date('Y-m-d H:i:s'),
                            'created_on' => date('Y-m-d H:i:s'),
                            'issued_date' => $medical_certificate->issued_date,
                            'expiration_date' => $medical_certificate->expiration_date,
                            'medical_facility_id' => 0,
                            'training_type_id' => 0,
                            'training_facility' => 0,
                            'locked_ind' => ($medical_certificate->document_status_id == 2) ? 1 : 0,
                            'newly_added' => 0,
                            'newly_updated' => 0,
                            'e1_migration' => 0,
                            'e1_date_updated',
                            'is_primary' => 0,
                            'triton_ind' => 0,
                            'document_status_id' => $medical_certificate->document_status_id
                        ];
    
                        /**
                         * Save Document Checklist Uploads
                         */
                        $document_checklist_upload_status = $document_checklist_upload_class->saveDocumentChecklistUploadData($document_checklist_upload_data);
                        if ($document_checklist_upload_status) 
                        {
                            /**
                             * Save / Update Document Header
                             */
                            $document_checklist_form_upload_datum = [
                                'user_id' => $medical_certificate->user_id,
                                'ck_list_id' => $ck_list_id,
                                'is_completed' => $medical_certificate->document_status_id,
                                'date_created' => date('Y-m-d H:i:s'),
                                'date_modified' => date('Y-m-d H:i:s'),
                                'created_by' => $admin_id,
                                'modified_by' => $admin_id
                            ];
    
                            $document_header_status = 'NEW';
                            $document_checklist_form_upload_class = new DocumentChecklistFormUpload();
                            $document_checklist_form_upload_class->setDBConnection($db_connection);
                            $medical_certificate_header = $document_checklist_form_upload_class->getByUserIdAndCkListId($medical_certificate->user_id, $ck_list_id);
                            if (!is_null($medical_certificate_header))
                            {
                                $document_header_status = 'UPDATED';
                                unset($document_checklist_form_upload_datum['date_created']);
                                $document_checklist_form_upload_datum['id'] = $medical_certificate_header->getId();
                            }
                            $document_checklist_form_upload_status = $document_checklist_form_upload_class->saveDocumentChecklistFormUploadData($document_checklist_form_upload_datum);
                            $total_updated++;
                            $limit_count++;
                            $history_logs[] = [
                                'success' => true,
                                'user_id' => $medical_certificate->user_id,
                                'ck_list_id' => $ck_list_id,
                                'document_entry_id' => $document_checklist_upload_status,
                                'document_header' => $document_checklist_form_upload_status,
                                'document_header_status' => $document_header_status
                            ];
                        }
                        else
                        {
                            /**
                             * Document not Saved  
                             */
                            $total_not_updated++;
                            $history_logs_not_updated[] = [
                                'success' => false,
                                'user_id' => $medical_certificate->user_id,
                                'ck_list_id' => $ck_list_id,
                                'remarks' => 'Document was not saved. Document Upload Id: ' . $medical_certificate->getId()
                            ];
                        }
                    }
                    else
                    {
                        /**
                         * Document was already been saved!
                         */
                        $total_not_updated++;
                        $history_logs_not_updated[] = [
                            'success' => false,
                            'user_id' => $medical_certificate->user_id,
                            'ck_list_id' => $ck_list_id,
                            'remarks' => 'Document was already been saved. Document Upload Id: ' . $document_checklist_upload_detail->getId()
                        ];
                    }
                }

                session()->flash('success_flash', true);
                session()->flash('message_flash', 'Successfully added/updated Medical Certificates Total: ' . $total_updated . ' ,and this are the list of not updated: ' . $total_not_updated . ' | Total of all data: ' . ($total_updated + $total_not_updated));
                session()->put('ck_list_id', $ck_list_id);
                session()->flash('history_logs', $history_logs);
                session()->put('history_logs_download', $history_logs);
                session()->flash('history_logs_not_updated', $history_logs_not_updated);
                session()->put('history_logs_not_updated_download', $history_logs_not_updated);
                session()->flash('total_updated', $total_updated);
                session()->flash('total_not_updated', $total_not_updated);
                session()->flash('total_acls', $acls);
                session()->flash('total_bls', $bls);
                session()->flash('total_pals', $pals);
                session()->flash('total_atls', $atls);

                return redirect()->back();
            }

            session()->flash('success_flash', true);
            session()->flash('message_flash', 'No new/updates for Medical Certificates!');
            
            return redirect()->back();
        }
    }

    public function downloadMedicalCertificateSuccessResultLogs ()
   	{
   		$history_logs = [];
   		if (session()->has('history_logs_download'))
   		{
   			$history_logs = session('history_logs_download');
   			session()->forget('history_logs_download');
   		}

   		return Excel::download(
   			new MedicalCertificateAdhocSuccessResultLogs(
   				$history_logs
   			), 
   			'medical-certificate-success-result-logs-' . date('Y-m-d') . time() . '.xlsx'
   		);
   	}

    public function downloadMedicalCertificateFailResultLogs ()
   	{
   		$history_logs_not_updated = [];
   		if (session()->has('history_logs_not_updated_download'))
   		{
   			$history_logs_not_updated = session('history_logs_not_updated_download');
   			session()->forget('history_logs_not_updated_download');
   		}

   		return Excel::download(
   			new MedicalCertificateAdhocFailedResultLogs(
   				$history_logs_not_updated
   			), 
   			'medical-certificate-fail-result-logs-' . date('Y-m-d') . time() . '.xlsx'
   		);
   	}
}