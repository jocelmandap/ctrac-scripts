<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Helpers\StringUtility;
use CTRAC\Model\Skills\SkillSetCategory;
use CTRAC\Model\Skills\SkillSet;
use CTRAC\Model\Skills\SkillsByCategory;

class SkillsController extends Controller
{
	protected $db_connection;

	/**
	 * @return void
	 */
	public function __construct()
	{
	    $this->db_connection = DatabaseConnection::CTRAC_QA;
	    if (config('app.env') != 'local')
	    {
	        $this->db_connection = DatabaseConnection::CTRAC_PROD;
	    }
	}

    public function viewUploader (SkillSetCategory $skill_category_class)
    {
    	/**
    	 * Get Skill Category List
    	 */
    	$skill_category_class->setDBConnection($this->db_connection);
    	$skill_category_list = $skill_category_class->getAllActive();

    	return view('skills.view', compact('skill_category_list'));
    }

    public function saveUploader (Request $request)
    {
    	$req_data = [];
    	$history_logs = [];
    	$total_added = 0;

    	$request->validate([
    		'skill_category' => 'required',
    		'upload_csv' => 'required'
    	]);

    	$file_name = time() . date('Y-m-d') . '-skills-uploads' . '.csv';
    	$request->upload_csv->storeAs('Skills', $file_name);
    	$skill_category_id = $request->skill_category;

    	if ($request->hasFile('upload_csv')) 
    	{
    		$file_path = storage_path('app/Skills/' . $file_name);
    		$csv_data = StringUtility::csvToArray($file_path);

    		if (!empty($csv_data))
    		{
    			$req_data = $csv_data;
    		}
    	}

    	if (!empty($req_data))
    	{
    		$skill_category_class = new SkillSetCategory();
    		$skill_category_class->setDBConnection($this->db_connection);
    		$category_detail = $skill_category_class->getById($skill_category_id);

    		if (!is_null($category_detail))
    		{
    			foreach ($req_data as $row) 
    			{
    				$datum = [];
    				$skill_class = new SkillSet();
    				$skill_class->setDBConnection($this->db_connection);
    				$skill_detail = $skill_class->getByName(htmlspecialchars($row['name']));

    				if (is_null($skill_detail))
    				{
    					$datum = [
    						'skill_name' => htmlspecialchars($row['name']),
    						'skill_description' => '',
    						'active' => 1
    					];

    					$skill_set_id = $skill_class->saveNew($datum);

    					if ($skill_set_id)
    					{
    						$skill_by_category_class = new SkillsByCategory();
    						$skill_by_category_class->setDBConnection($this->db_connection);
    						$result = $skill_by_category_class->saveNew([
    							'skill_category_id' => $category_detail->getId(),
    							'skill_set_id' => $skill_set_id
    						]);

    						if ($result)
    						{
    							$total_added++;
    							$history_logs[] = [
    								'success' => true,
    								'skill_name' => $row['name']
    							];
    						}
    					}
    				}
    			}

    			if ($total_added > 0)
    			{
    				session()->flash('success_flash', true);
    				session()->flash('message_flash', 'Successfully added new Skills! Total: ' . $total_added);
    				session()->flash('history_logs', $history_logs);

    				return redirect()->back();
    			}
    		}
    	}

    	session()->flash('success_flash', true);
    	session()->flash('message_flash', 'No new skills!');

    	return redirect()->back();
    }
}
