<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use CTRAC\Model\Scripts\CtracPage;
use CTRAC\Model\Scripts\CtracRole;
use Auth;
use Route;

class PagesController extends Controller
{
    public function index (CtracPage $page_class, CtracRole $role_class)
    {
        $page_list = $page_class->getAll();
        $role_list = $role_class->getAll();

    	return view('pages.view', 
    		compact(
    			'page_list',
    			'role_list'
    		)
    	);
    }

    public function savePage (Request $request, CtracPage $page_class)
    {
    	$request->validate([
    	    'name' => 'required',
    	    'route_name' => 'required',
    	    'role_id' => 'required'
    	]);

    	$route_name = $request->route_name;

    	if ($route_name != 'none')
    	{
    		if(!Route::has($route_name))
    		{
    			return back()->withErrors('Route name: ' . $route_name . ' does not exist.');
    		}
    	}

    	$data = [
    		'id' => $request->page_id,
    		'name' => $request->name,
    		'description' => $request->desc,
    		'route_name' => $route_name,
    		'order' => $request->ordering,
    		'is_active' => $request->is_active,
    		'parent_id' => $request->parent_id,
    		'icon_class' => $request->icon_class,
    		'bg_class' => $request->bg_class
    	];

    	$result = $page_class->savePage($data, false);

    	if ($result) 
    	{
    		
			if (isset($request->role_id) && !empty($request->role_id))
			{
				if (!is_null($request->page_id))
				{
					// Update User Role
					// Clear First all the Roles then add new roles
					if (!$result->roles->isEmpty())
					{
						$result->roles()->detach();
					}
					$result->roles()->attach($request->role_id);
				}
				else
				{
					// Add New User Role
					$result->roles()->attach($request->role_id);
				}
			}

    		$request->session()->flash('success_flash', true);
    		$request->session()->flash('message_flash', $request->name . ' page was successfully save!');
    		return redirect()->back();
    	}
    	$request->session()->flash('success_flash', false);
    	$request->session()->flash('message_flash', 'An error occur while saving it, please try again later!');

    	return redirect()->back();
    }

    public function deletePage(Request $request, CtracPage $page_class)
    {
    	if (!is_null($request->page_id))
    	{
    		$page = $page_class->getById($request->page_id);

    		if (!is_null($page))
    		{
    			$page->roles()->detach();

    			if ($page->delete())
    			{
    				$request->session()->flash('success_flash', true);
    				$request->session()->flash('message_flash', $request->name . ' was successfully deleted!');

    				return response()->json([
    					'status' => true,
    					'message' => 'Page was successfully deleted!'
    				]);
    			}
    		}

    		return response()->json([
    			'status' => false,
    			'message' => 'Page not available!'
    		]);
    	}

    	return response()->json([
    		'status' => false,
    		'message' => 'Empty Page Id!'
    	]);
    }
}
