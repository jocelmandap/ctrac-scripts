<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use CTRAC\Model\Gateways\Gateway;
use CTRAC\Helpers\DashboardLink;
use CTRAC\Helpers\StringUtility;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\Jobs\JobInt;
use Auth;

class GatewaysController extends Controller
{
    public function index ()
    {
    	$active_page = DashboardLink::CTRAC_GATEWAYS_PAGE;

    	return view('gateways.view', 
    		compact(
    			'active_page'
    		)
    	);
    }

    public function updateBulkGateway (Request $request)
    {
    	$gateway_data = [];
    	$history_logs = [];
    	$total_updates = 0;

    	$request->validate([
    		'upload_gateway_csv' => 'required'
    	]);

    	$file_name = time() . date('Y-m-d') . '-gateways-updates' . '.csv';
    	$request->upload_gateway_csv->storeAs('Gateways', $file_name);

    	if ($request->hasFile('upload_gateway_csv')) 
    	{
    		$file_path = storage_path('app/Gateways/' . $file_name);
    		$csv_data = StringUtility::csvToArray($file_path);

    		if (!empty($csv_data))
    		{
    			$gateway_data = $csv_data;
    		}
    	}

    	/**
    	 * Set DB Connection to Production
    	 */
    	$db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local') 
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }
        
    	if ($request->update_by == 'gateway')
        {
            if (!empty($gateway_data))
            {
                
                DB::connection($db_connection)->beginTransaction();
                try{
                    // Unapproved all gateways
                    DB::connection($db_connection)->table('gateways')->update(['is_approved' => 0]);

                    foreach ($gateway_data as $row) 
                    {
                        //Check gateway
                        $gatewayCode = DB::connection($db_connection)->table('gateways')->select()->where('gateway_code','=',$row['gateway_code'])->first();
                        //Find country
                        $country = DB::connection($db_connection)->table('countries')->select('country_code')
                            ->where('country_name','=',$row['country_name'])
                            ->orWhere('country_code','=',$row['country_name'])
                            ->first();
                        
                        if(!$gatewayCode){
                            $datum = [
                                'gateway_name' => $row['gateway_name'],
                                'gateway_code' => $row['gateway_code'],
                                'country_code' => $country->country_code,
                                'is_approved' => 1,
                                'stripe' => $row['stripe']
                            ];
                        }
                        else{
                            $datum = [
                                'is_approved' => 1,
                                'stripe' => $row['stripe']
                            ];
                        }
                        //Update or Insert gateway based on the csv
                        $updateGateway = DB::connection($db_connection)->table('gateways')->updateOrInsert([
                            'gateway_code' => $row['gateway_code']
                        ],$datum);
                        
                        if(!$updateGateway){
                            $history_logs[] = [
                                'success' => false,
                                'raw_data' => $row
                            ];
                            throw new \Exception('ERROR');
                        }

                        $history_logs[] = [
                            'success' => true,
                            'current_data' => [
                                'gateway_name' => $row['gateway_name'],
                                'gateway_code' => $row['gateway_code'],
                                'is_approved' => 1,
                                'stripe' => $row['stripe']
                            ],
                            'updated_data' => $datum,
                            'raw_data' => $row
                        ];

                        DB::connection($db_connection)->commit();
                    }
                }
                catch(\Exception $e) {
                    throw $e;
                    DB::connection($db_connection)->rollBack();
                }

                session()->flash('success_flash', true);
                session()->flash('update_by', $request->update_by);
                session()->flash('message_flash', 'Successfully updated Gateways! Total: ' . $total_updates);
                session()->flash('update_logs', $history_logs);

                return redirect()->back();
            }
        }

        if ($request->update_by == 'jobstripes')
        {
            if (!empty($gateway_data))
            {
                foreach ($gateway_data as $row) 
                {

                    $datum = [];
                    $job_code_class = new JobInt();
                    $job_code_class->setDBConnection($db_connection);
                    $job_code_details = $job_code_class->getAllByJobIntCode($row['job_code']);

                    if (!$job_code_details->isEmpty())
                    {

                        foreach ($job_code_details as $job_code_detail) 
                        {
                            $datum = [
                                'job_int_id' => $job_code_detail->getId(),
                                'stripe' => $row['stripes']
                            ];

                            if ($job_code_class->saveJobInt($datum))
                            {
                                $total_updates++;
                                $history_logs[] = [
                                    'success' => true,
                                    'current_data' => [
                                        'job_int_id' => $job_code_detail->getId(),
                                        'job_int_code' => $job_code_detail->getCode(),
                                        'stripe' => $job_code_detail->getStripe()
                                    ],
                                    'updated_data' => $datum,
                                    'raw_data' => $row
                                ];
                            }
                            else
                            {
                                $history_logs[] = [
                                    'success' => false,
                                    'current_data' => [
                                        'job_int_id' => $job_code_detail->getId(),
                                        'job_int_code' => $job_code_detail->getCode(),
                                        'stripe' => $job_code_detail->getStripe()
                                    ],
                                    'updated_data' => $datum,
                                    'raw_data' => $row
                                ];
                            }
                        }
                    }
                    else
                    {
                        $history_logs[] = [
                            'success' => false,
                            'raw_data' => $row
                        ];
                    }
                }

                session()->flash('success_flash', true);
                session()->flash('update_by', $request->update_by);
                session()->flash('message_flash', 'Successfully updated Job Stripes ! Total: ' . $total_updates);
                session()->flash('update_logs', $history_logs);

                return redirect()->back();
            }
        }

    	session()->flash('success_flash', true);
    	session()->flash('message_flash', 'No updates are made on Gateways Table');

    	return redirect()->back();
    }
}
