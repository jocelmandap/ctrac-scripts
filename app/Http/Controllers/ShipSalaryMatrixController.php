<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Helpers\StringUtility;
use CTRAC\Helpers\DateUtility;
use CTRAC\Model\Ships\ShipsSalaryMatrix;

class ShipSalaryMatrixController extends Controller
{
    public function view()
    {
    	return view('salary-matrix.view');
    }

    public function uploadMatrix (Request $request)
    {
    	$req_data = [];
    	$history_logs = [];
    	$total_new = 0;
    	$total_updated = 0;

    	$request->validate([
    		'upload_csv' => 'required'
    	]);

    	$file_name = time() . date('Y-m-d') . '-ship-salary-matrix-uploads' . '.csv';
    	$request->upload_csv->storeAs('salary-matrix', $file_name);

    	if ($request->hasFile('upload_csv')) 
    	{
    		$file_path = storage_path('app/salary-matrix/' . $file_name);
    		$csv_data = StringUtility::csvToArray($file_path);

    		if (!empty($csv_data))
    		{
    			$req_data = $csv_data;
    		}
    	}

    	if (!empty($req_data))
    	{
    		/**
    		 * Set DB Connection to Production
    		 */
    		$db_connection = DatabaseConnection::CTRAC_QA;
    		if (config('app.env') != 'local')
    		{
    		    $db_connection = DatabaseConnection::CTRAC_PROD;
    		}
    		$admin_id = 99999999999;
    		$city_id = 32250;
    		$currency_id = 144;
    		$country_code = 'PH';

    		foreach ($req_data as $row) 
    		{

    			$datum = [];
    			$ship_salary_class = new ShipsSalaryMatrix();
    			$ship_salary_class->setDBConnection($db_connection);
    			$ship_salary_details = $ship_salary_class->getByJobCode($row['job_int_code']);

    			/**
    			 * Insert new Ship Salary Matrix 
    			 * If Data not available in the table
    			 */
    			if ($ship_salary_details->isEmpty())
    			{
    				$datum = [
    					'ship_code' => $row['ship_code'],
    					'job_int_code' => $row['job_int_code'],
    					'country_code' => $country_code,
    					'city_id' => $city_id,
    					'currency_id' => $currency_id,
    					'hours_per_week' => $row['Basic Hours/week'],
    					'min_monthly_salary' => str_replace( ',', '', $row['min_monthly_salary'] ),
    					'guarantee_monthly_salary' => str_replace( ',', '', $row['guarantee_monthly_salary'] ),
    					'basic_monthly_salary' => str_replace( ',', '', $row['basic_monthly_salary'] ),
    					'initial_overtime' => $row['initial_overtime'],
    					'supplemental_overtime' => $row['supplemental_overtime'],
    					'overtime_rate' => $row['Overtime Rate after 303.1'],
    					'cba_vac_rate' => str_replace( ',', '', $row['cba_vac_rate'] ),
    					'contract_length_weeks' => $row['vacation_length_weeks'],
    					'vacation_length_weeks' => $row['contract_length_month'],
    					'tipping_position' => $row['tipping_position'],
    					'active_ind' => 1,
    					'created_by' => $admin_id
    				];

    				$new_ship_salary_class = new ShipsSalaryMatrix();
    				$new_ship_salary_class->setDBConnection($db_connection);
    				$new_ship_salary_class->fill($datum);

    				if ($new_ship_salary_class->save())
    				{
    					$total_new++;
    					$history_logs[] = [
    						'success' => true,
    						'ship_code' => $row['ship_code'],
    						'job_code' => $row['job_int_code']
    					];	
    				}
    			}
    			else
    			{
    				/**
    				 * Update Active Ship Salary Matrix
    				 * @param active_ind From 1 to 0
    				 */
    				$is_proceed = false;
    				foreach ($ship_salary_details as $ship_salary_detail) 
    				{
    					$ship_salary_detail->active_ind = 0;
    					$ship_salary_detail->last_updated_by = $admin_id;
    					$ship_salary_detail->save();
    					$is_proceed = true;
    				}

    				if ($is_proceed)
    				{
    					/**
    					 * Insert the Updated Ship Salary Matrix 
    					 */
    					$datum = [
    						'ship_code' => $row['ship_code'],
    						'job_int_code' => $row['job_int_code'],
    						'country_code' => $country_code,
    						'city_id' => $city_id,
    						'currency_id' => $currency_id,
    						'hours_per_week' => $row['Basic Hours/week'],
    						'min_monthly_salary' => str_replace( ',', '', $row['min_monthly_salary'] ),
                            'guarantee_monthly_salary' => str_replace( ',', '', $row['guarantee_monthly_salary'] ),
                            'basic_monthly_salary' => str_replace( ',', '', $row['basic_monthly_salary'] ),
    						'initial_overtime' => $row['initial_overtime'],
    						'supplemental_overtime' => $row['supplemental_overtime'],
    						'overtime_rate' => $row['Overtime Rate after 303.1'],
    						'cba_vac_rate' => $row['cba_vac_rate'],
    						'contract_length_weeks' => $row['vacation_length_weeks'],
    						'vacation_length_weeks' => $row['contract_length_month'],
    						'tipping_position' => $row['tipping_position'],
    						'active_ind' => 1,
    						'created_by' => $admin_id
    					];

    					$new_ship_salary_class = new ShipsSalaryMatrix();
    					$new_ship_salary_class->setDBConnection($db_connection);
    					$new_ship_salary_class->fill($datum);

    					if ($new_ship_salary_class->save())
    					{
    						$total_updated++;
    					}
    				}
    			}
    		}

    		session()->flash('success_flash', true);
    		session()->flash('message_flash', 'Successfully updated Ship Salary Matrix Total: ' . $total_updated . ' and new Ship Salary Matrix Total: ' . $total_new);
    		session()->flash('history_logs', $history_logs);
    		session()->flash('total_updated', $total_updated);
    		session()->flash('total_new', $total_new);

    		return redirect()->back();
    	}

    	session()->flash('success_flash', true);
    	session()->flash('message_flash', 'No new/updates for ship salary matrix!');

    	return redirect()->back();
    }
}