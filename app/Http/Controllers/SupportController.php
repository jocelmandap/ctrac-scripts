<?php

namespace CTRAC\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use CTRAC\Model\Users\User;
use CTRAC\Model\Users\MedicalApplicationUserAnswer;
use CTRAC\Model\Histories\HistoryRecord;
use CTRAC\Model\Jobs\JobApplication;
use CTRAC\Model\Jobs\JobRequest;
use CTRAC\Model\Requisitions\MedicalRequisition;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\Scripts\CtracSupportHistory;
use View;

class SupportController extends Controller
{
	public function rehireWithAccount(User $user_class)
	{
		return view('support.rehire.view');
	}

	public function saveRehireWithAccount(
		Request $request, 
		User $user_class, 
		JobApplication $job_application_class,
		CtracSupportHistory $support_history_class
	)
	{
		$jde_id = $request->jde_id;
		$jira = $request->jira;
		$app_env = $request->app_env;

		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if ($app_env == 'prod' && config('app.env') != 'local')
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}

		if (!is_null($jde_id))
		{
			$job_application_class->setDBConnection($db_connection);
			$support_history_class->setDBConnection($db_connection);
			$user_class->setDBConnection($db_connection);

			$user_details = $user_class->getByJDEId($jde_id);

			if (!is_null($user_details))
			{
				$job_application = $user_details->jobApplications()
												->where('status_code_id', '=', '17000')
												->first();

				if (!is_null($job_application))
				{
					$history_record_details = $job_application->historyRecords()
																->orderBy('history_record_id', 'desc')
																->first();

					if (!is_null($history_record_details))
					{
						$result_message = [
							'job_application_update' => 'Job Application Id# '. $job_application->getId() . ' did not update well',
							'user' => 'User user group did not update',
							'history_record_update' => 'Did not update the history record id #' . $history_record_details->getId(),
							'history_record_insert' => 'Did not insert history record message: Adhoc Support: Rehire (JIRA Ticket: ' . $jira . ')'
						];

						$date_time = date('Y-m-d H:i:s');

						/**
						 * Update Job Application to Approve to candidate status(1500)
						 */
						$job_application_data = [
							'job_application_id' => $job_application->getId(),
							'status_code_id' => '1500',
							'status_code_date' => date('Y-m-d'),
							'application_last_modified' => $date_time
						];

						if ($job_application_class->saveJobApplication($job_application_data) !== false)
						{
							$result_message['job_application_update'] = 'Job Application Id# '. $job_application->getId() . ' status change to Approve to Candidate (1500)';
						}

						/**
						 * Change User user group from Crew (17) to Applicant (15)
						 */
						$user_update_data = [
							'user_id' => $user_details->getId(),
							'hp_register_company' => '',
							'source_id' => ($user_details->getCountryCode() == 'PH') ? 171 : 0,
							'user_group_id' => 15
						];

						if ($user_class->saveUserData($user_update_data) !== false)
						{
							$result_message['user'] = 'User group from Crew (17) is updated to Applicant (15)';
						}

						$history_record_class = new HistoryRecord();
						$history_record_class->setDBConnection($db_connection);

						/**
						 * Update Latest History Record of the Job Application
						 */
						$history_record_update_data = [
							'history_record_id' => $history_record_details->getId(),
							'active' => 0,
							'deleted_by' => 99999999999
						];

						if ($history_record_class->saveHistoryRecord($history_record_update_data) !== false)
						{
							$result_message['history_record_update'] = 'Updated history record id# ' . $history_record_details->getId() . ' successfully';
						}

						/**
						 * Insert new History Record for Rehire record
						 */
						$history_record_data = [
							'job_application_id' => $job_application->getId(),
							'user_id' => $user_details->getId(),
							'status_code_id' => 1500,
							'admin_id' => 99999999999,
							'history_record_date' => $date_time,
							'history_record_notes' => 'Adhoc Support: Rehire (JIRA Ticket: ' . $jira . ')',
							'history_record_alert' => '',
							'active' => 1,
							'deleted_by' => 0
						];

						if ($history_record_class->saveHistoryRecord($history_record_data) !== false)
						{
							$result_message['history_record_insert'] = 'Added new history record for adhoc rehire';
						}

						/**
						 * Insert Support History Record
						 */
						$support_history_data = [
							'user_id' => $user_details->getId(),
							'ctrac_support_history_type_id' => 1, // Rehire Type
							'note' => 'Adhoc Support: Rehire (JIRA Ticket: ' . $jira . ')',
							'ctrac_user_email' => Auth::user()->getEmail(),
							'created_at' => $date_time,
							'created_by' => Auth::user()->getId()
						];

						if ($support_history_class->saveSupportHistory($support_history_data) !== false)
						{
							$result_message['history_support'] = 'Added new support history record for adhoc rehire';
						}

						return response()->json([
							'status' => true,
							'result_messages' => $result_message,
							'message' => 'Successfully updated the Crew for rehire!'
						]);
					}

					return response()->json([
						'status' => false,
						'message' => $jde_id . ' does not have any history record found'
					]);
				}

				return response()->json([
					'status' => false,
					'message' => $jde_id . ' does not have any job application in Crew status or 17000'
				]);
			}

			return response()->json([
				'status' => false,
				'message' => $jde_id . ' is not a valid JDE ID. Please enter a new one.'
			]);
		}

		return response()->json([
			'status' => false,
			'message' => 'Please enter JDE'
		]);
	}

	public function revertBlockCountries()
	{
		return view('support.block-countries.view');
	}

	public function revertBlockCountriesSubmit(Request $request, User $user_class)
	{

		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		
		} 
		$user_class->setDBConnection($db_connection);
		$user = $user_class->getByEmail($request->email);
		if (!is_null($user))
		{
		   $job_application = $user->jobApplications()
										   ->where('status_code_id', '=', '6908')
										   ->get();

		   /**
			* Job Application Details
			* Render a HTML form
			*/
		   $job_application_content = View::make('partials.block-countries.job-applications', [
			   'job_applications' => $job_application,
			   'user_details' => $user
		   ])->render();
		   return response()->json([
			   'status' => true,
			   'job_application_content' =>  $job_application_content
		   ]); 
		}
		return response()->json([
			   'status' => false,
			   'message' =>  'No Record Found'
		   ]); 
		
	}

	public function revertBlockCountriesUpdate(
		Request $request,
		JobApplication $job_application_class,
		CtracSupportHistory $support_history_class
	)
	{
		$job_id = $request->job_id;
		$jira = $request->jira;
		$comment = $request->comment;

		if (!is_null($job_id))
		{
			$result_message = [
				'job_application_update' => 'Job Application Id# '. $job_id . ' did not update well',
				'history_record_update' => 'Did not update the history record',
				'history_record_insert' => 'Did not insert history record',
				'support_history_insert' => 'Did not insert support history record'
			];

			/**
			 * Set DB Connection to Production
			 */
			$db_connection = DatabaseConnection::CTRAC_QA;
			if (config('app.env') != 'local')
			{
				$db_connection = DatabaseConnection::CTRAC_PROD;
			}

			$job_application_class->setDBConnection($db_connection);
			$support_history_class->setDBConnection($db_connection);

			$job_application = $job_application_class->getById($job_id);

			if (!is_null($job_application))
			{
				$history_record_details = $job_application->historyRecords()
															->orderBy('history_record_id', 'desc')
															->first();

				if (!is_null($history_record_details))
				{

					/**
					 * Update Job App
					 */
					$data = [
						'job_application_id' => $job_id,
						'status_code_id' => 1000,
						'status_code_date' => date('Y-m-d'),
						'application_last_modified' => date('Y-m-d H:i:s')
					];

					$result = $job_application_class->saveJobApplication($data, true);

					if ($result !== false)
					{
						$result_message['job_application_update'] = 'Job application ID#' . $job_id . ' was successfully updated';
					}

					$history_record_class = new HistoryRecord();
					$history_record_class->setDBConnection($db_connection);

					/**
					 * Update Latest History Record of the Job Application
					 */
					$history_record_update_data = [
						'history_record_id' => $history_record_details->getId(),
						'active' => 0,
						'deleted_by' => 99999999999
					];

					$history_record_update_result = $history_record_class->saveHistoryRecord($history_record_update_data);

					if ($history_record_update_result !== false)
					{
						$result_message['history_record_update'] = 'History Record Id#' . $history_record_details->getId() . ' was successfully updated';
					}

					/**
					 * Insert new History Record for Rehire record
					 */
					 
					$history_record_data = [
						'job_application_id' => $job_application->getId(),
						'user_id' => $job_application->getUserId(),
						'status_code_id' => 1000,
						'admin_id' => 99999999999,
						'history_record_date' => date('Y-m-d H:i:s'),
						'history_record_notes' => 'Adhoc Support: Bypass non-preferred country. ' . $comment . '  (JIRA Ticket: ' . $jira . ')',
						'history_record_alert' => '',
						'active' => 1,
						'deleted_by' => 0
					];

					$insert_history_record_result = $history_record_class->saveHistoryRecord($history_record_data);

					if ($insert_history_record_result !== false)
					{
						$result_message['history_record_insert'] = 'History Record Inserted Successfully ' . $insert_history_record_result;
					}

					/**
					 * Insert Support History Record
					 */
					$support_history_data = [
						'user_id' => $job_application->getUserId(),
						'ctrac_support_history_type_id' => 2, // Revert Failed Block Countries
						'note' => 'Adhoc Support: Bypass non-preferred country. ' . $comment . '  (JIRA Ticket: ' . $jira . ')',
						'ctrac_user_email' => Auth::user()->getEmail(),
						'created_at' => date('Y-m-d H:i:s'),
						'created_by' => Auth::user()->getId()
					];


					$support_history_result = $support_history_class->saveSupportHistory($support_history_data);

					if ($support_history_result !== false)
					{
						$result_message['support_history_insert'] = 'Adhoc Support: Bypass non-preferred country. ' . $comment . '  (JIRA Ticket: ' . $jira . ')';
					}

					/**
					 * Highright Crew Requests
					 * Render a HTML form
					 */
					$results_content = View::make('partials.block-countries.results-container', [
						'result_message' => $result_message,
						'result_status' => true
					])->render();

					return response()->json([
						'status' => true,
						'results' => $results_content
					]);
				}

				/**
				 * Highright Crew Requests
				 * Render a HTML form
				 */
				$results_content = View::make('partials.block-countries.results-container', [
					'result_message' => $result_message,
					'result_status' => false
				])->render();

				return response()->json([
					'status' => false,
					'results' => $results_content
				]);
			}
		}

		return response()->json([
			'status' => false
		]);
	}

	public function addHRSDTypeView(User $user_class)
	{
		return view('support.hrsd-type.view');
	}

	public function viewPersonalInfo ()
	{
		return view('support.personal-info.view');
	}

	public function searchPersonalInfo (Request $request, User $user_class)
	{
		$search = $request->search;
		$search_by = $request->search_by;
		$jira = $request->jira;

		/**
		 * Set DB Connection to Production
		 */
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		}

		$user_class->setDBConnection($db_connection);

		if ($search_by == 'jde_id')
		{
			$user_details = $user_class->getByJDEId($search);            
		}
		else
		{
			$user_details = $user_class->getByEmail($search);
		}

		if (!is_null($user_details))
		{
			$personal_data = $user_details->personalData;

			if (!is_null($personal_data))
			{
				/**
				 * User Personal Data
				 * Render a HTML form
				 */
				$personal_data_content = View::make('partials.personal-data.personal-details-data', [
					'user_details' => $user_details,
					'personal_data' => $personal_data
				])->render();

				return response()->json([
					'status' => true,
					'results' => $personal_data_content
				]);
			}
		}

		dd($user_details->personalData);
	}

	public function bypassMedicalExam()
	{
		return view('support.bypass-medical-exam.view');
	}

	public function bypassMedicalExamSubmit(Request $request, User $user_class, MedicalRequisition $medical_requisition_class)
	{
		$db_connection = DatabaseConnection::CTRAC_QA;
		if (config('app.env') != 'local')
		{
			$db_connection = DatabaseConnection::CTRAC_PROD;
		
		}

		$user_class->setDBConnection($db_connection);
		$user = $user_class->getByEmail($request->email);
		$medical_requisition_class->setDBConnection($db_connection);

		$requisitions = $medical_requisition_class->getAllActive();
		$job_request_ids = [];
		if (!$requisitions->isEmpty()) {
			foreach ($requisitions as $requisition) {
				$job_request_ids[] = $requisition->getJobRequestId();
			}
		}

		if (!is_null($user))
		{
		   $job_application = $user->jobApplications()
										   ->whereIn('job_request_id', $job_request_ids)
										   ->get();
		   /**
			* Job Application Details
			* Render a HTML form
			*/
		   $job_application_content = View::make('partials.bypass-medical-exam.job-applications', [
			   'job_applications' => $job_application,
			   'user_details' => $user
		   ])->render();
		   
		   return response()->json([
			   'status' => true,
			   'job_application_content' =>  $job_application_content
		   ]); 
		}
		return response()->json([
			   'status' => false,
			   'message' =>  'No Record Found'
		   ]); 
	}

	public function bypassMedicalExamUpdate(
		Request $request,
		MedicalApplicationUserAnswer $medical_application_user_answer_class,
		JobApplication $job_application_class,
		CtracSupportHistory $support_history_class
	)
	{
		$job_id = $request->job_id;
		$jira = $request->jira;

		if (!is_null($job_id))
		{
			$result_message = [
				'medical_exam_answer' => 'Job Application Id : # '. $job_id . ' - Medical exam did not bypassed',
				'support_history_insert' => 'Did not insert support history record'
			];

			/**
			 * Set DB Connection to Production
			 */
			$db_connection = DatabaseConnection::CTRAC_QA;
			if (config('app.env') != 'local')
			{
				$db_connection = DatabaseConnection::CTRAC_PROD;
			}

			$medical_application_user_answer_class->setDBConnection($db_connection);
			$support_history_class->setDBConnection($db_connection);
			$job_application_class->setDBConnection($db_connection);
			$job_application = $job_application_class->getById($job_id);

			$exam = $medical_application_user_answer_class->getByJobAppId($job_id);

			if(!is_null($exam))
			{

				$data = [
						'id' => $exam->getId(),
						'is_passed' => 1,
						'is_exam_completed' => 1,		
						'modified_date' => date('Y-m-d H:i:s'),
						'bypassed_exam' => 1
					];

				$result = $medical_application_user_answer_class->saveChanges($data, true);

				$result_message['medical_exam_answer'] = 'Job Application Id : # '. $job_id . ' - Medical exam was successfully bypassedss';
			}
			else
			{

				$medical_application_user_answer_class = new MedicalApplicationUserAnswer();
				$medical_application_user_answer_class->setDBConnection($db_connection);

				$data = [
						'user_id' => $job_application->getUserId(),
						'job_application_id' => $job_id,
						'is_passed' => 1,
						'is_exam_completed' => 1,
						'answers' => '{\n  \"1\": \"\",\n  \"2\": \"\",\n  \"3\": \"\",\n  \"4\": \"\",\n  \"5\": \"\",\n  \"6\": \"\",\n  \"7\": \"\",\n  \"8\": \"\",\n  \"9\": \"\",\n  \"10\": \"\",\n  \"11\": \"\",\n  \"12\": \"\",\n  \"13\": \"\",\n  \"14\": \"\",\n  \"15\": \"\",\n  \"16\": \"\",\n  \"17\": \"\",\n  \"18\": \"\",\n  \"19\": \"\",\n  \"20\": \"\",\n  \"21\": \"\",\n  \"22\": \"\",\n  \"23\": \"\",\n  \"24\": \"\",\n  \"25\": \"\",\n  \"26\": \"\",\n  \"27\": \"\",\n  \"28\": \"\",\n  \"29\": \"\",\n  \"30\": \"\",\n  \"31\": \"\",\n  \"32\": \"\",\n  \"33\": \"\",\n  \"34\": \"\",\n  \"35\": \"\",\n  \"36\": \"\",\n  \"37\": \"\",\n  \"38\": \"\",\n  \"39\": \"\",\n  \"40\": \"\",\n  \"42\": \"\",\n  \"43\": \"\",\n  \"44\": \"\",\n  \"45\": \"\",\n  \"46\": \"\",\n  \"47\": \"\",\n  \"48\": \"\",\n  \"49\": \"\",\n  \"50\": \"\",\n  \"51\": \"\",\n  \"52\": \"\",\n  \"53\": \"\",\n  \"54\": \"\",\n  \"55\": \"\",\n  \"56\": \"\",\n  \"57\": \"\",\n  \"58\": \"\",\n  \"59\": \"\",\n  \"60\": \"\",\n  \"61\": \"\"\n}',
						'current_exam_time' => 0.00000,
						'created_date' => date('Y-m-d H:i:s'),
						'modified_date' => date('Y-m-d H:i:s'),
						'bypassed_exam' => 1
					];


				$result = $medical_application_user_answer_class->saveNew($data, true);

				$result_message['medical_exam_answer'] = 'Job Application Id : # '. $job_id . ' - Medical exam was successfully bypassed';
			}

			/**
			 * Insert Support History Record
			 */
			$support_history_data = [
				'user_id' => $job_application->getUserId(),
				'ctrac_support_history_type_id' => 9, // Bypass Medical Exam
				'note' => 'Adhoc Support: Bypassed Medical Exam  (JIRA Ticket: ' . $jira . ')',
				'ctrac_user_email' => Auth::user()->getEmail(),
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => Auth::user()->getId()
			];


			$support_history_result = $support_history_class->saveSupportHistory($support_history_data);

			if ($support_history_result !== false)
			{
				$result_message['support_history_insert'] = 'Adhoc Support: Bypassed Medical Exam  (JIRA Ticket: ' . $jira . ')';
			}

			/**
			 * Highright Crew Requests
			 * Render a HTML form
			 */
			$results_content = View::make('partials.bypass-medical-exam.results-container', [
				'result_message' => $result_message,
				'result_status' => true
			])->render();

			return response()->json([
				'status' => true,
				'results' => $results_content
			]);
		}
	}
}
