<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;

class DocumentChecklist extends Model
{
	protected $fillable = [
		'ck_list_code',
		'ck_list_name',
		'ck_list_desc',
		'ck_list_html_desc',
		'ck_type_id',
		'ck_country_ind',
		'ck_upload_ind',
		'applicant_type_ids',
		'ck_filename',
		'ck_online_form',
		'ck_exp_date',
		'ck_has_exp',
		'html_view',
		'ck_crew_fill_ind',
		'ck_admin_fill_ind',
		'ck_US_only',
		'ck_form_model',
		'ck_show_ind',
		'ck_check_e1',
		'communication_id',
		'communication_hp_type',
		'req_status_code_id',
		'document_order',
		'show_dtl_status_ind',
		'ck_not_US',
		'document_downloadable',
		'admin_html_view',
		'offer_letter_ind',
		'admin_view_ind',
		'upload_form_ind',
		'show_crew_view',
		'fetch_data_ind',
		'fetch_data_func',
		'disclosure_ind',
		'disclosure_us_ind',
		'form_flds_crew_ind',
		'ct_ind',
		'use_form_flds_ind',
		'use_app_status_ind',
		'mult_appointment_ind',
		'mult_appointment_option',
		'use_header_status_ind',
		'doc_group_id',
		'use_form_model',
		'created_on',
		'created_by',
		'last_updated_on',
		'last_updated_by'
	];

	const CREATED_AT = 'created_on';
	const UPDATED_AT = 'last_updated_on';

    protected $table = 'document_checklist';

    protected $primaryKey = 'ck_list_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Document Group List Details
     */
    public function docGroup()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentChecklistGroup', 'doc_group_id', 'doc_group_id');
    }

    public function getId()
    {
    	return $this->ck_list_id;
    }

    public function getCode()
    {
    	return $this->ck_list_code;
    }

    public function getName()
    {
    	return $this->ck_list_name;
    }

    public function getDesc()
    {
    	return $this->ck_list_desc;
    }

    public function getById($ck_list_id)
    {
    	$result = $this->where('ck_list_id', '=', $ck_list_id)
    					->first();
    	return $result;
    }

    public function getAllOFferLetter()
    {
    	$result = $this->where('offer_letter_ind', '=', 1)
    					->get();
    	return $result;
    }
}
