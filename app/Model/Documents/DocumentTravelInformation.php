<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;

class DocumentTravelInformation extends Model
{
	protected $fillable = [
		'travel_info_id',
		'user_id',
		'assignment_date',
		'purchased_by_company',
		'local_ground_transpo_ind',
		'airline_id',
		'flight_number',
		'record_locator',
		'departure_country',
		'arrival_country',
		'arrival_date',
		'arrival_time',
		'departure_date',
		'departure_time',
		'arrival_airport_id',
		'departure_airport_id',
		'locked_ind',
		'filename',
		'file_path',
		'sequence_number',
		'tm_migrated',
		'document_status_id',
		'created_date',
		'created_by',
		'modified_date',
		'modified_by'
	];

    protected $table = 'document_travel_information';

    protected $primaryKey = 'travel_info_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get User Details
     */
    public function user()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'user_id');
    }

    /**
     * Get User Details
     */
    public function airline()
    {
        return $this->hasOne('CTRAC\Model\Air\Airline', 'airline_id', 'airline_id');
    }

    /**
     * Get User Modified Details
     */
    public function createdBy()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'created_by');
    }

    /**
     * Get User Modified Details
     */
    public function modifiedBy()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'modified_by');
    }

    public function getId()
    {
    	return $this->travel_info_id;
    }

    public function getById($travel_info_id)
    {
    	$result = $this->where('travel_info_id', '=', $travel_info_id)
    					->first();
    	return $result;
    }
}
