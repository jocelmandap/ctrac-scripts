<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;

class DocumentChecklistGroup extends Model
{
	protected $fillable = [
		'doc_group_code',
		'doc_group_name',
		'doc_group_category_id',
		'created_date'
	];

    protected $primaryKey = 'doc_group_id';

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Document Type Details
     */
    public function docType()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentChecklist', 'doc_group_id', 'doc_group_id');
    }

    public function getId()
    {
    	return $this->doc_group_id;
    }
}
