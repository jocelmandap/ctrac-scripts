<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;
use CTRAC\Model\Common\CRUDInterface;
use CTRAC\Helpers\DateUtility;
use Illuminate\Support\Facades\DB;

class DocumentChecklistUpload extends Model
implements CRUDInterface
{
    protected $fillable = [
    	'user_id',
    	'ck_list_id',
    	'filename',
    	'file_path',
    	'status',
    	'comment',
    	'last_updated_by',
    	'last_updated_on',
    	'created_on',
    	'issued_date',
    	'expiration_date',
    	'appointment_date',
    	'issued_country',
    	'document_number',
    	'visa_type_id',
    	'medical_facility_id',
    	'training_type_id',
    	'training_facility',
    	'training_code',
    	'training_description',
    	'training_trainer_name',
    	'training_position',
    	'job_request_id',
    	'job_int_id',
    	'src_position',
    	'clearance_id',
    	'locked_ind',
    	'newly_added',
    	'newly_updated',
    	'e1_migration',
    	'e1_date_updated',
    	'is_primary',
    	'triton_ind',
    	'document_status_id'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Ck List Details
     */
    public function ckList()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentChecklist', 'ck_list_id', 'ck_list_id');
    }

    /**
     * Get Document Status
     */
    public function docStatus()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentFormUploadsStatus', 'doc_status_id', 'document_status_id');
    }

    /**
     * Get User Details
     */
    public function user()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'user_id');
    }

    /**
     * Get last update User Details
     */
    public function lastUserUpdate()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'last_updated_by');
    }
    
    public function getId()
    {
    	return $this->id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getCkListId()
    {
    	return $this->ck_list_id;
    }

    public function getFilename()
    {
    	return $this->filename;
    }

    public function getFilePath()
    {
    	return $this->file_path;
    }

    public function getStatus()
    {
    	return $this->status;
    }

    public function isLock()
    {
        return $this->locked_ind;
    }

    public function getCreatedOn($is_raw = true, $date_format = 'F d, Y h:i A')
    {
        if (!$is_raw && !is_null($this->created_on))
        {
            return DateUtility::changeDateFormat($this->created_on, 'Y-m-d H:i:s', $date_format);
        }
        return $this->created_on;
    }

    public function getUpdatedOn($is_raw = true, $date_format = 'F d, Y h:i A')
    {
        if (!$is_raw && !is_null($this->last_updated_on))
        {
            return DateUtility::changeDateFormat($this->last_updated_on, 'Y-m-d H:i:s', $date_format);
        }
        return $this->last_updated_on;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->get();
    	return $result;
    }

    public function getByCkListId($ck_list_id, $limit = '')
    {
        $result = $this->where('ck_list_id', '=', $ck_list_id);

        if (!empty($limit))
        {
            $result->offset(0)
                   ->limit($limit);
        }
                        
        return $result->get();
    }

    public function getByUserIdAndCkListId($user_id, $ck_list_id)
    {
        $result = $this->where('user_id', '=', $user_id)
                        ->where('ck_list_id', '=', $ck_list_id)
                        ->get();
        return $result;
    }

    public function getByFilePathAndCkListId($file_path, $ck_list_id)
    {
        $result = $this->where('file_path', '=', $file_path)
                        ->where('ck_list_id', '=', $ck_list_id)
                        ->first();
        return $result;
    }

    public function saveNew($data, $is_return_id)
    {
	   $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
	   $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveDocumentChecklistUploadData($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }

    public function removeById($id)
    {
    	return null;
    }

    public function getNewHireDocumentStatusList (
        $document_types = [],
        $document_statuses = [],
        $from_date_modified = '',
        $to_date_modified = '',
        $jde_ids = []
    )
    {
        return $this->select(
                        "u.jde_id AS Employee ID",
                        DB::raw("CONCAT(pd.first_name, ' ', pd.last_name) AS `Employee Name`"),
                        "u.country_code AS Nationality",
                        "ji.job_int_code AS Job Code",
                        "u.email AS Employee Email Address",
                        "dc.ck_list_name AS Document Name",
                        DB::raw(
                            "CASE
                                WHEN document_checklist_uploads.document_status_id = 1 THEN 'Pending Document Approval'
                                WHEN document_checklist_uploads.document_status_id = 2 THEN 'Completed'
                                WHEN document_checklist_uploads.document_status_id = 3 THEN 'Rejected'
                                WHEN document_checklist_uploads.document_status_id = 5 THEN 'Audited'
                                ELSE 'Document Not Submitted'
                            END AS `Document Status`"
                        ),
                        DB::raw(
                            "CASE
                                WHEN dc.ck_list_name = 'STCW' THEN dtct.cert_type_name
                                WHEN dc.ck_list_name = 'Visa' THEN vt.visa_type_name
                                ELSE 'N/A'
                            END AS `Document Specific Name`"
                        ),
                        DB::raw("CONCAT(apd.first_name, ' ', apd.last_name) AS `Modified By`"),
                        "document_checklist_uploads.last_updated_on AS Date Uploaded / Completed",
                        "document_checklist_uploads.issued_date AS Date of Issuance",
                        "document_checklist_uploads.expiration_date AS Date of Expiration"
                        // "oa.assignment_date AS Joining Date",
                        // "s.ship_name AS Joining Vessel"
                    )
                    ->join('users AS u', 'document_checklist_uploads.user_id', '=', 'u.user_id')
                    ->join('personal_data AS pd', 'u.personal_data_id', '=', 'pd.personal_data_id')
                    ->join('document_checklist_form_uploads AS dcfu', function ($join) {
                        $join->on('document_checklist_uploads.user_id', '=', 'dcfu.user_id')
                             ->on('document_checklist_uploads.ck_list_id', '=', 'dcfu.ck_list_id');
                    })
                    ->join('document_checklist AS dc', 'document_checklist_uploads.ck_list_id', '=', 'dc.ck_list_id')
                    ->join('users AS ua', 'document_checklist_uploads.last_updated_by', '=', 'ua.user_id')
                    ->join('personal_data AS apd', 'ua.personal_data_id', '=', 'apd.personal_data_id')
                    // ->leftJoin('onboard_assignments AS oa', function ($join) {
                    //     $join->on('document_checklist_uploads.user_id', '=', 'oa.user_id' )
                    //          ->where('oa.assignment_date', '=', DB::raw("(select max(`assignment_date`) from onboard_assignments where user_id = document_checklist_uploads.user_id)"));
                    // })
                    // ->leftJoin('ships AS s', 'oa.ship_id', '=', 's.ship_id')
                    // ->leftJoin('job_applications AS ja', function ($join) {
                    //     $join->on('oa.job_application_id', '=', 'ja.job_application_id')
                    //          ->orOn('dcfu.job_application_id', '=', 'ja.job_application_id');
                    // })
                    ->leftJoin('job_applications AS ja', 'dcfu.job_application_id', '=', 'ja.job_application_id')
                    ->leftJoin('job_int AS ji', 'ja.job_int_id', '=', 'ji.job_int_id')
                    ->leftJoin('document_training_certificate_type AS dtct', 'document_checklist_uploads.training_type_id', '=', 'dtct.cert_type_id')
                    ->leftJoin('visa_type AS vt', 'document_checklist_uploads.visa_type_id', '=', 'vt.visa_type_id')
                    ->whereIn('u.jde_id', $jde_ids)
                    ->whereIn('document_checklist_uploads.ck_list_id', $document_types)
                    ->whereIn('document_checklist_uploads.document_status_id', $document_statuses)
                    ->where('document_checklist_uploads.last_updated_on', '>=', $from_date_modified)
                    ->where('document_checklist_uploads.last_updated_on', '<=', $to_date_modified)
                    ->get();
    }
}
