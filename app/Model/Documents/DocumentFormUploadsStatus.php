<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;

class DocumentFormUploadsStatus extends Model
{
    protected $fillable = [
    	'doc_status_code',
    	'doc_status_name',
    	'doc_status_image',
    	'doc_completed_ind',
    	'doc_locked_ind',
    	'doc_uploaded_req',
    	'doc_allow_audit',
    	'doc_background_ind',
    	'doc_crew_note',
    	'doc_show_completed_ind',
    	'doc_req_comment_ind',
    	'doc_show_coord_note',
    	'doc_show_dtl_stat_ind',
    	'doc_show_detail_status',
    	'created_by',
    	'created_on',
    	'last_updated_by',
    	'last_updated_on',
    	'doc_use_alternate_ind',
    	'doc_alternate_status_name',
    	'doc_alternate_status_image',
    	'communication_id'
    ];

    public $timestamps = false;

    protected $table = 'document_form_uploads_status';

    protected $primaryKey = 'doc_status_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->doc_status_id;
    }

    public function getDocStatusCode()
    {
    	return $this->doc_status_code;
    }

    public function getDocStatusName()
    {
    	return $this->doc_status_name;
    }

    public function getDocStatusImage()
    {
    	return $this->doc_status_image;
    }

    public function getById($doc_status_id)
    {
    	$result = $this->where('doc_status_id', '=', $doc_status_id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	$result = $this->where('doc_status_id', '!=', 0)
                        ->get();
        return $result;
    }
}
