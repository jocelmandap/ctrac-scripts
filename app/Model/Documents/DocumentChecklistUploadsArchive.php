<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;
use CTRAC\Helpers\DateUtility;

class DocumentChecklistUploadsArchive extends Model
{
    protected $fillable = [
    	'id',
    	'user_id',
    	'ck_list_id',
    	'filename',
    	'file_path',
    	'status',
    	'comment',
    	'last_updated_by',
    	'last_updated_on',
    	'created_on',
    	'issued_date',
    	'expiration_date',
    	'appointment_date',
    	'issued_country',
    	'document_number',
    	'visa_type_id',
    	'medical_facility_id',
    	'training_type_id',
    	'training_facility',
    	'training_code',
    	'training_description',
    	'training_trainer_name',
    	'training_position',
    	'job_request_id',
    	'job_int_id',
    	'src_position',
    	'clearance_id',
    	'locked_ind',
    	'newly_added',
    	'newly_updated',
    	'e1_migration',
    	'e1_date_updated',
    	'is_primary',
    	'triton_ind',
    	'document_status_id'
    ];

    public $timestamps = false;

    protected $table = 'document_checklist_uploads_archives';

    protected $primaryKey = 'archive_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Ck List Details
     */
    public function ckList()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentChecklist', 'ck_list_id', 'ck_list_id');
    }

    /**
     * Get Document Status
     */
    public function docStatus()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentFormUploadsStatus', 'doc_status_id', 'document_status_id');
    }

    /**
     * Get User Details
     */
    public function user()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'user_id');
    }

    /**
     * Get last update User Details
     */
    public function lastUserUpdate()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'last_updated_by');
    }

    /**
     * Get Training Details
     */
    public function training()
    {
        return $this->hasOne('CTRAC\Model\Trainings\DocumentTrainingCertificateType', 'cert_type_id', 'training_type_id');
    }
    
    public function getId()
    {
    	return $this->archive_id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getCkListId()
    {
    	return $this->ck_list_id;
    }

    public function getFilename()
    {
    	return $this->filename;
    }

    public function getFilePath()
    {
    	return $this->file_path;
    }

    public function getStatus()
    {
    	return $this->status;
    }

    public function getTrainingTypeId()
    {
        return $this->training_type_id;
    }

    public function getTrainingFacility()
    {
        return $this->training_facility;
    }

    public function getIssuedCountry()
    {
        return $this->issued_country;
    }

    public function getTrainingCode()
    {
        return $this->training_code;
    }

    public function getTrainingDesc()
    {
        return $this->training_description;
    }

    public function getTrainingTrainerName()
    {
        return $this->training_trainer_name;
    }

    public function getIssueDate($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->issued_date))
        {
            return DateUtility::changeDateFormat($this->issued_date, 'Y-m-d', $date_format);
        }
        return $this->issued_date;
    }

    public function getExpirationDate($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->expiration_date))
        {
            return DateUtility::changeDateFormat($this->expiration_date, 'Y-m-d', $date_format);
        }
        return $this->expiration_date;
    }

    public function getCreatedOn($is_raw = true, $date_format = 'F d, Y h:i A')
    {
        if (!$is_raw && !is_null($this->created_on))
        {
            return DateUtility::changeDateFormat($this->created_on, 'Y-m-d H:i:s', $date_format);
        }
        return $this->created_on;
    }

    public function getUpdatedOn($is_raw = true, $date_format = 'F d, Y h:i A')
    {
        if (!$is_raw && !is_null($this->last_updated_on))
        {
            return DateUtility::changeDateFormat($this->last_updated_on, 'Y-m-d H:i:s', $date_format);
        }
        return $this->last_updated_on;
    }

    public function getById($archive_id)
    {
    	$result = $this->where('archive_id', '=', $archive_id)
    					->first();
    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->get();
    	return $result;
    }

    public function saveNew($data, $is_return_id)
    {
	   $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
	   $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function removeById($id)
    {
    	return null;
    }
}
