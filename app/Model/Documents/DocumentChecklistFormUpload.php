<?php

namespace CTRAC\Model\Documents;

use Illuminate\Database\Eloquent\Model;
use CTRAC\Model\Common\CRUDInterface;
use CTRAC\Helpers\DateUtility;
use Illuminate\Support\Facades\DB;

class DocumentChecklistFormUpload extends Model
implements CRUDInterface
{
    protected $fillable = [
    	'user_id',
    	'ck_list_id',
    	'is_completed',
    	'document_comment',
    	'document_child_status_id',
    	'doc_prev_status',
    	'date_created',
    	'date_modified',
    	'created_by',
    	'modified_by',
    	'candidate_type',
    	'processed_by',
    	'job_application_id',
    	'date_reviewed',
    	'reviewed_by',
    	'reviewed_ind',
    	'triton_ind',
    	'note',
    	'job_int_id',
    	'accepted_date',
    	'declined_date',
    	'locked_agreement_ind'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Ck List Details
     */
    public function ckList()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentChecklist', 'ck_list_id', 'ck_list_id');
    }

    /**
     * Get User Details
     */
    public function user()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'user_id');
    }

    /**
     * Get User Modified Details
     */
    public function createdBy()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'created_by');
    }

    /**
     * Get User Modified Details
     */
    public function modifiedBy()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'modified_by');
    }

    /**
     * Get Job Applications Details
     */
    public function jobApplications()
    {
        return $this->hasOne('CTRAC\Model\Jobs\JobApplication', 'job_application_id', 'job_application_id');
    }

    /**
     * Get Document Status
     */
    public function docStatus()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentFormUploadsStatus', 'doc_status_id', 'is_completed');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getCkListId()
    {
    	return $this->ck_list_id;
    }

    public function getIsCompleted()
    {
    	return $this->is_completed;
    }

    public function getDocumentComment()
    {
    	return $this->document_comment;
    }

    public function getDocumentChildStatusId()
    {
    	return $this->document_child_status_id;
    }

    public function getDocPrevStatus()
    {
    	return $this->doc_prev_status;
    }

    public function getDateCreated($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->date_created))
        {
            return DateUtility::changeDateFormat($this->date_created, 'Y-m-d H:i:s', $date_format);
        }
        return $this->date_created;
    }

    public function getDateModified($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->date_modified))
        {
            return DateUtility::changeDateFormat($this->date_modified, 'Y-m-d H:i:s', $date_format);
        }
        return $this->date_modified;
    }

    public function getCreatedBy()
    {
    	return $this->created_by;
    }

    public function getModifiedBy()
    {
    	return $this->modified_by;
    }

    public function getCandidateType()
    {
    	return $this->candidate_type;
    }

    public function getProcessedBy()
    {
    	return $this->processed_by;
    }

    public function getJobApplicationId()
    {
    	return $this->job_application_id;
    }

    public function getDateReviewed()
    {
    	return $this->date_reviewed;
    }

    public function getReviewedBy()
    {
    	return $this->reviewed_by;
    }

    public function getReviewedInd()
    {
    	return $this->reviewed_ind;
    }

    public function getTritonInd()
    {
    	return $this->triton_ind;
    }

    public function getNote()
    {
    	return $this->not;
    }

    public function getJobIntId()
    {
    	return $this->job_int_id;
    }

    public function getJobRequestId()
    {
        return $this->job_request_id;
    }

    public function getAcceptedDate()
    {
    	return $this->accepted_date;
    }

    public function getDeclinedDate()
    {
    	return $this->declined_date;
    }

    public function getLockedAgreementInd()
    {
    	return $this->locked_agreement_ind;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->get();
    	return $result;
    }

    public function getByUserIdAndCkListId($user_id, $ck_list_id)
    {
        $result = $this->where('user_id', '=', $user_id)
                        ->where('ck_list_id', '=', $ck_list_id)
                        ->first();
        return $result;
    }

    public function saveNew($data, $is_return_id)
    {
    	$this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
    	$obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveDocumentChecklistFormUploadData($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }

    public function removeById($id)
    {
    	return null;
    }

    public function getNewHireMainDocumentStatusList (
        $document_types = [],
        $document_statuses = [],
        $from_date_modified = '',
        $to_date_modified = '',
        $jde_ids = []
    )
    {
        return $this->select(
                        "u.jde_id AS Employee ID",
                        DB::raw("CONCAT(pd.first_name, ' ', pd.last_name) AS `Employee Name`"),
                        "u.country_code AS Nationality",
                        "ji.job_int_code AS Job Code",
                        "u.email AS Employee Email Address",
                        "dc.ck_list_name AS Document Name",
                        DB::raw(
                            "CASE
                                WHEN document_checklist_form_uploads.is_completed = 1 THEN 'Pending Document Approval'
                                WHEN document_checklist_form_uploads.is_completed = 2 THEN 'Completed'
                                WHEN document_checklist_form_uploads.is_completed = 3 THEN 'Rejected'
                                WHEN document_checklist_form_uploads.is_completed = 5 THEN 'Audited'
                                ELSE 'Document Not Submitted'
                            END AS `Document Status`"
                        ),
                        DB::raw("CONCAT(apd.first_name, ' ', apd.last_name) AS `Modified By`"),
                        "document_checklist_form_uploads.date_modified AS Date Uploaded / Completed",
                        "oa.assignment_date AS Joining Date",
                        "s.ship_name AS Joining Vessel"
                    )
                    ->join('users AS u', 'document_checklist_form_uploads.user_id', '=', 'u.user_id')
                    ->join('personal_data AS pd', 'u.personal_data_id', '=', 'pd.personal_data_id')
                    ->join('document_checklist AS dc', 'document_checklist_form_uploads.ck_list_id', '=', 'dc.ck_list_id')
                    ->join('users AS ua', 'document_checklist_form_uploads.modified_by', '=', 'ua.user_id')
                    ->join('personal_data AS apd', 'ua.personal_data_id', '=', 'apd.personal_data_id')
                    ->leftJoin('job_applications AS ja', 'document_checklist_form_uploads.job_application_id', '=', 'ja.job_application_id')
                    ->leftJoin('job_int AS ji', 'ja.job_int_id', '=', 'ji.job_int_id')
                    ->leftJoin('onboard_assignments AS oa', function ($join) {
                        $join->on('document_checklist_form_uploads.user_id', '=', 'oa.user_id' )
                             ->where('oa.assignment_date', '=', DB::raw("(select max(`assignment_date`) from onboard_assignments where user_id = document_checklist_form_uploads.user_id)"));
                    })
                    ->leftJoin('ships AS s', 'oa.ship_id', '=', 's.ship_id')
                    ->whereIn('u.jde_id', $jde_ids)
                    ->whereIn('document_checklist_form_uploads.ck_list_id', $document_types)
                    ->whereIn('document_checklist_form_uploads.is_completed', $document_statuses)
                    ->where('document_checklist_form_uploads.date_modified', '>=', $from_date_modified)
                    ->where('document_checklist_form_uploads.date_modified', '<=', $to_date_modified)
                    ->get();
    }
}
