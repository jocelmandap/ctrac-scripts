<?php

namespace CTRAC\Model\Histories;

use Illuminate\Database\Eloquent\Model;
use CTRAC\Helpers\DateUtility;

class HistoryRecord extends Model
{
	protected $fillable = [
		'user_id',
		'job_application_id',
		'status_code_id',
		'admin_id',
		'history_record_date',
		'history_record_notes',
		'history_record_alert',
		'active',
		'deleted_by'
	];

	public $timestamps = false;
    protected $primaryKey = 'history_record_id';

    public function setDBConnection($connection)
    {
    	$this->connection = $connection;
    }

    /**
     * User Details
     */
    public function user()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'user_id');
    }

    /**
     * Admin User Details
     */
    public function admin()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'admin_id');
    }

    /**
     * Job Application Details
     */
    public function jobApplication()
    {
        return $this->hasOne('CTRAC\Model\Jobs\JobApplication', 'user_id', 'user_id');
    }

    /**
     * Get Status Details
     */
    public function status()
    {
        return $this->hasOne('CTRAC\Model\Statuses\StatusCode', 'status_code_id', 'status_code_id');
    }

    public function getId()
    {
    	return $this->history_record_id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getJobApplicationId()
    {
    	return $this->job_application_id;
    }

    public function getStatusCodeId()
    {
    	return $this->status_code_id;
    }

    public function getAdminId()
    {
    	return $this->admin_id;
    }

    public function getCreatedAt($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->history_record_date))
        {
            return DateUtility::changeDateFormat($this->history_record_date, 'Y-m-d H:i:s', $date_format);
        }
        return $this->history_record_date;
    }

    public function getNotes()
    {
    	return $this->history_record_notes;
    }

    public function getAlert()
    {
    	return $this->history_record_alert;
    }

    public function getIsActive()
    {
    	return $this->active;
    }

    public function getDeletedBy()
    {
    	return $this->deleted_by;
    }

    public function getById($history_record_id)
    {
    	$result = $this->where('history_record_id', '=', $history_record_id)
    					->first();
    	return $result;
    }

    public function getByJobApplicationId($job_application_id)
    {
    	$result = $this->where('job_application_id', '=', $job_application_id)
    					->get();
    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->get();
    	return $result;
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $this->getById($this->getId());
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['history_record_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $obj;
            }            
        }

        return false;
    }

    public function saveHistoryRecord($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['history_record_id']) && !empty($data['history_record_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
