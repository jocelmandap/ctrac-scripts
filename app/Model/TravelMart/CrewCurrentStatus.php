<?php

namespace CTRAC\Model\TravelMart;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CrewCurrentStatus extends Model
{
    protected $fillable = [
    	'EmployeeID',
        'CrewStatus',
        'SignOnOffDate',
        'SignOnDate',
        'SignOffDate',
        'ShipCode',
        'Seaport',
        'ReasonCode',
        'JobCode',
        'BrandCode',
        'CostCenter',
        'AppraisalCount',
        'WarningCount',
        'ContractWeeks',
        'Tenure',
        'SignOn',
        'SignOff',
        'DateCreated'
    ];

    public $timestamps = false;

    protected $table = 'tm_crew_current_status';

    protected $primaryKey = null;

    public $incrementing = false;

    protected $user;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function userDetails()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'jde_id', 'EmployeeID');
    }

    public function getJdeId()
    {
    	return $this->EmployeeID;
    }

    public function getCrewStatus()
    {
        return $this->CrewStatus;
    }

    public function getSignOnOffDate()
    {
        return $this->SignOnOffDate;
    }

    public function getShipCode()
    {
        return $this->ShipCode;
    }

    public function getJobCode()
    {
        return $this->JobCode;
    }

    public function getBrandCode()
    {
        return $this->BrandCode;
    }

    public function getCostCenter()
    {
        return $this->CostCenter;
    }

    public function getByShipCodeBrandCodeAndCostCenter($ship_code, $brand_code, $cost_center)
    {
        $result = $this->where([
                        ['ShipCode', '=', $ship_code],
                        ['BrandCode', '=', $brand_code],
                        ['CostCenter', '=', $cost_center]
                    ])->get();

        if (!$result->isEmpty())
        {
            $data = [];
            foreach ($result as $row) 
            {
                $user = $row->userDetails()->first();
                if (!is_null($user))
                {
                    $data[] = $user;
                }
            }

            return $data;
        }

        return false;
    }

    public function getUserIdsByShipCodeBrandCodeCostCenterAndJobCode (
        $ship_code, 
        $brand_code, 
        $cost_center,
        $job_code
    )
    {
        $results = DB::table('tm_crew_current_status')
                        ->join('users', 'tm_crew_current_status.EmployeeID', '=', 'users.jde_id')
                        ->select(
                            'users.user_id'
                        )
                        ->where([
                            ['tm_crew_current_status.ShipCode', '=', $ship_code],
                            ['tm_crew_current_status.BrandCode', '=', $brand_code],
                            ['tm_crew_current_status.CostCenter', '=', $cost_center],
                            ['tm_crew_current_status.JobCode', '=', $job_code]
                        ])
                        ->get();

        return $results;
    }
}
