<?php

namespace CTRAC\Model\Gateways;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    protected $fillable = [
    	'gateway_name',
    	'country_code',
    	'gateway_code',
    	'is_approved',
    	'stripe'
    ];

    public $timestamps = false;

    protected $primaryKey = 'gateway_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->gateway_id;
    }

    public function getGatewayName()
    {
    	return $this->gateway_name;
    }

    public function getCountryCode()
    {
    	return $this->country_code;
    }

    public function getGatewayCode()
    {
    	return $this->gateway_code;
    }

    public function getIsApproved()
    {
    	return $this->is_approved;
    }

    public function getStripe()
    {
    	return $this->stripe;
    }

    public function getById($gateway_id)
    {
    	$result = $this->where('gateway_id', '=', $gateway_id)
    					->first();
    	return $result;
    }

    public function getByGatewayCode($gateway_code)
    {
        $result = $this->where('gateway_code', '=', $gateway_code)
                        ->first();
        return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function createGateway($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function updateGateway($data, $is_return_id = false)
    {
        $user_venue = $this->getById($data['gateway_id']);

        if (!is_null($user_venue))
        {
            $user_venue->fill($data);

            if ($user_venue->save())
            {
                if ($is_return_id)
                {
                    return $user_venue->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveGateway($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['gateway_id']) && !empty($data['gateway_id']))
            {
                return $this->updateGateway($data, $is_return_id);
            }

            return $this->createGateway($data, $is_return_id);
        }
        return false;
    }
}
