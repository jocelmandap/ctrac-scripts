<?php

namespace CTRAC\Model\Requisitions;

use Illuminate\Database\Eloquent\Model;

class PeoplesoftPosition extends Model
{
    protected $fillable = [
    	'status',
    	'company_code',
    	'eff_date',
    	'position_code',
    	'description',
    	'short_desc',
    	'unit',
    	'dept_id',
    	'job_code',
    	'reports_to',
    	'location',
    	'emp_id',
    	'name',
    	'hire_date',
    	'comprate',
    	'action',
    	'action_reason',
    	'empl_status',
    	'region'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getStatus()
    {
    	return $this->status;
    }

    public function getCompanyCode()
    {
    	return $this->company_code;
    }

    public function getEffDate()
    {
    	return $this->eff_date;
    }

    public function getPositionCode()
    {
    	return $this->position_code;
    }

    public function getDescription()
    {
    	return $this->description;
    }

    public function getShortDesc()
    {
    	return $this->short_desc;
    }

    public function getUnit()
    {
    	return $this->unit;
    }

    public function getDeptId()
    {
    	return $this->dept_id;
    }

    public function getJobCode()
    {
    	return $this->job_code;
    }

    public function getReportsTo()
    {
    	return $this->reports_to;
    }

    public function getLocation()
    {
    	return $this->location;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByPositionCode($position_code)
    {
    	$result = $this->where('position_code', '=', $position_code)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getByManagerId($data['position_code']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function savePositions($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['position_code']) && !empty($data['position_code']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
