<?php

namespace CTRAC\Model\Requisitions;

use Illuminate\Database\Eloquent\Model;

class PeoplesoftJobCodeList extends Model
{
    protected $fillable = [
    	'job_code',
    	'job_description'
    ];

    public $timestamps = false;

    protected $table = 'peoplesoft_job_code_list';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getJobCode()
    {
    	return $this->job_code;
    }

    public function getJobDescription()
    {
    	return $this->job_description;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByJobCode($job_code)
    {
    	$result = $this->where('job_code', '=', $job_code)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getByJobCode($data['job_code']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveJobCode($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['job_code']) && !empty($data['job_code']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
