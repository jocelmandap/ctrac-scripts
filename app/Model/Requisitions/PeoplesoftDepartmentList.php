<?php

namespace CTRAC\Model\Requisitions;

use Illuminate\Database\Eloquent\Model;

class PeoplesoftDepartmentList extends Model
{
    protected $fillable = [
    	'set_id',
    	'dept_id',
    	'eff_date',
    	'status',
    	'description',
    	'company_code',
    	'location',
    	'dept_id_for_ctrac'
    ];

    public $timestamps = false;

    protected $table = 'peoplesoft_department_list';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getSetId()
    {
    	return $this->set_id;
    }

    public function getDeptId()
    {
    	return $this->dept_id;
    }

    public function getEffDate()
    {
    	return $this->eff_date;
    }

    public function getStatus()
    {
    	return $this->status;
    }

    public function getDescription()
    {
    	return $this->description;
    }

    public function getCompanyCode()
    {
    	return $this->company_code;
    }

    public function getLocation()
    {
    	return $this->location;
    }

    public function getDeptIdForCtrac()
    {
    	return $this->dept_id_for_ctrac;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByDeptId($dept_id)
    {
    	$result = $this->where('dept_id', '=', $dept_id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getByDeptId($data['dept_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveDepartmentList($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['dept_id']) && !empty($data['dept_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
