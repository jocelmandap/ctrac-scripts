<?php
	namespace CTRAC\Model\Requisitions;

	use Illuminate\Database\Eloquent\Model;

	class MedicalRequisition extends Model
	{
		protected $fillable = [
			'id',
			'job_request_id',
			'type',
			'is_active',
			'created_date',
			'created_by'
		];

		protected $table = 'medical_requisition';
		public $timestamps = false;

		protected $primaryKey = 'id';

		public function setDBConnection($connection)
		{
			$this->connection = $connection;
		}

		public function getId()
		{
			return $this->id;
		}

		public function getJobRequestId()
		{
			return $this->job_request_id;
		}

		public function getType()
		{
			return $this->type;
		}

		public function isActive()
		{
			return $this->is_active;
		}

		public function getAllActive()
		{
			$obj = $this->where('is_active', '1');
			return $obj->get();
		}

	}
?>