<?php

namespace CTRAC\Model\Requisitions;

use Illuminate\Database\Eloquent\Model;

class PeoplesoftRequisitionIdList extends Model
{
    protected $fillable = [
    	'job_opening',
        'job_code',
        'position',
        'created',
        'status',
        'stat_dt',
        'openings',
        'target_openings',
        'unit',
        'dept_id',
        'co',
        'location',
        'posting_title',
        'justification',
        'req_sent',
        'comments',
        'rclform',
        'rclhrform',
        'int_ext',
        'req_type',
        'job_type',
        'id_replacement',
        'name',
        'mgrid',
        'name_2',
        'recruiter',
        'name3',
        'status2',
        'descr',
        'country'
    ];

    public $timestamps = false;

    protected $table = 'peoplesoft_requisition_id_list';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->job_opening;
    }

    public function getJobCode()
    {
    	return $this->job_code;
    }

    public function getPosition()
    {
    	return $this->position;
    }

    public function getCreated()
    {
    	return $this->created;
    }

    public function getById($job_opening)
    {
    	$result = $this->where('job_opening', '=', $job_opening)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function updateReq($data, $is_return_id = false)
    {
        $user_venue = $this->getById($data['job_opening']);

        if (!is_null($user_venue))
        {
            $user_venue->fill($data);

            if ($user_venue->save())
            {
                if ($is_return_id)
                {
                    return $user_venue->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveRequisition($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['job_opening']) && !empty($data['job_opening']))
            {
                return $this->updateReq($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
