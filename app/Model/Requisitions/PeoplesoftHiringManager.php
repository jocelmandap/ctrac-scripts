<?php

namespace CTRAC\Model\Requisitions;

use Illuminate\Database\Eloquent\Model;

class PeoplesoftHiringManager extends Model
{
    protected $fillable = [
    	'peoplesoft_mgr_id',
    	'peoplesoft_role_id',
    	'hiring_manager_name',
    	'country_code',
    	'pay_status',
    	'email_address'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getManagerId()
    {
    	return $this->peoplesoft_mgr_id;
    }

    public function getRoleId()
    {
    	return $this->peoplesoft_role_id;
    }

    public function getManagerName()
    {
    	return $this->hiring_manager_name;
    }

    public function getCountryCode()
    {
    	return $this->country_code;
    }

    public function getpayStatus()
    {
    	return $this->pay_status;
    }

    public function getEmail()
    {
    	return $this->email_address;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByManagerId($peoplesoft_mgr_id)
    {
    	$result = $this->where('peoplesoft_mgr_id', '=', $peoplesoft_mgr_id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getByManagerId($data['peoplesoft_mgr_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveHiringManagers($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['peoplesoft_mgr_id']) && !empty($data['peoplesoft_mgr_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
