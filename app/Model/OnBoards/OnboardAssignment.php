<?php

namespace CTRAC\Model\OnBoards;

use Illuminate\Database\Eloquent\Model;

class OnboardAssignment extends Model
{
    protected $fillable = [
    	'assignment_date',
    	'sign_off_date',
    	'brand_id',
    	'ship_id',
    	'visa_type',
    	'date_created',
    	'created_by',
    	'date_modified',
    	'modified_by',
    	'job_application_id',
    	'user_id'
    ];

    public $timestamps = false;

    protected $primaryKey = 'assignment_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Ship Details
     */
    public function ship()
    {
        return $this->hasOne('CTRAC\Model\Ships\Ship', 'ship_id', 'ship_id');
    }

    public function getId()
    {
    	return $this->assignment_id;
    }

    public function getAssignmentDate()
    {
    	return $this->assignment_date;
    }

    public function getSignOffDate()
    {
    	return $this->sign_off_date;
    }

    public function getBrandId()
    {
    	return $this->brand_id;
    }

    public function getShipId()
    {
    	return $this->ship_id;
    }

    public function getVisaType()
    {
    	return $this->visa_type;
    }

    public function getJobApplicationId()
    {
    	return $this->job_application_id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getById($assignment_id)
    {
    	$result = $this->where('assignment_id', '=', $assignment_id)
    					->first();

    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->first();

    	return $result;
    }
}
