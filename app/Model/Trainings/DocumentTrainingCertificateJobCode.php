<?php

namespace CTRAC\Model\Trainings;

use Illuminate\Database\Eloquent\Model;

class DocumentTrainingCertificateJobCode extends Model
{
    protected $fillable = [
    	'id',
    	'job_int_id',
    	'cert_type_id',
    	'created_on',
    	'created_by'
    ];

    public $timestamps = false;

    protected $table = 'document_training_certificate_jobcode';

    protected $primaryKey = 'id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getJobIntId()
    {
        return $this->job_int_id;
    }

    public function getCertTypeId()
    {
        return $this->cert_type_id;
    }

    public function getCreatedOn()
    {
        return $this->created_on;
    }

    public function getCreatedBy()
    {
        return $this->created_by;
    }

    public function getByJobIntId($job_int_id)
    {
        $result = $this->where('job_int_id', '=', $job_int_id)
                        ->get();
        return $result;
    }

    public function getByJobIntIdCertTypeId($job_int_id,$cert_type_id)
    {
        $result = $this->where('job_int_id', '=', $job_int_id) 
                        ->where('cert_type_id', '=', $cert_type_id)
                        ->get();
        return $result;
    }

    public function createCertificate($data, $is_return_id = false)
    {
        
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function updateCertificate($data, $is_return_id = false)
    {
        $user_venue = $this->getById($data['id']);

        if (!is_null($user_venue))
        {
            $user_venue->fill($data);

            if ($user_venue->save())
            {
                if ($is_return_id)
                {
                    return $user_venue->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveCertificate($data, $is_return_id = true)
    {

        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->updateCertificate($data, $is_return_id);
            }

            return $this->createCertificate($data, $is_return_id);
        }
        return false;
    }
}
