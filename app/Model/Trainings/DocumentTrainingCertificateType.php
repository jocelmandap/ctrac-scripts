<?php

namespace CTRAC\Model\Trainings;

use Illuminate\Database\Eloquent\Model;

class DocumentTrainingCertificateType extends Model
{
    protected $fillable = [
    	'cert_type_id',
    	'cert_type_code',
    	'cert_type_name',
    	'cert_order',
    	'use_country_ind',
    	'use_jobcode_ind',
    	'bypass_validation_ind',
    	'show_ind',
    	'created_on',
    	'created_by',
    	'last_updated_on',
    	'last_updated_by',
    	'use_job_code_ind',
    	'has_expiry_ind'
    ];

    public $timestamps = false;

    protected $table = 'document_training_certificate_type';

    protected $primaryKey = 'cert_type_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->cert_type_id;
    }

    public function getCode()
    {
    	return $this->cert_type_code;
    }

    public function getName()
    {
    	return $this->cert_type_name;
    }

    public function getOrder()
    {
        return $this->cert_order;
    }

    public function getCountryInd()
    {
        return $this->use_country_ind;
    }

    public function getJobCodeInd()
    {
        return $this->use_jobcode_ind;
    }

    public function getBypassValidationInd()
    {
        return $this->bypass_validation_ind;
    }

    public function getExpiryInd()
    {
        return $this->has_expiry_ind;
    }

    public function getByCertificateCode($cert_type_code)
    {
        $result = $this->where('cert_type_code', '=', $cert_type_code)
                        ->first();
        return $result;
    }

    public function getByCertificateId($cert_type_id)
    {
        $result = $this->where('cert_type_id', '=', $cert_type_id)
                        ->first();
        return $result;
    }

    public function getShowInd()
    {
        return $this->show_ind;
    }

    public function createCertificate($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function updateCertificate($data, $is_return_id = false)
    {
        $user_venue = $this->getById($data['cert_type_id']);

        if (!is_null($user_venue))
        {
            $user_venue->fill($data);

            if ($user_venue->save())
            {
                if ($is_return_id)
                {
                    return $user_venue->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveCertificate($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['cert_type_id']) && !empty($data['cert_type_id']))
            {
                return $this->updateCertificate($data, $is_return_id);
            }

            return $this->createCertificate($data, $is_return_id);
        }
        return false;
    }

    public function getAllCertificateTypeCodes ()
    {
        $result = $this->get();

        return $this->pluck('cert_type_code')->toArray();
    }
}
