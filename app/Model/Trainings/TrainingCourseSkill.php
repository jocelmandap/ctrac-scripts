<?php

namespace CTRAC\Model\Trainings;

use Illuminate\Database\Eloquent\Model;

class TrainingCourseSkill extends Model
{
    protected $fillable = [
    	'training_course_id',
    	'skill_set_id',
    	'skill_level_id',
    	'is_active',
    	'created_at',
    	'created_by',
    	'updated_at',
    	'updated_by',
    	'deleted_at'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function skill()
    {
    	return $this->hasOne('CTRAC\Model\Skills\SkillSet', 'skill_set_id', 'skill_set_id');
    }

    public function level()
    {
    	return $this->hasOne('CTRAC\Model\Skills\SkillLevel', 'skill_level_id', 'skill_level_id');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getTrainingCourseId()
    {
    	return $this->training_course_id;
    }

    public function getSkillSetId()
    {
    	return $this->skill_set_id;
    }

    public function getSkillLevelId()
    {
    	return $this->skill_level_id;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getById($training_course_skill_id)
    {
    	$result = $this->where('id', '=', $training_course_skill_id)
    					->first();
    	return $result;
    }

    public function getByTrainingCourseId($training_course_id)
    {
    	$result = $this->where('training_course_id', '=', $training_course_id)
    					->get();
    	return $result;
    }

    public function addNewItem($data, $is_return_id = false)
    {
     	$this->fill($data);

     	$result = $this->save();

     	if ($is_return_id)
     	{
     		return $this->getId();
     	}

     	return $result;
    }

    public function updateItem($data, $is_return_id = false)
    {
     	$obj = $this->getById($data['id']);

     	if (!is_null($obj))
     	{
     		$obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $result;
            }
     	}

     	return false;
    }

    public function saveTrainingCodeSkills($data, $is_return_id = true)
	{
		if (!empty($data))
		{
			if (isset($data['id']) && !empty($data['id']))
			{
				return $this->updateItem($data, $is_return_id);
			}

			return $this->addNewItem($data, $is_return_id);
		}
		return false;
	}

    public function removeTrainingCodeSkills($training_course_skill_id)
    {
        $result = $this->where('id', '=', $training_course_skill_id)
                        ->delete();

        return $result;
    }
}
