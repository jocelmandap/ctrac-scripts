<?php

namespace CTRAC\Model\Trainings;

use Illuminate\Database\Eloquent\Model;

class TrainingCourse extends Model
{
    protected $fillable = [
    	'training_code',
    	'training_name',
    	'training_desc',
    	'training_type_desc',
    	'is_active',
    	'regulation_type',
    	'owner',
    	'expiration_date',
    	'expire_at_sign_off',
    	'issue_date_required',
    	'stwc_regulation',
    	'num_months_expiration',
    	'expire_at_eplan_change',
    	'print_certificate',
    	'active_flag',
    	'location',
    	'created_at',
    	'created_by',
    	'updated_at',
    	'updated_by',
    	'deleted_at'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function TrainingSkills()
    {
        return $this->hasMany('CTRAC\Model\Trainings\TrainingCourseSkill');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getTrainingCode()
    {
    	return $this->training_code;
    }

    public function getTrainingName()
    {
    	return $this->training_name;
    }

    public function getTrainingDesc()
    {
    	return $this->training_desc;
    }

    public function getTrainingTypeDesc()
    {
    	return $this->training_type_desc;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getExpirationDate()
    {
    	return $this->expiration_date;
    }

    public function getNumMonthsExpiration()
    {
    	return $this->num_months_expiration;
    }

    public function getById($training_course_id)
    {
    	$result = $this->where('id', '=', $training_course_id)
    					->first();
    	return $result;
    }

    public function getByTrainingCode($training_code)
    {
    	$result = $this->where('training_code', '=', $training_code)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function addNewItem($data, $is_return_id = false)
    {
     	$this->fill($data);

     	$result = $this->save();

     	if ($is_return_id)
     	{
     		return $this->getId();
     	}

     	return $result;
    }

    public function updateItem($data, $is_return_id = false)
    {
     	$obj = $this->getById($data['id']);

     	if (!is_null($obj))
     	{
     		$obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $result;
            }
     	}

     	return false;
    }

    public function saveTrainingCodes($data, $is_return_id = true)
	{
		if (!empty($data))
		{
			if (isset($data['id']) && !empty($data['id']))
			{
				return $this->updateItem($data, $is_return_id);
			}

			return $this->addNewItem($data, $is_return_id);
		}
		return false;
	}
}
