<?php

namespace CTRAC\Model\Trainings;

use Illuminate\Database\Eloquent\Model;

class TrainingCourseJob extends Model
{
    protected $fillable = [
    	'training_course_id',
    	'job_int_id',
    	'created_at',
    	'created_by',
    	'updated_at',
    	'updated_by'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function trainingCourse()
    {
        return $this->hasOne('CTRAC\Model\Trainings\TrainingCourse', 'id', 'training_course_id');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getTrainingCourseId()
    {
    	return $this->training_course_id;
    }

    public function getJobIntId()
    {
    	return $this->job_int_id;
    }

    public function getById($training_course_job_id)
    {
    	$result = $this->where('id', '=', $training_course_job_id)
    					->first();
    	return $result;
    }

    public function getByJobIntId($job_int_id)
    {
    	$result = $this->where('job_int_id', '=', $job_int_id)
    					->get();
    	return $result;
    }

    public function getByTrainingCourseId($training_course_id)
    {
    	$result = $this->where('training_course_id', '=', $training_course_id)
    					->get();
    	return $result;
    }

    public function addNewItem($data, $is_return_id = false)
    {
     	$this->fill($data);

     	$result = $this->save();

     	if ($is_return_id)
     	{
     		return $this->getId();
     	}

     	return $result;
    }

    public function updateItem($data, $is_return_id = false)
    {
     	$obj = $this->getById($data['id']);

     	if (!is_null($obj))
     	{
     		$obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $result;
            }
     	}

     	return false;
    }

    public function saveTrainingCourseJobs($data, $is_return_id = true)
	{
		if (!empty($data))
		{
			if (isset($data['id']) && !empty($data['id']))
			{
				return $this->updateItem($data, $is_return_id);
			}

			return $this->addNewItem($data, $is_return_id);
		}
		return false;
	}

	public function removeTrainingCourseJob($training_course_job_id)
	{
	    $result = $this->where('id', '=', $training_course_job_id)
	                    ->delete();

	    return $result;
	}
}
