<?php

namespace CTRAC\Model\Others;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
	public function setDBConnection($connection)
	{
	    $this->connection = $connection;
	}
	
    public function getId()
    {
        return $this->bank_id;
    }

    public function getName()
    {
        return $this->bank_name;
    }

    public function getAll()
    {
    	return static::all();
    }
}
