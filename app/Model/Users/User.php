<?php

namespace CTRAC\Model\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_code',
        'email',
        'password',
        'user_group_id',
        'personal_data_id',
        'emergency_id',
        'emergency_relationship',
        'military_service_data',
        'military_docs',
        'reset_password',
        'brand_id',
        'region_id',
        'progress_info',
        'img_avatar',
        'resume_name',
        'applicant_priority',
        '_status_code_id',
        '_status_code_date',
        'upload_video',
        'hp_office_number',
        'hp_company',
        'approved',
        'last_login_date',
        'no_education',
        'no_employment',
        'source_id',
        'hp_register_company',
        'newsletter',
        'error_count',
        'student',
        'school_id',
        'alliance_school_id',
        'referred_by',
        'hp_doc_company_id',
        'hp_doc_location_id',
        'jde_id',
        'peoplesoft_id',
        'registration_date',
        'is_onboard',
        'department_id',
        'user_token',
        'applicant_type_id',
        'ship_id',
        'esign',
        'is_active',
        'e1_import',
        'corp_user_ind',
        'e1_import_update',
        'e1_migrated_ind',
        'mns_position_ind',
        'edit_employee_ind'
    ];

    public $timestamps = false;

    protected $primaryKey = 'user_id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function personalData()
    {
        return $this->hasOne('CTRAC\Model\Users\PersonalData', 'personal_data_id', 'personal_data_id');
    }

    /**
     * User Job Applications
     */
    public function jobApplications()
    {
        return $this->hasMany('CTRAC\Model\Jobs\JobApplication', 'user_id', 'user_id');
    }

    /**
     * Get Country Details
     */
    public function country()
    {
        return $this->hasOne('CTRAC\Model\Countries\Country', 'country_code', 'country_code');
    }

    /**
     * Get Document Checklist Form Uploads Details
     */
    public function generalDocumentStatus()
    {
        return $this->hasOne('CTRAC\Model\Documents\DocumentChecklistFormUpload', 'user_id', 'user_id');
    }

    /**
     * Get Onboard Assignment Details
     */
    public function assignment()
    {
        return $this->hasOne('CTRAC\Model\OnBoards\OnboardAssignment', 'user_id', 'user_id');
    }

    /**
     * User Allotee Details
     */
    public function allotees()
    {
        return $this->hasMany('CTRAC\Model\Users\PersonalDataAllotee', 'user_id', 'user_id');
    }

    public function sourceType()
    {
        return $this->hasOne('CTRAC\Model\Users\Source', 'id', 'source_id');
    }

    public function businessPartner()
    {
        return $this->hasOne('CTRAC\Model\Users\BusinessPartner', 'company_name', 'hp_register_company');
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getCountryCode()
    {
        return $this->country_code;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getJDEId()
    {
        return $this->jde_id;
    }

    public function getById($user_id)
    {
        $result = $this->where('user_id', '=', $user_id)
                        ->first();
        return $result;
    }

    public function getByJDEId($jde_id)
    {
        $result = $this->where('jde_id', '=', $jde_id)
                        ->first();
        return $result;
    }

    public function getByEmail($email)
    {
        $result = $this->where('email', '=', $email)
                        ->first();
        return $result;
    }

    public function getUsersByShipAndJobCode (
        $status_code_id, 
        $brand_id, 
        $ship_id, 
        $job_group_code, 
        $job_int_code
    )
    {
        $results = DB::table('job_applications')
                        ->leftJoin('onboard_assignments', 'job_applications.job_application_id', '=', 'onboard_assignments.job_application_id')
                        ->leftJoin('ships', 'ships.ship_id', '=', 'onboard_assignments.ship_id')
                        ->leftJoin('job_int', 'job_int.job_int_id', '=', 'job_applications.job_int_id')
                        ->leftJoin('job_requests', 'job_requests.job_request_id', '=', 'job_applications.job_request_id')
                        ->select(
                            'job_applications.job_application_id',
                            'job_applications.user_id',
                            'onboard_assignments.assignment_id',
                            'ships.ship_id',
                            'ships.ship_code',
                            'ships.ship_name',
                            'job_int.job_int_id',
                            'job_int.job_int_code',
                            'job_requests.job_group_code',
                            'job_requests.job_request_title'
                        )
                        ->where([
                            ['job_applications.status_code_id', '=', $status_code_id],
                            ['job_applications.brand_id', '=', $brand_id],
                            ['onboard_assignments.ship_id', '=', $ship_id],
                            ['job_requests.job_group_code', '=', $job_group_code],
                            ['job_int.job_int_code', '=', $job_int_code]
                        ])
                        ->get();

        return $results;
    }

    public function saveNew($data, $is_return_id)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
        $obj = $this->getById($data['user_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveUserData($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['user_id']) && !empty($data['user_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}