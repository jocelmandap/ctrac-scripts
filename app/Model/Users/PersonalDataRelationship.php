<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class PersonalDataRelationship extends Model
{
	public function setDBConnection($connection)
	{
	    $this->connection = $connection;
	}
	
    public function getId()
    {
        return $this->relationship_id;
    }

    public function getName()
    {
        return $this->relationship_name;
    }

    public function getAll()
    {
    	return static::all();
    }
}
