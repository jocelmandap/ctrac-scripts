<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class BusinessPartner extends Model
{
    protected $fillable = [
    	'company_name',
        'company_code',
        'type',
        'created_on',
        'created_by',
        'last_updated_by',
        'last_updated_on',
        'active_ind',
        'country_code_id'
    ];

    protected $table = 'business_partners';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->company_name;
    }

    public function getCode()
    {
    	return $this->company_code;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}