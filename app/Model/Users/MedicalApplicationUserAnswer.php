<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class MedicalApplicationUserAnswer extends Model
{
	protected $fillable = [
		'id',
		'user_id',
		'job_application_id',
		'is_passed',
		'is_exam_completed',
		'answers',
		'created_date',
		'modified_date',
		'current_exam_time',
		'bypassed_exam'
	];

	public $timestamps = false;

	protected $primaryKey = 'id';

	public function setDBConnection($connection)
	{
		$this->connection = $connection;
	}

	/**
	 * Get Exam Details
	 */
	public function jobApplicationId()
	{
	    return $this->hasOne('CTRAC\Model\Jobs\JobApplication', 'job_application_id', 'job_application_id');
	}

	public function getId()
	{
		return $this->id;
	}

	public function getUserId()
	{
		return $this->user_id;
	}

	public function getJobApplicationId()
	{
		return $this->job_application_id;
	}

	public function getIsPassed()
	{
		return $this->is_passed;
	}

	public function getIsExamCompleted()
	{
		return $this->is_exam_completed;
	}

	public function getAnswers()
	{
		return $this->answers;
	}

	public function getByPassedExam()
	{
		return $this->bypassed_exam;
	}

	public function getByJobAppId($job_application_id)
	{
		$result = $this->where('job_application_id', '=', $job_application_id)
						->first();
		return $result;
	}

	public function getById($id)
	{
		$result = $this->where('id', '=', $id)
						->first();
		return $result;
	}

	public function saveNew($data, $is_return_id)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['id']);
        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $obj;
            }            
        }

        return false;
    }


}
