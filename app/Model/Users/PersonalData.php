<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class PersonalData extends Model
{
    protected $fillable = [
    	'first_name',
    	'middle_name',
    	'last_name',
    	'title_name',
    	'other_first_name',
    	'other_middle_name',
    	'other_last_name',
    	'fathers_name',
    	'mothers_name',
    	'sex',
    	'telephone_data',
    	'street_address',
    	'city',
    	'state',
    	'country_code',
    	'zip_code',
    	'address_from_date',
    	'address_to_date',
    	'skype',
    	'21years',
    	'18years',
    	'gateway_id',
    	'contact_id',
    	'sched_id',
    	'medical_id',
    	'per_street_address',
    	'per_city',
    	'per_state',
    	'per_zip_code',
    	'per_country_code',
    	'per_address_from_date',
    	'per_address_to_date',
    	'permanent_indicator',
    	'mailing_street_address',
    	'mailing_city',
    	'mailing_state',
    	'mailing_zip_code',
    	'mailing_country_code',
    	'father_fname',
    	'father_mname',
    	'father_lname',
    	'father_country_code',
    	'mother_fname',
    	'mother_mname',
    	'mother_lname',
    	'mother_country_code',
    	'EC_fullname',
    	'EC_relation',
    	'EC_email',
    	'EC_phone_number',
    	'EC_cell_number',
    	'EC_street_address',
    	'EC_city',
    	'EC_state',
    	'EC_zip_code',
    	'EC_country_code',
    	'BF_fullname',
    	'BF_relation',
    	'BF_email',
    	'BF_phone_number',
    	'BF_cell_number',
    	'BF_percent',
    	'BF_street_address',
    	'BF_city',
    	'BF_state',
    	'BF_zip_code',
    	'BF_country_code',
    	'BF_fullname1',
    	'BF_relation1',
    	'BF_email1',
    	'BF_phone_number1',
    	'BF_cell_number1',
    	'BF_percent1',
    	'BF_street_address1',
    	'BF_city1',
    	'BF_state1',
    	'BF_zip_code1',
    	'BF_country_code1',
    	'passport_num',
    	'passport_country',
    	'passport_issued_date',
    	'passport_expiry_date',
    	'worked_on_ship',
    	'areas_of_interest',
    	'worked_for_royal',
    	'knows_royal',
    	'royal_employees',
    	'worked_brand',
    	'marital_status',
    	'birthdate',
    	'birthplace_city',
    	'birthplace_state',
    	'birthplace_country_code',
    	'sss',
    	'social_media_pages',
    	'philhealth_number',
    	'hdmf_number',
    	'license_number',
    	'spouse_ind',
    	'spouse_first_name',
    	'spouse_middle_name',
    	'spouse_last_name',
    	'mothers_maiden_name',
    	'highest_education_id',
    	'last_updated_date',
    	'last_updated_by'
    ];

    protected $primaryKey = 'personal_data_id';

    public $timestamps = false;

    protected $table = 'personal_data';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function country()
    {
        return $this->hasOne('CTRAC\Model\Countries\Country', 'country_code', 'country_code');
    }

    public function getId()
    {
    	return $this->personal_data_id;
    }

	public function getFirstName()
	{
		return $this->first_name;
	}

	public function getMiddleName()
	{
		return $this->middle_name;
	}

	public function getLastName()
	{
		return $this->last_name;
	}

    public function getGender()
    {
        return $this->sex;
    }

    public function getFullName()
    {
        if (!is_null($this->middle_name))
        {
            return $this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name;
        }
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function getBirthdateCity()
    {
        return $this->birthplace_city;
    }

    public function getBirthdateState()
    {
        return $this->birthplace_state;
    }

    public function getBirthdateCountry()
    {
        return $this->birthplace_country_code;
    }

	public function getById($personal_data_id)
	{
		$result = $this->where('personal_data_id', '=', $personal_data_id)
						->first();
		return $result;
	}

    public function saveNew($data, $is_return_id)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
        $obj = $this->getById($data['personal_data_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function savePersonalData($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['personal_data_id']) && !empty($data['personal_data_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }

    public function getPersonalDataByFullName($data)
    {
        $results = $this->where([
                        ['first_name', '=', $data['first_name']],
                        ['middle_name', '=', $data['middle_name']],
                        ['last_name', '=', $data['last_name']],
                        ['fathers_name', '=', $data['fathers_name']]
                    ])->get();

        return $results;
    }
}
