<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class EmployeeTraining extends Model
{
    protected $fillable = [
    	'jde_id',
    	'training_course_id',
    	'issue_date',
    	'expire_date',
    	'date_updated',
    	'certificate_number',
    	'issue_country',
    	'trainer_name',
    	'created_at',
    	'updated_at'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getJDEId()
    {
    	return $this->jde_id;
    }

    public function getTrainingCourseId()
    {
    	return $this->training_course_id;
    }

    public function getIssueDate()
    {
    	return $this->issue_date;
    }

    public function getExpireDate()
    {
    	return $this->expire_date;
    }

    public function getDateUpdated()
    {
    	return $this->date_updated;
    }

    public function getCertificateNumber()
    {
    	return $this->certificate_number;
    }

    public function getIssueCountry()
    {
    	return $this->issue_country;
    }

    public function getTrainerName()
    {
    	return $this->trainer_name;
    }

    public function getById($employee_training_id)
    {
    	$result = $this->where('id', '=', $employee_training_id)
    					->first();
    	return $result;
    }

    public function getByJDEId($jde_id)
    {
    	$result = $this->where('jde_id', '=', $jde_id)
    					->get();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function addNewItem($data, $is_return_id = false)
    {
     	$this->fill($data);

     	$result = $this->save();

     	if ($is_return_id)
     	{
     		return $this->getId();
     	}

     	return $result;
    }

    public function updateItem($data, $is_return_id = false)
    {
     	$obj = $this->getById($data['id']);

     	if (!is_null($obj))
     	{
     		$obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $result;
            }
     	}

     	return false;
    }

    public function saveEmployeeTrainings($data, $is_return_id = true)
	{
		if (!empty($data))
		{
			if (isset($data['id']) && !empty($data['id']))
			{
				return $this->updateItem($data, $is_return_id);
			}

			return $this->addNewItem($data, $is_return_id);
		}
		return false;
	}
}
