<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class PersonalDataAllotee extends Model
{
	protected $fillable = [
		'user_id',
		'allotee_firstname',
		'allotee_middlename',
		'allotee_lastname',
		'allotee_bank',
		'allotee_account_no',
		'allotee_percentage',
		'relationship_id',
		'allotee_telephone_no',
		'allotee_mobile_no',
		'allotee_street_address',
		'allotee_city',
		'allotee_state',
		'allotee_zip_code',
		'allotee_country_code',
		'created_date',
		'created_by',
		'last_updated_date',
		'last_updated_by'
	];

    protected $table = 'personal_data_allotee';
    protected $primaryKey = 'allotee_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_updated_date';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }
    
    /**
     * Get Bank Details
     */
    public function bank()
    {
        return $this->hasOne('CTRAC\Model\Others\Bank', 'bank_id', 'allotee_bank');
    }

    /**
     * Get Relationship Details
     */
    public function relationship()
    {
        return $this->hasOne('CTRAC\Model\Users\PersonalDataRelationship', 'relationship_id', 'relationship_id');
    }

    public function getId()
    {
        return $this->allotee_id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getFullName()
    {
        return $this->allotee_firstname . ' ' . $this->allotee_lastname . ', ' . $this->allotee_middlename;
    }

    public function getFirstName()
    {
        return $this->allotee_firstname;
    }

    public function getMiddleName()
    {
        return $this->allotee_middlename;
    }

    public function getLastName()
    {
        return $this->allotee_lastname;
    }

    public function getBankId()
    {
        return $this->allotee_bank;
    }

    public function getAccountNo()
    {
        return $this->allotee_account_no;
    }

    public function getPercentage()
    {
        return $this->allotee_percentage;
    }

    public function getRelationshipId()
    {
        return $this->relationship_id;
    }

    public function getTelNo()
    {
        return $this->allotee_telephone_no;
    }

    public function getMobileNo()
    {
        return $this->allotee_mobile_no;
    }

    public function getStreet()
    {
        return $this->allotee_street_address;
    }

    public function getCity()
    {
        return $this->allotee_city;
    }

    public function getState()
    {
        return $this->allotee_state;
    }

    public function getZipCode()
    {
        return $this->allotee_zip_code;
    }

    public function getCreatedAt()
    {
        return $this->created_date;
    }

    public function getCreatedBy()
    {
        return $this->created_by;
    }

    public function getUpdatedAt()
    {
        return $this->last_updated_date;
    }

    public function getUpdatedBy()
    {
        return $this->last_updated_by;
    }

    public function getById($allotee_id)
    {
        $result = $this->where('allotee_id', '=', $allotee_id)
                        ->first();
        return $result;
    }

    public function saveNew($data, $is_return_id)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = true)
    {
        $obj = $this->getById($data['allotee_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveAlloteeData($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['allotee_id']) && !empty($data['allotee_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
