<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class UserVenue extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'update_date';

    protected $fillable = [
    	'user_id',
    	'venue_id',
    	'start_date',
    	'end_date',
    	'created_by',
    	'updated_by',
    	'update_date',
    	'created_date'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getVenueId()
    {
    	return $this->venue_id;
    }

    public function getStartDate()
    {
    	return $this->start_date;
    }

    public function getEndDate()
    {
    	return $this->end_date;
    }

    public function getById($user_venue_id)
    {
    	$result = $this->where('id', '=', $user_venue_id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function removeVenueSkillById($user_venue_id)
    {
    	$result = $this->where('id', '=', $user_venue_id)
    				->delete();

    	return $result;
    }

    public function createUserVenue($data, $is_return_id = false)
    {
    	$this->fill($data);

    	$result = $this->save();

    	if ($is_return_id)
    	{
    		return $this->getId();
    	}

    	return $result;
    }

    public function updateUserVenue($data, $is_return_id = false)
    {
    	$user_venue = $this->getById($data['id']);

    	if (!is_null($user_venue))
    	{
    		$user_venue->fill($data);

            if ($user_venue->save())
            {
                if ($is_return_id)
                {
                    return $user_venue->getId();
                }

                return $result;
            }
    	}

    	return false;
    }

    public function saveUserVenue($data, $is_return_id = true)
   	{
   		if (!empty($data))
   		{
   			if (isset($data['id']) && !empty($data['id']))
   			{
   				return $this->updateUserVenue($data, $is_return_id);
   			}

   			return $this->createUserVenue($data, $is_return_id);
   		}
   		return false;
   	}
}
