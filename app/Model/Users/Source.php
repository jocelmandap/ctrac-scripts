<?php

namespace CTRAC\Model\Users;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $fillable = [
    	'name',
        'url',
        'manual',
        'show_indicator',
        'show_id',
        'for_student',
        'country_code',
        'register_show_ind',
        'source_category_id',
        'school_require_hp'
    ];

    protected $table = 'sources';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function getShowId()
    {
    	return $this->show_id;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
