<?php

namespace CTRAC\Model\Forms;

use Illuminate\Database\Eloquent\Model;
use CTRAC\Model\Common\CRUDInterface;

class FormCandidateData extends Model
implements CRUDInterface
{
    protected $fillable = [
    	'user_id',
    	'CDF_firstName',
    	'CDF_middleName',
    	'CDF_lastName',
    	'CDF_otherSurname',
    	'CDF_add',
    	'CDF_Apt',
    	'CDF_city',
    	'CDF_state',
    	'CDF_zip',
    	'CDF_country_code',
    	'CDF_primaryType',
    	'CDF_primaryNumCt',
    	'CDF_primaryNum',
    	'CDF_altType',
    	'CDF_altNumCt',
    	'CDF_altNum',
    	'CDF_birthdate',
    	'CDF_status',
    	'CDF_gender',
    	'CDF_sss',
    	'CDF_gateway',
    	'CDF_dateavail',
    	'CDF_emailAdd',
    	'CDF_bcity',
    	'CDF_bstate',
    	'CDF_bcountry',
    	'CDF_education',
    	'CDF_school',
    	'CDF_ffirstName',
    	'CDF_fmiddleName',
    	'CDF_flastName',
    	'CDF_fcountry',
    	'CDF_mfirstName',
    	'CDF_mmiddleName',
    	'CDF_mlastName',
    	'CDF_mcountry',
    	'CDF_passport_file',
    	'CDF_passp_Num',
    	'CDF_passp_issCountry',
    	'CDF_passp_issDate',
    	'CDF_passp_expiryDate',
    	'CDF_gateway_file',
    	'CDF_dual_passp_Num',
    	'CDF_dual_passp_issCountry',
    	'CDF_dual_passp_issDate',
    	'CDF_dual_passp_expiryDate',
    	'CDF_visa_c1',
    	'CDF_visa_d',
    	'CDF_visa_us_issdate',
    	'CDF_visa_us_expdate',
    	'CDF_visa_sc_s',
    	'CDF_visa_sc_m',
    	'CDF_visa_sc_issdate',
    	'CDF_visa_sc_expdate',
    	'CDF_visa_sc_iss_ct',
    	'CDF_o1_issdate',
    	'CDF_o1_expdate',
    	'CDF_o1_ct',
    	'CDF_o1_rest',
    	'CDF_o2_issdate',
    	'CDF_o2_expdate',
    	'CDF_o2_ct',
    	'CDF_o2_rest',
    	'CDF_english',
    	'CDF_spanish',
    	'CDF_french',
    	'CDF_italian',
    	'CDF_portuguese',
    	'CDF_german',
    	'CDF_dutch',
    	'CDF_cantonese',
    	'CDF_mandarin',
    	'CDF_other_lang1',
    	'CDF_other_lang1_lvl',
    	'CDF_other_lang2',
    	'CDF_other_lang2_lvl',
    	'CDF_other_lang3',
    	'CDF_other_lang3_lvl',
    	'CDF_other_lang4',
    	'CDF_other_lang4_lvl',
    	'CDF_EM_name',
    	'CDF_EM_relationship',
    	'CDF_EM_phoneNum',
    	'CDF_EM_cel',
    	'CDF_EM_email1',
    	'CDF_EM_email2',
    	'CDF_EM_add',
    	'CDF_EM_city',
    	'CDF_EM_zip',
    	'CDF_EM_state',
    	'CDF_EM_country_code',
    	'CDF_EM2_name',
    	'CDF_EM2_relationship',
    	'CDF_EM2_phoneNum',
    	'CDF_EM2_cel',
    	'CDF_EM2_email1',
    	'CDF_EM2_email2',
    	'CDF_EM2_add',
    	'CDF_EM2_city',
    	'CDF_EM2_zip',
    	'CDF_EM2_state',
    	'CDF_EM2_country_code',
    	'CDF_B_same_inc',
    	'CDF_B_name',
    	'CDF_B_phoneNum',
    	'CDF_B_cel',
    	'CDF_B_add',
    	'CDF_B_city',
    	'CDF_B_zip',
    	'CDF_B_state',
    	'CDF_B_country_code',
    	'CDF_B_relationship',
    	'CDF_B_email',
    	'CDF_B_per',
    	'CDF_B_name1',
    	'CDF_B_phoneNum1',
    	'CDF_B_cel1',
    	'CDF_B_add1',
    	'CDF_B_city1',
    	'CDF_B_zip1',
    	'CDF_B_state1',
    	'CDF_B_country_code1',
    	'CDF_B_relationship1',
    	'CDF_B_email1',
    	'CDF_B_per1',
    	'CDF_brand',
    	'CDF_hbu',
    	'CDF_interview',
    	'CDF_position',
    	'CDF_hp',
    	'CDF_recruiter',
    	'CDF_jobcode',
    	'CDF_dateinterview',
    	'CDF_e1',
    	'created_on',
    	'last_updated_on',
    	'updated_by',
    	'CDF_Finish_ind',
    	'img_avatar',
    	'esign',
    	'CDF_gateway_acknowledge',
    	'CDF_drug_acknowledge',
    	'CDF_disapproved_id',
    	'CDF_disapproved_comments',
    	'hireright_submit_ind',
    	'hireright_submit_attempt_ind',
    	'hireright_data_constructed_ind',
    	'hireright_submit_executed_ind'
    ];

    public $timestamps = false;

    protected $table = 'form_candidate_data';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getHirerightSubmitInd()
    {
    	return $this->hireright_submit_ind;
    }

    public function getHirerightSubmitAttemptInd()
    {
    	return $this->hireright_submit_attempt_ind;
    }

    public function getHirerightDataConstructedInd()
    {
    	return $this->hireright_data_constructed_ind;
    }

    public function getHirerightExecutedInd()
    {
    	return $this->hireright_submit_executed_ind;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->first();
    	return $result;
    }

    public function getByUserIdAll($user_id)
    {
        $result = $this->where('user_id', '=', $user_id)
                        ->get();
        return $result;
    }

    public function saveNew($data, $is_return_id)
    {
    	$this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
    	$obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function savePersonalDataForm($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }

    public function removeById($id)
    {
        $result = $this->where('id', '=', $id)
                        ->delete();
        return $result;
    }
}
