<?php

namespace CTRAC\Model\ILO;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $fillable = [
		'name',
		'code',
		'brand',
		'created_at',
		'updated_at'
	];

    protected $table = 'ilo_ship_list';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function getCode()
    {
    	return $this->code;
    }

    public function getBrand()
    {
    	return $this->brand;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	$obj = $this->orderBy('brand', 'asc');
    	return $obj->get();
    }
}
