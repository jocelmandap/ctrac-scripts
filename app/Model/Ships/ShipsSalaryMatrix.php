<?php

namespace CTRAC\Model\Ships;

use Illuminate\Database\Eloquent\Model;

class ShipsSalaryMatrix extends Model
{
	protected $fillable = [
		'ship_code',
		'job_int_code',
		'country_code',
		'city_id',
		'currency_id',
		'hours_per_week',
		'min_monthly_salary',
		'guarantee_monthly_salary',
		'basic_monthly_salary',
		'initial_overtime',
		'supplemental_overtime',
		'overtime_rate',
		'cba_vac_rate',
		'contract_length_weeks',
		'vacation_length_weeks',
		'tipping_position',
		'active_ind',
		'created_by',
		'created_on',
		'last_updated_by',
		'last_updated_on'
	];

	const CREATED_AT = 'created_on';
	const UPDATED_AT = 'last_updated_on';

    protected $table = 'ships_salary_matrix';

    protected $primaryKey = 'ship_mtrx_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->ship_mtrx_id;
    }

    public function getShipCode()
    {
    	return $this->ship_code;
    }

    public function getJobCode()
    {
    	return $this->job_int_code;
    }

    public function getCountryCode()
    {
    	return $this->country_code;
    }

    public function getCityId()
    {
    	return $this->city_id;
    }

    public function getCurrencyId()
    {
    	return $this->currency_id;
    }

    public function getHoursPerWeek()
    {
    	return $this->hours_per_week;
    }

    public function getMinMonthlySalary()
    {
    	return $this->min_monthly_salary;
    }

    public function getGuaranteeMonthlySalary()
    {
    	return $this->guarantee_monthly_salary;
    }

    public function getBasicMonthlySalary()
    {
    	return $this->basic_monthly_salary;
    }

    public function getSupplementalOvertime()
    {
    	return $this->supplemental_overtime;
    }

    public function getInitialOvertime()
    {
    	return $this->initial_overtime;
    }

    public function getOvertimeRate()
    {
    	return $this->overtime_rate;
    }

    public function getCbaVacRate()
    {
    	return $this->cba_vac_rate;
    }

    public function getContractLengthweeks()
    {
    	return $this->contract_length_weeks;
    }

    public function getVacationLengthWeeks()
    {
    	return $this->vacation_length_weeks;
    }

    public function getTippingPosition()
    {
    	return $this->tipping_position;
    }

    public function getIsActive()
    {
    	return $this->active_ind;
    }

    public function getById($ship_mtrx_id)
    {
    	$result = $this->where('ship_mtrx_id', '=', $ship_mtrx_id)
    					->first();
    	return $result;
    }

    public function getByJobCode($job_code, $is_active = 1)
    {
    	$result = $this->where([
    						['job_int_code', '=', $job_code],
    						['active_ind', '=', $is_active]
    					])->get();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
