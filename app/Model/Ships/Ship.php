<?php

namespace CTRAC\Model\Ships;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $fillable = [
    	'ship_code',
    	'ship_name',
    	'brand_id'
    ];

    public $timestamps = false;

    protected $primaryKey = 'ship_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->ship_id;
    }

    public function getShipCode()
    {
    	return $this->ship_code;
    }

    public function getShipName()
    {
    	return $this->ship_name;
    }

    public function getBrandId()
    {
    	return $this->brand_id;
    }

    public function getById($ship_id)
    {
    	$result = $this->where('ship_id', '=', $ship_id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
