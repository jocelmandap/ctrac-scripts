<?php

namespace CTRAC\Model\Countries;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
    	'country_code',
    	'country_name',
    	'nationality',
    	'region_id',
    	'dialing_code',
    	'gmt',
    	'blocked',
    	'blocked_nat',
    	'status_code_id',
    	'country_code_id'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getCountryCode()
    {
    	return $this->country_code;
    }

	public function getCountryName()
	{
		return $this->country_name;
	}

	public function getNationality()
	{
		return $this->nationality;
	}    

	public function getRegionId()
	{
		return $this->region_id;
	}

	public function getGmt()
	{
		return $this->gmt;
	}

	public function getBlocked()
	{
		return $this->blocked;
	}    

	public function getBlockedNationality()
	{
		return $this->blocked_nat;
	}

	public function getStatusCodeId()
	{
		return $this->status_code_id;
	}

	public function getCountryCodeId()
	{
		return $this->country_code_id;
	}

	public function getAll()
	{
		$result = $this->where('country_code', '!=', '')
                        ->get();
        return $result;
	}
}
