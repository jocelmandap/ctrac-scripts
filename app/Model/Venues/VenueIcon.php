<?php

namespace CTRAC\Model\Venues;

use Illuminate\Database\Eloquent\Model;

class VenueIcon extends Model
{
    protected $fillable = [
    	'venue_id',
    	'required_months',
    	'img_path',
    	'created_by',
    	'updated_by',
    	'update_date',
    	'created_date'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getVenueId()
    {
    	return $this->venue_id;
    }

    public function getRequiredMonths()
    {
    	return $this->required_months;
    }

    public function getImgPath()
    {
    	return $this->img_path;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
