<?php

namespace CTRAC\Model\Venues;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    protected $fillable = [
    	'code',
    	'name',
    	'created_by',
    	'updated_by',
    	'update_date',
    	'created_date'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function icon()
    {
        return $this->hasOne('CTRAC\Model\Venues\VenueIcon');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getCode()
    {
    	return $this->code;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
