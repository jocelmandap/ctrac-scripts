<?php

namespace CTRAC\Model\Statuses;

use Illuminate\Database\Eloquent\Model;

class StatusGroup extends Model
{
    protected $fillable = [
    	'id',
    	'status_group_name',
    	'fail_ind',
    	'status_group_rank',
    	'career_id'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->status_group_name;
    }

    public function getFailInd()
    {
    	return $this->fail_ind;
    }

    public function getRank()
    {
    	return $this->status_group_rank;
    }

    public function getCareerId()
    {
    	return $this->career_id;
    }
}
