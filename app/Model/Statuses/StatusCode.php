<?php

namespace CTRAC\Model\Statuses;

use Illuminate\Database\Eloquent\Model;

class StatusCode extends Model
{
    protected $fillable = [
    	'status_code_name',
    	'status_code_description',
    	'communication_id',
    	'status_code_trigger',
    	'status_group',
    	'possible_outcomes',
    	'removable',
    	'show_ind',
    	'require_comments',
    	'disable_assignment',
    	'offer_letter_ind',
    	'bypass_recipe',
    	'bypass_recipe_school',
    	'offer_letter_origin_status_ind',
    	'fail_status_group_id'
    ];

    public $timestamps = false;

    protected $primaryKey = 'status_code_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function statusGroup()
    {
        return $this->hasOne('CTRAC\Model\Statuses\StatusGroup', 'id', 'status_group');
    }

    public function getId()
    {
    	return $this->status_code_id;
    }

    public function getName()
    {
    	return $this->status_code_name;
    }

    public function getDescription()
    {
    	return $this->status_code_description;
    }

    public function getStatusGroup()
    {
    	return $this->status_group;
    }

    public function getById($status_code_id)
    {
    	$result = $this->where('status_code_id', '=', $status_code_id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function getByIds($status_code_ids = [])
    {
        $result = $this->whereIn('status_code_id', $status_code_ids)
                        ->get();

        return $result;
    }
}
