<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillSetCategory extends Model
{
    protected $fillable = [
    	'skill_category_name',
    	'parent_id',
    	'active'
    ];

    protected $primaryKey = 'skill_category_id';

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->skill_category_id;
    }

    public function getName()
    {
    	return $this->skill_category_name;
    }

    public function getParentId()
    {
    	return $this->parent_id;
    }

    public function getIsActive()
    {
    	return $this->active;
    }

    public function getById($skill_category_id)
    {
    	$result = $this->where('skill_category_id', '=', $skill_category_id)
    					->first();
    	return $result;
    }

    public function getAllActive()
    {
    	$obj = $this->where('active', '1');
    	return $obj->get();
    }

    public function getAll()
    {
    	return static::all();
    }
}
