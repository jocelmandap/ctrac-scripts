<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillLevel extends Model
{
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'modified_on';

    protected $fillable = [
    	'skill_level_name',
    	'skill_level_desc',
    	'is_active',
    	'created_on',
    	'created_by',
    	'modified_on',
    	'modified_by'
    ];

    protected $primaryKey = 'skill_level_id';

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->skill_level_id;
    }

    public function getName()
    {
    	return $this->skill_level_name;
    }

    public function getDescription()
    {
    	return $this->skill_level_desc;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getById($skill_level_id)
    {
    	$result = $this->where('skill_level_id', '=', $skill_level_id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
