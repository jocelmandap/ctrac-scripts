<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillCombo extends Model
{
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'modified_on';

    protected $fillable = [
    	'skill_combo_code',
    	'skill_combo_name',
    	'skill_combo_desc',
    	'is_active',
    	'created_on',
    	'created_by',
    	'modified_on',
    	'modified_by'
    ];

    protected $primaryKey = 'skill_combo_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function skills()
    {
        return $this->hasMany('CTRAC\Model\Skills\SkillComboDetail', 'skill_combo_id');
    }

    public function getId()
    {
    	return $this->skill_combo_id;
    }

    public function getComboCode()
    {
    	return $this->skill_combo_code;
    }

    public function getComboName()
    {
    	return $this->skill_combo_name;
    }

    public function getComboDesc()
    {
    	return $this->skill_combo_desc;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getById($skill_combo_id)
    {
    	$result = $this->where('skill_combo_id', '=', $skill_combo_id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
