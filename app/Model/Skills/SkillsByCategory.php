<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillsByCategory extends Model
{
	protected $fillable = [
		'skill_category_id',
		'skill_set_id'
	];

	public $timestamps = false;

	public function setDBConnection($connection)
	{
	    $this->connection = $connection;
	}

	public function getId()
	{
		return $this->id;
	}

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = true)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = true)
    {
        $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveSkillByCategory($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }
            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
