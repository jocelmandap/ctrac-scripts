<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillSet extends Model
{
    protected $fillable = [
    	'skill_name',
    	'skill_description',
    	'active',
    	'show_skill_ind'
    ];

    protected $primaryKey = 'skill_set_id';

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->skill_set_id;
    }

	public function getName()
	{
		return $this->skill_name;
	}

	public function getDescription()
	{
		return $this->skill_description;
	}

	public function getIsActive()
	{
		return $this->active;
	}

	public function getShowSkillInd()
	{
		return $this->show_skill_ind;
	}

	public function getById($skill_set_id)
	{
		$result = $this->where('skill_set_id', '=', $skill_set_id)
						->first();

		return $result;
	}

	public function getByName($skill_name)
	{
		$result = $this->where('skill_name', '=', $skill_name)
						->first();
		return $result;
	}

	public function getAll()
	{
		return static::all();
	}

	public function saveNew($data, $is_return_id = true)
	{
	    $this->fill($data);

	    $result = $this->save();

	    if ($is_return_id)
	    {
	        return $this->getId();
	    }

	    return $result;
	}

	public function saveChanges($data, $is_return_id = true)
	{
	    $obj = $this->getById($data['skill_set_id']);

	    if (!is_null($obj))
	    {
	        $obj->fill($data);

	        if ($obj->save())
	        {
	            if ($is_return_id)
	            {
	                return $obj->getId();
	            }

	            return true;
	        }            
	    }

	    return false;
	}

	public function saveSkill($data, $is_return_id = true)
	{
	    if (!empty($data))
	    {
	        if (isset($data['skill_set_id']) && !empty($data['skill_set_id']))
	        {
	            return $this->saveChanges($data, $is_return_id);
	        }
	        return $this->saveNew($data, $is_return_id);
	    }
	    return false;
	}    
}
