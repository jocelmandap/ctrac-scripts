<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillSetJoin extends Model
{
    protected $fillable = [
    	'skill_set_id',
    	'foreign_id',
    	'foreign_table',
    	'is_a_must',
    	'skill_level_id',
    	'read',
    	'write',
    	'speak',
    	'comprehend',
    	'last_update'
    ];

    public $timestamps = false;

    protected $primaryKey = 'skill_set_join_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->skill_set_join_id;
    }

    public function getSkillSetId()
    {
        return $this->skill_set_id;
    }

    public function getUserId()
    {
    	return $this->foreign_id;
    }

    public function getForeignTable()
    {
    	return $this->foreign_table;
    }

    public function getById($skill_set_join_id)
    {
    	$result = $this->where('skill_set_join_id', '=', $skill_set_join_id)
    					->first();

    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('foreign_id', '=', $user_id)
    					->get();

    	return $result;
    }

    public function createSkillSetJoins($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function updateSkillSetJoins($data, $is_return_id = false)
    {
        $user_venue = $this->getById($data['skill_set_join_id']);

        if (!is_null($user_venue))
        {
            $user_venue->fill($data);

            if ($user_venue->save())
            {
                if ($is_return_id)
                {
                    return $user_venue->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveSkillSetJoins($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['skill_set_join_id']) && !empty($data['skill_set_join_id']))
            {
                return $this->updateSkillSetJoins($data, $is_return_id);
            }

            return $this->createSkillSetJoins($data, $is_return_id);
        }
        return false;
    }
}
