<?php

namespace CTRAC\Model\Skills;

use Illuminate\Database\Eloquent\Model;

class SkillComboDetail extends Model
{
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'modified_on';

    protected $fillable = [
    	'skill_combo_detail_id',
    	'skill_combo_id',
    	'skill_set_id',
    	'skill_level_id',
    	'is_required',
    	'is_active',
    	'created_on',
    	'created_by',
    	'modified_on',
    	'modified_by'
    ];

    protected $primaryKey = 'skill_combo_detail_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function skillDetail()
    {
        return $this->hasOne('CTRAC\Model\Skills\SkillSet', 'skill_set_id', 'skill_set_id');
    }

    public function skillLevel()
    {
        return $this->hasOne('CTRAC\Model\Skills\SkillLevel', 'skill_level_id', 'skill_level_id');
    }

    public function getId()
    {
    	return $this->skill_combo_detail_id;
    }

    public function getSkillComboId()
    {
    	return $this->skill_combo_id;
    }

    public function getSkillSetId()
    {
    	return $this->skill_set_id;
    }

    public function getSkillLevelId()
    {
    	return $this->skill_level_id;
    }

    public function getIsRequired()
    {
    	return $this->is_required;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getById($skill_combo_detail_id)
    {
    	$result = $this->where('skill_combo_detail_id', '=', $skill_combo_detail_id)
    					->first();

    	return $result;
    }

    public function getBySkillComboId($skill_combo_id)
    {
    	$result = $this->where('skill_combo_id', '=', $skill_combo_id)
    					->get();

    	return $result;
    }
}
