<?php

namespace CTRAC\Model\Hirerights;

use Illuminate\Database\Eloquent\Model;

class HirerightPackage extends Model
{
	protected $fillable = [
		'account_code',
		'package_number',
		'package_name',
		'is_package',
		'is_active',
		'career_id',
		'package_scope_id',
		'date_created',
		'created_by',
		'date_modified',
		'modified_by'
	];

	public $timestamps = false;

    protected $primaryKey = 'package_id';

    public function setDBConnection($connection)
    {
    	$this->connection = $connection;
    }

    public function getId()
    {
    	return $this->package_id;
    }

	public function getAccountCode()
	{
		return $this->account_code;
	}

	public function getPackageNumber()
	{
		return $this->package_number;
	}

	public function getPackageName()
	{
		return $this->package_name;
	}

	public function getIsPackage()
	{
		return $this->is_package;
	}

	public function getIsActive()
	{
		return $this->is_active;
	}

	public function getCareerId()
	{
		return $this->career_id;
	}

	public function getPackageScopeId()
	{
		return $this->package_scope_id;
	}

	public function getDateCreated()
	{
		return $this->date_created;
	}

	public function getCreatedBy()
	{
		return $this->created_by;
	} 

	public function getDateModified()
	{
		return $this->date_modified;
	} 

	public function getModifiedBy()
	{
		return $this->modified_by;
	}

	public function getById($package_id)
	{
		$result = $this->where('package_id', '=', $package_id)
						->first();
		return $result;
	}

	public function getAll()
	{
		return static::all();
	}    
}
