<?php

namespace CTRAC\Model\Hirerights;

use Illuminate\Database\Eloquent\Model;

class HirerightJobintPackage extends Model
{
    protected $fillable = [
    	'jobint_id',
    	'package_id',
    	'is_us',
    	'is_active',
    	'time_created'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get the jobint details
     */
    public function jobInt()
    {
        return $this->hasOne('CTRAC\Model\Jobs\JobInt', 'job_int_id', 'jobint_id');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getJobintId()
    {
    	return $this->jobint_id;
    }

    public function getPackageId()
    {
    	return $this->package_id;
    }

    public function getIsUS()
    {
    	return $this->is_us;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getTimeCreated()
    {
    	return $this->time_created;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByJobIntId($job_int_id)
    {
    	$result = $this->where('jobint_id', '=', $job_int_id)
    					->get();
    	return $result;
    }
}
