<?php

namespace CTRAC\Model\Hirerights;

use Illuminate\Database\Eloquent\Model;

class HirerightAllowedStatus extends Model
{
    protected $fillable = [
    	'status_code_id',
    	'date_created',
    	'created_by',
    	'date_modified',
    	'modified_by',
    	'is_active',
    ];

    public $timestamps = false;

    protected $table = 'hireright_allowed_status';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getStatusCodeId()
    {
    	return $this->status_code_id;
    }

    public function getDateCreated()
    {
    	return $this->date_created;
    }

    public function getCreatedBy()
    {
    	return $this->created_by;
    }

    public function getDateModified()
    {
    	return $this->date_modified;
    }

    public function getModifiedBy()
    {
    	return $this->modified_by;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	$result = $this->where('id', '!=', 0)
                        ->get();
        return $result;
    }
}
