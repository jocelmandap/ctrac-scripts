<?php

namespace CTRAC\Model\Hirerights;

use Illuminate\Database\Eloquent\Model;

class HirerightCrewRequest extends Model
{
    protected $fillable = [
    	'request_id',
    	'user_id',
    	'job_application_id',
    	'package_id',
    	'service_name',
    	'hireright_id',
    	'reg_id',
    	'order_service_number',
    	'request_status',
    	'order_status',
    	'result_status',
    	'report_status',
    	'adjudication_status',
    	'comments',
    	'in_package',
    	'is_active',
    	'date_created',
    	'created_by',
    	'date_modified',
    	'modified_by',
    	'date_last_checked'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getRequestId()
    {
    	return $this->request_id;
    }

    public function getJobApplicationId()
    {
    	return $this->job_application_id;
    }

    public function getPackageId()
    {
    	return $this->package_id;
    }

    public function getServiceName()
    {
    	return $this->service_name;
    }

    public function getHirerightId()
    {
    	return $this->hireright_id;
    }

    public function getRequestStatus()
    {
    	return $this->request_status;
    }

    public function getAdjudicationStatus()
    {
    	return $this->adjudication_status;
    }

    public function getById($id)
    {
    	$result = $this->where('id', '=', $id)
    					->first();
    	return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->get();
    	return $result;
    }
}
