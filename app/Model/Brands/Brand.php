<?php

namespace CTRAC\Model\Brands;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
    	'brand_id',
    	'brand_name',
    	'brand_description',
    	'brand_email',
    	'created_on',
    	'created_by',
    	'last_updated_on',
    	'last_updated_by'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->brand_id;
    }

    public function getName()
    {
    	return $this->brand_name;
    }

    public function getDescription()
    {
    	return $this->brand_description;
    }

    public function getEmail()
    {
    	return $this->brand_email;
    }

    public function getById($brand_id)
    {
    	$result = $this->where('brand_id', '=', $brand_id)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
