<?php

namespace CTRAC\Model\Common;

interface CRUDInterface
{
    public function saveNew($data, $is_return_id);

    public function saveChanges($data, $is_return_id);

    public function removeById($id);

    public function getById($id);
}
