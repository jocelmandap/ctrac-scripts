<?php

namespace CTRAC\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class JobGroup extends Model
{
    protected $fillable = [
    	'job_group_code',
    	'job_group_name',
    	'job_marketing_description',
    	'department_id',
    	'wizard_views_data'
    ];

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getCode()
    {
    	return $this->job_group_code;
    }

	public function getName()
	{
		return $this->job_group_name;
	}

	public function getMarketingDesc()
	{
		return $this->job_marketing_description;
	}    

	public function getDepartmentId()
	{
		return $this->department_id;
	}

	public function getWizardViewData()
	{
		return $this->wizard_views_data;
	}

	public function getByGroupCode($job_group_code)
	{
		$result = $this->where('job_group_code', '=', $job_group_code)
						->first();
		return $result;
	}

	public function getAll()
	{
		return static::all();
	}
}
