<?php

namespace CTRAC\Model\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JobApplication extends Model
{
    protected $fillable = [
		'user_id',
		'job_request_id',
		'application_date',
		'application_last_modified',
		'application_origin',
		'application_step',
		'qualification_answers_score',
		'application_status',
		'assignation_id',
		'video_interview_id',
		'upload_video',
		'source',
		'status_code_id',
		'status_code_date',
		'agreement_signature',
		'job_int_id',
		'skills_score',
		'brand_id',
		'emp_ind',
		'source_career_ind',
		'esign',
		'recipe_dtlId',
		'recipe_score',
		'remove_from_actual_ind',
		'file_path',
		'meet_recipe_ind'
	];

	public $timestamps = false;

    protected $primaryKey = 'job_application_id';

    public function setDBConnection($connection)
    {
    	$this->connection = $connection;
    }

    /**
     * Get Job Int Details
     */
    public function jobInt()
    {
        return $this->hasOne('CTRAC\Model\Jobs\JobInt', 'job_int_id', 'job_int_id');
    }

    /**
     * Get Country Details
     */
    public function country()
    {
        return $this->hasOne('CTRAC\Model\Countries\Country', 'country_code', 'application_origin');
    }

    /**
     * Get Status Details
     */
    public function status()
    {
        return $this->hasOne('CTRAC\Model\Statuses\StatusCode', 'status_code_id', 'status_code_id');
    }

    /**
     * User Details
     */
    public function user()
    {
        return $this->hasOne('CTRAC\Model\Users\User', 'user_id', 'user_id');
    }

     /**
     * User Job Request
     */
    public function jobRequest()
    {
        return $this->hasOne('CTRAC\Model\Jobs\JobRequest', 'job_request_id', 'job_request_id');
    }

    /**
     * Job Application history records
     */
    public function historyRecords()
    {
        return $this->hasMany('CTRAC\Model\Histories\HistoryRecord', 'job_application_id', 'job_application_id');
    }

    /**
     * Get Exam Details
     */
    public function medicalExamAnswer()
    {
        return $this->hasOne('CTRAC\Model\Users\MedicalApplicationUserAnswer', 'job_application_id', 'job_application_id');
    }

    public function getId()
    {
    	return $this->job_application_id;
    }

	public function getUserId()
	{
		return $this->user_id;
	}

	public function getJobRequestId()
	{
		return $this->job_request_id;
	}

	public function getApplicationDate()
	{
		return $this->application_date;
	}

	public function getApplicationOrigin()
	{
		return $this->application_origin;
	}

	public function getApplicationStep()
	{
		return $this->application_step;
	}

	public function getQualificationAnswersScore()
	{
		return $this->qualification_answers_score;
	}

	public function getApplicationStatus()
	{
		return $this->application_status;
	}

	public function getStatusCodeId()
	{
		return $this->status_code_id;
	}

	public function getJobIntId()
	{
		return $this->job_int_id;
	}

	public function getById($job_application_id)
	{
		$result = $this->where('job_application_id', '=', $job_application_id)
						->first();
		return $result;
	}  

	public function getByUserId($user_id)
	{
		$result = $this->where('user_id', '=', $user_id)
						->get();
		return $result;
	}

	public function saveNew($data, $is_return_id = false)
	{
	    $this->fill($data);

	    $result = $this->save();

	    if ($is_return_id)
	    {
	        return $this->getId();
	    }

	    return $this->getById($this->getId());
	}

	public function saveChanges($data, $is_return_id = false)
	{
	    $obj = $this->getById($data['job_application_id']);

	    if (!is_null($obj))
	    {
	        $obj->fill($data);

	        if ($obj->save())
	        {
	            if ($is_return_id)
	            {
	                return $obj->getId();
	            }

	            return $obj;
	        }            
	    }

	    return false;
	}

	public function saveJobApplication($data, $is_return_id = true)
	{
	    if (!empty($data))
	    {
	        if (isset($data['job_application_id']) && !empty($data['job_application_id']))
	        {
	            return $this->saveChanges($data, $is_return_id);
	        }

	        return $this->saveNew($data, $is_return_id);
	    }
	    return false;
	}

	public function getAutoFailPDFandOfferLetterReport (
	    $status_code = [],
	    $from_status_code_date = '',
	    $to_status_code_date = '',
	    $offer_letter_ids = []
	)
	{
	    return $this->select(
	    					'u.user_id AS UserId',
	                        DB::raw("CONCAT(pd.first_name, ' ', pd.last_name) AS Name"),
	                        'u.email AS Email',
	                        'sc.status_code_id AS StatusId',
	                        'sc.status_code_name AS StatusName',
	                        'dc.ck_list_desc AS OfferDocName',
	                        DB::raw("CONCAT(ji.job_int_code, ' - ', ji.job_int_name) AS JobCode"),
	                        DB::raw("DATE_FORMAT(pdf_doc.date_modified, '%b %d, %Y- %h:%i %p') AS PDFDateModified"),
	                        DB::raw("DATE_FORMAT(passport_doc.date_modified, '%b %d, %Y- %h:%i %p') AS PassportDateModified"),
	                        DB::raw("DATE_FORMAT(offer_doc.date_created, '%b %d, %Y- %h:%i %p') AS OfferDateCreated"),
	                        DB::raw("DATE_FORMAT(offer_doc.date_modified, '%b %d, %Y- %h:%i %p') AS OfferDateModified"),
	                        DB::raw("CONCAT(pdadmin.first_name, ' ', pdadmin.last_name) AS Recruiter"),
	                        DB::raw(
	                            "CASE
	                                WHEN (s.show_id = 'hpartner' AND bp.type = 1) THEN 'Hiring Partner'
	                                WHEN (s.show_id = 'hpartner' AND bp.type = 2) THEN 'Referral Source'
	                                WHEN s.show_id = 'school' THEN 'School'
	                                ELSE 'Direct'
	                            END AS SourceType"
	                        ),
	                        DB::raw(
	                            "CASE
	                                WHEN s.show_id = 'hpartner' THEN bp.company_name
	                                ELSE s.name
	                            END AS Source"
	                        ),
	                        'hr.status_code_id AS HistoryStatusId',
	                        DB::raw("DATE_FORMAT(hr.history_record_date, '%b %d, %Y- %h:%i %p') AS HistoryRecordDate")
	                    )
	                    ->join('document_checklist_form_uploads AS pdf_doc', function ($join) {
	                        $join->on('job_applications.user_id', '=', 'pdf_doc.user_id')
	                             ->where('pdf_doc.ck_list_id', 109);
	                    })
	                    ->join('document_checklist_form_uploads AS passport_doc', function ($join) {
	                        $join->on('job_applications.user_id', '=', 'passport_doc.user_id')
	                             ->where('passport_doc.ck_list_id', 37);
	                    })
	                    ->join('document_checklist_form_uploads AS offer_doc', function ($join) use ($offer_letter_ids) {
	                        $join->on('job_applications.user_id', '=', 'offer_doc.user_id')
	                             ->whereIn('offer_doc.ck_list_id', $offer_letter_ids);
	                    })
	                    ->leftJoin('document_checklist AS dc', 'offer_doc.ck_list_id', '=', 'dc.ck_list_id')
	                    ->join('history_records AS hr', function ($join) use ($status_code) {
	                        $join->on('job_applications.user_id', '=', 'hr.user_id')
	                             ->whereIn('hr.status_code_id', $status_code);
	                    })
	                    ->join('users AS uadmin', 'offer_doc.created_by', '=', 'uadmin.user_id')
	                    ->join('personal_data AS pdadmin', 'uadmin.personal_data_id', '=', 'pdadmin.personal_data_id')
	                    ->join('users AS u', 'job_applications.user_id', '=', 'u.user_id')
	                    ->join('personal_data AS pd', 'u.personal_data_id', '=', 'pd.personal_data_id')
	                    ->leftJoin('sources AS s', 'u.source_id', '=', 's.id')
	                    ->leftJoin('business_partners AS bp', 'u.hp_register_company', '=', 'bp.company_name')
	                    ->join('status_codes AS sc', 'job_applications.status_code_id', '=', 'sc.status_code_id')
	                    ->leftJoin('job_int AS ji', 'job_applications.job_int_id', '=', 'ji.job_int_id')
	                    ->whereIn('job_applications.status_code_id', $status_code)
	                    ->where('job_applications.status_code_date', '>=', $from_status_code_date)
	                    ->where('job_applications.status_code_date', '<=', $to_status_code_date)
	                    ->groupBy('job_applications.user_id')
	                    ->get();
	} 
}
