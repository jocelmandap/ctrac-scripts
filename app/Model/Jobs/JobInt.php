<?php

namespace CTRAC\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class JobInt extends Model
{
    protected $fillable = [
    	'job_int_code',
    	'job_int_name',
    	'job_int_desc',
    	'job_int_max',
    	'job_int_min_range',
    	'job_int_max_range',
    	'job_request_id',
    	'job_int_status',
    	'job_int_brand',
    	'pipeline_id',
    	'pipeline_code',
    	'allow_shorten',
    	'career_id',
    	'stripe'
    ];

    public $timestamps = false;

    protected $primaryKey = 'job_int_id';

    protected $table = 'job_int';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->job_int_id;
    }

    public function getCode()
    {
    	return $this->job_int_code;
    }

    public function getName()
    {
    	return $this->job_int_name;
    }

    public function getDescription()
    {
    	return $this->job_int_desc;
    }

    public function getRequestId()
    {
    	return $this->job_request_id;
    }

    public function getBrand()
    {
    	return $this->job_int_brand;
    }

    public function getStripe()
    {
        return $this->stripe;
    }

    public function getById($job_int_id)
    {
    	$result = $this->where('job_int_id', '=', $job_int_id)
    					->first();
    	return $result;
    }

    public function getByJobIntCode($job_int_code)
    {
        $result = $this->where('job_int_code', '=', $job_int_code)
                        ->first();
        return $result;
    }

    public function getAllByJobIntCode($job_int_code)
    {
        $result = $this->where('job_int_code', '=', $job_int_code)
                        ->get();
        return $result;
    }

    public function getAllByStatus($job_int_status)
    {
        $result = $this->where('job_int_status', '=', $job_int_status)
                        ->get();
        return $result;
    }

    public function getByJobIntCodeAndJobIntStatus($job_int_code, $job_int_status)
    {
        $result = $this->where([
            ['job_int_code', '=', $job_int_code],
            ['job_int_status', '=', $job_int_status]
        ])->first();
        return $result;
    }

    public function getAll()
    {
    	return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['job_int_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveJobInt($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['job_int_id']) && !empty($data['job_int_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
