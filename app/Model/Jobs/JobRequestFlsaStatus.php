<?php

namespace CTRAC\Model\Jobs;

use Illuminate\Database\Eloquent\Model;

class JobRequestFlsaStatus extends Model
{
    protected $fillable = [
    	'flsa_status_code',
    	'flsa_status_name',
    	'active_ind',
    	'created_on',
    	'created_by',
    	'last_updated_on',
    	'last_updated_by'
    ];

    public $timestamps = false;

    protected $table = 'job_request_flsa_status';

    protected $primaryKey = 'flsa_status_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->flsa_status_id;
    }

    public function getFlsaStatusCode()
    {
    	return $this->flsa_status_code;
    }

    public function getFlsaStatusName()
    {
    	return $this->flsa_status_name;
    }

    public function getById($flsa_status_id)
    {
    	$result = $this->where('flsa_status_id', '=', $flsa_status_id)
    					->first();
    	return $result;
    }

    public function getByName($flsa_status_name)
    {
    	$result = $this->where('flsa_status_name', '=', $flsa_status_name)
    					->first();
    	return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
