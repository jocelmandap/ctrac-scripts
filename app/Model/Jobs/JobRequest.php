<?php

namespace CTRAC\Model\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JobRequest extends Model
{
	const CREATED_AT = 'created_on';
	const UPDATED_AT = 'last_updated_on';

    protected $fillable = [
    	'job_group_code',
    	'brand_id',
    	'user_id',
    	'external_request_title',
    	'job_request_title',
    	'job_request_description',
    	'job_specific_questions_timer',
    	'job_specific_questions_timer_msr',
    	'request_create_date',
    	'request_start_date',
    	'request_end_date',
    	'request_status',
    	'wizard_views_data',
    	'required_views',
    	'max_allowed',
    	'pre_count',
    	'pre_que_ind',
    	'possible_outcome',
    	'peoplesoft_requisition_id',
    	'peoplesoft_position_id',
    	'hiring_manager_id',
    	'hireright_id',
    	'city_id',
    	'job_type_id',
    	'eeo_job_category_id',
    	'flsa_status_id',
    	'position_type_id',
    	'recruiter_user_id',
    	'created_on',
    	'created_by',
    	'last_updated_on',
    	'last_updated_by'
    ];

    protected $primaryKey = 'job_request_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->job_request_id;
    }

    public function getGroupCode()
    {
    	return $this->job_group_code;
    }

    public function getBrandId()
    {
    	return $this->brand_id;
    }

    public function getUserId()
    {
    	return $this->user_id;
    }

    public function getTitle()
    {
    	return $this->job_request_title;
    }

    public function getDescription()
    {
    	return $this->job_request_description;
    }

    public function getRequestStatus()
    {
        return $this->request_status;
    }

    public function getPeoplesoftRequisitionId()
    {
        return $this->peoplesoft_requisition_id;
    }

    public function getById($job_request_id)
    {
    	$result = $this->where('job_request_id', '=', $job_request_id)
    					->first();

    	return $result;
    }

    public function getByPeoplesoftRequisitionId($peoplesoft_requisition_id)
    {
        $result = $this->where('peoplesoft_requisition_id', '=', $peoplesoft_requisition_id)
                        ->first();

        return $result;
    }

    public function getByUserId($user_id)
    {
    	$result = $this->where('user_id', '=', $user_id)
    					->first();

    	return $result;
    }

    public function getAll()
    {
        return static::all();
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['job_request_id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveJobRequest($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['job_request_id']) && !empty($data['job_request_id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }

    public function getPDFListReport (
        $application_status = [],
        $pdf_document_status = [],
        $job_codes = [],
        $from_offer_date = '',
        $to_offer_date = ''
    )
    {
        $query = $this->select(
                            DB::raw("CONCAT(pd.first_name, ' ', pd.last_name) AS Name"),
                            "u.email AS Email",
                            "u.country_code AS Nationality",
                            "ji.job_int_code AS JobCode",
                            "ji.job_int_brand AS Brand",
                            "job_request_title AS JobTitle",
                            DB::raw("DATE_FORMAT(dgd.Offer_Date_Created, '%Y-%m-%d') AS OfferSent"),
                            DB::raw("DATE_FORMAT(dgd.Offer_Date_Accepted, '%Y-%m-%d') AS OfferAccepted"),
                            DB::raw("DATE_FORMAT(dcfu.date_modified, '%Y-%m-%d') AS PDFSubmitted"),
                            DB::raw(
                                "CASE
                                    WHEN dcfu.is_completed = 1 THEN 'Pending Document Approval'
                                    WHEN dcfu.is_completed = 2 THEN 'Completed'
                                    WHEN dcfu.is_completed = 3 THEN 'Rejected'
                                    WHEN dcfu.is_completed = 5 THEN 'Audited'
                                    ELSE 'Document Not Submitted'
                                END AS Status"
                            ),
                            DB::raw(
                                "CASE
                                    WHEN (s.show_id = 'hpartner' AND bp.type = 1) THEN 'Hiring Partner'
                                    WHEN (s.show_id = 'hpartner' AND bp.type = 2) THEN 'Referral Source'
                                    WHEN s.show_id = 'school' THEN 'School'
                                    ELSE 'Direct'
                                END AS SourceType"
                            ),
                            DB::raw(
                                "CASE
                                    WHEN s.show_id = 'hpartner' THEN bp.company_name
                                    ELSE s.name
                                END AS Source"
                            )
                        )
                        ->join('job_applications AS ja', function ($join) use ($application_status) {
                            $join->on('job_requests.job_request_id', '=', 'ja.job_request_id')
                                 ->whereIn('ja.status_code_id', $application_status);
                        })
                        ->join('document_checklist_form_uploads AS dcfu', function ($join) use ($pdf_document_status) {
                            $join->on('ja.user_id', '=', 'dcfu.user_id')
                                 ->where('dcfu.ck_list_id', 109)
                                 ->whereIn('dcfu.is_completed', $pdf_document_status);
                        })
                        ->join('users AS u', 'ja.user_id', '=', 'u.user_id')
                        ->join('personal_data AS pd', 'u.personal_data_id', '=', 'pd.personal_data_id')
                        ->join('document_general_data AS dgd', function ($join) use ($from_offer_date, $to_offer_date) {
                            $join->on('dgd.user_id', '=', 'ja.user_id')
                                 ->where('dgd.Offer_Date_Created', '>=', $from_offer_date)
                                 ->where('dgd.Offer_Date_Created', '<=', $to_offer_date);
                        })
                        ->leftJoin('sources AS s', 'u.source_id', '=', 's.id')
                        ->leftJoin('business_partners AS bp', 'u.hp_register_company', '=', 'bp.company_name');

        if (!empty( $job_codes ))
        {
            $query->join('job_int AS ji', function ($join) use ($job_codes) {
                $join->on('ja.job_int_id', '=', 'ji.job_int_id')
                     ->whereIN('ji.job_int_code', $job_codes);
            });
        }
        else
        {
            $query->join('job_int AS ji', 'ja.job_int_id', '=', 'ji.job_int_id');
        }
        
        return $query->get();
    }
}
