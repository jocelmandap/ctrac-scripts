<?php

namespace CTRAC\Model\Scripts;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class CtracSupportHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'ctrac_support_history_type_id',
        'note',
        'ctrac_user_email',
        'created_at',
        'created_by'
    ];

    public $timestamps = false;

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getHistoryTypeId()
    {
    	return $this->ctrac_support_history_type_id;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getCreatedBy()
    {
        return $this->created_by;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getById($id)
    {
        $result = $this->where('id', '=', $id)
                        ->first();
        return $result;
    }

    public function getByUserId($user_id)
    {
        $result = $this->where('user_id', '=', $user_id)
                        ->get();
        return $result;
    }

    public function saveNew($data, $is_return_id)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $result;
    }

    public function saveChanges($data, $is_return_id)
    {
        $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return true;
            }            
        }

        return false;
    }

    public function saveSupportHistory($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}