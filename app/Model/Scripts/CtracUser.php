<?php

namespace CTRAC\Model\Scripts;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use CTRAC\Helpers\DateUtility;
use CTRAC\Notifications\WelcomeUser;
use CTRAC\Notifications\UserPassword;
use CTRAC\Notifications\RequisitionResult;
use CTRAC\Notifications\DeactivateAccount;
use CTRAC\Notifications\UpdateAlloteeAdmin;
use File;

class CtracUser extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'profile_photo', 
        'description', 
        'password', 
        'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
    	return $this->belongsToMany('CTRAC\Model\Scripts\CtracRole', 'ctrac_role_ctrac_user', 'ctrac_user_id', 'ctrac_role_id');
    }

    /**
    * @param string|array $roles
    */
    public function authorizeRoles($roles)
    {
    	if (is_array($roles)) {
        	return $this->hasAnyRole($roles) || 
                	abort(401, 'This action is unauthorized.');
      	}
      	return $this->hasRole($roles) || 
            	abort(401, 'This action is unauthorized.');
    }

    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles)
    {
    	return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
    	return null !== $this->roles()->where('name', $role)->first();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getProfilePhoto()
    {
        if (!is_null($this->profile_photo) && !empty($this->profile_photo))
        {
            if (\File::exists(public_path('uploads/Profiles/' . $this->profile_photo)))
            {
                return asset('/uploads/Profiles/' . $this->profile_photo);
            }
        }
        return asset('/images/default-profile-photo.jpg');
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getIsActive()
    {
        return $this->is_active;
    }

    public function getCreatedAt($is_raw = true, $date_format = 'F d, Y h:i A')
    {
        if (!$is_raw && !is_null($this->created_at))
        {
            return DateUtility::changeDateFormat($this->created_at, 'Y-m-d H:i:s', $date_format);
        }
        return $this->created_at;
    }

    public function getUpdatedAt($is_raw = true, $date_format = 'F d, Y h:i A')
    {
        if (!$is_raw && !is_null($this->updated_at))
        {
            return DateUtility::changeDateFormat($this->updated_at, 'Y-m-d H:i:s', $date_format);
        }
        return $this->updated_at;
    }

    public function getById($user_id)
    {
        $result = $this->where('id', '=', $user_id)
                        ->first();
        return $result;
    }

    public function checkEmailIfValid($email, $user_id)
    {
        $user_detail = $this->getById($user_id);
        $results = $this->where('email', '=', $email)
                        ->get();

        if ($results->isEmpty())
        {
            return true;
        }

        $is_valid = true;
        foreach ($results as $result) 
        {
            if ($result->getId() != $user_detail->getId())
            {
                $is_valid = false;
            }
        }

        return $is_valid;
    }

    public function getAll()
    {
        return CtracUser::all()->sortByDesc("created_at");
    }

    /**
     * Send Email Notification for Successfull Registration
     *
     */
    public function emailWelcomeMessage()
    {
        $this->notify(new WelcomeUser());
    }

    /**
     * Send Email Notification for Login Credentials
     *
     * @param  string  $password
     * @return void
     */
    public function emailLoginCredentials($password)
    {
        $this->notify(new UserPassword($password));
    }

    /**
     * Send Email Notification Result for Requisition Daily Uploaded
     *
     * @param  obj  $uploader
     * @param  array  $logs
     * @return void
     */
    public function emailRequisitionResults($uploader, $logs)
    {
        $this->notify(new RequisitionResult($uploader, $logs));
    }

    /**
     * Send Email Notification for De-activating Account
     *
     * @return void
     */
    public function emailDeactivateAccount()
    {
        $this->notify(new DeactivateAccount());
    }

    /**
     * Send Email Notification for updating Allotee Details
     *
     * @param  obj  $crew_details
     * @return void
     */
    public function emailAlloteeUpdate($crew_details)
    {
        $this->notify(new UpdateAlloteeAdmin($crew_details));
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $this->getById($this->getId());
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $obj;
            }            
        }

        return false;
    }

    public function saveUser($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
