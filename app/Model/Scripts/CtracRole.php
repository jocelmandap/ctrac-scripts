<?php

namespace CTRAC\Model\Scripts;

use Illuminate\Database\Eloquent\Model;
use CTRAC\Helpers\DateUtility;

class CtracRole extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'name', 'description'
	];

    public function users()
    {
     	return $this->belongsToMany('CTRAC\Model\Scripts\CtracUser', 'ctrac_role_ctrac_user', 'ctrac_role_id', 'ctrac_user_id');
    }

    public function pages()
    {
    	return $this->belongsToMany('CTRAC\Model\Scripts\CtracPage', 'ctrac_role_pages', 'ctrac_role_id', 'ctrac_page_id');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function getDescription()
    {
    	return $this->description;
    }

    public function getAll()
    {
        return CtracRole::all();
    }

    public function getById($role_id)
    {
        $result = $this->where('id', '=', $role_id)
                        ->first();
        return $result;
    }

    public function getByName($name)
    {
        $result = $this->where('name', '=', $name)
                        ->first();
        return $result;
    }

    public function getCreatedAt($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->created_at))
        {
            return DateUtility::changeDateFormat($this->created_at, 'Y-m-d H:i:s', $date_format);
        }
        return $this->created_at;
    }

    public function getUpdatedAt($is_raw = true, $date_format = 'F d, Y')
    {
        if (!$is_raw && !is_null($this->updated_at))
        {
            return DateUtility::changeDateFormat($this->updated_at, 'Y-m-d H:i:s', $date_format);
        }
        return $this->updated_at;
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $this->getById($this->getId());
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $obj;
            }            
        }

        return false;
    }

    public function saveRole($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
