<?php

namespace CTRAC\Model\Scripts;

use Illuminate\Database\Eloquent\Model;

class CtracPage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'route_name',
        'order',
        'is_active',
        'parent_id',
        'icon_class',
        'bg_class'
    ];

    public function roles()
    {
    	return $this->belongsToMany('CTRAC\Model\Scripts\CtracRole', 'ctrac_role_pages', 'ctrac_page_id', 'ctrac_role_id');
    }

    public function getId()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function getDescription()
    {
    	return $this->description;
    }

    public function getRouteName()
    {
    	return $this->route_name;
    }

    public function getOrder()
    {
    	return $this->order;
    }

    public function getIsActive()
    {
    	return $this->is_active;
    }

    public function getParentId()
    {
    	return $this->parent_id;
    }

    public function getIconClass()
    {
    	return $this->icon_class;
    }

    public function getBgClass()
    {
    	return $this->bg_class;
    }

    public function getAll()
    {
        return CtracPage::all();
    }

    public function getById($page_id)
    {
        $result = $this->where('id', '=', $page_id)
                        ->first();
        return $result;
    }

    public function saveNew($data, $is_return_id = false)
    {
        $this->fill($data);

        $result = $this->save();

        if ($is_return_id)
        {
            return $this->getId();
        }

        return $this->getById($this->getId());
    }

    public function saveChanges($data, $is_return_id = false)
    {
        $obj = $this->getById($data['id']);

        if (!is_null($obj))
        {
            $obj->fill($data);

            if ($obj->save())
            {
                if ($is_return_id)
                {
                    return $obj->getId();
                }

                return $obj;
            }            
        }

        return false;
    }

    public function savePage($data, $is_return_id = true)
    {
        if (!empty($data))
        {
            if (isset($data['id']) && !empty($data['id']))
            {
                return $this->saveChanges($data, $is_return_id);
            }

            return $this->saveNew($data, $is_return_id);
        }
        return false;
    }
}
