<?php

namespace CTRAC\Model\Schools;

use Illuminate\Database\Eloquent\Model;

class SchoolDegreeType extends Model
{
    protected $fillable = [
    	'degree_type_name',
    	'degree_type_code',
    	'show_ind'
    ];

    public $timestamps = false;

    protected $primaryKey = 'degree_type_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->degree_type_id;
    }

    public function getName()
    {
    	return $this->degree_type_name;
    }

    public function getCode()
    {
    	return $this->degree_type_code;
    }

    public function getShowInd()
    {
    	return $this->show_ind;
    }

    public function getById($degree_type_id)
    {
    	$result = $this->where('degree_type_id', '=', $degree_type_id)
    					->first();
    	return $result;
    }

    public function getByDegreeTypeCode($degree_type_code)
    {
        $result = $this->where('degree_type_code', '=', $degree_type_code)
                        ->first();
        return $result;
    }

    public function getAll()
    {
    	return static::all();
    }
}
