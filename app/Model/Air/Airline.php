<?php

namespace CTRAC\Model\Air;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    protected $fillable = [
		'airline_code',
		'airline_name'
	];

    protected $table = 'airlines';

    protected $primaryKey = 'airline_id';

    public function setDBConnection($connection)
    {
        $this->connection = $connection;
    }

    public function getId()
    {
    	return $this->airline_id;
    }

    public function getCode()
    {
    	return $this->airline_code;
    }

    public function getName()
    {
    	return $this->airline_name;
    }

    public function getById($airline_id)
    {
    	$result = $this->where('airline_id', '=', $airline_id)
    					->first();
    	return $result;
    }
}
