<?php

namespace CTRAC\Exports;

use CTRAC\Model\Scripts\CtracUser;
use Maatwebsite\Excel\Concerns\FromCollection;

class TestExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return CtracUser::all();
    }
}
