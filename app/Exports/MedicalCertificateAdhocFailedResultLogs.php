<?php

namespace CTRAC\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MedicalCertificateAdhocFailedResultLogs implements FromView
{
	public $history_logs;

	public function __construct($history_logs)
	{
	    $this->history_logs = $history_logs;
	}

    public function view(): View
    {
        return view('exports.adhoc-medical-certificate-fail-result-logs', [
            'history_logs' => $this->history_logs
        ]);
    }
    
}