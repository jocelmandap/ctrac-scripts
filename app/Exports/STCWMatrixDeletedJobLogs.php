<?php

namespace CTRAC\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class STCWMatrixDeletedJobLogs implements FromView
{
	public $delete_cert_logs;

	public function __construct($delete_cert_logs)
	{
	    $this->delete_cert_logs = $delete_cert_logs;
	}

    public function view(): View
    {
        return view('exports.stcw-matrix-deleted-job-logs', [
            'delete_cert_logs' => $this->delete_cert_logs
        ]);
    }
    
}