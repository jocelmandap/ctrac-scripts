<?php

namespace CTRAC\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class GovernmentDocumentUploaderFailResultLogs implements FromView
{
	public $history_logs_not_updated;

	public function __construct($history_logs_not_updated)
	{
	    $this->history_logs_not_updated = $history_logs_not_updated;
	}

    public function view(): View
    {
        return view('exports.government-uploader-fail-result-logs', [
            'history_logs_not_updated' => $this->history_logs_not_updated
        ]);
    }
    
}