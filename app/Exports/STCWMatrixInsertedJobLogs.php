<?php

namespace CTRAC\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class STCWMatrixInsertedJobLogs implements FromView
{
	public $history_logs;

	public function __construct($history_logs)
	{
	    $this->history_logs = $history_logs;
	}

    public function view(): View
    {
        return view('exports.stcw-matrix-inserted-job-logs', [
            'history_logs' => $this->history_logs
        ]);
    }
    
}