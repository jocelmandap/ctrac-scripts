<?php

namespace CTRAC\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CrewPendingAndCompletedDocumentsExport implements FromView
{
	public $user_document_data;

	public function __construct($user_document_data)
	{
	    $this->user_document_data = $user_document_data;
	}

    public function view(): View
    {
        return view('exports.pending-completed-documents', [
            'user_document_data' => $this->user_document_data
        ]);
    }
    
}
