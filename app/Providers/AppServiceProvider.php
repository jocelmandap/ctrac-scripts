<?php

namespace CTRAC\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.side-menu', function ($view) {
            $user = Auth::user();
            $role = $user->roles->first();
            $parent_pages = [];
            $child_pages = [];

            $pages = $role->pages->where('is_active', '=', 1)->sortBy('order');

            if (!$pages->isEmpty()) 
            {
                foreach ($pages as $parent_page) 
                {
                    if ($parent_page->getParentId() == 0) {
                        $parent_pages[] = $parent_page;
                    }
                }

                foreach ($pages as $child_page) 
                {
                    if ($child_page->getParentId() != 0) {
                        $child_pages[] = $child_page;
                    }
                }
            }

            $view->with('role', $role);
            $view->with('pages', $pages);
            $view->with('parent_pages', $parent_pages);
            $view->with('child_pages', $child_pages);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
