<?php

namespace CTRAC\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequisitionResult extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($uploader, $logs)
    {
        $this->uploader = $uploader;
        $this->logs = $logs;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = [
            'user' => $notifiable,
            'uploader' => $this->uploader,
            'logs' => $this->logs
        ];

        return (new MailMessage)
                    ->from(config('emails.emailer_from'), 'Royal Caribbean Cruises Ltd')
                    ->subject('C-TRAC APP: Daily Requisition Results for ' . date('F d, Y'))
                    ->view('emails.requisition-results', $data);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
