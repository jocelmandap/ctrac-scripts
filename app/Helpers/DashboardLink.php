<?php

namespace CTRAC\Helpers;

class DashboardLink
{
    const MAIN_DASHBOARD = 'main-dashboard';
    const VENUE_SKILLS_PAGE = 'venue-skills-page';
    const BACKGROUND_CHECKS_PAGE = 'background-checks-page';
    const CRISIS_MANAGEMENT_PAGE = 'crisis-management';
    const CTRAC_REPORTS_PAGE = 'ctrac-reports';
    const CTRAC_GATEWAYS_PAGE = 'ctrac-gateways';
}
