<?php

namespace CTRAC\Helpers;

use Illuminate\Database\Eloquent\Model;

class StringUtility
{
	public static function csvToArray($filename = '', $delimiter = ',')
	{
		if (!file_exists($filename) || !is_readable($filename))
		{
			return false;
		}		       

	   $header = null;
	   $data = array();
	   if (($handle = fopen($filename, 'r')) !== false)
	   {
	       while (($row = fgetcsv($handle, 4000, $delimiter)) !== false)
	       {
	           if (!$header)
	               $header = $row;
	           else
	               $data[] = array_combine($header, $row);
	       }
	       fclose($handle);
	   }

	   return $data;
	}

	public static function crisisCsvToArray($filename = '', $delimiter = ',')
	{
		if (!file_exists($filename) || !is_readable($filename))
		{
			return false;
		}		       

		$header = null;
		$data = array();
		if (($handle = fopen($filename, 'r')) !== false)
		{

		    while (($row = fgetcsv($handle, 4000, $delimiter)) !== false)
		    {
		        if (!$header)
		           $header = $row;
		        else
		           $data[] = array_combine($header, $row);
		    }
		    fclose($handle);
		}

		return $data;
	}

	public static function requisitionCsvToArray($filename = '', $delimiter = ',')
	{
		if (!file_exists($filename) || !is_readable($filename))
		{
			return false;
		}		       

		$header = null;
		$data = array();
		$count = 0;
		if (($handle = fopen($filename, 'r')) !== false)
		{

		    while (($row = fgetcsv($handle, 4000, $delimiter)) !== false)
		    {
		    	if ($count != 0)
		    	{
		    		if (!$header) {
		    		   $header = $row;
		    		} else {
		    		   $data[] = $row;
		    		}
		    	}
		        $count++;
		    }
		    fclose($handle);
		}

		return $data;
	}

	public static function requisitionDailyCsvToArray ($filename = '')
	{
		$header = null;
		$data = array();
		if (($handle = fopen($filename, 'r')) !== false)
		{
		    $line = array();
			$i = 0;

			while ( $row = fgets ($handle, 9999) ) {

				if (!$header)
				{
				    $header = explode( "\r\n", $row );
				}
				else
				{
					$datum = explode( "\r\n", $row );
					if (isset($datum[0]) && !empty($datum))
					{
						$data[] = explode( "|", $datum[0] );
					}
				}
			}
		    fclose($handle);
		}

		return $data;
	}
}
