<?php

namespace CTRAC\Helpers;

use DateTime;
use Log;

class DateUtility
{
    /**
     * Change date format of a given date into a given new date format
     * @param Date $DateString
     * @param String $originalDateFormat
     * @param String $newDateFormat
     * @return Date
     */
    public static function changeDateFormat(
        $dateString,
        $originalDateFormat = 'm/d/Y',
        $newDateFormat = 'Y-m-d'
    )
    {
        if (!is_null($dateString))
        {
            try
            {
                $myDateTime = DateTime::createFromFormat($originalDateFormat, $dateString);
                if (is_null($myDateTime)) throw new Exception("Empty Date String", 1);
                if (is_object($myDateTime))
                {
                    return $myDateTime->format($newDateFormat);
                }
            }
            catch(Exception $e)
            {
                Log::info($e->getMessage());
            }
        }
        return null;
    }

    /**
     * Compute how many days two dates differ
     * @param String $date1
     * @param String $date2
     * @return Integer
     */
    public static function countDaysDifference($date1, $date2)
    {
        $days = 0;
        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        return $days;
    }

    /**
     * Get the Birthdate with the exact year of birth
     * @param  integer $age
     * @param  string $date_format
     * @return string 
     */
    public static function getBirthdateByAge($age, $date_format = 'Y-m-d')
    {
        if (is_numeric($age) && $age > 0 && $age <= self::MAX_AGE)
        {
            $year = date('Y') - (int)$age;
            $month = date('m');
            $day = date('d');
            
            if (checkdate($month, $day, $year))
            {
                $dateString = $year . '-' . $month . '-' . $day;
                return self::changeDateFormat($dateString, self::FORMAT_Y_M_D, $date_format);
            }
        }

        return date($date_format);
    }

    public static function getAgeByBirthdate($birthdate)
    {
        if (!empty($birthdate))
        {
            $birthdate = new DateTime($birthdate);
            $today = new DateTime('today');
            $age = $birthdate->diff($today)->y;

            return $age;
        }

        return null;
    }

}
