<?php

namespace CTRAC\Helpers;

use CTRAC\Helpers\curlUtility;

class clientHttpHelper extends curlUtility
{
	protected $handle;

	function __construct($handle)
	{
		$this->handle = $handle;
	}

	private function formBuilder( $form_params )
	{
		return http_build_query( $form_params );
	}

	public function post($form_params = [], $return_json = true)
	{
		$this->init( $this->handle );
		// $this->setOpt(CURLOPT_URL, $this->handle);
		$this->setOpt(CURLOPT_POST, count($form_params));
		$this->setOpt(CURLOPT_RETURNTRANSFER, true);
		$this->setOpt(CURLOPT_POSTFIELDS, filter_var($this->formBuilder( $form_params ), FILTER_SANITIZE_STRING));
		$this->setOpt(CURLOPT_SSL_VERIFYPEER, false);
		$this->setOpt(CURLOPT_SSL_VERIFYHOST, false);
		$raw_result = $this->exec();

		if ($return_json)
		{
			return $this->json();
		}
		return $raw_result;
	}
}