<?php

namespace CTRAC\Helpers;

use Mail;
use Log;

class MailUtility
{
    public static function testMail($email)
    {
        $data = [
            'email' => $email,
            'name' => 'Test Mail',
            'subject' => 'Welcome to Test Mailer',
            'app_url' => 'none',
            'app_logo_url' => 'none'
        ];
        $emailTemplate = 'emails.test';

        Mail::send($emailTemplate, $data, function ($message) use ($data)
        {
            $message->from('noreply@mail.com', 'Test Mail');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });

        Log::info('Sent welcome email to ' . $email);
    }

    public static function sendActivation($email, $user, $key)
    {
        $data = [
            'email' => $email,
            'user' => $user,
            'key' => $key,
            'subject' => 'Activate your account',
            'app_url' => config('balicover.email.app_url'),
            'app_logo_url' => config('balicover.email.app_logo_url')
        ];
        $emailTemplate = 'emails.activation';

        Mail::send($emailTemplate, $data, function ($message) use ($data)
        {
            $message->from(config('balicover.email.no_reply_email'), 'BaliCover.com.au');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });

        Log::info('Sent activation key to ' . $data['email']);
    }

    public static function sendWelcome($email, $name)
    {
        $data = [
            'email' => $email,
            'name' => $name,
            'subject' => 'Welcome to BaliCover.com.au',
            'app_url' => config('balicover.email.app_url'),
            'app_logo_url' => config('balicover.email.app_logo_url')
        ];
        $emailTemplate = 'emails.welcome';

        Mail::send($emailTemplate, $data, function ($message) use ($data)
        {
            $message->from(config('balicover.email.no_reply_email'), 'BaliCover.com.au');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });

        Log::info('Sent welcome email to ' . $email);
    }

}
