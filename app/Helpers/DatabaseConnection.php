<?php

namespace CTRAC\Helpers;

use Illuminate\Database\Eloquent\Model;

class DatabaseConnection
{
	const CTRAC_QA = 'ctrac_qa_db';
	const CTRAC_DEV = 'ctrac_dev_db';
	const CTRAC_PROD = 'ctrac_prod_db';
}
