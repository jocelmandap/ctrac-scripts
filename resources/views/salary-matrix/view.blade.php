@extends('layouts.master')

@section('title', 'CTRAC Ship Salary Matrix')

@section('header-content')

	<h1>
		Ship Salary Matrix
		<small>Uploader</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Ship Salary Matrix</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('ship_salary_matrix_submit') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="upload_csv" class="col-md-4 control-label">Upload a CSV File</label>

						    <div class="col-md-6">
						        <input type="file" id="upload_csv" name="upload_csv">

						        <p class="help-block">Please download the sample format <a href="{{ asset('uploads/salary-matrix/sample_ship_salary_matrix.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label class="col-md-4 control-label">&nbsp;</label>

						    <div class="col-md-6">
						        <button type="submit" class="btn btn-primary">Submit</button>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>
		<!-- ./col -->

		@if (Session::has('history_logs'))
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body">
						<p>Total Updated: <label class="label label-info">{{ Session::get('total_updated') }}</label></p>
						<p>Total New: <label class="label label-warning">{{ Session::get('total_new') }}</label></p>
						<p>Over All Total updates: <label class="label label-success">{{ (Session::get('total_new') + Session::get('total_updated')) }}</label></p>
						@if (!empty(Session::get('history_logs')))
							<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
								<thead>
						            <tr>
						            	<th>#</th>
						                <th>Ship Code</th>
						                <th>Job Code</th>
						            </tr>
								</thead>
								<tbody>
									@php
										$count = 1;
									@endphp
									@foreach (Session::get('history_logs') as $log)
										
										<tr>
											<td>{{ $count }}</td>
											<td>{{ $log['ship_code'] }}</td>
											<td>{{ $log['job_code'] }}</td>
										</tr>
										
										@php
											$count++;
										@endphp

									@endforeach	
								</tbody>
							</table>
						@endif
					</div>	
				</div>
			</div>
		@endif

		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection