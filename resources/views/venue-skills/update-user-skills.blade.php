@extends('layouts.master')

@section('title', 'Update User Skills')

@section('header-content')

	<h1>
		Update User Skills
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Update User Skills</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-3">
						<div class="form-group">
						    <label>Brands</label>
						    <select class="form-control adding-type" name="brand_id" id="brand_id">
			                    @if (!$brands->isEmpty())
			                    	
									@foreach ($brands as $brand)

										<option 
											value="{{ $brand->getId() }}" 
											@if (isset($default_brand) && $default_brand == $brand->getId()) selected @endif
											>{{ $brand->getName() }}({{ $brand->getId() }})</option>
										
									@endforeach

			                    @else
									<option value="">Empty</option>
			                    @endif
						    </select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						    <label>Ships</label>
						    <select class="form-control adding-type" name="ship_id" id="ship_id">
			                    @if (!$ships->isEmpty())
			                    	
									@foreach ($ships as $ship)

										<option 
											value="{{ $ship->getShipCode() }}" 
											@if (isset($default_ship) && $default_ship == $ship->getId()) selected @endif
											>{{ $ship->getShipName() }}</option>
										
									@endforeach

			                    @else
									<option value="">Empty</option>
			                    @endif
						    </select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
						    <label>Cost Center</label>
						    <input 
						    	type="text" 
						    	class="form-control" 
						    	id="cost_center" 
						    	name="cost_center" 
						    	value="{{ (isset($default_cost_center)) ? $default_cost_center : '' }}"
						    	placeholder="Please add Cost Center here">
						    <span class="error-container cost-center-error-container"></span>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
						    <label>Job Int Code</label>
						    <input 
						    	type="text" 
						    	class="form-control" 
						    	id="job_int_code" 
						    	name="job_int_code" 
						    	value="{{ (isset($default_job_int_code)) ? $default_job_int_code : '' }}"
						    	placeholder="Please add job code here">
						    	<input type="hidden" id="is_valid">
						    <span class="error-container job-int-code-error-container"></span>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
		    			    <label>&nbsp;</label>
		    			    <button type="button" id="submit_form" class="btn btn-primary" style="display: block;">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->

	<div class="row">
		<div class="col-md-4 total-results-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
		<div class="col-md-4 skill-combos-container data-containers" style="display: none;">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Update Skills of <span class="total-results" style="font-weight: bold; color: #d82828;"></span> Users</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
					    <label>Skill Combos</label>
					    <select class="form-control adding-type" name="skill_combo_id" id="skill_combo_id">
		                    @if (!$skill_combos->isEmpty())
		                    	
								@foreach ($skill_combos as $skill_combo)

									<option value="{{ $skill_combo->getId() }}">{{ $skill_combo->getComboCode() }}</option>
									
								@endforeach

		                    @else
								<option value="">Empty</option>
		                    @endif
					    </select>
					</div>
					<div class="form-group" style="text-align: right;">
	    			    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#update-confirm-modal">Update <i class="fa fa-refresh"></i></button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 total-update-results-container" style="display: none;">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Total Update Results</h3>
				</div>
				<div class="box-body">
					<!-- small box -->
					<div class="small-box total-results-container-box bg-aqua">
						<div class="inner">
							<h3 class="total-update-results"></h3>
							<p>Total Updated User Skills</p>
						</div>
						<div class="icon">
							<i class="fa fa-check" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8 skill-combo-details-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
		<div class="col-md-4 user-list-result-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>

@endsection

@section('modal-content')
	
	@include('venue-skills.modals.update-user-skills-confirmation')

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var UpdateUserSkillsHandler = {
				init : function () {
					UpdateUserSkillsHandler.reset_results();
					UpdateUserSkillsHandler.trigger_submit();
					UpdateUserSkillsHandler.trigger_update();
					UpdateUserSkillsHandler.on_change_skill_combos();
				},
				reset_results : function () {
					$('.data-containers').slideUp();
					setTimeout(function () {
						$('.total-results-container').html('');
					}, 1000);
				},
				trigger_submit : function () {
					$(document).on('click', '#submit_form', function () {
						var job_int_code = $('#job_int_code').val();
						$('.total-update-results-container').slideUp();
						$('.total-results-container-box').removeClass('bg-aqua').removeClass('bg-red').removeClass('bg-green');

						if (job_int_code.length == 0) {
							$('.job-int-code-error-container').text('Please add Job Int Code').slideDown();
							UpdateUserSkillsHandler.trigger_submit();
							return false;
						}
						
						UpdateUserSkillsHandler.validate_job_code();						
					});
				},
				trigger_update : function () {
					$(document).on('click', '#update_user_skills', function () {
						var skill_combo_id = $('#skill_combo_id').val();
						var user_ids = $('#user_ids').val();
						
						$.ajax({
							url: '{{ route('update_user_skill_set_joins') }}',
							type: 'POST',
							dataType: 'json',
							data: {skill_combo_id: skill_combo_id, user_ids: user_ids},
							beforeSend : function () {
								$('#update_user_skills')
									.prop('disabled', true)
									.removeClass('btn-info')
									.addClass('btn-warning')
									.html('Submitting...');
							},
							success : function (response) {
								console.log(response);
								if (response.status == true) {
									$('.total-results-container-box').removeClass('bg-aqua').addClass('bg-green');
									$('.total-update-results').text(response.total_results);

									setTimeout(function () {
										$('#update-confirm-modal').modal('hide');
										$('#update_user_skills')
											.removeClass('btn-warning')
											.addClass('btn-success')
											.html('Successfully Submitted!');
										$('.total-update-results-container').slideDown();
									}, 400);
								} else {
									$('.total-results-container-box').removeClass('bg-aqua').addClass('bg-red');
									$('.total-update-results-container').slideUp();
									$('.total-update-results').text('0');
									$('#update_user_skills')
										.removeClass('btn-warning')
										.addClass('btn-danger')
										.html('Not Submitted!');

									$('#update-confirm-modal').modal('hide');
									alert('User Skills not updated!');
								}

								setTimeout(function () {
									$('#update-confirm-modal').modal('hide');
									$('#update_user_skills')
										.prop('disabled', false)
										.removeClass('btn-success')
										.removeClass('btn-danger')
										.addClass('btn-info')
										.html('Yes');
								}, 1500);
							}
						});						
					});
				},
				get_submit_results : function (data) {
					$.ajax({
						url: '{{ route('get_users_by_ship_and_job_code') }}',
						type: 'POST',
						dataType: 'json',
						data: data,
						beforeSend : function () {
							$('#submit_form')
								.prop('disabled', true)
								.removeClass('btn-primary')
								.addClass('btn-warning')
								.html('Submitting...');
						},
						success : function (response) {
							if (response.status == true) {
								$('.total-results').text(response.total);
								$('.total-results-container').html(response.content);
								$('.user-list-result-container').html(response.user_content);
								$('#skill_combo_id').trigger('change');

								setTimeout(function () {
									$('#submit_form')
										.removeClass('btn-warning')
										.addClass('btn-success')
										.html('Successfully Submitted!');
									$('.data-containers').slideDown();
								}, 400);
							} else {
								UpdateUserSkillsHandler.reset_results();
								$('#submit_form')
									.removeClass('btn-warning')
									.addClass('btn-danger')
									.html('Not Submitted!');
								UpdateUserSkillsHandler.reset_results();
								alert('Data empty!');
							}

							setTimeout(function () {
								$('#submit_form')
									.prop('disabled', false)
									.removeClass('btn-success')
									.removeClass('btn-danger')
									.addClass('btn-primary')
									.html('Submit');
							}, 1500);
						}
					});
				},
				validate_job_code : function () {
					var status_code_id = $('#status_code_id').val();
					var brand_id = $('#brand_id').val();
					var ship_code = $('#ship_id').val();
					var cost_center = $('#cost_center').val();
					var job_int_code = $('#job_int_code').val();

					var data = {
						brand_id : brand_id,
						ship_code : ship_code,
						cost_center : cost_center,
						job_int_code : job_int_code
					};

					$.ajax({
						url: '{{ route('validate_job_int_code') }}',
						type: 'POST',
						dataType: 'json',
						data: {job_int_code: job_int_code},
						success : function (response) {
							if (response.status == true) {
								if (response.is_available == true) {
									UpdateUserSkillsHandler.get_submit_results(data);
									$('.job-int-code-error-container').text('').slideUp();
									$('#is_valid').val('');
								} else {
									UpdateUserSkillsHandler.reset_results();
									$('.job-int-code-error-container').text('Please add a valid Job Code').slideDown();
									$('#is_valid').val('0');
								}
							} else {
								UpdateUserSkillsHandler.reset_results();
								$('.job-int-code-error-container').text('Please add a valid Job Code').slideDown();
								$('#is_valid').val('0');
							}
						}
					});
				},
				on_change_skill_combos : function () {
					$(document).on('change', '#skill_combo_id', function () {
						var skill_combo_id = $(this).val();

						$.ajax({
							url: '{{ route('get_skill_combo_details') }}',
							type: 'POST',
							dataType: 'json',
							data: {skill_combo_id: skill_combo_id},
							success : function (response) {
								if (response.status == true) {
									$('.skill-combo-details-container').html(response.content);
								} else {
									alert('Skill Combo not available!');
								}
							}
						});
					});
				}
			};

			UpdateUserSkillsHandler.init();
		});
	</script>
@endsection