@extends('layouts.master')

@section('title', 'Add TLC Codes | Venue Skills')

@section('header-content')

	<h1>
		TLC Codes
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Add TLC Codes</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add <i class="fa fa-plus"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('save_tlc_codes') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group">
								<label for="upload_user_csv">Upload a CSV File</label>
								<input type="file" id="upload_tlc_csv" name="upload_tlc_csv">

								<p class="help-block">Please download the sample format <a href="{{ asset('uploads/venues/tlc-codes.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
							</div>
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')

@endsection