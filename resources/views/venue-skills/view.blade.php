@extends('layouts.master')

@section('title', 'Venue Skills')

@section('header-content')

	<h1>
		Venue Skills
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Venue Skills</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row">
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>&nbsp;</h3>

					<p>Add User Venue Skills</p>
				</div>
				<div class="icon">
					<i class="fa fa-plus"></i>
				</div>
				<a href="{{ route('add_venue_skills_view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>{{ $total_user_venues }}</h3>

					<p>Truncate User Venue Skills</p>
				</div>
				<div class="icon">
					<i class="fa fa-trash""></i>
				</div>
				<a href="{{ route('clear_venue_skills') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>&nbsp;</h3>

					<p>Update User Skills</p>
				</div>
				<div class="icon">
					<i class="fa fa-refresh" aria-hidden="true"></i>
				</div>
				<a href="{{ route('update_user_skills_view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>&nbsp;</h3>

					<p>Upload TLC codes</p>
				</div>
				<div class="icon">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</div>
				<a href="{{ route('add_tlc_codes_view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-navy color-palette">
				<div class="inner">
					<h3>&nbsp;</h3>

					<p>Add TLC Code Skills</p>
				</div>
				<div class="icon">
					<i class="fa fa-asterisk" aria-hidden="true" style="color: #424242;"></i>
				</div>
				<a href="{{ route('add_tlc_code_skills_view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-gray color-palette">
				<div class="inner">
					<h3>&nbsp;</h3>

					<p>Upload Employee TLC Details</p>
				</div>
				<div class="icon">
					<i class="fa fa-users" aria-hidden="true"></i>
				</div>
				<a href="{{ route('add_employee_tlc_codes_view') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>
	<!-- /.row -->

@endsection