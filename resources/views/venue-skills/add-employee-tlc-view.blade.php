@extends('layouts.master')

@section('title', 'Add Employee TLC Codes | Venue Skills')

@section('header-content')

	<h1>
		Add Employee TLC Codes
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Add Employee TLC Codes</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add <i class="fa fa-plus"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('save_employee_tlc_codes') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group">
								<label for="upload_user_csv">Upload a CSV File</label>
								<input type="file" id="upload_tlc_csv" name="upload_tlc_csv">

								<p class="help-block">Please download the sample format <a href="{{ asset('uploads/venues/EDHTLC-20180115.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
							</div>
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
		<!-- ./col -->
		@if (Session::has('failed_employee_training_uploads') && !empty(Session::get('failed_employee_training_uploads')))
			<div class="col-md-6">
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Faileds <i class="fa fa-times"></i></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						@php
							$failed_data = Session::get('failed_employee_training_uploads');
						@endphp

						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>JDE ID</th>
									<th>TLC Code</th>
									<th>TLC Description</th>
								</tr>
							</thead>
							<tbody>
								@if (!empty($failed_data))
									
									@foreach ($failed_data as $row)
										
										<tr>
											<td>{{ $row['EmployeeID'] }}</td>
											<td>{{ $row['TLCcode'] }}</td>
											<td>{{ $row['TLCdescription'] }}</td>
										</tr>

									@endforeach					

								@endif
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		@endif
		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')

@endsection