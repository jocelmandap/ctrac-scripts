@extends('layouts.master')

@section('title', 'Add Trainings for Job Ints | Venue Skills')

@section('header-content')

	<h1>
		Trainings for Job Ints
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Add Trainings for Job Ints</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-plus-circle"></i> Add</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">
					<div class="col-md-6">
						<div class="form-group">
							<label for="job_int_id">Choose Job Int</label>
						    <select class="form-control" name="job_int_id" id="job_int_id" placeholder="Search or Select Job Int">
			                    @if (!$job_ints->isEmpty())

									@foreach ($job_ints as $job_int)

										<option value="{{ $job_int->getId() }}">{{ $job_int->getId() }} | {{ $job_int->getCode() }} | {{ ucfirst(strtolower($job_int->getName())) }}</option>

									@endforeach

			                    @else
			                    	<option value="">None</option>
			                    @endif
						    </select>
						</div>
					</div>
					<div class="col-md-6"></div>
					<div class="col-md-12">
						<div style="border: 1px solid #001f3f;">
							<h4 class="tlc-job-action-header-title bg-navy" style="padding: 10px 10px; margin-top: 0;">Add Training</h4>
							<div class="row">
								<input type="hidden" id="form_action_type" value="add">
								<input type="hidden" id="training_course_job_id" value="">
								<div class="col-md-8" style="padding-left: 30px;">
									<div class="form-group">
										<label for="training_course_id">TLC Code</label>
									    <select class="form-control" name="training_course_id" id="training_course_id" placeholder="Search or Select TLC">
						                    @if (!$training_courses->isEmpty())

												@foreach ($training_courses as $training_course)

													<option value="{{ $training_course->getId() }}">{{ $training_course->getTrainingCode() }} - {{ $training_course->getTrainingName() }}</option>

												@endforeach

						                    @else
						                    	<option value="">None</option>
						                    @endif
									    </select>
									</div>
								</div>
								<div class="col-md-4" style="padding-right: 30px;">
									<label>&nbsp;</label>
									<button id="save_tlc_jobs" class="btn btn-primary btn-block">Add</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-info-circle"></i> Training List</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">
					<div class="col-md-12">
						<div style="border: 1px solid #00a65a;">
							<h4 class="tlc-job-header-title bg-green" style="padding: 10px 10px; margin-top: 0;">TLC not Available</h4>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-striped table-hover" style="padding: 30px;">
										<thead>
											<th>TLC Name</th>
											<th>Actions</th>
										</thead>
										<tbody class="tlc-jobs-container">
											<tr class="no-content-tlc-jobs">
												<td colspan="3">No TLC Available</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var TLCJobsHandler = {
				init : function () {
					$('#training_course_id').selectize()[0].selectize.clear();
					$('#job_int_id').selectize({
						onChange: function(job_int_id) {
							if (job_int_id.length > 0) {
								TLCJobsHandler.reset_add_jobs();
								TLCJobsHandler.load_tlc_jobs(job_int_id);
							}
						}
					});
					TLCJobsHandler.init_first_instance_tlc();
					TLCJobsHandler.add_tlc_jobs();
					TLCJobsHandler.update_tlc_jobs();
					TLCJobsHandler.remove_tlc_jobs();
				},
				init_first_instance_tlc : function () {
					var job_int_id = $('#job_int_id').val();
					TLCJobsHandler.load_tlc_jobs(job_int_id);
				},
				reset_add_jobs : function () {
					$('#save_tlc_jobs').text('Add');
					$('.tlc-job-action-header-title').text('Add Training');
					$('#training_course_job_id').val('');
					$('#form_action_type').val('add');
					$('#training_course_id').selectize()[0].selectize.clear();
				},
				load_tlc_jobs : function (job_int_id) {
					$.ajax({
						url: '{{ route('load_tlc_jobs') }}',
						type: 'POST',
						dataType: 'json',
						data: {job_int_id: job_int_id},
						success : function (response) {
							console.log(response);
							if (response.status == true) {
								$('.tlc-job-header-title').text(response.job_int_name);
								$('.tlc-jobs-container').html(response.training_course_jobs);
							} else {
								alert('An Error occure while fetching data! Please reload your page');
							}
						}
					});
				},
				add_tlc_jobs : function () {
					$(document).on('click', '#save_tlc_jobs', function () {
						var job_int_id = $('#job_int_id').val();
						var training_course_id = $('#training_course_id').val();
						var training_course_job_id = $('#training_course_job_id').val();
						var form_action_type = $('#form_action_type').val();

						var data = {
							job_int_id : job_int_id,
							training_course_id : training_course_id,
							training_course_job_id : training_course_job_id,
							form_action_type : form_action_type
						};

						$.ajax({
							url: '{{ route('save_tlc_jobs') }}',
							type: 'POST',
							dataType: 'json',
							data: data,
							success : function (response) {
								if (response.status == true) {
									$('.no-content-tlc-jobs').remove();
									$('#training_course_id').selectize()[0].selectize.clear();

									if (form_action_type == 'update') {
										$('#training_course_job_' + training_course_job_id).html(response.content);
									} else {
										$('.tlc-jobs-container').append(response.content);
									}

									TLCJobsHandler.reset_add_jobs();
								} else {
									alert('An Error occure while fetching data! Please reload your page');
								}
							}
						});
					});
				},
				update_tlc_jobs : function () {
					$(document).on('click', '.tlc-jobs-container button.update-tlc-jobs', function () {
						var training_course_job_id = $(this).data('id');
						var training_course_id = $(this).data('course');

						$('#form_action_type').val('update');
						$('#training_course_job_id').val(training_course_job_id);
						$('#training_course_id').selectize()[0].selectize.setValue(training_course_id);

						$('#save_tlc_jobs').text('Update');
						$('.tlc-job-action-header-title').text('Update Training');
					});
				},
				remove_tlc_jobs : function () {
					$(document).on('click', '.tlc-jobs-container button.delete-tlc-jobs', function () {
						var training_course_job_id = $(this).data('id');

						$.ajax({
							url: '{{ route('remove_tlc_jobs') }}',
							type: 'POST',
							dataType: 'json',
							data: {training_course_job_id: training_course_job_id},
							success : function (response) {
								if (response.status == true) {
									$('#training_course_job_' + training_course_job_id).fadeOut('400', function() {
										$(this).remove();
									});
								} else {
									alert('An Error occure while fetching data! Please reload your page');
								}
							}
						});
					});
				}
			};
			
			TLCJobsHandler.init();
		});
	</script>
@endsection