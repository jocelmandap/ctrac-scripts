@extends('layouts.master')

@section('title', 'Add TLC Code Skills | Venue Skills')

@section('header-content')

	<h1>
		TLC Code Skills
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Add TLC Code Skills</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-plus-circle"></i> Add</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">
					<div class="col-md-6">
						<div class="form-group">
							<label for="training_course">Choose TLC Code</label>
						    <select class="form-control" name="training_course" id="training_course" placeholder="Search or Select TLC Codes">
			                    @if (!$training_codes->isEmpty())

									@foreach ($training_codes as $training_code)

										<option value="{{ $training_code->getId() }}">{{ $training_code->getTrainingCode() }} - {{ $training_code->getTrainingName() }}</option>

									@endforeach

			                    @else
			                    	<option value="">None</option>
			                    @endif
						    </select>
						</div>
					</div>
					<div class="col-md-6"></div>
					<div class="col-md-12">
						<div style="border: 1px solid #001f3f;">
							<h4 class="tlc-skill-action-header-title bg-navy" style="padding: 10px 10px; margin-top: 0;">Add TLC Skills</h4>
							<div class="row">
								<input type="hidden" id="form_action_type" value="add">
								<input type="hidden" id="training_course_skill_id" value="">
								<div class="col-md-5" style="padding-left: 30px;">
									<div class="form-group">
										<label for="skill_set">Skill Sets</label>
									    <select class="form-control" name="skill_set" id="skill_set" placeholder="Search or Select Skill">
						                    @if (!$skill_sets->isEmpty())

												@foreach ($skill_sets as $skill_set)

													<option value="{{ $skill_set->getId() }}">{{ $skill_set->getName() }}</option>

												@endforeach

						                    @else
						                    	<option value="">None</option>
						                    @endif
									    </select>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="skill_level">Skill Levels</label>
									    <select class="form-control" name="skill_level" id="skill_level">
						                    @if (!$skill_levels->isEmpty())

												@foreach ($skill_levels as $skill_level)

													<option value="{{ $skill_level->getId() }}">{{ $skill_level->getName() }}</option>

												@endforeach

						                    @else
						                    	<option value="">None</option>
						                    @endif
									    </select>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 30px;">
									<label>&nbsp;</label>
									<button id="save_tlc_skills" class="btn btn-primary btn-block">Add</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-info-circle"></i> TLC Skills</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">
					<div class="col-md-12">
						<div style="border: 1px solid #00a65a;">
							<h4 class="tlc-skill-header-title bg-green" style="padding: 10px 10px; margin-top: 0;">TLC not Available</h4>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-striped table-hover" style="padding: 30px;">
										<thead>
											<th>Skill Name</th>
											<th>Skill Level</th>
											<th>Actions</th>
										</thead>
										<tbody class="tlc-skills-container">
											<tr class="no-content-tlc-skills">
												<td colspan="3">No Skills Available</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var TLCSkillsHandler = {
				init : function () {
					$('#skill_set').selectize()[0].selectize.clear();
					$('#training_course').selectize({
						onChange: function(training_course_id) {
							if (training_course_id.length > 0) {
								TLCSkillsHandler.reset_add_skills();
								TLCSkillsHandler.load_tlc_skills(training_course_id);
							}
						}
					});
					TLCSkillsHandler.init_first_instance_tlc();
					TLCSkillsHandler.add_tlc_skills();
					TLCSkillsHandler.update_tlc_skills();
					TLCSkillsHandler.remove_tlc_skills();
				},
				init_first_instance_tlc : function () {
					var training_course_id = $('#training_course').val();
					TLCSkillsHandler.load_tlc_skills(training_course_id);
				},
				reset_add_skills : function () {
					$('#save_tlc_skills').text('Add');
					$('.tlc-skill-action-header-title').text('Add TLC Skills');
					$('#training_course_skill_id').val('');
					$('#form_action_type').val('add');
					$('#skill_set').selectize()[0].selectize.clear();
					$('#skill_level').val(1);
				},
				load_tlc_skills : function (training_course_id) {
					$.ajax({
						url: '{{ route('load_tlc_skills') }}',
						type: 'POST',
						dataType: 'json',
						data: {training_course_id: training_course_id},
						success : function (response) {
							console.log(response);
							if (response.status == true) {
								$('.tlc-skill-header-title').text(response.training_course_name);
								$('.tlc-skills-container').html(response.training_course_skills);
							} else {
								alert('An Error occure while fetching data! Please reload your page');
							}
						}
					});
				},
				add_tlc_skills : function () {
					$(document).on('click', '#save_tlc_skills', function () {
						var training_course_id = $('#training_course').val();
						var training_course_skill_id = $('#training_course_skill_id').val();
						var skill_set = $('#skill_set').val();
						var skill_level = $('#skill_level').val();
						var form_action_type = $('#form_action_type').val();

						var data = {
							training_course_id : training_course_id,
							training_course_skill_id : training_course_skill_id,
							skill_set_id : skill_set,
							skill_level_id : skill_level,
							form_action_type : form_action_type
						};

						$.ajax({
							url: '{{ route('save_tlc_skills') }}',
							type: 'POST',
							dataType: 'json',
							data: data,
							success : function (response) {
								if (response.status == true) {
									$('.no-content-tlc-skills').remove();
									$('#skill_set').selectize()[0].selectize.clear();

									if (form_action_type == 'update') {
										$('#training_skill_set_' + training_course_skill_id).html(response.content);
									} else {
										$('.tlc-skills-container').append(response.content);
									}

									TLCSkillsHandler.reset_add_skills();
								} else {
									alert('An Error occure while fetching data! Please reload your page');
								}
							}
						});
					});
				},
				update_tlc_skills : function () {
					$(document).on('click', '.tlc-skills-container button.update-tlc-skills', function () {
						var training_course_skill_id = $(this).data('id');
						var skill_set_id = $(this).data('skill');
						var skill_level_id = $(this).data('level');

						$('#form_action_type').val('update');
						$('#training_course_skill_id').val(training_course_skill_id);
						$('#skill_set').selectize()[0].selectize.setValue(skill_set_id);
						$('#skill_level').val(skill_level_id);

						$('#save_tlc_skills').text('Update');
						$('.tlc-skill-action-header-title').text('Update TLC Skills');
					});
				},
				remove_tlc_skills : function () {
					$(document).on('click', '.tlc-skills-container button.delete-tlc-skills', function () {
						var training_course_skill_id = $(this).data('id');

						$.ajax({
							url: '{{ route('remove_tlc_skills') }}',
							type: 'POST',
							dataType: 'json',
							data: {training_course_skill_id: training_course_skill_id},
							success : function (response) {
								if (response.status == true) {
									$('#training_skill_set_' + training_course_skill_id).fadeOut('400', function() {
										$(this).remove();
									});
								} else {
									alert('An Error occure while fetching data! Please reload your page');
								}
							}
						});
					});
				}
			};
			
			TLCSkillsHandler.init();
		});
	</script>
@endsection