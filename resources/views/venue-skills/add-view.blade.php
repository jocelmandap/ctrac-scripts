@extends('layouts.master')

@section('title', 'Venue Skills')

@section('header-content')

	<h1>
		Venue Skills
		<small>CTRAC users venue skills scripts</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Venue Skills</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add <i class="fa fa-plus"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('save_add_venue_skills') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group">
							    <label>Type of Adding Data</label>
							    <select class="form-control adding-type" name="upload_type">
				                    <option value="user_id_upload">Single</option>
				                    <option value="csv_id_upload">Multiple (CSV File)</option>
							    </select>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
							    <label>Type of User ID</label>
							    <select class="form-control" name="user_type_id">
				                    <option value="user_id">User ID</option>
				                    <option value="jde_id">User JDE ID</option>
							    </select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group user-id-type-container upload-type">
								<label for="user_ids">ID</label>
								<input type="text" class="form-control" id="user_ids" name="user_ids" placeholder="Please add the User ID or JDE ID, Ex: 123, 345, 123 or 123 only">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group user-id-type-container upload-type">
								<label for="venue">Venues</label>
							    <select class="form-control" name="venue" id="venue">
				                    @if (!$venues->isEmpty())

										@foreach ($venues as $venue)

											<option value="{{ $venue->getId() }}">{{ $venue->getName() }}</option>

										@endforeach

				                    @else
				                    	<option value="">None</option>
				                    @endif
							    </select>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form-group user-id-type-container upload-type">
								<label for="start_date">Start Date</label>
								<input type='text' class="form-control datepicker" name="start_date" id="start_date" placeholder="MM/DD/YYYY" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group user-id-type-container upload-type">
								<label for="end_date">End Date</label>
								<input type='text' class="form-control datepicker" name="end_date" id="end_date" placeholder="MM/DD/YYYY" />
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group upload-type-container upload-type">
								<label for="upload_user_csv">Upload a CSV File</label>
								<input type="file" id="upload_user_csv" name="upload_user_csv">

								<p class="help-block">Please download the sample format <a href="{{ asset('uploads/venues/csv_test.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
							</div>
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var VenueAddHandler = {
				init : function () {
					VenueAddHandler.change_type_of_adding();
					VenueAddHandler.init_datepicker();
					$('.adding-type').trigger('change');
				},
				change_type_of_adding : function () {
					$(document).on('change', '.adding-type', function () {
						var self = $(this);
						var value = self.val();

						if (value == 'user_id_upload') {
							$('.upload-type-container').slideUp();
							$('.user-id-type-container').slideDown();
						}

						if (value == 'csv_id_upload') {
							$('.user-id-type-container').slideUp();
							$('.upload-type-container').slideDown();
						}
					});
				},
				init_datepicker : function () {
					$('.datepicker').datepicker({
					    format: 'mm/dd/yyyy',
					    autoclose: true,
					    todayHighlight: true
					});
				}
			};

			VenueAddHandler.init();
		});
	</script>
@endsection