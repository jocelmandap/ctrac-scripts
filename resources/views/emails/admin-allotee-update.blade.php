@extends('layouts.master-email')

@section('content')

	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Hi {{ $user->getName() }},</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">You updated the allotment details of {{ $crew_details->personalData->getFullName() }} with Employee ID <strong>{{ $crew_details->getJDEId() }}</strong></p>

@endsection