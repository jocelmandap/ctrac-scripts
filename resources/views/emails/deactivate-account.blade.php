@extends('layouts.master-email')

@section('content')

	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Hi {{ $user->getName() }},</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Sorry to inform you that your account will be de-activated this {{ date('F, d Y') }}</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Thank you for using CTRAC APP by C-TRAC Team</p>

@endsection