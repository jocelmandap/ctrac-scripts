@extends('layouts.master-email')

@section('content')

	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Hi {{ $user->getName() }},</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Please see the below results for the daily requisition upload.</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Daily Requisition Uploader: <strong>{{ $uploader->getName() }}</strong></p>
	
	@if (!empty($logs))
		<table style="width:100%; margin:0 0 40px; padding:0;">
			<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
				<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
					<strong>#</strong>
				</td>
				<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
					<strong>Requisition ID</strong>
				</td>
				<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
					<strong>Message Log</strong>
				</td>
			</tr>
			@if (!empty($logs['history_logs']))
				@php
					$count = 1;
				@endphp
				@foreach ($logs['history_logs'] as $log)
					<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
						<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
							{{ $count }}
						</td>
						<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
							{{ $log['requisition_id'] }}
						</td>
						<td style="font-size: 14px; border-radius: 25px; text-align: left; vertical-align: top; margin: 0; padding: 0;" align="left"  valign="top">
							{!! $log['message_log'] !!}
						</td>
					</tr>

					@php
						$count++;
					@endphp
				@endforeach
			@else
				<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
					<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
						N /A
					</td>
					<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
						N /A
					</td>
					<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
						N /A
					</td>
				</tr>
			@endif
		</table>
		
		@if (!empty($logs['job_int_logs']))
			<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">&nbsp;</p>
			<table style="width:100%; margin:0 0 40px; padding:0;">
				<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
					<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
						<strong>#</strong>
					</td>
					<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
						<strong>Job Code</strong>
					</td>
					<td style="font-size: 14px; border-radius: 25px; text-align: left; vertical-align: top; margin: 0; padding: 0;" align="left"  valign="top">
						<strong>Job Description</strong>
					</td>
				</tr>
				@php
					$count = 1;
				@endphp
				@foreach ($logs['job_int_logs'] as $log2)
					<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
						<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
							{{ $count }}
						</td>
						<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
							{{ $log2['job_code'] }}
						</td>
						<td style="font-size: 14px; border-radius: 25px; text-align: left; vertical-align: top; margin: 0; padding: 0;" align="left"  valign="top">
							{!! $log2['job_desc'] !!}
						</td>
					</tr>

					@php
						$count++;
					@endphp
				@endforeach
			</table>
		@endif
	@endif

@endsection