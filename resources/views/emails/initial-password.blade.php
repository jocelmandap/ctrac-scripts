@extends('layouts.master-email')

@section('content')

	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Hi {{ $user->getName() }},</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Here is your credentials for C-TRAC APP: </p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;"><strong>Email: </strong> {{ $user->getEmail() }}</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;"><strong>Password: </strong> {{ $password }}</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Please click the button below to login:</p>

	<table style="width:100%; margin:0 0 40px; padding:0;">
		<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
			<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
				<a href="{{ url('/login') }}" style="background-color:#00a65a; display:block; color: #ffffff; border-radius:4px; font-weight:bold; text-decoration: none; padding:10px 20px; margin:0 20%;">Login</a>
			</td>
		</tr>
	</table>

@endsection