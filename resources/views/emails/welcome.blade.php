@extends('layouts.master-email')

@section('content')

	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Hi {{ $user->getName() }},</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Welcome to C-TRAC APP and Thanks for Signing Up!</p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Your registered role will be: <strong>{{ $user->roles()->first()->getName() }}</strong></p>
	<p style="font-size: 14px; line-height: 1.6em; font-weight: normal; margin: 0 0 10px; padding: 0;">Please click the button below to get started:</p>

	<table style="width:100%; margin:0 0 40px; padding:0;">
		<tr style="font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
			<td style="font-size: 14px; border-radius: 25px; text-align: center; vertical-align: top; margin: 0; padding: 0;" align="center"  valign="top">
				<a href="{{ url('/') }}" style="background-color:#00a65a; display:block; color: #ffffff; border-radius:4px; font-weight:bold; text-decoration: none; padding:10px 20px; margin:0 20%;">Get Started</a>
			</td>
		</tr>
	</table>

@endsection