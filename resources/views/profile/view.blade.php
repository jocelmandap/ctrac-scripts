@extends('layouts.master')

@section('title', 'User Profile')

@section('head')

	<link rel="stylesheet" href="/bower_components/datatables/media/css/dataTables.bootstrap.min.css">

@endsection

@section('header-content')

	<h1 class="page-title-header">
		User Profile
		<small>My details</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">User Profile</li>
	</ol>

@endsection

@section('main-content')
	
	<div class="row">
		<div class="col-md-3">

			<!-- Profile Image -->
			<div class="box box-primary">
				<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="{{ $user_details->getProfilePhoto() }}" alt="{{ $user_details->getName() }}" title="{{ $user_details->getName() }}">

					<h3 class="profile-username text-center">{{ $user_details->getName() }}</h3>

					<p class="text-muted text-center">{{ $user_role->getName() }}</p>

					<blockquote style="border-left: 5px solid #83de83;">
						@if (!is_null($user_details->getDescription()))
							<footer style="color: #3c8dbc;">{{ $user_details->getDescription() }}</footer>
						@else
							<footer style="color: #3c8dbc;">I'm not a great programmer; I'm just a good programmer with great habits.</footer>
						@endif
					</blockquote>

					<ul class="list-group list-group-unbordered">
						<li class="list-group-item">
							<b>Team Name</b> <a class="pull-right">{{ $user_role->getName() }}</a>
						</li>
						<li class="list-group-item">
							<b>Total Team Members</b> <a class="pull-right">{{ $user_members->count() }}</a>
						</li>
						<li class="list-group-item">
							<b>Total Page Access</b> <a class="pull-right">@if ($user_role->getId() == 1) All @else $user_role->pages->count() @endif</a>
						</li>
						<li class="list-group-item">
							<b>Member Since: </b> <a class="pull-right">{{ $user_details->getCreatedAt(false, 'F d, Y') }}</a>
						</li>
					</ul>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#team-members" data-toggle="tab">Team Members</a></li>
					<li><a href="#update-profile" data-toggle="tab">Update Profile Details</a></li>
					<li><a href="#change-password" data-toggle="tab">Change Password</a></li>
				</ul>
				<div class="tab-content">
					<div class="active tab-pane" id="team-members">
						<div class="row">
							@if (!$user_members->isEmpty())
								
								@foreach ($user_members as $member)

									@if ($member->getId() != $user_details->getId())

										<div class="member-container col-md-3">
											<div class="member-content-container">
												<img class="profile-user-img img-responsive img-circle" src="{{ $member->getProfilePhoto() }}" alt="{{ $member->getName() }}" title="{{ $member->getName() }}">
												<h3 class="profile-username text-center">{{ $member->getName() }}</h3>
												<p class="text-muted text-center">{{ $member->roles()->first()->getName() }}</p>
												<blockquote>
													@if (!is_null($member->getDescription()))
														<footer style="color: #3c8dbc;">{{ $member->getDescription() }}</footer>
													@else
														<footer style="color: #3c8dbc;">I'm not a great programmer; I'm just a good programmer with great habits.</footer>
													@endif
												</blockquote>
											</div>
										</div>

									@endif

								@endforeach

							@endif
						</div>
					</div>
					<div class="tab-pane" id="update-profile">
						<form class="form-horizontal" method="POST" action="{{ route('my_profile_save') }}" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="hidden" name="form_type" value="update-profile">
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Name</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="name" id="name" value="{{ $user_details->getName() }}" placeholder="Name">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email</label>

								<div class="col-sm-10">
									<input type="email" class="form-control" name="email" value="{{ $user_details->getEmail() }}" id="email" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="desc" class="col-sm-2 control-label">Description</label>

								<div class="col-sm-10">
									<textarea 
										name="desc" 
										class="form-control" 
										id="desc" 
										cols="30"
										rows="5"
										style="resize: none;" 
										placeholder="Description">{{ $user_details->getDescription() }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="profile" class="col-sm-2 control-label">Upload Profile Photo</label>
								
								<div class="col-sm-10">
									<input type="file" id="profile" name="profile">
									<p class="help-block">Please download the sample format <a href="{{ asset('uploads/requisitions/closed_req_sample.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
								</div>
							</div>
							<div class="form-group hide">
								<div class="col-sm-offset-2 col-sm-10">
									<div class="checkbox">
										<label>
											<input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-danger">Submit</button>
								</div>
							</div>
						</form>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="change-password">
						<form class="form-horizontal" method="POST" action="{{ route('my_profile_save') }}">
							{{ csrf_field() }}
							<input type="hidden" name="form_type" value="change-password">
							<div class="form-group hide">
								<label for="name" class="col-sm-2 control-label">Name</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" name="name" id="name" value="{{ $user_details->getName() }}" placeholder="Name">
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="col-sm-2 control-label">Password</label>

								<div class="col-sm-10">
									<input type="password" class="form-control" name="password" id="password" value="" placeholder="Enter password here">
								</div>
							</div>
							<div class="form-group">
								<label for="password_confirmation" class="col-sm-2 control-label">Confirm Password</label>

								<div class="col-sm-10">
									<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="" placeholder="Enter password confirmation here">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-danger">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- /.nav-tabs-custom -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->

@endsection

@section('modal-content')

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var UserHandler = {
				init : function () {

				},
				isValid : function (user_search_by, user_detail) {
					if (user_search_by == 'email') {
						var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
						return pattern.test(user_detail);
					} else {
						return $.isNumeric(user_detail);
					}
				}
			};

			UserHandler.init();
		});
	</script>
@endsection