@extends('layouts.master')

@section('title', 'Archive Documents')

@section('head')

	<link rel="stylesheet" href="/bower_components/datatables/media/css/dataTables.bootstrap.min.css">

@endsection

@section('header-content')

	<h1 class="page-title-header">
		Archive Documents
		<small>CM's Archive Documents</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Archive Documents</li>
	</ol>
	
	<div class="app-env-container">
		<label for="app_env">App Environment: </label>
		<select id="app_env">
			<option value="prod">Prod</option>
			<option value="test">Test</option>
		</select>
	</div>

@endsection

@section('main-content')
	
	<!-- Small boxes (Stat box) -->
	<div class="row background-check-form-container">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Search Crew Archive Documents</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-3">
						<div class="form-group">
							<label for="user_search_by">Search By</label>
							<select name="user_search_by" id="user_search_by" class="form-control">
								<option value="email">E-mail</option>
								<option value="jde_id">Employee ID</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="user_detail" id="user_detail_label">E-mail</label>
							<input type="text" class="form-control" id="user_detail" name="user_detail" placeholder="Crew E-mail Address">
							<span class="error-container user-error-container"></span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="user_search_by">Document Type</label>
							<select name="s_document_id" id="s_document_id" class="form-control">
								<option value="119">STCW</option>
								<option value="36">Seaman's Book</option>
								<option value="0">All</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>&nbsp</label>
							<button id="submit_search" class="btn btn-primary form-control">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->

	<div class="row archive-results-container">
	    <div class="col-xs-12">
	        <div class="box box-success">
	        	<div class="box-header">
	        		<h3 class="box-title">List of Archive Documents</h3>
	        	</div>
	        	<div class="box-body">
					<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th>#</th>
				                <th>Document Name</th>
				                <th>Document Status</th>
				                <th>Created At</th>
				                <th>Last Updated At</th>
				                <th>Last Updated By</th>
				                <th>Actions</th>
				            </tr>
						</thead>
						<tbody class="document-table-content">
							<tr>
								<td>0</td>
							    <td>N / A</td>
							    <td>N / A</td>
							    <td>N / A</td>
							    <td>N / A</td>
							    <td>N / A</td>
							    <td>N / A</td>
							</tr>
						</tbody>
					</table>
					<div class="pull-right footer-admin-pagination">
						
					</div>
	        	</div>
	        </div>
	    </div>
	</div>

@endsection

@section('modal-content')
	
	@include('archive-documents.modals.document-details')

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			var ArchiveHandler = {
				init : function () {
					ArchiveHandler.onchange_search_by();
					ArchiveHandler.submit_search();
					ArchiveHandler.view_document();
				},
				isValid : function (user_search_by, user_detail) {
					if (user_search_by == 'email') {
						var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
						return pattern.test(user_detail);
					} else {
						return $.isNumeric(user_detail);
					}
				},
				onchange_search_by : function () {
					$('#user_search_by').change(function () {
						if ($(this).val() == 'email') {
							$('#user_detail_label').text('E-mail');
							$('#user_detail').val('').prop({
								placeholder: 'Crew E-mail Address',
								type: 'email',
							});
							$('.archive-results-container').fadeOut('400');
						} else {
							$('#user_detail_label').text('Employee Id');
							$('#user_detail').val('').prop({
								placeholder: 'Crew Employee Id',
								type: 'number',
							});
							$('.archive-results-container').fadeOut('400');
						}
					});
				},
				submit_search : function () {
					$(document).on('click', '#submit_search', function () {
						var user_search_by = $('#user_search_by').val();
						var user_detail = $('#user_detail').val();
						var document_id = $('#s_document_id').val();
						var app_env = $('#app_env').val();

						var data = {
							user_search_by : user_search_by,
							user_detail : user_detail,
							document_id : document_id,
							app_env : app_env
						};

						if ($.trim(user_detail).length >= 1) {

							if (ArchiveHandler.isValid(user_search_by, user_detail)) {
								$.ajax({
									url: '{{ route('archive_document_submit_search') }}',
									type: 'POST',
									dataType: 'json',
									data: data,
									beforeSend: function () {
										$('.archive-results-container').fadeOut('400', function() {
											$('#document-table-container').DataTable().destroy();
											$('#submit_search').removeClass('btn-primary')
																.addClass('btn-warning')
																.html('Submitting...');
										});
									},
									success: function (response) {
										if (response.status == true) {
											setTimeout(function () {
												$('.document-table-content').html(response.search_result);
												$('#document-table-container').DataTable();
												$('.archive-results-container').slideDown('400', function() {
													$('#submit_search')
														.removeClass('btn-warning')
														.addClass('btn-success')
														.html('Successfully Submitted!');

													setTimeout(function () {
														$('#submit_search')
															.removeClass('btn-success')
															.removeClass('btn-danger')
															.addClass('btn-primary')
															.html('Submit');
													}, 1500);
												});

											}, 400);

										} else {
											$('#submit_search')
												.removeClass('btn-warning')
												.removeClass('btn-danger')
												.addClass('btn-primary')
												.html('Submit');
											alert('No Archive files for ' + user_detail);
										}
									}
								});
							} else {
								if (user_search_by == 'email') {
									alert('Please enter a valid E-mail Address');
								} else {
									alert('Please enter a valid Employee ID');
								}
							}
						} else {
							if (user_search_by == 'jde_id') {
								user_search_by = 'Employee ID';
							} else {
								user_search_by = 'E-mail Address';
							}
							alert('Please enter ' + user_search_by);
						}
					});
				},
				view_document : function () {
					$(document).on('click', '#document-table-container .view-document', function () {
						var self = $(this);
						$('#view-document-modal').modal('show');
						
						var archive_id = self.data('id');
						var file_path = self.data('path');
						var file_ext = self.data('ext');
						var app_env = $('#app_env').val();

						$('.view-document-name').text('#' + archive_id + '- ' + self.data('name'));

						if (archive_id && file_ext != '') {
							$.ajax({
								url: '{{ route('archive_document_view_document_details') }}',
								type: 'POST',
								dataType: 'json',
								data: {
									archive_id: archive_id,
									file_path: file_path,
									doc_ext: file_ext,
									app_env : app_env
								},
								beforeSend: function () {
									var content = `
										<div class="loader-bg-container">
											<div class="dv-loader-bg">
											 	<i class="fa fa-cog fa-spin fa-3x"></i>
											 	<p>Updating...</p>
											</div>
										</div>
									`;
									$('.view-document-content').html(content);
								},
								success : function (response) {
									if (response.status == true) {
										var content = response.content;
										setTimeout(function () {
											$('.loader-bg-container').fadeOut('slow', function() {
												$('.view-document-content').html(content);
											});
										}, 400);
									} else {
										$('.loader-bg-container').fadeOut('slow', function() {
											$('.view-document-content').html('<h2 style="text-align: center;background: #f88;color: #fff;margin: 0;padding: 20px;">Document Details Not Available!</h2>');
										});
									}
								}
							});
						}
					});
				}
			};

			ArchiveHandler.init();
		});
	</script>
@endsection