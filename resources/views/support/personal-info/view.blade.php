@extends('layouts.master')

@section('title', 'Update Personal Info on C-TRAC | HRSD Support')

@section('header-content')

	<h1>
		Update Personal Info on C-TRAC
		<small>HRSD Support</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">HRSD Support</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row background-check-form-container">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Enter Employee ID or Email Address</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group" style="display: inline-block; width: 100%;">
						<label for="user_search_by" class="col-md-4 control-label">Search By</label>
						
						<div class="col-md-6">
							<select name="user_search_by" id="user_search_by" class="form-control">
								<option value="jde_id">Employee ID</option>
								<option value="email">E-mail</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="display: inline-block; width: 100%;">
					    <label for="jde_id" id="search_label" class="col-md-4 control-label">Employee ID</label>

					    <div class="col-md-6">
					        <input type="text" class="form-control" id="search" name="search" placeholder="Please enter Employee ID here">
					        <span class="error-container search-error-container">Please Enter Employee ID / Email Address.</span>
					    </div>
					</div>

					<div class="form-group" style="display: inline-block; width: 100%;">
					    <label for="jira" class="col-md-4 control-label">JIRA Ticket No.</label>

					    <div class="col-md-6">
					        <input type="text" class="form-control" id="jira" name="jira" placeholder="Please enter JIRA Ticket Number">
					        <span class="error-container jira-error-container">Please add JIRA Ticket Number.</span>
					    </div>
					</div>
					
					<div class="form-group" style="display: inline-block; width: 100%;">
						<label class="col-md-4 control-label">&nbsp;</label>
						<div class="col-md-6">
						    <button id="submit_search_form" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 personal-info-results">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
	<!-- /.row -->

@endsection

@section('modal-content')
	
{{-- 	@include('background-checks.modals.hireright-job-packages')
	@include('background-checks.modals.how-it-works') --}}

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var PersonalInfoHandler = {
				init : function () {
					PersonalInfoHandler.init_accordion();
					PersonalInfoHandler.search_by();
					PersonalInfoHandler.submit_form();
				},
				init_accordion : function () {
					$(document).on('hidden.bs.collapse', '.panel-group', function toggleIcon(e) {
						$(e.target)
						    .prev('.panel-heading')
						    .find(".more-less")
						    .toggleClass('glyphicon-plus glyphicon-minus');
					});

					$(document).on('shown.bs.collapse', '.panel-group', function toggleIcon(e) {
						$(e.target)
						    .prev('.panel-heading')
						    .find(".more-less")
						    .toggleClass('glyphicon-plus glyphicon-minus');
					});
				},
				search_by : function () {
					$(document).on('change', '#user_search_by', function () {
						if ($(this).val() == 'jde_id') {
							$('#search_label').text('Employee ID');
							$('#search').prop('placeholder', 'Please enter Employee ID here');
						} else {
							$('#search_label').text('Email Address');
							$('#search').prop('placeholder', 'Please enter Email Address here');
						}
					});
				},
				is_email : function (email) {
					var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
					return pattern.test(email);
				},
				validate_form : function () {
					var is_valid = true;
					var search_by = $('#user_search_by').val();
					var search = $('#search').val();
					var jira = $('#jira').val();

					if (search.length > 0) {
						if (search_by == 'jde_id') {
							if (!$.isNumeric(search)) {
								$('.search-error-container').text('Please Enter a valid Employee ID').slideDown();
								is_valid = false;
							}
						} else {
							if (!PersonalInfoHandler.is_email(search)) {
								$('.search-error-container').text('Please Enter a valid Email Address').slideDown();
								is_valid = false;
							}
						}
					} else {
						$('.search-error-container').slideDown();
						is_valid = false;
					}

					if (jira.length > 0) {
						$('.jira-error-container').slideUp();
					} else {
						$('.jira-error-container').slideDown();
						is_valid = false;
					}

					return is_valid;
				},
				submit_form : function () {
					$(document).on('click', '#submit_search_form', function () {
						var search_by = $('#user_search_by').val();
						var search = $('#search').val();
						var jira = $('#jira').val();
						var is_valid = PersonalInfoHandler.validate_form();

						if (is_valid) {
							$.ajax({
								url: '{{ route('support_hrsd_personal_info_search') }}',
								type: 'POST',
								dataType: 'json',
								data: {
									search_by: search_by,
									search: search,
									jira: jira
								},
								beforeSend : function () {
									$('#submit_search_form')
										.prop('disabled', true)
										.removeClass('btn-primary')
										.addClass('btn-warning')
										.html('Submitting...');
								},
								success : function (response) {
									if (response.status == true) {
										$('.personal-info-results').html(response.results);
										setTimeout(function () {
											$('#submit_search_form')
												.removeClass('btn-warning')
												.addClass('btn-success')
												.html('Successfully Submitted!');
											$('.personal-info-results').slideDown();
										}, 400);
									} else {
										$('#submit_search_form')
											.removeClass('btn-warning')
											.addClass('btn-danger')
											.html('Not Submitted!');
										// BackgroundCheckHandler.reset_background_check();
									}

									setTimeout(function () {
										$('#submit_search_form')
											.prop('disabled', false)
											.removeClass('btn-success')
											.removeClass('btn-danger')
											.addClass('btn-primary')
											.html('Submit');
									}, 1500);
								}
							});
						}
					});
				}
			};

			PersonalInfoHandler.init();
		});
	</script>
@endsection