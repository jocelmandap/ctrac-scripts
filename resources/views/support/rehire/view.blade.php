@extends('layouts.master')

@section('title', 'Rehire with Account on C-TRAC | HRSD Support')

@section('header-content')

	<h1>
		Rehire with Account on C-TRAC
		<small>HRSD Support</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">HRSD Support</li>
	</ol>
	
	<div class="app-env-container hide">
		<label for="app_env">App Environment: </label>
		<select id="app_env">
			<option value="prod">Prod</option>
			<option value="test">Test</option>
		</select>
	</div>
@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row background-check-form-container">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add Crew Member JDE ID</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group" style="display: inline-block; width: 100%;">
					    <label for="jde_id" class="col-md-4 control-label">JDE ID</label>

					    <div class="col-md-6">
					        <input type="number" class="form-control" id="jde_id" name="jde_id" placeholder="Please enter JDE ID here">
					        <span class="error-container jde-error-container">Please add JDE ID.</span>
					    </div>
					</div>

					<div class="form-group" style="display: inline-block; width: 100%;">
					    <label for="jira" class="col-md-4 control-label">JIRA Ticket No.</label>

					    <div class="col-md-6">
					        <input type="text" class="form-control" id="jira" name="jira" placeholder="Please enter JIRA Ticket Number">
					        <span class="error-container jira-error-container">Please add JIRA Ticket Number.</span>
					    </div>
					</div>
					
					<div class="form-group" style="display: inline-block; width: 100%;">
						<label class="col-md-4 control-label">&nbsp;</label>
						<div class="col-md-6">
						    <button id="submit_jde_form" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 results-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
	<!-- /.row -->

@endsection

@section('modal-content')
	
	@include('background-checks.modals.hireright-job-packages')
	@include('background-checks.modals.how-it-works')

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var RehireHandler = {
				init : function () {
					RehireHandler.submit_form();
				},
				reset_result : function () {
					$('.data-containers').slideUp();
					setTimeout(function () {
						$('.data-containers').html('');
					}, 1000);
				},
				validate_form : function (jde_id, jira) {
					var is_valid = true;
					if (jde_id.length > 0) {
						$('.jde-error-container').slideUp();
					} else {
						$('.jde-error-container').slideDown();
						is_valid = false;
					}

					if (jira.length > 0) {
						$('.jira-error-container').slideUp();
					} else {
						$('.jira-error-container').slideDown();
						is_valid = false;
					}
					
					return is_valid;
				},
				submit_form : function () {
					$(document).on('click', '#submit_jde_form', function () {
						var jde_id = $('#jde_id').val();
						var jira = $('#jira').val();
						var app_env = $('#app_env').val();
						var is_valid = RehireHandler.validate_form(jde_id, jira);

						if (is_valid) {
							$.ajax({
								url: '{{ route('support_rehire_with_account_save') }}',
								type: 'POST',
								dataType: 'json',
								data: {
									jde_id: jde_id, 
									jira: jira, 
									app_env: app_env
								},
								beforeSend : function () {
									$('.data-containers').slideUp();
									$('#submit_jde_form')
										.prop('disabled', true)
										.removeClass('btn-primary')
										.addClass('btn-warning')
										.html('Submitting...');
								},
								success : function (response) {
									if (response.status == true) {
										var items = '';
										var count = 1;
										$.each(response.result_messages, function(key, value) {
											items += `
												<tr>
													<td> ` + count + `</td>
													<td> ` + value + `</td>
												</tr>
											`;
											count++;
										});

										var html = `
											<div class="box box-success">
												<div class="box-header with-border">
													<h3 class="box-title">Results</h3>
												</div>
												<div class="box-body bg-green">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th>#</th>
																<th>Message</th>
															</tr>
														</thead>
														<tbody>
															` + items + `
														</tbody>
													</table>
												</div>
											</div>
										`;

										$('.results-container').html(html);

										setTimeout(function () {
											$('#submit_jde_form')
												.removeClass('btn-warning')
												.addClass('btn-success')
												.html('Successfully Submitted!');
											$('.data-containers').slideDown();
										}, 400);
									} else {
										var html = `
											<div class="box box-danger">
												<div class="box-header with-border">
													<h3 class="box-title">Results</h3>
												</div>
												<div class="box-body bg-red">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th>#</th>
																<th>Message</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>1</td>
																<td>
																	` + response.message + `
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										`;
										$('.results-container').html(html);
										$('.data-containers').slideDown();

										$('#submit_jde_form')
											.removeClass('btn-warning')
											.addClass('btn-danger')
											.html('Not Submitted!');
									}

									setTimeout(function () {
										$('#submit_jde_form')
											.prop('disabled', false)
											.removeClass('btn-success')
											.removeClass('btn-danger')
											.addClass('btn-primary')
											.html('Submit');
									}, 1500);
								}
							});
						}
					});
				}
			};

			RehireHandler.init();
		});
	</script>
@endsection