@extends('layouts.master')

@section('title', 'Bypass Medical Exam on C-TRAC | HRSD Support')

@section('header-content')

	<h1>
		Bypass Medical Exam
		<small>HRSD Support</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">HRSD Support</li>
	</ol>
	
	<div class="app-env-container hide">
		<label for="app_env">App Environment: </label>
		<select id="app_env">
			<option value="prod">Prod</option>
			<option value="test">Test</option>
		</select>
	</div>
@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row bypass-medical-exam-container">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add Candidate Email Address</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group" style="display: inline-block; width: 100%;">
					    <label for="email" class="col-md-4 control-label">Email Address</label>

					    <div class="col-md-6">
					        <input type="text" class="form-control" id="email" name="email" placeholder="Please enter Email here">
					        <span class="error-container email-error-container">Please add Email Address.</span>
					    </div>
					</div>

					<div class="form-group" style="display: inline-block; width: 100%;">
					    <label for="jira" class="col-md-4 control-label">JIRA Ticket No.</label>

					    <div class="col-md-6">
					        <input type="text" class="form-control" id="jira" name="jira" placeholder="Please enter JIRA Ticket Number">
					        <span class="error-container jira-error-container">Please add JIRA Ticket Number.</span>
					    </div>
					</div>
					
					<div class="form-group" style="display: inline-block; width: 100%;">
						<label class="col-md-4 control-label">&nbsp;</label>
						<div class="col-md-6">
						    <button id="submit_email_form" class="btn btn-primary">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 results-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-md-12 job-app-container">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
@endsection

@section('modal-content')
	

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var BypassMedicalExamHandler = {
				init : function () {
					BypassMedicalExamHandler.submit_form();
					BypassMedicalExamHandler.bypassed_exam();
				},
				reset_result : function () {
					$('.data-containers').slideUp();
					setTimeout(function () {
						$('.data-containers').html('');
					}, 1000);
				},
				validate_form : function (email, jira) {
					var is_valid = true;
					if (email.length > 0) {
						$('.email-error-container').slideUp();
					} else {
						$('.email-error-container').slideDown();
						is_valid = false;
					}

					if (jira.length > 0) {
						$('.jira-error-container').slideUp();
					} else {
						$('.jira-error-container').slideDown();
						is_valid = false;
					}
					
					return is_valid;
				},
				submit_form : function () {
					$(document).on('click', '#submit_email_form', function () {
						var email = $('#email').val();
						var jira = $('#jira').val();
						var app_env = $('#app_env').val();
						var is_valid = BypassMedicalExamHandler.validate_form(email, jira);

						if (is_valid) {
							$.ajax({
								url: '{{ route('support_bypass_medical_exam_submit') }}',
								type: 'POST',
								dataType: 'json',
								data: {
									email: email, 
									jira: jira, 
									app_env: app_env
								},
								beforeSend : function () {
									$('.data-containers').slideUp();
									$('#submit_email_form')
										.prop('disabled', true)
										.removeClass('btn-primary')
										.addClass('btn-warning')
										.html('Submitting...');
								},
								success : function (response) {
									if (response.status == true) {

										$('.job-app-container').html(response.job_application_content);

										setTimeout(function () {
											$('#submit_email_form')
												.removeClass('btn-warning')
												.addClass('btn-success')
												.html('Successfully Submitted!');
											$('.data-containers').slideDown();
										}, 400);
									} else {
										alert(response.message);
									}

									setTimeout(function () {
										$('#submit_email_form')
											.prop('disabled', false)
											.removeClass('btn-success')
											.removeClass('btn-danger')
											.addClass('btn-primary')
											.html('Submit');
									}, 1500);
								}
							});
						}
					});
				},
				bypassed_exam :function (){
					$(document).on("click", ".bypassed-exam-answer", function(){
						var jira = $('#jira').val();
						var job_id = $(this).data("id");
						var comment = $('#comment').val();
						$.ajax({
							url: '{{ route('support_bypass_medical_exam_update') }}',
							type: 'POST',
							dataType: 'json',
							data: {
								jira : jira,
								job_id : job_id,
								comment : comment
							},
							success : function(response) {
								if (response.status == true) {
									$('.results-container').html(response.results);
									$('#submit_email_form').trigger('click');
								} else {
									alert('Query not Executed!');
								}

							}
						})
					});
				}
			};

			BypassMedicalExamHandler.init();
		});
	</script>
@endsection