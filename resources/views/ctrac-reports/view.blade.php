@extends('layouts.master')

@section('title', 'Background Checks')

@section('header-content')

	<h1>
		CTRAC Reports
		<small>Background Checker for CM's</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Background Check</li>
	</ol>

@endsection

@section('main-content')

	<h3>No Content For Now</h3>

@endsection

@section('foot')
	
@endsection