@extends('layouts.master')

@section('title', 'Allotee')

@section('header-content')

	<h1>
		Allotee
		<small>CM's Allotee Details</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Allotee</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row background-check-form-container">
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add Crew Member JDE ID</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group user-id-type-container upload-type">
						<label for="jde_id">JDE ID</label>
						<input type="number" class="form-control" id="jde_id" name="jde_id" placeholder="Please Add JDE ID here">
						<span class="error-container jde-error-container">Please Add JDE ID.</span>
					</div>
					
					<button id="submit_jde_form" class="btn btn-primary">Submit</button>
					<button id="info_modal" class="btn btn-info hide"><i class="fa fa-info-circle"></i> How it Works</button>
					
					<span class="submit-status-container warning"></span>

					<div class="user-details-container data-containers">
						<!-- Data will be loaded via AJAX -->
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					
				</div>
			</div>
		</div>
		<div class="col-md-8 personal-details-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
	<!-- /.row -->

	<div class="row">
		<div class="col-md-12 allotee-details-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>

@endsection

@section('modal-content')
	
	@include('allotees.modals.form')
	@include('allotees.modals.delete-allotee')

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var FormHandler = {
				allotee_id : '',
				allotee_user_id : '',
				allotee_action : '',
				allotee_total_percentage : 0,
				active_allotee_current_percentage : 0,
				init : function () {
					FormHandler.submit_form();
					FormHandler.view_allotee_modal();
					FormHandler.add_new_allotee_modal();
					FormHandler.edit_allotee_modal();
					FormHandler.save_allotee_form();
					FormHandler.delete_allotee_modal();
				},
				validate_form : function (jde_id) {
					if (jde_id.length > 0) {
						$('.jde-error-container').slideUp();
						return true;
					}
					$('.jde-error-container').slideDown();
					return false;
				},
				submit_form : function () {
					$(document).on('click', '#submit_jde_form', function () {
						var jde_id = $('#jde_id').val();
						var is_valid = FormHandler.validate_form(jde_id);

						FormHandler.allotee_id = '';
						FormHandler.allotee_action = '';
						FormHandler.allotee_user_id = '';
						FormHandler.allotee_total_percentage = 0;

						if (is_valid) {
							$.ajax({
								url: '{{ route('allotee_check_details') }}',
								type: 'POST',
								dataType: 'json',
								data: {jde_id: jde_id},
								beforeSend : function () {
									$('#submit_jde_form')
										.prop('disabled', true)
										.removeClass('btn-primary')
										.addClass('btn-warning')
										.html('Submitting...');
								},
								success : function (response) {
									if (response.status == true) {
										FormHandler.allotee_user_id = response.user_id;
										FormHandler.allotee_total_percentage = response.total_allotee_percentage;
										$('.allotee-details-container').html(response.allotee_content);
										$('.personal-details-container').html(response.personal_details_content);
										setTimeout(function () {
											$('#submit_jde_form')
												.removeClass('btn-warning')
												.addClass('btn-success')
												.html('Successfully Submitted!');
											$('.allotee-details-container').slideDown();
										}, 400);
									} else {
										alert(response.message);
										$('#submit_jde_form')
											.removeClass('btn-warning')
											.addClass('btn-danger')
											.html('Not Submitted!');
										// FormHandler.reset_background_check();
									}

									setTimeout(function () {
										$('#submit_jde_form')
											.prop('disabled', false)
											.removeClass('btn-success')
											.removeClass('btn-danger')
											.addClass('btn-primary')
											.html('Submit');
									}, 1500);
								}
							});
						}
					});
				},
				set_form_data : function (data, action) {
					FormHandler.allotee_id = '';
					FormHandler.allotee_action = action;
					$('.allotee-form-loader').hide();
					FormHandler.validate_percentage();

					if (FormHandler.allotee_action == 'show') {
						$('#allotee-form input').prop('disabled', true);
						$('#allotee-form select').prop('disabled', true);
						$('#submit_allotee_form').hide();
					} else if (FormHandler.allotee_action == 'edit') {
						$('#allotee-form input').prop('disabled', false);
						$('#allotee-form select').prop('disabled', false);
						$('#submit_allotee_form').show();
					} else if (FormHandler.allotee_action == 'add') {
						$('#allotee-form input').prop('disabled', false);
						$('#allotee-form select').prop('disabled', false);
						$('#allotee-form')[0].reset();
						$('#submit_allotee_form').show();
						return;
					}

					FormHandler.allotee_id = data.allotee_id;

					$('#form-type').val(action);
					$('#first_name').val(data.allotee_firstname);
					$('#middle_name').val(data.allotee_middlename);
					$('#last_name').val(data.allotee_lastname);
					$('#bank_id').val(data.allotee_bank);
					$('#account_no').val(data.allotee_account_no);
					$('#percent').val(data.allotee_percentage);
					$('#relationship_id').val(data.relationship_id);
					$('#tel').val(data.allotee_telephone_no);
					$('#mobile').val(data.allotee_mobile_no);
					$('#street').val(data.allotee_street_address);
					$('#city').val(data.allotee_city);
					$('#state').val(data.allotee_state);
					$('#postal_code').val(data.allotee_zip_code);
					$('#country').val(data.allotee_country_code);
					$('#allotee_id').val(data.allotee_id);
				},
				get_form_data : function () {
					var data = {
						active_allotee_current_percentage : FormHandler.active_allotee_current_percentage,
						user_id : FormHandler.allotee_user_id,
						allotee_id : FormHandler.allotee_id,
						action : FormHandler.allotee_action,
						first_name : $('#first_name').val(),
						middle_name : $('#middle_name').val(),
						last_name : $('#last_name').val(),
						bank_id : $('#bank_id').val(),
						account_no : $('#account_no').val(),
						percent : $('#percent').val(),
						relationship_id : $('#relationship_id').val(),
						tel : $('#tel').val(),
						mobile : $('#mobile').val(),
						street : $('#street').val(),
						city : $('#city').val(),
						state : $('#state').val(),
						postal_code : $('#postal_code').val(),
						country : $('#country').val()
					}

					return data;
				},
				add_new_allotee_modal : function () {
					$(document).on('click', '#add_new_allotee', function () {
						$('.allotee-form-title').text('Add New Allotee');
						$('#allotee-form-modal').modal('show');

						FormHandler.active_allotee_current_percentage = 0;
						FormHandler.set_form_data('', 'add');
					});
				},
				edit_allotee_modal : function () {
					$(document).on('click', '.allotee-details-container .edit-allotee', function () {
						// PageHandler.clearFormData();
						$('.allotee-form-title').text('Edit Allotee Details');
						$('#allotee-form-modal').modal('show');
						FormHandler.active_allotee_current_percentage = $(this).data('percentage');
						var allotee_id = $(this).data('id');
						
						$.ajax({
							url: '{{ route('allotee_fetch_details') }}',
							type: 'POST',
							dataType: 'json',
							data: {allotee_id: allotee_id},
							beforeSend: function () {
								$('.allotee-form-loader').show();
							},
							success: function (response) {
								if (response.status == true) {
									FormHandler.set_form_data(response.allotee_details, 'edit');
								} else {
									alert(response.message);
								}
							}
						});
					});
				},
				view_allotee_modal : function () {
					$(document).on('click', '.allotee-details-container .view-allotee', function () {
						$('.allotee-form-title').text('View Allotee Details');
						$('#allotee-form-modal').modal('show');
						var allotee_id = $(this).data('id');

						$.ajax({
							url: '{{ route('allotee_fetch_details') }}',
							type: 'POST',
							dataType: 'json',
							data: {allotee_id: allotee_id},
							beforeSend: function () {
								$('.allotee-form-loader').show();
							},
							success: function (response) {
								if (response.status == true) {
									FormHandler.set_form_data(response.allotee_details, 'show');
								} else {
									alert(response.message);
								}
							}
						});
					});
				},
				validate_percentage : function () {
					$('#percent').keyup(function() {
						var total_percentage = 100;
						if (FormHandler.allotee_total_percentage != 0) {
							if (FormHandler.allotee_action == 'add') {
								total_percentage = 100 - FormHandler.allotee_total_percentage;
							}
							if (FormHandler.allotee_action == 'edit') {
								total_percentage = (100 - FormHandler.allotee_total_percentage) + FormHandler.active_allotee_current_percentage;
							}
						}

						if ($(this).val() > total_percentage) {
							alert("No numbers above " + total_percentage);
							$(this).val(total_percentage);
						}
					});
				},
				validate_form_fields : function () {
					var is_valid = true;

					if ($('#first_name').val().length > 0) {
						$('.fname-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.fname-error-container').text('First Name is required').show();
					}

					if ($('#middle_name').val().length > 0) {
						$('.mname-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.mname-error-container').text('Middle Name is required').show();
					}

					if ($('#last_name').val().length > 0) {
						$('.lname-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.lname-error-container').text('Last Name is required').show();
					}

					if ($('#account_no').val().length > 0) {
						$('.account-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.account-error-container').text('Account Number is required').show();
					}

					if ($('#percent').val().length > 0) {
						if (FormHandler.allotee_action == 'add') {
							var total_percentage = 100 - FormHandler.allotee_total_percentage;
							if ($('#percent').val() > total_percentage) {
								is_valid = false;
								$('.percent-error-container').text('No numbers above ' + total_percentage).show();
							} else {
								$('.percent-error-container').text('').hide();
							}
						}
						if (FormHandler.allotee_action == 'edit') {
							var total_percentage = (100 - FormHandler.allotee_total_percentage) + FormHandler.active_allotee_current_percentage;
							if ($('#percent').val() > total_percentage) {
								is_valid = false;
								$('.percent-error-container').text('No numbers above ' + total_percentage).show();
							} else {
								$('.percent-error-container').text('').hide();
							}
						}
					} else {
						is_valid = false;
						$('.percent-error-container').text('Enter Allotee Percentage').show();
					}

					if ($('#tel').val().length > 0) {
						$('.tel-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.tel-error-container').text('Telephone Number is required').show();
					}

					if ($('#mobile').val().length > 0) {
						$('.mobile-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.mobile-error-container').text('Mobile Number is required').show();
					}

					if ($('#street').val().length > 0) {
						$('.street-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.street-error-container').text('Street Address is required').show();
					}

					if ($('#city').val().length > 0) {
						$('.city-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.city-error-container').text('City is required').show();
					}

					if ($('#state').val().length > 0) {
						$('.state-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.state-error-container').text('State/Region/District is required').show();
					}

					if ($('#postal_code').val().length > 0) {
						$('.postal-error-container').text('').hide();
					} else {
						is_valid = false;
						$('.postal-error-container').text('Code/Postal Code is required').show();
					}

					return is_valid;
				},
				save_allotee_form : function () {
					$(document).on('click', '#submit_allotee_form', function () {
						var is_valid = FormHandler.validate_form_fields();
						if (is_valid) {
							$.ajax({
								url: '{{ route('allotee_save_details') }}',
								type: 'POST',
								dataType: 'json',
								data: FormHandler.get_form_data(),
								beforeSend: function () {
									$('.allotee-form-loader').show();
								},
								success: function (response) {
									if (response.status == true) {
										$('#submit_jde_form').trigger('click');
									} else {
										alert(response.message);
									}
									$('.allotee-form-loader').hide();
									$('#allotee-form-modal').modal('hide');
								}
							});
						}
					});
				},
				delete_allotee_modal : function () {
					$(document).on('click', '.allotee-details-container .delete-allotee', function () {
						$('.delete-allotee-name').text($(this).data('name'));
						$('.delete-allotee-form-loader').hide();
						$('#view-delete-allotee-modal').modal('show');
						FormHandler.allotee_id = $(this).data('id');
						FormHandler.submit_delete_allotee();
					});
				},
				submit_delete_allotee : function () {
					$(document).on('click', '#submit_delete_allotee', function () {
						$.ajax({
							url: '{{ route('allotee_delete_details') }}',
							type: 'POST',
							dataType: 'json',
							data: {
								allotee_id: FormHandler.allotee_id,
								user_id: FormHandler.allotee_user_id
							},
							beforeSend: function () {
								$('.delete-allotee-form-loader').show();
							},
							success: function (response) {
								if (response.status == true) {
									$('#submit_jde_form').trigger('click');
								} else {
									alert(response.message);
								}
								$('.delete-allotee-form-loader').hide();
								$('#view-delete-allotee-modal').modal('hide');
							}
						});
					});
				}
			};

			FormHandler.init();
		});
	</script>
@endsection