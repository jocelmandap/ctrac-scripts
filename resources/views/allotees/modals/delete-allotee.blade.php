<div class="modal modal-danger fade" id="view-delete-allotee-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="delete-allotee-form-loader">
			    <div class="div-table">
			        <div class="div-table-row">
			            <div class="div-table-cell">
			                <div class="loader-bg-container">
			                    <div class="dv-loader-bg">
			                        <i class="fa fa-cog fa-spin fa-3x"></i>
			                        <p>Loading...</p>
			                    </div>
			                </div>      
			            </div>
			        </div>
			    </div>
			</div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Delete Allotee Details</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete <span class="delete-allotee-name" style="text-decoration: underline;"></span> Allotee details?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
				<button type="button" id="submit_delete_allotee" class="btn btn-outline">Yes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>