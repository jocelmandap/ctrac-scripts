<!-- Modal -->
<div class="modal fade" id="allotee-form-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;">
            <div class="allotee-form-loader">
                <div class="div-table">
                    <div class="div-table-row">
                        <div class="div-table-cell">
                            <div class="loader-bg-container">
                                <div class="dv-loader-bg">
                                    <i class="fa fa-cog fa-spin fa-3x"></i>
                                    <p>Loading...</p>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title allotee-form-title">Add New Allotee</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST" id="allotee-form" action="{{ route('admin_users_save') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="first_name" class="col-md-4 control-label">First Name</label>

                        <div class="col-md-6">
                            <input id="first_name" type="text" class="form-control" name="first_name" value="">
                            <span class="error-container fname-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="middle_name" class="col-md-4 control-label">Middle Name</label>

                        <div class="col-md-6">
                            <input id="middle_name" type="text" class="form-control" name="middle_name" value="">
                            <span class="error-container mname-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label">Last Name</label>

                        <div class="col-md-6">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="">
                            <span class="error-container lname-error-container"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="bank_id" class="col-md-4 control-label">Bank</label>

                        <div class="col-md-6">
                            <select name="bank_id" id="bank_id" class="form-control">
                                @if (!$bank_list->isEmpty())
                                    @foreach ($bank_list as $bank)
                                        <option value="{{ $bank->getId() }}">{{ $bank->getName() }}</option>
                                    @endforeach
                                @else
                                    <option value="0">None</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="account_no" class="col-md-4 control-label">Account No.</label>

                        <div class="col-md-6">
                            <input id="account_no" type="number" class="form-control" name="account_no" value="">
                            <span class="error-container account-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="percent" class="col-md-4 control-label">Percentage</label>

                        <div class="col-md-6">
                            <input type="number" id="percent" class="form-control" name="percent" value="" max='100'>
                            <span class="error-container percent-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="relationship_id" class="col-md-4 control-label">Relationship</label>

                        <div class="col-md-6">
                            <select name="relationship_id" id="relationship_id" class="form-control">
                                @if (!$relationship_list->isEmpty())
                                    @foreach ($relationship_list as $relationship)
                                        <option value="{{ $relationship->getId() }}">{{ $relationship->getName() }}</option>
                                    @endforeach
                                @else
                                    <option value="0">None</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tel" class="col-md-4 control-label">Telephone Number</label>

                        <div class="col-md-6">
                            <input type="number" id="tel" class="form-control" name="tel" value="">
                            <span class="error-container tel-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mobile" class="col-md-4 control-label">Mobile Number</label>

                        <div class="col-md-6">
                            <input type="number" id="mobile" class="form-control" name="mobile" value="">
                            <span class="error-container mobile-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="street" class="col-md-4 control-label">Street Address</label>

                        <div class="col-md-6">
                            <input id="street" type="text" class="form-control" name="street" value="">
                            <span class="error-container street-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="city" class="col-md-4 control-label">City</label>

                        <div class="col-md-6">
                            <input id="city" type="text" class="form-control" name="city" value="">
                            <span class="error-container city-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="state" class="col-md-4 control-label">State/Region/District</label>

                        <div class="col-md-6">
                            <input id="state" type="text" class="form-control" name="state" value="">
                            <span class="error-container state-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="postal_code" class="col-md-4 control-label">Code/Postal Code</label>

                        <div class="col-md-6">
                            <input type="number" id="postal_code" class="form-control" name="postal_code" value="">
                            <span class="error-container postal-error-container"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="country" class="col-md-4 control-label">Country</label>

                        <div class="col-md-6">
                            <select name="country" id="country" class="form-control">
                                @if (!$country_list->isEmpty())
                                    @foreach ($country_list as $country)
                                        <option value="{{ $country->getCountryCode() }}">{{ $country->getCountryName() }}</option>
                                    @endforeach
                                @else
                                    <option value="0">None</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="submit_allotee_form">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>