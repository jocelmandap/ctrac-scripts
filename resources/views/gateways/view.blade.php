@extends('layouts.master')

@section('title', 'CTRAC Gateways')

@section('header-content')

	<h1>
		CTRAC Gateways
		<small>CTRAC gateways</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">CTRAC Gateways</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('gateway_update') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="update_by" class="col-md-4 control-label">Update By:</label>

						    <div class="col-md-6">
						        <select id="update_by" name="update_by" class="form-control">
						        	<option value="gateway">Gateway</option>
						        	<option value="jobstripes">Job Stripes</option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="upload_gateway_csv" class="col-md-4 control-label">Upload a CSV File</label>

						    <div class="col-md-6">
						        <input type="file" id="upload_gateway_csv" name="upload_gateway_csv">

						        <p class="help-block">Please download the sample format <a href="{{ asset('uploads/gateways/csv_test_gateways.csv') }}" id="sample_dl" target="_blank" rel="noopener noreferrer">here</a></p>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label class="col-md-4 control-label">&nbsp;</label>

						    <div class="col-md-6">
						        <button type="submit" class="btn btn-primary">Submit</button>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>
		<!-- ./col -->

		@if (Session::get('update_by') == 'gateway')
			
			@if (Session::has('update_logs'))
				<div class="col-md-6">
					<div class="box box-success">
						<div class="box-header with-border">
							<h3 class="box-title">Update Logs</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
												
							<div class="col-md-12" style="height: 400px; overflow-y: auto;">
								<div class="form-group">
									@php
										$logs = Session::get('update_logs');
										$count = 0;
									@endphp
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>Gateway Name</th>
												<th>Gateway Code</th>
												<th>Current Data</th>
												<th>Updated Data</th>
												<th>Status Update</th>
											</tr>
										</thead>
										<tbody>
											@if (!empty($logs))
												
												@foreach ($logs as $log)
													@php
														$count++;
													@endphp
													<tr>
														<td>{{ $count }}</td>
														@if (isset($log['current_data']))
															<td>{{ $log['current_data']['gateway_name'] }}</td>
															<td>{{ $log['current_data']['gateway_code'] }}</td>
															<td>{{ $log['current_data']['is_approved'] . ' | ' . $log['current_data']['stripe'] }}</td>
														@else
															<td>{{ $log['raw_data']['gateway_code'] }}</td>
															<td>{{ $log['raw_data']['gateway_code'] }}</td>
															<td>No Data Available</td>
														@endif
														
														@if (isset($log['updated_data']))
															<td>{{ $log['updated_data']['is_approved'] . ' | ' . $log['updated_data']['stripe'] }}</td>
														@else
															<td>No Updates</td>
														@endif
														<td>@if ($log['success']) <span style="color: green;">Success</span> @else <span style="color: red;">Not Success</span> @endif</td>
													</tr>

												@endforeach

											@else
												<tr>
													<td colspan="5"> No Updates Made</td>
												</tr>
											@endif
										</tbody>
									</table>

								</div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
				</div>
			@endif

		@endif

		@if (Session::get('update_by') == 'jobstripes')
			
			@if (Session::has('update_logs'))
				<div class="col-md-6">
					<div class="box box-success">
						<div class="box-header with-border">
							<h3 class="box-title">Update Logs</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
												
							<div class="col-md-12" style="height: 400px; overflow-y: auto;">
								<div class="form-group">
									@php
										$logs = Session::get('update_logs');
										$count = 0;
									@endphp
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>Job ID</th>
												<th>Job Code</th>
												<th>Current Data</th>
												<th>Updated Data</th>
												<th>Status Update</th>
											</tr>
										</thead>
										<tbody>
											@if (!empty($logs))
												
												@foreach ($logs as $log)
													@php
														$count++;
													@endphp
													<tr>
														<td>{{ $count }}</td>
														@if (isset($log['current_data']))
															<td>{{ $log['current_data']['job_int_id'] }}</td>
															<td>{{ $log['current_data']['job_int_code'] }}</td>
															<td>{{ $log['current_data']['stripe'] }}</td>
														@else
															<td>None</td>
															<td>{{ $log['raw_data']['job_code'] }}</td>
															<td>{{ $log['raw_data']['stripes'] }}</td>
														@endif
														
														@if (isset($log['updated_data']))
															<td>{{ $log['updated_data']['stripe'] }}</td>
														@else
															<td>No Updates</td>
														@endif
														<td>@if ($log['success']) <span style="color: green;">Success</span> @else <span style="color: red;">Not Success</span> @endif</td>
													</tr>

												@endforeach

											@else
												<tr>
													<td colspan="5"> No Updates Made</td>
												</tr>
											@endif
										</tbody>
									</table>

								</div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
				</div>
			@endif

		@endif

		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')

	<script type="text/javascript">
		$(document).ready(function () {
			var GatewayHandler = {
				init : function () {
					GatewayHandler.changeSampleDownload();
				},
				changeSampleDownload : function () {
					$(document).on('change', '#update_by', function() {
						if ($(this).val() == 'gateway') {
							$('#sample_dl').prop('href', '{{ asset('uploads/gateways/csv_test_gateways.csv') }}');
						} else {
							$('#sample_dl').prop('href', '{{ asset('uploads/gateways/jobstripes_sample.csv') }}');
						}
					});
				}
			};

			GatewayHandler.init();
		});
	</script>

@endsection