@extends('layouts.master')

@section('title', 'Crisis Management')

@section('header-content')

	<h1>
		Crisis Management
		<small>Crisis management batch 4 and 5</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Update Crisis management</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Upload <i class="fa fa-plus"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				{{-- <form action="{{ route('update_crisis_management') }}" role="form" method="POST" enctype="multipart/form-data"> --}}
				{{-- <form action="{{ route('update_crisis_management_pdf_only') }}" role="form" method="POST" enctype="multipart/form-data"> --}}
				<form action="{{ route('add_new_crisis_management_pdf_only') }}" role="form" method="POST" enctype="multipart/form-data">
				{{-- <form action="{{ route('cleanup_crisis_pdf') }}" role="form" method="POST" enctype="multipart/form-data"> --}}
				{{-- <form action="{{ route('cleanup_crisis_pd') }}" role="form" method="POST" enctype="multipart/form-data"> --}}
					{{ csrf_field() }}
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group">
								<label for="upload_csv">Upload a CSV File</label>
								<input type="file" id="upload_csv" name="upload_csv">

								<p class="help-block">Please download the sample format <a href="{{ asset('uploads/venues/tlc-codes.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
							</div>
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
		<!-- ./col -->
		@if (Session::has('success_flash_data'))
		<div class="col-md-6">
			<div class="box @if (Session::get('success_flash_data')) box-success @else box-danger @endif">
				<div class="box-header with-border">
					<h3 class="box-title">Results</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					@if (Session::get('success_flash_data'))
						<div class="col-md-12 bg-green color-palette" style="margin-bottom: 10px;">
							<h3>PDF Success Total: {{ count(Session::get('pdf_success_data')) }}</h3>
							<small>Personal Data Form Table</small>
							<a class="btn btn-info" data-toggle="collapse" href="#pdf_collapse" role="button" aria-expanded="false" aria-controls="pdf_collapse">
							    View Data
							</a>
							<div class="collapse" id="pdf_collapse">
								<code>{{ var_dump(Session::get('pdf_success_data')) }}</code>
							</div>
						</div>
						<br> 
						<div class="col-md-12 bg-green color-palette" style="margin-bottom: 10px;">
							<h3>PDF Success Total: {{ count(Session::get('pd_success_data')) }}</h3>
							<small>Personal Data Table</small>
							<a class="btn btn-info" data-toggle="collapse" href="#pd_collapse" role="button" aria-expanded="false" aria-controls="pd_collapse">
							    View Data
							</a>
							<div class="collapse" id="pd_collapse">
								<code>{{ var_dump(Session::get('pd_success_data')) }}</code>
							</div>
						</div>
						<br> 
						<div class="col-md-12 bg-red color-palette" style="margin-bottom: 10px;">
							<h3>PDF Failed Total: {{ count(Session::get('pdf_failed_data')) }}</h3>
							<small>Personal Data Form Table</small>
							<a class="btn btn-info" data-toggle="collapse" href="#pdf_failed_data" role="button" aria-expanded="false" aria-controls="pdf_failed_data">
							    View Data
							</a>
							<div class="collapse" id="pdf_failed_data">
								<code>{{ var_dump(Session::get('pdf_failed_data')) }}</code>
							</div>
						</div>
						<br>
						<div class="col-md-12 bg-red color-palette" style="margin-bottom: 10px;">
							<h3>PD Failed Total: {{ count(Session::get('pd_failed_data')) }}</h3>
							<small>Personal Data Table</small>
							<a class="btn btn-info" data-toggle="collapse" href="#pd_failed_data" role="button" aria-expanded="false" aria-controls="pd_failed_data">
							    View Data
							</a>
							<div class="collapse" id="pd_failed_data">
								<code>{{ var_dump(Session::get('pd_failed_data')) }}</code>
							</div>
						</div> 
						<br> 
					@else
						<div class="col-md-12 bg-red color-palette" style="margin-bottom: 10px;">
							<h3>Failed to Load</h3>
						</div>
					@endif
					
				</div>
			</div>
		</div>
		<!-- ./col -->
		@endif
	</div>
	<!-- /.row -->

@endsection

@section('foot')

@endsection