<!doctype html>
<html style="margin:0; padding:0;">
	<head>
		<meta charset="UTF-8">
		<title>{{ config('app.name') }}</title>
	</head>
	<body style="margin:0; padding:0; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;">
		<table bgcolor="#FFFFFF" style="font-size: 100%; line-height: 1.6em; width: 100%; margin:0; padding:20px 0 0;">
		 <tr>
			 <td bgcolor="#3c8dbc" style="font-size: 100%; line-height: 1.6em; clear: both !important; display: block !important; max-width: 600px !important; margin: 0 auto; padding:20px;">
				 <h1 style="height:auto; margin:0 auto; text-align:center;">
					<a href="{{ url('/') }}" style="text-decoration: none; color: #FFFFFF;">
						C-TRAC APP
					</a>
				 </h1>
			 </td>
		 </tr>
	 </table>
		<table bgcolor="#FFFFFF" style="font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding:0 0 20px;">
			<tr style="width:100%; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0; border-bottom-width:1px;">
				<td class="container" bgcolor="#FFFFFF" style="font-size: 100%; line-height: 1.6em; clear: both !important; display: block !important; max-width: 600px !important; Margin: 0 auto; padding: 20px;">
					<table style="font-size: 100%; line-height: 1.6em; width: 100%; margin: 0; padding: 0;">
						<tr style="width:100%; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">
							<td style="width:100%; font-size: 100%; line-height: 1.6em; margin: 0; padding: 0;">

								@yield('content')

								<h2 style="font-size: 16px; line-height: 1.2em; color: #111111; font-weight: normal; margin: 0 0 20px; padding: 0;">Sincerely,<br />{{ config('app.name') }}</h2>
								
								<p style="border-bottom:1px solid #CCC; width:30%; margin:0 auto 50px; padding-top:50px;"></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<table style="background:#222222; width:100%;">
			<tr>
				<td style="max-width: 600px !important; margin: 0 auto; padding: 0;">
					<p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;line-height:1.5em;margin-top:0;text-align:center;font-size:12px;margin: 0; padding: 20px;">If you’re having trouble clicking the "Notification Action" button, copy and paste the URL below into your web browser: <a href="http://10.27.80.100:443" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#3869d4" target="_blank" rel="noopener noreferrer">{{ url('/') }}</a></p>
				</td>
			</tr>
		</table>
	</body>
</html>