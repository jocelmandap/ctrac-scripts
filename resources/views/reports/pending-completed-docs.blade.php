@extends('layouts.master')

@section('title', 'New Hire Document Statuses w/ Expiration | C-TRAC Reports')
 
@section('header-content')

	<h1>
		New Hire Document Statuses w/ Expiration
		<small>C-TRAC Reports</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">New Hire Document Statuses w/ Expiration</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Retrieve Data <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('reports_crew_pending_completed_doc_upload') }}" role="form" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="doc_status" class="col-md-4 control-label">Document Status:</label>

						    <div class="col-md-8">
						        <select id="doc_status" name="doc_status[]" multiple="multiple" class="form-control">
						        	<option value="1" selected>Pending Document Approval</option>
						        	<option value="2" selected>Completed</option>
						        	<option value="3">Rejected</option>
						        	<option value="5" selected>Audited</option>
						        	<option value="0" selected>Document not submitted</option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="action" class="col-md-4 control-label">Action:</label>

						    <div class="col-md-8">
						        <select id="action" name="action" class="form-control action">
						        	<option value="upload_csv">Upload</option>
						        	<option value="employee_id">Search By Employee ID</option>
						        </select>
						    </div>
						</div>

						<div class="form-group employee-id-container" style="display: inline-block; width: 100%;">
						    <label for="action" class="col-md-4 control-label">Employee ID:</label>

						    <div class="col-md-8">
						        <input type="number" id="employee_id" name="employee_id" class="form-control">
						    </div>
						</div>

						<div class="form-group upload-container" style="display: inline-block; width: 100%;">
						    <label for="upload_csv" class="col-md-4 control-label">Upload a CSV File</label>

						    <div class="col-md-8">
						        <input type="file" id="upload_csv" name="upload_csv" accept=".csv">

						        <p class="help-block">Please download the sample format <a href="{{ asset('uploads/reports/sample_new_hire_document_statuses.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="from_date" class="col-md-4 control-label">From</label>

						    <div class="col-md-8">
						        <div class='input-group date' id='from_date'>
						            <input type='text' name="from_date" class="form-control modified-date-completed" readonly="true" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="to_date" class="col-md-4 control-label">To</label>

						    <div class="col-md-8">
						        <div class='input-group date' id='to_date'>
						            <input type='text' name="to_date" class="form-control modified-date-completed" readonly="true" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

						<div class="form-group" style="display: none; width: 100%;">
						    <label for="to_date" class="col-md-4 control-label">&nbsp;</label>

						    <div class="col-md-8">
						        <div class="checkbox">
						            <label>
						            	<input type="checkbox" name="is_download_report"> Download Results?
						            </label>
						        </div>
						    </div>
						</div>

						

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.row -->
	
	@if (Session::has('user_document_data'))
		
		@php
			$user_document_data = Session::get('user_document_data');
		@endphp

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body">

						<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Employee ID</th>
									<th>Full Name</th>
									<th>Nationality</th>
									<th>Job Code</th>
									<th>Email</th>
									<th>Document Name</th>
									<th>Document Specific Name</th>
									<th>Status</th>
									<th>Modified By</th>
									<th>Date Uploaded/Completed</th>
									<th>Date of Issuance</th>
									<th>Date of Expiration</th>
									{{-- <th>Joining Date</th>
									<th>Joining Vessel</th> --}}
								</tr>
							</thead>
							<tbody>
								@if (!empty($user_document_data))
									@foreach ($user_document_data as $user_document_detail)

										<tr>
											<td>{{ $user_document_detail['Employee ID'] }}</td>
											<td>{{ $user_document_detail['Employee Name'] }}</td>
											<td>{{ $user_document_detail['Nationality'] }}</td>
											<td>{{ $user_document_detail['Job Code'] }}</td>
											<td>{{ $user_document_detail['Employee Email Address'] }}</td>
											<td>{{ $user_document_detail['Document Name'] }}</td>
											<td>{{ $user_document_detail['Document Specific Name'] }}</td>
											<td>{{ $user_document_detail['Document Status'] }}</td>
											<td>{{ $user_document_detail['Modified By'] }}</td>
											<td>{{ $user_document_detail['Date Uploaded / Completed'] }}</td>
											<td>{{ $user_document_detail['Date of Issuance'] ?? 'N/A' }}</td>
											<td>{{ $user_document_detail['Date of Expiration'] ?? 'N/A' }}</td>
											{{-- <td>{{ $user_document_detail['Joining Date'] ?? 'No Data' }}</td>
											<td>{{ $user_document_detail['Joining Vessel'] ?? 'No Data' }}</td> --}}
										</tr>

									@endforeach

								@else
									<tr>
										<td colspan="13">No Data</td>
									</tr>
								@endif
							</tbody>
						</table>

					</div>	
				</div>
			</div>
		</div>

	@endif

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(function () {
			$("#doc_status").select2({
			 	allowClear:true,
				placeholder: 'Select Document Status'
			});

			var curr = '{{ date('Y-m-d h:i A') }}';

			$('#document-table-container').DataTable({
		    	dom           : 'lBfrtip',
    	        buttons       : [
	                {
	                    extend: 'excel',
	                    title: 'New Hires Document Status Reports EXCEL - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                },
	                {
	                    extend: 'csv',
	                    title: 'New Hires Document Status Reports CSV - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                }
    	        ],
    	        "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
    	        'paging'      : true,
    	        'lengthChange': true,
    	        'searching'   : true,
    	        'ordering'    : true,
    	        'info'        : true,
    	        'autoWidth'   : false,
    	        responsive    : true
		    });

		    $('#from_date').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY',
		    	useCurrent:true,
		    	defaultDate: 'January 1, {{ date('Y') }} 12:00 AM'
		    });

		    $('#to_date').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY',
		    	useCurrent:true,
		    	defaultDate: new Date()
		    });
		});
		$(document).ready(function () {
			var FormHandler = {
				init : function () {
					$('.employee-id-container').slideUp('400', function () {
						$('.upload-container').slideDown();
					});
					FormHandler.toggle_action();
				},
				toggle_action : function () {
					$(document).on('change', '.action', function () {
						if ($(this).val() == 'upload_csv') {
							$('.employee-id-container').slideUp('400', function () {
								$('.upload-container').slideDown();
							});
						} else {
							$('.upload-container').slideUp('400', function () {
								$('.employee-id-container').slideDown();
							});
						}
					});
				}
			};

			FormHandler.init();
		});
	</script>
@endsection