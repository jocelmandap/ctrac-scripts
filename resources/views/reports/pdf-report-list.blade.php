@extends('layouts.master')

@section('title', 'Personal Data Form Reports | C-TRAC Reports')

@section('header-content')

	<h1>
		Personal Data Form Reports
		<small>C-TRAC Reports</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Personal Data Form Reports</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Retrieve Data <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('reports_pdf_with_job_code_submit') }}" id="form" role="form" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="application_status" class="col-md-4 control-label">Application Status:</label>

						    <div class="col-md-6">
						        <select id="application_status" name="application_status" class="form-control action">
						        	<option value="all">All</option>
						        	@if ( !$status_codes->isEmpty() )
						        		@foreach ($status_codes as $status_code)
						        			<option value="{{ $status_code->getId() }}">{{ $status_code->getId() . ' - ' . $status_code->getName() }}</option>
						        		@endforeach
						        	@endif
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="pdf_document_status" class="col-md-4 control-label">PDF Document Status:</label>

						    <div class="col-md-6">
						        <select id="pdf_document_status" name="pdf_document_status[]" class="form-control action" multiple="multiple">
						        	<option value="0" selected>Document Not Submitted</option>
						        	<option value="1" selected>Pending Document Approval</option>
						        	<option value="2">Completed</option>
						        	<option value="5">Audited</option>
						        	<option value="3">Rejected</option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="job_code_ids" class="col-md-4 control-label">Job Codes:</label>

						    <div class="col-md-6">
						        <select id="job_code_ids" name="job_code_ids[]" class="form-control action" multiple="multiple">
						        	<option value="all" selected>All</option>
						        	@if ( !$job_codes->isEmpty() )
						        		@foreach ($job_codes->unique('job_int_code') as $job_code)
						        			<option value="{{ $job_code->getCode() }}">{{ $job_code->getCode() . ' - ' . $job_code->getName() }}</option>
						        		@endforeach
						        	@endif
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="from_offer_date" class="col-md-4 control-label">From Offer Date</label>

						    <div class="col-md-6">
						        <div class='input-group date-from' id='from_offer_date'>
						            <input type='text' name="from_offer_date" class="form-control modified-date-completed" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="to_offer_date" class="col-md-4 control-label">To Offer Date</label>

						    <div class="col-md-6">
						        <div class='input-group date-to' id='to_offer_date'>
						            <input type='text' name="to_offer_date" class="form-control modified-date-completed" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn pull-right btn-primary">Submit</button>
						<button type="button" class="btn clear-fields">Clear Fields</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.row -->
	
	@if (Session::has('records'))
		
		@php
			$records = Session::get('records');
		@endphp

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body">

						<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Nationality</th>
									<th>Job Code</th>
									<th>Job Title</th>
									<th>Brand</th>
									<th>Offer Date Sent</th>
									<th>Offer Date Accepted</th>
									<th>PDF Last Date Modified</th>
									<th>Document Status</th>
									<th>Source Type</th>
									<th>Source</th>
								</tr>
							</thead>
							<tbody>
								@if ( !$records->isEmpty() )
									@foreach ($records as $record_detail)
										
										<tr>
											<td>{{ $record_detail->Name }}</td>
											<td>{{ $record_detail->Email }}</td>
											<td>{{ $record_detail->Nationality }}</td>
											<td>{{ $record_detail->JobCode }}</td>
											<td>{{ $record_detail->JobTitle }}</td>
											<td>{{ $record_detail->Brand }}</td>
											<td>{{ $record_detail->OfferSent }}</td>
											<td>{{ $record_detail->OfferAccepted ?? 'Not Yet Accepted' }}</td>
											<td>{{ $record_detail->PDFSubmitted }}</td>
											<td>{{ $record_detail->Status }}</td>
											<td>{{ $record_detail->SourceType }}</td>
											<td>{{ $record_detail->Source ?? 'No Data' }}</td>
										</tr>

									@endforeach

								@else
									<tr>
										<td colspan="13">No Data</td>
									</tr>
								@endif
							</tbody>
						</table>

					</div>	
				</div>
			</div>
		</div>

	@endif

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(function () {

			$("#job_code_ids, #pdf_document_status").select2({
			 	allowClear:true,
				placeholder: 'Select Job Codes'
			});

			var curr = '{{ date('Y-m-d h:i A') }}';

		    $('#document-table-container').DataTable({
		    	dom           : 'lBfrtip',
    	        buttons       : [
	                {
	                    extend: 'excel',
	                    title: 'PDF report list EXCEL - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                },
	                {
	                    extend: 'csv',
	                    title: 'PDF report list CSV - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                }
    	        ],
    	        "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
    	        'paging'      : true,
    	        'lengthChange': true,
    	        'searching'   : true,
    	        'ordering'    : true,
    	        'info'        : true,
    	        'autoWidth'   : false,
    	        responsive    : true
		    });

		    var date = new Date();
			var firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
			var lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
			
		    $('.date-from').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY',
		    	useCurrent:true,
		    	defaultDate: firstDate
		    });

		    $('.date-to').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY',
		    	useCurrent:true,
		    	defaultDate: lastDate
		    });

		    $(document).ready(function () {
		    	var FormHandler = {
		    		init : function () {

		    		}
		    	};

		    	FormHandler.init();
		    });
		});
	</script>
@endsection