@extends('layouts.master')

@section('title', 'New Hires Travel Information | C-TRAC Reports')

@section('header-content')

	<h1>
		New Hires Travel Information
		<small>C-TRAC Reports</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">New Hires Travel Information</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Retrieve Data <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('document_travel_info_submit') }}" id="form" role="form" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="action" class="col-md-4 control-label">Action:</label>

						    <div class="col-md-6">
						        <select id="action" name="action" class="form-control action">
						        	<option value="bulk">Bulk Employee ID</option>
						        	<option value="by_employee_id">Enter Employee ID's</option>
						        </select>
						    </div>
						</div>

						<div class="form-group upload-container" style="display: inline-block; width: 100%;">
						    <label for="upload_csv" class="col-md-4 control-label">Upload a CSV File</label>

						    <div class="col-md-6">
						        <input type="file" id="upload_csv" name="upload_csv" accept=".csv">

						        <p class="help-block">Please download the sample format <a href="{{ asset('uploads/reports/sample_document_travel_info.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
						    </div>
						</div>

						<div class="form-group employee-id-container" style="display: inline-block; width: 100%;">
						    <label for="employee_ids" class="col-md-4 control-label">Employee ID:</label>

						    <div class="col-md-6">
						        <input type="text" id="employee_ids" name="employee_ids" class="form-control">
						        <span class="help-block">Note: Please enter if you want to add another entry</span>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="departure_country" class="col-md-4 control-label">Departure Country:</label>

						    <div class="col-md-6">
						        <select id="departure_country" name="departure_country" class="form-control country">
						        	<option></option>
						        	@if (!$countries->isEmpty())
						        		@foreach($countries as $country)
						        			<option value="{{ $country->getCountryCode() }}">{{ $country->getCountryCode() . ' - ' . $country->getCountryName() }}</option>
						        		@endforeach
						        	@endif
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="arrival_country" class="col-md-4 control-label">Arrival Country:</label>

						    <div class="col-md-6">
						        <select id="arrival_country" name="arrival_country" class="form-control country">
						        	<option></option>
						        	@if (!$countries->isEmpty())
						        		@foreach($countries as $country)
						        			<option value="{{ $country->getCountryCode() }}">{{ $country->getCountryCode() . ' - ' . $country->getCountryName() }}</option>
						        		@endforeach
						        	@endif
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="departure_date" class="col-md-4 control-label">Departure Date</label>

						    <div class="col-md-6">
						        <div class='input-group date' id='departure_date'>
						            <input type='text' name="departure_date" class="form-control modified-date-completed" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="arrival_date" class="col-md-4 control-label">Arrival Date</label>

						    <div class="col-md-6">
						        <div class='input-group date' id='arrival_date'>
						            <input type='text' name="arrival_date" class="form-control modified-date-completed" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="button" class="btn pull-right clear-fields">Clear Fields</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.row -->
	
	@if (Session::has('records'))
		
		@php
			$records = Session::get('records');
		@endphp

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body">

						<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Employee Id</th>
									<th>Name</th>
									<th>Job Description</th>
									<th>Ship Name</th>
									<th>Sign On Date</th>
									<th>Reason Code</th>
									<th>Nationality</th>
									<th>Hiring Partner Name</th>
									<th>Cost Center</th>
									<th>Country From</th>
									<th>Country To</th>
									<th>Departure Date</th>
									<th>Arrival Date</th>
									<th>Airport From</th>
									<th>Airport To</th>
									<th>Source</th>
								</tr>
							</thead>
							<tbody>
								@if (!empty($records))
									@foreach ($records as $record_detail)

										<tr>
											<td>{{ $record_detail['EmployeeID'] }}</td>
											<td>{{ $record_detail['Name'] }}</td>
											<td>{{ $record_detail['JobDescription'] }}</td>
											<td>{{ $record_detail['shipCode'] }}</td>
											<td>{{ $record_detail['SignOnDate'] }}</td>
											<td>{{ $record_detail['ReasonCode'] }}</td>
											<td>{{ $record_detail['Nationality'] }}</td>
											<td>{{ $record_detail['HiringPartnerName'] }}</td>
											<td>{{ $record_detail['CostCenter'] }}</td>
											<td>{{ $record_detail['CountryFrom'] }}</td>
											<td>{{ $record_detail['CountryTo'] }}</td>
											<td>{{ $record_detail['DepartureDate'] }}</td>
											<td>{{ $record_detail['ArrivalDate'] }}</td>
											<td>{{ $record_detail['AirportFrom'] }}</td>
											<td>{{ $record_detail['AirportTo'] }}</td>
											<td>{{ $record_detail['Source'] }}</td>
										</tr>

									@endforeach

								@else
									<tr>
										<td colspan="14">No Data</td>
									</tr>
								@endif
							</tbody>
						</table>

					</div>	
				</div>
			</div>
		</div>

	@endif

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(function () {

			$('.country').selectize();
			$('#employee_ids').selectize({
			    delimiter: ',',
			    persist: false,
			    create: function(input) {
			        return {
			            value: input,
			            text: input
			        }
			    }
			});

			var curr = '{{ date('Y-m-d h:i A') }}';

		    $('#document-table-container').DataTable({
		    	dom           : 'lBfrtip',
    	        buttons       : [
	                {
	                    extend: 'excel',
	                    title: 'New Hires Travel Info EXCEL - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                },
	                {
	                    extend: 'csv',
	                    title: 'New Hires Travel Info CSV - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                }
    	        ],
    	        "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
    	        'paging'      : true,
    	        'lengthChange': true,
    	        'searching'   : true,
    	        'ordering'    : true,
    	        'info'        : true,
    	        'autoWidth'   : false,
    	        responsive    : true
		    });
			
		    $('.date').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY',
		    	useCurrent:true,
		    	defaultDate: new Date()
		    });

		    $(document).ready(function () {
		    	var FormHandler = {
		    		init : function () {
		    			$('.employee-id-container').slideUp('400', function () {
		    				$('.upload-container').slideDown();
		    			});
		    			FormHandler.toggle_action();
		    			FormHandler.clear_fields();
		    		},
		    		toggle_action : function () {
		    			$(document).on('change', '.action', function () {
		    				if ($(this).val() == 'bulk') {
		    					$('.employee-id-container').slideUp('400', function () {
		    						$('.upload-container').slideDown();
		    					});
		    				} else {
		    					$('.upload-container').slideUp('400', function () {
		    						$('.employee-id-container').slideDown();
		    					});
		    				}
		    			});
		    		},
		    		clear_fields : function () {
		    			$(document).on('click', '.clear-fields', function () {
		    				$('#form')[0].reset();
		    				$('#departure_country')[0].selectize.clear();
		    				$('#arrival_country')[0].selectize.clear();
		    				$('#employee_ids')[0].selectize.clear();
		    				$('.employee-id-container').slideUp('400', function () {
		    					$('.upload-container').slideDown();
		    				});
		    				$('#departure_date').data("DateTimePicker").date(new Date());
		    				$('#arrival_date').data("DateTimePicker").date(new Date());
		    			});
		    		}
		    	};

		    	FormHandler.init();
		    });
		});
	</script>
@endsection