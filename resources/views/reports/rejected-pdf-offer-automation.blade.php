@extends('layouts.master')

@section('title', 'Rejected PDF and Offer Letter due to Automation | C-TRAC Reports')

@section('header-content')

	<h1>
		Rejected PDF and Offer Letter due to Automation
		<small>C-TRAC Reports</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Rejected PDF and Offer Letter due to Automation</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Retrieve Data <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('reports_rejected_pdf_offer_automation_submit') }}" role="form" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="rejected_status" class="col-md-4 control-label">Rejected Status:</label>

						    <div class="col-md-6">
						        <select id="rejected_status" name="rejected_status" class="form-control">
						        	<option value="All">All</option>
						        	@if (!$rejected_status_codes->isEmpty())
						        		@foreach ($rejected_status_codes as $rejected_status_code)
						        			<option value="{{ $rejected_status_code->getId() }}">{{ $rejected_status_code->getName() }}</option>
						        		@endforeach
						        	@endif
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="from_date" class="col-md-4 control-label">From</label>

						    <div class="col-md-6">
						        <div class='input-group date' id='from_date'>
						            <input type='text' name="from_date" class="form-control modified-date-completed" readonly="true" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="to_date" class="col-md-4 control-label">To</label>

						    <div class="col-md-6">
						        <div class='input-group date' id='to_date'>
						            <input type='text' name="to_date" class="form-control modified-date-completed" readonly="true" />
						            <span class="input-group-addon">
						                <span class="glyphicon glyphicon-calendar"></span>
						            </span>
						        </div>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.row -->
	
	@if (Session::has('records'))
		
		@php
			$records = Session::get('records');
			$offer_letter_ids = Session::get('offer_letter_ids');
			$passport_document_id = Session::get('passport_document_id');
			$pdf_document_id = Session::get('pdf_document_id');
		@endphp

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body">

						<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Applicant Name</th>
									<th>Email Address</th>
									<th>Job Code</th>
									<th>Offer Letter</th>
									<th>Offer Letter Date Issued</th>
									<th>Offer Letter Issuer</th>
									<th>Offer Letter Date Accepted</th>
									<th>Date PDF Last Updated</th>
									<th>Date Passport Last Uploaded</th>
									<th>Status Code</th>
									<th>Status Name</th>
									<th>Auto Fail Date</th>
									<th>Source Type</th>
									<th>Source</th>
								</tr>
							</thead>
							<tbody>
								@if (!empty($records))
									@foreach ($records as $record_detail)

										<tr>
											<td>{{ $record_detail->Name }}</td>
											<td>{{ $record_detail->Email }}</td>
											<td>{{ $record_detail->JobCode }}</td>
											<td>{{ $record_detail->OfferDocName }}</td>
											<td>{{ $record_detail->OfferDateCreated }}</td>
											<td>{{ $record_detail->Recruiter }}</td>
											<td>{{ $record_detail->OfferDateModified }}</td>
											<td>{{ $record_detail->PDFDateModified }}</td>
											<td>{{ $record_detail->PassportDateModified }}</td>
											<td>{{ $record_detail->StatusId }}</td>
											<td>{{ $record_detail->StatusName }}</td>
											<td>{{ $record_detail->HistoryRecordDate }}</td>
											<td>{{ $record_detail->SourceType }}</td>
											<td>{{ $record_detail->Source ?? 'N/A' }}</td>
										</tr>

									@endforeach

								@else
									<tr>
										<td colspan="13">No Data</td>
									</tr>
								@endif
							</tbody>
						</table>

					</div>	
				</div>
			</div>
		</div>

	@endif

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(function () {
			var curr = new Date;

		    $('#document-table-container').DataTable({
		    	dom           : 'lBfrtip',
    	        buttons       : [
	                {
	                    extend: 'excel',
	                    title: 'Rejected PDF and Offer Letter Auto EXCEL - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                },
	                {
	                    extend: 'csv',
	                    title: 'Rejected PDF and Offer Letter Auto CSV - ' + curr,
	                    exportOptions: {
	                        columns: "thead th:not(.noExport)"
	                    }
	                }
    	        ],
    	        "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
    	        'paging'      : true,
    	        'lengthChange': true,
    	        'searching'   : true,
    	        'ordering'    : true,
    	        'info'        : true,
    	        'autoWidth'   : false,
    	        responsive    : true
		    });
			
		    $('#from_date').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY LT',
		    	useCurrent:true,
		    	defaultDate: new Date(curr.setDate(curr.getDate() - curr.getDay()))
		    });

		    $('#to_date').datetimepicker({
		    	allowInputToggle: true,
		    	ignoreReadonly: true,
		    	format: 'MMMM D, YYYY LT',
		    	useCurrent:true,
		    	defaultDate: new Date(curr.setDate(curr.getDate() - curr.getDay()+6))
		    });
		});
	</script>
@endsection