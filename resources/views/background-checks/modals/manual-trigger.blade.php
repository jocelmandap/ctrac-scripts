<!-- Modal -->
<div class="modal fade" id="manual-trigger-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> Manual Trigger Background Check</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to manually trigger background check using the HireRight Application not the C-TRAC application?</p>
                <p><i><b>Note:</b> The background check on C-TRAC will not proceed anymore if this was executed.</i></p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="form-candidate-id-manual-trigger">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-success proceed-manual-trigger">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>