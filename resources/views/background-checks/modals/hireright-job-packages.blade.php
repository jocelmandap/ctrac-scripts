<!-- Modal -->
<div class="modal fade" id="hireright-job-package-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Hireright Job Package for ID# <span class="hireright-job-int-id"></span></h4>
            </div>
            <div class="modal-body">
                <div class="hireright-job-packages-content">
                	<!-- Load via AJAX-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>