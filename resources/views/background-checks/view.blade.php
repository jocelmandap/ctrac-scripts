@extends('layouts.master')

@section('title', 'Background Checks')

@section('header-content')

	<h1>
		Background Checks
		<small>Background Checker for CM's</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Background Check</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row background-check-form-container">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add Crew Member JDE ID</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group user-id-type-container upload-type">
						<label for="jde_id">JDE ID</label>
						<input type="number" class="form-control" id="jde_id" name="jde_id" placeholder="Please Add JDE ID here">
						<span class="error-container jde-error-container">Please Add JDE ID.</span>
					</div>
					
					<button id="submit_jde_form" class="btn btn-primary">Submit</button>
					<button id="info_modal" class="btn btn-info hide"><i class="fa fa-info-circle"></i> How it Works</button>
					
					<span class="submit-status-container warning"></span>

					<div class="user-details-container data-containers">
						<!-- Data will be loaded via AJAX -->
					</div>

				</div>
				<!-- /.box-body -->

				<div class="box-footer">
					
				</div>
			</div>
		</div>
		<div class="col-md-6 posible-queries-main-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
	<!-- /.row -->

	<div class="row">
		<div class="col-md-12 form-candidate-data-main-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6 document-checklist-form-uploads-main-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
		<div class="col-md-6 document-checklist-uploads-main-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 highright-main-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
		<div class="col-md-6 job-applications-main-container data-containers">
			<!-- Data will be loaded via AJAX -->
		</div>
	</div>

	<div class="row">
		
	</div>

@endsection

@section('modal-content')
	
	@include('background-checks.modals.hireright-job-packages')
	@include('background-checks.modals.how-it-works')
	@include('background-checks.modals.manual-trigger')

@endsection

@section('foot')
	<script>
		$(document).ready(function () {
			var BackgroundCheckHandler = {
				init : function () {
					BackgroundCheckHandler.reset_background_check();
					BackgroundCheckHandler.submit_form();
					BackgroundCheckHandler.init_accordion();
					BackgroundCheckHandler.view_hireright_job_application();
					BackgroundCheckHandler.submit_query_completed();
					BackgroundCheckHandler.submit_query_insert_form_uploads();
					BackgroundCheckHandler.submit_query_insert_uploads();
					BackgroundCheckHandler.submit_query_update_created_on();
					BackgroundCheckHandler.submit_update_form_candidate_data();
					BackgroundCheckHandler.info_modal();
					BackgroundCheckHandler.manual_trigger_modal();
					BackgroundCheckHandler.submit_update_manual_trigger();
				},
				reset_background_check : function () {
					$('.data-containers').slideUp();
					setTimeout(function () {
						$('.data-containers').html('');
					}, 1000);
				},
				validate_form : function (jde_id) {
					if (jde_id.length > 0) {
						$('.jde-error-container').slideUp();
						return true;
					}
					$('.jde-error-container').slideDown();
					return false;
				},
				submit_form : function () {
					$(document).on('click', '#submit_jde_form', function () {
						var jde_id = $('#jde_id').val();
						var is_valid = BackgroundCheckHandler.validate_form(jde_id);

						if (is_valid) {
							$.ajax({
								url: '{{ route('background_check_submit') }}',
								type: 'POST',
								dataType: 'json',
								data: {jde_id: jde_id},
								beforeSend : function () {
									$('#submit_jde_form')
										.prop('disabled', true)
										.removeClass('btn-primary')
										.addClass('btn-warning')
										.html('Submitting...');
								},
								success : function (response) {
									if (response.status == true) {
										$('.form-candidate-data-main-container').html(response.form_candidate_content);
										$('.document-checklist-form-uploads-main-container').html(response.document_checlist_form_upload_content);
										$('.document-checklist-uploads-main-container').html(response.document_checlist_upload_content);
										$('.highright-main-container').html(response.hireright_crew_request_content);
										$('.job-applications-main-container').html(response.job_application_content);
										$('.posible-queries-main-container').html(response.posible_queries);
										$('.user-details-container').html(response.user_details_content);
										setTimeout(function () {
											$('#submit_jde_form')
												.removeClass('btn-warning')
												.addClass('btn-success')
												.html('Successfully Submitted!');
											$('.data-containers').slideDown();
										}, 400);
									} else {
										$('#submit_jde_form')
											.removeClass('btn-warning')
											.addClass('btn-danger')
											.html('Not Submitted!');
										BackgroundCheckHandler.reset_background_check();
									}

									setTimeout(function () {
										$('#submit_jde_form')
											.prop('disabled', false)
											.removeClass('btn-success')
											.removeClass('btn-danger')
											.addClass('btn-primary')
											.html('Submit');
									}, 1500);
								}
							});
						}
					});
				},
				init_accordion : function () {
					$(document).on('hidden.bs.collapse', '.panel-group', function toggleIcon(e) {
						$(e.target)
						    .prev('.panel-heading')
						    .find(".more-less")
						    .toggleClass('glyphicon-plus glyphicon-minus');
					});

					$(document).on('shown.bs.collapse', '.panel-group', function toggleIcon(e) {
						$(e.target)
						    .prev('.panel-heading')
						    .find(".more-less")
						    .toggleClass('glyphicon-plus glyphicon-minus');
					});
				},
				view_hireright_job_application : function () {
					$(document).on('click', '.job-applications-main-container .get-hireright-job-package', function () {
						$('#hireright-job-package-modal').modal('show');
						var job_int_id = $(this).data('jobint');
						$('.hireright-job-int-id').text(job_int_id);

						if (job_int_id) {
							$.ajax({
								url: '{{ route('background_check_hireright_get_job_package') }}',
								type: 'POST',
								dataType: 'json',
								data: {job_int_id: job_int_id},
								beforeSend: function () {
									var content = `
										<div class="loader-bg-container">
											<div class="dv-loader-bg">
											 	<i class="fa fa-cog fa-spin fa-3x"></i>
											 	<p>Updating...</p>
											</div>
										</div>
									`;
									$('.hireright-job-packages-content').html(content);
								},
								success : function (response) {
									if (response.status == true) {
										var content = response.content;
										setTimeout(function () {
											$('.loader-bg-container').fadeOut('slow', function() {
												$('.hireright-job-packages-content').html(content);
											});
										}, 400);
									} else {
										$('.loader-bg-container').fadeOut('slow', function() {
											$('.hireright-job-packages-content').html('<h2 style="text-align: center;background: #f88;color: #fff;margin: 0;padding: 20px;">Hireright Job Code Not Included</h2>');
										});
									}
								}
							});
						}
					});
				},
				submit_query_completed : function () {
					$(document).on('click', '#submit_query_completed', function () {
						var document_checklist_form_upload_id = $(this).data('id');
						var status_key = $(this).data('completed');
						var date_modified = $('.modified-date-completed').val();
						
						if (document_checklist_form_upload_id != '') {
							$.ajax({
								url: '{{ route('update_document_checklist_form_upload_status') }}',
								type: 'POST',
								dataType: 'json',
								data: {
									document_checklist_form_upload_id: document_checklist_form_upload_id, 
									status_key: status_key,
									date_modified: date_modified
								},
								success : function (response) {
									if (response.status == true) {
										$('#submit_jde_form').trigger('click');
									} else {
										alert('Query not Executed!');
									}
								}
							});
						}
					});
				},
				submit_query_insert_form_uploads : function () {
					$(document).on('click', '#submit_query_insert_form_uploads', function () {
						var user_id = $(this).data('user');
						
						if (user_id != '') {
							$.ajax({
								url: '{{ route('save_document_checklist_form_upload') }}',
								type: 'POST',
								dataType: 'json',
								data: {user_id: user_id},
								success : function (response) {
									if (response.status == true) {
										$('#submit_jde_form').trigger('click');
									} else {
										alert('Query not Executed!');
									}
								}
							});
						}
					});
				},
				submit_query_insert_uploads : function () {
					$(document).on('click', '#submit_query_insert_uploads', function () {
						var user_id = $(this).data('user');
						
						if (user_id != '') {
							$.ajax({
								url: '{{ route('save_document_checklist_upload') }}',
								type: 'POST',
								dataType: 'json',
								data: {user_id: user_id},
								success : function (response) {
									if (response.status == true) {
										$('#submit_jde_form').trigger('click');
									} else {
										alert('Query not Executed!');
									}
								}
							});
						}
					});
				},
				submit_query_update_created_on : function () {
					$(document).on('click', '#submit_query_update_created_on', function () {
						var id = $(this).data('id');
						
						if (id != '') {
							$.ajax({
								url: '{{ route('update_created_on_document_checklist_upload') }}',
								type: 'POST',
								dataType: 'json',
								data: {id: id},
								success : function (response) {
									if (response.status == true) {
										$('#submit_jde_form').trigger('click');
									} else {
										alert('Query not Executed!');
									}
								}
							});
						}
					});
				},
				submit_update_form_candidate_data : function () {
					$(document).on('click', '#submit_update_form_candidate_data', function () {
						var user_id = $(this).data('user');
						
						if (user_id != '') {
							$.ajax({
								url: '{{ route('update_form_candidate_data') }}',
								type: 'POST',
								dataType: 'json',
								data: {user_id: user_id},
								success : function (response) {
									if (response.status == true) {
										$('#submit_jde_form').trigger('click');
									} else {
										alert('Query not Executed!');
									}
								}
							});
						}
					});
				},
				info_modal : function () {
					$('#info_modal').click(function () {
						$('#how-it-works-modal').modal('show');
					});
				},
				manual_trigger_modal : function () {
					$(document).on('click', '.manual-trigger-bgc', function () {
						$('#form-candidate-id-manual-trigger').val( $(this).data('id') );
						$('#manual-trigger-modal').modal('show');
					});
				},
				submit_update_manual_trigger : function () {
					$(document).on('click', '.proceed-manual-trigger', function () {
						var form_candidate_id = $('#form-candidate-id-manual-trigger').val();
						
						if (form_candidate_id != '') {
							$.ajax({
								url: '{{ route('update_form_candidate_data_manual_trigger') }}',
								type: 'POST',
								dataType: 'json',
								data: {form_candidate_id: form_candidate_id},
								success : function (response) {
									if (response.status == true) {
										BackgroundCheckHandler.reset_background_check();
										setTimeout(function () {
											$('#manual-trigger-modal').modal('hide');
											$('#submit_jde_form').trigger('click');
										}, 1000);
									} else {
										alert('PDF not Found!');
									}
								}
							});
						}
					});
				}
			};

			BackgroundCheckHandler.init();
		});
	</script>
@endsection