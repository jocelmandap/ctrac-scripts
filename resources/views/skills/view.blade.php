@extends('layouts.master')

@section('title', 'Skills Uploader')

@section('header-content')

	<h1>
		Skills
		<small>Upload C-TRAC Skills</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Skills</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Upload <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('skills_upload_submit') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">

						<div class="row">
							<div class="form-group" style="display: inline-block; width: 100%;">
							    <label for="skill_category" class="col-md-4 control-label">Skill Categories</label>

							    <div class="col-md-6">
							        <select id="skill_category" name="skill_category" class="form-control">
							        	@if (!$skill_category_list->isEmpty())
											@foreach ($skill_category_list as $skill_category)
												<option value="{{ $skill_category->getId() }}">{{ $skill_category->getName() }}</option>
											@endforeach
							        	@else
											<option value="">Empty</option>
							        	@endif
							        </select>
							    </div>
							</div>

							<div class="form-group" style="display: inline-block; width: 100%;">
								<label for="upload_csv" class="col-md-4 control-label">Upload a CSV File</label>
								<div class="col-md-6">
									<input type="file" id="upload_csv" name="upload_csv">

									<p class="help-block">Please download the sample format <a href="{{ asset('uploads/skills/skills_sample.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
								</div>
							</div>

							<label class="col-md-4 control-label">&nbsp;</label>
							<div class="col-md-6">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>
		@if (Session::has('history_logs'))
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body" style="overflow-y: scroll; height: 400px;">

						<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Skill Name</th>
					            </tr>
							</thead>
							<tbody>
								@php
									$count = 1;
								@endphp
								@foreach (Session::get('history_logs') as $log)
									
									<tr>
										<td>{{ $count }}</td>
										<td>{{ $log['skill_name'] }}</td>
									</tr>
									
									@php
										$count++;
									@endphp

								@endforeach	
							</tbody>
						</table>
					</div>	
				</div>
			</div>
		@endif
	</div>
	<!-- /.row -->

@endsection