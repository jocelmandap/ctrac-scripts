@extends('layouts.master')

@section('title', 'Medical Certificates | Adhoc')

@section('header-content')

	<h1>
		Medical Certificates
		<small>Adhoc</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Medical Certificates</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
                <!-- form start -->
				<form action="{{ route('adhoc_medical_cert_submit') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">

                        <div class="form-group" style="display: inline-block; width: 100%;">
                            <table class="table table-striped">
                                <thead>
                                    <th></th>
                                    <th>Documents Count</th>
                                    <th>Uploaded Documents Count</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Advance Cardiovascular Life Support:</td>
                                        <td>{{ $medical_document_list_count['acls'] }}</td>
                                        <td>{{ $medical_document_list_count['acls_uploaded'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Basic Life Saving:</td>
                                        <td>{{ $medical_document_list_count['bls'] }}</td>
                                        <td>{{ $medical_document_list_count['bls_uploaded'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pediatric Advanced Life Support:</td>
                                        <td>{{ $medical_document_list_count['pals'] }}</td>
                                        <td>{{ $medical_document_list_count['pals_uploaded'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Advanced Trauma Life Support:</td>
                                        <td>{{ $medical_document_list_count['atls'] }}</td>
                                        <td>{{ $medical_document_list_count['atls_uploaded'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group" style="display: inline-block; width: 100%;">
                            <label for="limit" class="col-md-5 control-label">Total Number to be Upload</label>
                            <input type="hidden" name="start" value="1">
                            <div class="col-md-7">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="2000">2000</option>
                                </select>
                            </div>
                        </div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label class="col-md-8 control-label">&nbsp;</label>

						    <div class="col-md-4">
						        <button type="submit" class="btn btn-primary">Start</button>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>
		<!-- ./col -->

		@if (Session::has('history_logs') || Session::has('history_logs_not_updated'))
			<div class="col-md-8">
				
				@if (Session::has('history_logs'))
					<div class="box box-success">
						<div class="box-header with-border">
							<h3 class="box-title">Success Results: </h3>
							<a 
								href="{{ route('adhoc_medical_cert_success_result_logs_download') }}" 
								target="_blank" 
								class="btn btn-info btn-xs pull-right">Download</a>
						</div>
						<div class="box-body" style="overflow-y: auto; max-height: 400px;">
							<p>Total ACLS: <label class="label label-info">{{ Session::get('total_acls') }}</label></p>
							<p>Total BLS: <label class="label label-info">{{ Session::get('total_bls') }}</label></p>
							<p>Total PALS: <label class="label label-info">{{ Session::get('total_pals') }}</label></p>
							<p>Total ATLS: <label class="label label-info">{{ Session::get('total_atls') }}</label></p>
							<p>Total New / Updated: <label class="label label-info">{{ Session::get('total_updated') }}</label></p>
							@if (!empty(Session::get('history_logs')))
								<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
									<thead>
							            <tr>
							            	<th>#</th>
							                <th>User ID</th>
							                <th>Document ID</th>
							                <th>Document Entry ID</th>
							                <th>Document Header ID</th>
							                <th>Document Header Update Type</th>
							            </tr>
									</thead>
									<tbody>
										@php
											$count = 1;
										@endphp
										@foreach (Session::get('history_logs') as $log)
											
											<tr>
												<td>{{ $count }}</td>
												<td>{{ $log['user_id'] }}</td>
												<td>{{ $log['ck_list_id'] }}</td>
												<td>{{ $log['document_entry_id'] }}</td>
												<td>{{ $log['document_header'] }}</td>
												<td>{{ $log['document_header_status'] }}</td>
											</tr>
											
											@php
												$count++;
											@endphp

										@endforeach	
									</tbody>
								</table>
							@endif
						</div>	
					</div>
				@endif

				@if (Session::has('history_logs_not_updated'))
					<div class="box box-danger">
						<div class="box-header with-border">
							<h3 class="box-title">Not Updated Results: </h3>
							<a 
								href="{{ route('adhoc_medical_cert_fail_result_logs_download') }}" 
								target="_blank" 
								class="btn btn-info btn-xs pull-right">Download</a>
						</div>
						<div class="box-body" style="overflow-y: auto; max-height: 400px;">
							<p>Total of Not Updated: <label class="label label-info">{{ Session::get('total_not_updated') }}</label></p>
							@if (!empty(Session::get('history_logs_not_updated')))
								<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
									<thead>
							            <tr>
							            	<th>#</th>
							                <th>User ID</th>
							                <th>Document ID Not Updated</th>
							                <th>Remarks</th>
							            </tr>
									</thead>
									<tbody>
										@php
											$count = 1;
										@endphp
										@foreach (Session::get('history_logs_not_updated') as $log)
											
											<tr>
												<td>{{ $count }}</td>
												<td>{{ $log['user_id'] }}</td>
												<th>{{ $log['ck_list_id'] }}</th>
												<th>{{ $log['remarks'] }}</th>
											</tr>
											
											@php
												$count++;
											@endphp

										@endforeach	
									</tbody>
								</table>
							@endif
						</div>	
					</div>
				@endif
			</div>
		@endif

@endsection