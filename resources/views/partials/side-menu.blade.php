<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				{{-- <img src="/bower_components/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}
				<img src="{{ Auth::user()->getProfilePhoto() }}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{{ Auth::user()->getName() }}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li @if (Request::url() == route('main_dashboard')) class="active" @endif>
				<a href="{{ route('main_dashboard') }}">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li>
			@if ($role->getName() == 'Super Admin')
				<li class="header">ADMIN PAGES</li>
				<li class="@if (Request::url() == route('admin_role_list')) active @endif">
					<a href="{{ route('admin_role_list') }}">
						<i class="fa fa-user-secret" aria-hidden="true"></i> 
						<span>Role List</span>
					</a>
				</li>
				<li class="@if (Request::url() == route('admin_users_list')) active @endif">
					<a href="{{ route('admin_users_list') }}">
						<i class="fa fa-users" aria-hidden="true"></i> 
						<span>Admin User List</span>
					</a>
				</li>
				<li class="@if (Request::url() == route('admin_page_list')) active @endif">
					<a href="{{ route('admin_page_list') }}">
						<i class="fa fa-files-o" aria-hidden="true"></i> 
						<span>Page List</span>
					</a>
				</li>
				<li class="@if (Request::url() == route('support_hrsd_type_view')) active @endif">
					<a href="{{ route('support_hrsd_type_view') }}">
						<i class="fa fa-ticket" aria-hidden="true"></i> 
						<span>HRSD Type</span>
					</a>
				</li>
				<li class="treeview @if (Request::url() == route('add_venue_skills_view')) active @endif">
					<a href="#">
						<i class="fa fa-address-book"></i>
						<span>Venue Skills</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li @if (Request::url() == route('add_venue_skills_view')) class="active" @endif>
							<a href="{{ route('add_venue_skills_view') }}"><i class="fa fa-circle-o"></i> Add User Venue Skills</a>
						</li>
						<li @if (Request::url() == route('clear_venue_skills')) class="active" @endif>
							<a href="{{ route('clear_venue_skills') }}"><i class="fa fa-circle-o"></i> Truncate User Venue Skills</a>
						</li>
						<li @if (Request::url() == route('update_user_skills_view')) class="active" @endif>
							<a href="{{ route('update_user_skills_view') }}"><i class="fa fa-circle-o"></i> Update User Skills</a>
						</li>
						<li @if (Request::url() == route('add_tlc_codes_view')) class="active" @endif>
							<a href="{{ route('add_tlc_codes_view') }}"><i class="fa fa-circle-o"></i> Upload TLC codes</a>
						</li>
						<li @if (Request::url() == route('add_tlc_code_skills_view')) class="active" @endif>
							<a href="{{ route('add_tlc_code_skills_view') }}"><i class="fa fa-circle-o"></i> Add TLC Code Skills</a>
						</li>
						<li @if (Request::url() == route('add_tlc_job_ints')) class="active" @endif>
							<a href="{{ route('add_tlc_job_ints') }}"><i class="fa fa-circle-o"></i> Add Trainings for Job Ints</a>
						</li>
						<li @if (Request::url() == route('add_employee_tlc_codes_view')) class="active" @endif>
							<a href="{{ route('add_employee_tlc_codes_view') }}"><i class="fa fa-circle-o"></i> Upload Employee TLC Details</a>
						</li>
					</ul>
					{{-- <a href="{{ route('venue_skills_index') }}">
						<i class="fa fa-address-book" aria-hidden="true"></i> <span>Internal Promotions</span>
					</a> --}}
				</li>
			@endif
			<li class="header" style="text-transform: uppercase;">{{ Auth::user()->roles()->first()->getName() }} PAGES</li>
			@if (!$pages->isEmpty())
				
				@foreach ($parent_pages as $parent_page)

					@php
						$route_name = $parent_page->getRouteName();
						if (!Route::has($route_name))
						{
							$route_name = 'home';
						}

						$is_have_child = false;
						$child_pages_data = array();
						if (!empty($child_pages)) {
							foreach ($child_pages as $child_page) 
							{
								if ($parent_page->getId() == $child_page->getParentId()) {
									$is_have_child = true;
									$child_pages_data[] = $child_page;
								}
							}
						}

						if ($parent_page->getRouteName() == 'none')
						{
							$is_have_child = true;
						}
					@endphp
						
					@if ($is_have_child)

						<li class="treeview">
							<a href="#">
								<i class="fa {{ $parent_page->getIconClass() }}"></i>
								<span>{{ $parent_page->getName() }}</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								@if (!empty($child_pages_data))

									@foreach ($child_pages_data as $child_page_data)

										@php
											$child_route_name = $child_page_data->getRouteName();
											if (!Route::has($child_route_name))
											{
												$child_route_name = 'home';
											}
										@endphp

										<li @if (Request::url() == route($child_route_name)) class="active" @endif>
											<a href="{{ route($child_route_name) }}">
												<i class="{{ $child_page_data->getIconClass() }}"></i> 
												{{ $child_page_data->getName() }}
											</a>
										</li>

									@endforeach

								@endif
							</ul>
						</li>

					@else

						<li class="@if (Request::url() == route($route_name)) active @endif">
							<a href="{{ route($route_name) }}">
								<i class="{{ $parent_page->getIconClass() }}" aria-hidden="true"></i> 
								<span>{{ $parent_page->getName() }}</span>
							</a>
						</li>

					@endif

				@endforeach

			@endif
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>