<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Crew Details</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Employee ID</th>
					<th>E-mail Address</th>
					<th>Crew Full Name</th>
					<th>Date of Birth</th>
					<th>Place of Birth(State|City)</th>
				</tr>
			</thead>
			<tbody id="allotee_details_container">
				@if (!is_null($personal_details))

					<tr>
						<td>{{ $user_details->getJDEId() }}</td>
						<td>{{ $user_details->getEmail() }}</td>
						<td>{{ strtoupper($personal_details->getFullName()) }}</td>
						<td>{{ $personal_details->getBirthdate() }}</td>
						<td>{{ $personal_details->getBirthdateState() . ' | ' . $personal_details->getBirthdateCity() }}</td>
					</tr>
			
				@else

					<tr>
						<td colspan="9" style="text-align: center;">Empty!</td>
					</tr>

				@endif
				
			</tbody>
		</table>
	</div>
</div>