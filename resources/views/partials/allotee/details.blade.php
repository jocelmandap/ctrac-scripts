<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Allotee Details</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Last Name</th>
					<th>Bank</th>
					<th>Account No.</th>
					<th>Percentage</th>
					<th>Relationship</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody id="allotee_details_container">
				@if (!is_null($allotee_details) && !$allotee_details->isEmpty())

					@foreach ($allotee_details as $row)

						<tr>
							<td>{{ $row->getId() }}</td>
							<td>{{ $row->getFirstName() }}</td>
							<td>{{ $row->getMiddleName() }}</td>
							<td>{{ $row->getLastName() }}</td>
							<td>{{ $row->bank->getName() }}</td>
							<td>{{ $row->getAccountNo() }}</td>
							<td>{{ $row->getPercentage() . '%' }}</td>
							<td>{{ (!is_null($row->relationship)) ? $row->relationship->getName() : 'No Data' }}</td>
							<td>
								<button
									data-toggle="tooltip"
									title="View More Details" 
									class="view-allotee btn btn-info btn-xs" 
									data-id="{{ $row->getId() }}" 
									data-action="view"><i class="fa fa-eye"></i></button>
								<button
									data-toggle="tooltip"
									title="Edit"
									class="edit-allotee btn btn-warning btn-xs" 
									data-id="{{ $row->getId() }}" 
									data-percentage="{{ $row->getPercentage() }}" 
									data-action="edit"><i class="fa fa-pencil"></i></button>
								<button
									data-toggle="tooltip"
									title="Delete"  
									class="delete-allotee btn btn-danger btn-xs" 
									data-id="{{ $row->getId() }}"
									data-name="{{ $row->getFullName() }}"
									data-action="delete"><i class="fa fa-trash"></i></button>
							</td>
						</tr>

					@endforeach
			
				@else

					<tr>
						<td colspan="9" style="text-align: center;">Empty!</td>
					</tr>

				@endif
				
				@if ($total_allotee_percentage < 100)
					<tr>
						<td colspan="8" style="text-align: center;">&nbsp;</td>
						<td>
							<button
								id="add_new_allotee" 
								class="btn btn-success btn-xs" 
								data-id="{{ $user_id }}" 
								data-action="view"><i class="fa fa-plus"></i> Add New Allotee</button>
						</td>
					</tr>
				@endif
				
			</tbody>
		</table>
	</div>
</div>