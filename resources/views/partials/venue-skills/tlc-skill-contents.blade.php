@if (!$training_course_skills->isEmpty())
	
	@foreach ($training_course_skills as $training_course_skill)
		
		@php
			$skill = $training_course_skill->skill()->first();
			$level = $training_course_skill->level()->first();
		@endphp

		@if (!is_null($skill) && !is_null($level))
			
			<tr id="training_skill_set_{{ $training_course_skill->getId() }}">
				<td>{{ $skill->getName() }}</td>
				<td>{{ $level->getName() }}</td>
				<td>
					<button 
						class="update-tlc-skills btn btn-xs btn-info" 
						data-id="{{ $training_course_skill->getId() }}"
						data-skill="{{ $skill->getId() }}"
						data-level="{{ $level->getId() }}"
						title="Update"
						data-toggle="tooltip" 
						><i class="fa fa-pencil" aria-hidden="true"></i></button>
					<button 
						class="delete-tlc-skills btn btn-xs btn-danger" 
						data-id="{{ $training_course_skill->getId() }}"
						data-skill="{{ $skill->getId() }}"
						data-level="{{ $level->getId() }}"
						data-toggle="tooltip"
						title="Delete" 
						><i class="fa fa-trash" aria-hidden="true"></i></button>
				</td>
			</tr>

		@endif

	@endforeach

@else
	<tr class="no-content-tlc-skills">
		<td colspan="3">No Skills Available</td>
	</tr>
@endif


