<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">User List Results</h3>
	</div>
	<div class="box-body">
		<table id="total-user-list-table" class="table table-striped table-hover">
			<thead>
				<tr>
					<th>User ID</th>
					<th>Email</th>
					<th>JDE ID</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>User ID</th>
					<th>Email</th>
					<th>JDE ID</th>
				</tr>
			</tfoot>
			<tbody>
				@if (!empty($user_details))
					
					@foreach ($user_details as $row)

						<tr>
							<td>{{ $row->getId() }}</td>
							<td>{{ $row->getEmail() }}</td>
							<td>{{ $row->getJDEId() }}</td>
						</tr>

					@endforeach					

				@endif
			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function () {
		$('#total-user-list-table').DataTable();
	});
</script>