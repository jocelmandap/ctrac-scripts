<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Total Results of Users</h3>
	</div>
	<div class="box-body">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>{{ $results->count() }}</h3>
				<input type="hidden" id="user_ids" value="{{ $user_ids }}">
				<p>Update User Skills</p>
			</div>
			<div class="icon">
				<i class="fa fa-users" aria-hidden="true"></i>
			</div>
		</div>
	</div>
</div>