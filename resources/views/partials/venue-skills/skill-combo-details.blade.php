<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Skill Combos for {{ $skill_combos->getComboCode() }} - ID#{{ $skill_combos->getId() }}</h3>
	</div>
	<div class="box-body" style="max-height: 540px; overflow: auto;">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Skill Set ID</th>
					<th>Skill Name</th>
					<th>Skill Level</th>
					<th>Is Required</th>
				</tr>
			</thead>
			<tbody class="skill-combo-details-table">
				@if (!$skill_combo_details->isEmpty())
					
					@foreach ($skill_combo_details as $row)
						
						@php
							$skill = $row->skillDetail()->first();
							$level = $row->skillLevel()->first();
						@endphp

						<tr @if ($row->getIsRequired() > 0) class="is-required" @endif>
							<td>{{ $row->getId() }}</td>
							<td>{{ $row->getSkillSetId() }}</td>
							<td>{{ $skill->getName() }}</td>
							<td>{{ $level->getName() }}</td>
							<td>
								
								@if ($row->getIsRequired() == 1)
									Yes - Interview
								@elseif ($row->getIsRequired() == 2)
									Yes
								@else
									No
								@endif

							</td>
						</tr>

					@endforeach					

				@endif
			</tbody>
		</table>
	</div>
</div>