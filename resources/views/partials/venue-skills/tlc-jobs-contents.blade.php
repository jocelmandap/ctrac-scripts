@if (!$training_course_jobs->isEmpty())
	
	@foreach ($training_course_jobs as $training_course_job)
		
		@php
			$training_course = $training_course_job->trainingCourse()->first();
		@endphp

		@if (!is_null($training_course))
			
			<tr id="training_course_job_{{ $training_course_job->getId() }}">
				<td>{{ $training_course->getTrainingCode() }} - {{ $training_course->getTrainingName() }}</td>
				<td>
					<button 
						class="update-tlc-jobs btn btn-xs btn-info" 
						data-id="{{ $training_course_job->getId() }}"
						data-course="{{ $training_course->getId() }}"
						title="Update"
						data-toggle="tooltip" 
						><i class="fa fa-pencil" aria-hidden="true"></i></button>
					<button 
						class="delete-tlc-jobs btn btn-xs btn-danger" 
						data-id="{{ $training_course_job->getId() }}"
						data-course="{{ $training_course->getId() }}"
						data-toggle="tooltip"
						title="Delete" 
						><i class="fa fa-trash" aria-hidden="true"></i></button>
				</td>
			</tr>

		@endif

	@endforeach

@else
	<tr class="no-content-tlc-jobs">
		<td colspan="3">No TLC Available</td>
	</tr>
@endif