<div class="panel panel-default">
    <div class="panel-body">
    	<h4>User Details</h4>
    	<table class="table table-striped">
    		<thead>
    			<tr>
    				<th>User ID</th>
    				<th>Country Code</th>
    				<th>Email</th>
    				<th>JDE ID</th>
    			</tr>
    		</thead>
    		<tbody id="form_candidate_data_container">
    			<tr class="success">
    				<td>{{ $user_details->getId() }}</td>
    				<td>{{ $user_details->country_code }}</td>
    				<td>{{ $user_details->getEmail() }}</td>
    				<td>{{ $user_details->getJDEId() }}</td>
    			</tr>
    		</tbody>
    	</table>
    </div>
</div>