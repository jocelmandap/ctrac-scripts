<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Document Checklist Uploads</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Ck List ID</th>
					<th>File Name</th>
					<th>Status</th>
					<th>Date Created</th>
				</tr>
			</thead>
			<tbody id="form_candidate_data_container">
				@if (!is_null($document_checlist_upload_data) && !$document_checlist_upload_data->isEmpty())
					
					@foreach ($document_checlist_upload_data as $row)
						
						@if ($row->getCkListId() == 124 || $row->getCkListId() == 125 || $row->getCkListId() == 109)

							<tr @if ($row->getCkListId() == 124 || $row->getCkListId() == 125) class="highlight-table-row" @endif>
								<td>{{ $row->getId() }}</td>
								<td>@if (!is_null($row->ckList)) {{ $row->ckList->getName() }} @else {{ $row->getCkListId() }} @endif</td>
								<td>{{ $row->getFilename() }}</td>
								<td>{{ $row->getStatus() }}</td>
								<td>{{ CTRAC\Helpers\DateUtility::changeDateFormat($row->getCreatedOn(), 'Y-m-d H:i:s', 'F d, Y g:i A') }}</td>
							</tr>

						@endif

					@endforeach					

				@endif
			</tbody>
		</table>
	</div>
</div>