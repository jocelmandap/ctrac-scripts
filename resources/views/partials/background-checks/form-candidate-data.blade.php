<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Form Candidate Data</h3>
		@if ($form_candidate_data->getHirerightSubmitInd() == 1 && 
			$form_candidate_data->getHirerightSubmitAttemptInd() == 0 && 
			$form_candidate_data->getHirerightDataConstructedInd() == 0 && 
			$form_candidate_data->getHirerightExecutedInd() == 0
		)
			<button class="btn btn-warning manual-trigger-bgc btn-xs pull-right" data-id="{{ $form_candidate_data->getId() }}">Manual Trigger</button>
		@endif
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>User ID</th>
					<th>Full Name</th>
					<th>Hireright Submit Ind</th>
					<th>Hireright Submit Attempt Ind</th>
					<th>Hireright Data Constructed Ind</th>
					<th>Hireright Submit Executed Ind</th>
				</tr>
			</thead>
			<tbody id="form_candidate_data_container">
				@if (!is_null($form_candidate_data))

					@php
						$f_name = (!is_null($form_candidate_data->CDF_firstName) && !empty($form_candidate_data->CDF_firstName)) ? $form_candidate_data->CDF_firstName : '';
						$m_name = (!is_null($form_candidate_data->CDF_middleName) && !empty($form_candidate_data->CDF_middleName)) ? substr($form_candidate_data->CDF_middleName, 0, 1) : 'None';
						$l_name = (!is_null($form_candidate_data->CDF_lastName) && !empty($form_candidate_data->CDF_lastName)) ? $form_candidate_data->CDF_lastName : '';
						$full_name = $f_name . ' ' . $m_name . ' ' . $l_name;
					@endphp

					<tr>
						<td>{{ $form_candidate_data->getId() }}</td>
						<td>{{ $form_candidate_data->getUserId() }}</td>
						<td>{{ $full_name }}</td>
						<td>{{ $form_candidate_data->getHirerightSubmitInd() }}</td>
						<td>{{ $form_candidate_data->getHirerightSubmitAttemptInd() }}</td>
						<td>{{ $form_candidate_data->getHirerightDataConstructedInd() }}</td>
						<td>{{ $form_candidate_data->getHirerightExecutedInd() }}</td>
					</tr>

				@endif
			</tbody>
		</table>
	</div>
</div>