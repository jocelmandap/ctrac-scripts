<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Highright Crew Requests</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Request ID</th>
					<th>Package ID</th>
					<th>Service Name</th>
					<th>Request Status</th>
				</tr>
			</thead>
			<tbody id="form_candidate_data_container">
				@if (!is_null($hireright_crew_request_data) && !$hireright_crew_request_data->isEmpty())
					
					@foreach ($hireright_crew_request_data as $row)
						
						<tr>
							<td>{{ $row->getId() }}</td>
							<td>{{ $row->getRequestId() }}</td>
							<td>{{ $row->getPackageId() }}</td>
							<td>{{ $row->getServiceName() }}</td>
							<td>{{ $row->getRequestStatus() }}</td>
						</tr>

					@endforeach					
				@else

					<tr>
						<td class="empty-highright-crew-request" colspan="5" style="text-align: center;">Empty!</td>
					</tr>

				@endif
			</tbody>
		</table>
	</div>
</div>