<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Job Int ID</th>
			<th>Job Code</th>
			<th>Package ID</th>
			<th>Package Name</th>
			<th>Is US</th>
			<th>Is Active</th>
		</tr>
	</thead>
	<tbody id="form_candidate_data_container">
		@if (!is_null($job_int_packages) && !$job_int_packages->isEmpty())
			
			@foreach ($job_int_packages as $row)
				
				<tr>
					<td>{{ $row->getId() }}</td>
					<td>{{ $row->getJobintId() }}</td>
					<td>@if (!is_null($row->jobInt)) {{ $row->jobInt->job_int_code }} @else None @endif</td>
					<td>{{ $row->getPackageId() }}</td>
					<td>
						@php
							$package_name = 'None';
						@endphp

						@if (!$hireright_packages->isEmpty())
							
							@foreach ($hireright_packages as $package)
								
								@if ($row->getPackageId() == $package->getId())
									
									@php
										$package_name = $package->getPackageName();
									@endphp

								@endif

							@endforeach

						@endif
						{{ $package_name }}
					</td>
					<td>
						@if ($row->getIsUS())
							<label class="label label-success">Yes</label>
						@else
							<label class="label label-danger">No</label>
						@endif
					</td>
					<td>
						@if ($row->getIsActive())
							<label class="label label-success">Yes</label>
						@else
							<label class="label label-danger">No</label>
						@endif
					</td>
				</tr>

			@endforeach

		@else

			<tr>
				<td colspan="6" style="text-align: center;">Job Int Package is Empty!</td>
			</tr>					

		@endif
	</tbody>
</table>