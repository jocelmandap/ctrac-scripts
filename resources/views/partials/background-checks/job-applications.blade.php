<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Job Applications</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Job Request ID</th>
					<th>Application Step</th>
					<th>Status Code</th>
					<th>Job Code</th>
					<th>Hireright Job Packages</th>
				</tr>
			</thead>
			<tbody id="form_candidate_data_container">
				@if (!is_null($job_application_data) && !$job_application_data->isEmpty())
					
					@foreach ($job_application_data as $row)
						
						@if (!$hireright_statuses->isEmpty())
							
							@php
								$is_allowed = false;
							@endphp

							@foreach ($hireright_statuses as $status)

								@if ($status->getStatusCodeId() == $row->getStatusCodeId())
									
									@php
										$is_allowed = true;
									@endphp

								@endif

							@endforeach

						@endif

						<tr>
							<td>{{ $row->getId() }}</td>
							<td>{{ $row->getJobRequestId() }}</td>
							<td>{{ $row->getApplicationStep() }}</td>
							<td>
								@if ($is_allowed)
									<label class="label label-success background-check-job-app-status" title="@if (!is_null($row->status)) {{ $row->status->getName() }} @endif" data-toggle="tooltip">{{ $row->getStatusCodeId() }}</label>
								@else
									<label class="label label-danger background-check-job-app-status" title="@if (!is_null($row->status)) {{ $row->status->getName() }} @endif" data-toggle="tooltip">{{ $row->getStatusCodeId() }}</label>
								@endif
							</td>
							<td style="text-align: center;">
								<strong>@if (!is_null($row->jobInt)) {{ $row->jobInt->getCode() }} @else None @endif<strong>
							</td>
							<td>
								@if ($row->getJobIntId())
									<button class="btn btn-info get-hireright-job-package btn-xs" data-jobint="{{ $row->getJobIntId() }}">View</button>
								@else
									<button class="btn btn-danger btn-xs" disabled="true">None</button>
								@endif
							</td>
						</tr>

					@endforeach					
				@else

					<tr>
						<td colspan="">Empty!</td>
					</tr>

				@endif
			</tbody>
		</table>
	</div>
</div>