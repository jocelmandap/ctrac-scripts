<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Document Checklist Form Uploads</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Document Name</th>
					<th>Is Completed</th>
					<th>Status Name</th>
					<th>Date Created</th>
				</tr>
			</thead>
			<tbody id="form_candidate_data_container">
				@if (!is_null($document_checlist_form_upload_data) && !$document_checlist_form_upload_data->isEmpty())
					
					@foreach ($document_checlist_form_upload_data as $row)
						
						@if ($row->getCkListId() == 124 || $row->getCkListId() == 125 || $row->getCkListId() == 109)

							<tr @if ($row->getCkListId() == 124 || $row->getCkListId() == 125) class="highlight-table-row" @endif>
								<td>{{ $row->getId() }}</td>
								<td>@if (!is_null($row->ckList)) {{ $row->ckList->getName() }} @else Unknown Document @endif</td>
								<td>{{ $row->getIsCompleted() }}</td>
								<td>
									@php
										$status_name = 'None';
									@endphp
									@if (!$document_form_upload_statuses->isEmpty())
										
										@foreach ($document_form_upload_statuses as $status)

											@if ($status->getId() == $row->getIsCompleted())

												@php
													$status_name = $status->getDocStatusName();
													break;
												@endphp

											@endif

										@endforeach

									@endif
									{{ $status_name }}
								</td>
								<td>{{ CTRAC\Helpers\DateUtility::changeDateFormat($row->getDateCreated(), 'Y-m-d H:i:s', 'F d, Y g:i A') }}</td>
							</tr>

						@endif
						
					@endforeach					

				@endif
			</tbody>
		</table>
	</div>
</div>