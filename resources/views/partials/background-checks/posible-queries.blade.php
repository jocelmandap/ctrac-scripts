<div class="box box-warning">
	<div class="box-header with-border">
		<h3 class="box-title">Action List</h3>
	</div>
	<div class="box-body">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				
			@if (!$hireright_crew_request_data->isEmpty())
				<div class="alert alert-success">
					<strong>Success!</strong> Background check is already Triggered!
				</div>
			@else
				<div class="alert alert-danger">
					Background check not yet Triggered!
				</div>
			@endif

			@if ($form_candidate_data->getHirerightSubmitInd() == 1 && 
				$form_candidate_data->getHirerightSubmitAttemptInd() == 1 && 
				$form_candidate_data->getHirerightDataConstructedInd() == 1 && 
				$form_candidate_data->getHirerightExecutedInd() == 1
			)
				<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingOne">
				        <h4 class="panel-title">
				            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				                <i class="more-less glyphicon glyphicon-plus"></i>
				                Re-Trigger Background Check
				            </a>
				        </h4>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				        <div class="panel-body">
				            <p><i><b><i class="fa fa-info-circle"></i> How it works?</b> If the Document Checklist Form Uploads table and Document Checklist Uploads table has a higlighted green row, and also if the job application has a hireright job package</i></p>
				            <div style="text-align: center;">
				            	<button id="submit_update_form_candidate_data" class="btn btn-warning" data-user="{{ $user_id }}">Re-Trigger Background Check</button>
				            </div>
				        </div>
				    </div>
				</div>
			@endif
			
			@php
				// CK List ID 124 or 125
				$is_ck_list_id_available = false;
				$document_checklist_form_upload_id = 0;
			@endphp
			
			@if (!is_null($document_checlist_form_upload_data) && !$document_checlist_form_upload_data->isEmpty())

				@foreach ($document_checlist_form_upload_data as $row)
					
					@if ($row->getCkListId() == 124 || $row->getCkListId() == 125)
						
						@php
							$is_ck_list_id_available = true;
							$document_checklist_form_upload_id = $row->getId();
						@endphp

					@endif

				@endforeach					

			@endif				
			@if (!$is_ck_list_id_available)
				<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingTwo">
				        <h4 class="panel-title">
				            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
				                <i class="more-less glyphicon glyphicon-plus"></i>
				                Add Document Checklist Form Uploads
				            </a>
				        </h4>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				        <div class="panel-body">
				        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
				        	<code style="display: none;">
				        		INSERT INTO document_checklist_form_uploads SET user_id = {{ $user_id }}, ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
				        	</code>
				        	<div style="text-align: center;">
				        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="{{ $user_id }}">Add Entry</button>
				        	</div>
				        </div>
				    </div>
				</div>
			@endif
			
			@php
				// CK List ID 124 or 125
				// created_on > 2016-03-07
				$is_ck_list_id_available = false;
				$is_created_on_valid = false;
				$document_checklist_upload_id = 0;
			@endphp

			@if (!is_null($document_checlist_upload_data) && !$document_checlist_upload_data->isEmpty())

				@foreach ($document_checlist_upload_data as $row)
					
					@if ($row->getCkListId() == 124 || $row->getCkListId() == 125)
						
						@php
							$is_ck_list_id_available = true;
						@endphp
						
						@if (strtotime($row->getCreatedOn()) > strtotime('2016-03-07'))
							
							@php
								$is_created_on_valid = true;
								$document_checklist_upload_id = $row->getId();
							@endphp

						@endif

					@endif

				@endforeach					

			@endif
			
			@if (!$is_ck_list_id_available || !$is_created_on_valid)

				<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingThree">
				        <h4 class="panel-title">
				            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
				                <i class="more-less glyphicon glyphicon-plus"></i>
				                Add Document Checklist Uploads
				            </a>
				        </h4>
				    </div>
				    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				        <div class="panel-body">
				        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Uploads.</i></p>
				        	<code style="display: none;">
				        		INSERT INTO document_checklist_uploads SET user_id = {{ $user_id }}, ck_list_id = 124, filename = 'hireright_consent', file_path = NULL, status = 'submitted', comment = NULL, last_updated_by = 999999999999, last_updated_on = NOW(), created_on = NOW(), issued_date = CURDATE(), expiration_date = CURDATE(), medical_facility_id = 0, training_type_id = 0, locked_ind = 0, newly_added = 0, newly_updated = 0, e1_migration = 0, is_primary = 0, triton_ind = 0, document_status_id = 1;
				        	</code>
				        	<div style="text-align: center;">
				        		<button id="submit_query_insert_uploads" class="btn btn-warning btn-large" data-user="{{ $user_id }}">Add Entry</button>
				        	</div>
				        	<br>
				        	@if ($document_checklist_upload_id)
				        		<label style="display: block;">Update Created On Query</label>
				        		<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will update the created on date time column in the green highlighted row.</i></p>
				        		<code>
				        			UPDATE document_checklist_uploads SET created_on = NOW() WHERE id = {{ $document_checklist_upload_id }};
				        		</code>
				        		<div style="text-align: center;">
				        			<button id="submit_query_update_created_on" class="btn btn-warning" data-id="{{ $document_checklist_upload_id }}">Update Created On</button>
				        		</div>
				        	@endif
				        </div>
				    </div>
				</div>

			@endif
			
			@if ($is_ck_list_id_available)
				<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="headingTwo">
				        <h4 class="panel-title">
				            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
				                <i class="more-less glyphicon glyphicon-plus"></i>
				                Set Background Check to Completed
				            </a>
				        </h4>
				    </div>
				    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				        <div class="panel-body">
				        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will only work if Document Checklist Form Uploads has a higlighted green row. This will set Background Check status to completed.</i></p>
				        	<div class="row">
				        	    <div class='col-sm-9'>
				        	        <div class="form-group">
				        	            <div class='input-group date' id='modified-date'>
				        	                <input type='text' class="form-control modified-date-completed" />
				        	                <span class="input-group-addon">
				        	                    <span class="glyphicon glyphicon-calendar"></span>
				        	                </span>
				        	            </div>
				        	        </div>
				        	    </div>
				        	    <div class='col-sm-3'>
				        	    	<button id="submit_query_completed" class="btn btn-warning" data-id="{{ $document_checklist_form_upload_id }}" data-completed="7">Submit</button>
				        	    </div>
				        	    <script type="text/javascript">
				        	        $(function () {
				        	            $('#modified-date').datetimepicker({
				        	            	format: 'MMMM D, YYYY LT',
				        	            	useCurrent:true,
				        	            	defaultDate: new Date()
				        	            });
				        	        });
				        	    </script>
				        	</div>
				        </div>
				    </div>
				</div>
			@endif

			<div class="cron-job-bg-check-container">
				<p>
					{{-- https://rclctrac.com/requirements/hireright_send_request_cron --}}
					<a href="#" id="run_cron" target="_blank" rel="noopener noreferrer" class="btn btn-large btn-info">RUN CRON JOBS HERE</a>
				</p>
			</div>

		</div><!-- panel-group -->
	</div>
</div>