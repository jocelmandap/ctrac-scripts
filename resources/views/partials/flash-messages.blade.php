@if (Session::has('success_flash'))
	<div class="alert-handler-class callout @if (Session::get('success_flash')) callout-success @else callout-danger @endif">
	    @if (Session::get('success_flash')) 
	    	<h4>Success!</h4> 
	    @else 
			<h4>Error!</h4> 
	    @endif

	    {{ Session::get('message_flash') }}
	</div>
@endif