<table id="document-details-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
	
	<thead>
		<tr>
			<th>Field Name</th>
			<th>Value</th>
		</tr>
	</thead>

	<tbody>
		
		<!-- STCW Table -->
		@if ($results->getCkListId() == 119)

			<tr>
				<td>Training Type: </td>
				<td>
					@if (!is_null($results->training))
						{{ $results->training->getName() }}
					@else
						{{ $results->getTrainingTypeId() }}
					@endif
				</td>
			</tr>
			<tr>
				<td>Training Facility: </td>
				<td>{{ $results->getTrainingFacility() }}</td>
			</tr>
			<tr>
				<td>Country of Issue: </td>
				<td>{{ $results->getIssuedCountry() }}</td>
			</tr>
			<tr>
				<td>Certificate Number: </td>
				<td>{{ $results->getTrainingCode() }}</td>
			</tr>
			<tr>
				<td>Description: </td>
				<td>{{ $results->getTrainingDesc() }}</td>
			</tr>
			<tr>
				<td>Trainer / Maritime Training Center: </td>
				<td>{{ $results->getTrainingTrainerName() }}</td>
			</tr>
			<tr>
				<td>Issue Date: </td>
				<td>{{ $results->getIssueDate(false) }}</td>
			</tr>
			<tr>
				<td>Expiry Date: </td>
				<td>{{ $results->getExpirationDate(false) }}</td>
			</tr>
			<tr>
				<td>File Name: </td>
				<td>{{ $results->getFileName() }}</td>
			</tr>

		@else

			<tr>
				<td>Field Empty</td>
				<td>N / A</td>
			</tr>

		@endif

	</tbody>

</table>

<div class="document-details-file-container">
	@if (!empty($file))

		@if ($file_type == 'image')
				
			<img src="{{ $file }}" id="document-preview-img" />

		@else
			
			<iframe src="{{ $file }}" id="document-preview-file"></iframe>

		@endif

	@endif
</div>