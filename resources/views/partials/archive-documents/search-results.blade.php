@if (!$results->isEmpty())

	@foreach ($results as $result)
		@php
			$file_path = $result->getFilePath();
			$doc_ext = '';
			if (!is_null($file_path))
			{
				$doc_ext = strtolower(substr(strrchr($file_path, '.'), 1));
			}
		@endphp
								
		<tr id="document_row_{{ $result->getId() }}">
			<td>
				{{ $result->getId() }}
			</td>
			<td>
				@if (!is_null($result->ckList))
					{{ $result->ckList->getName() }}
				@else
					Document ID# {{ $result->getCkListId() }}
				@endif
			</td>
			<td>
				@if (!is_null($result->docStatus))
					{{ $result->docStatus->getDocStatusName() }}
				@else
					No Status Indicated
				@endif
			</td>
			<td>
				{{ $result->getCreatedOn(false) }}
			</td>
			<td>
				{{ $result->getUpdatedOn(false) }}
			</td>
			<td>
				
				@if (!is_null($result->lastUserUpdate))
				
					@if ($result->lastUserUpdate->personalData)

						{{ $result->lastUserUpdate->personalData->getFullName() }}

					@endif

				@else
					
					N / A

				@endif
			</td>
			<td>
				<a href="{{ route('archive_document_download_document', ['file_path' => $file_path]) }}" target="_blank" class="button btn btn-xs btn-success">Download File</a>
				<button 
					class="button btn btn-xs btn-warning view-document" 
					data-id="{{ $result->getId() }}"
					data-path="{{ $file_path }}"
					data-ext="{{ $doc_ext }}"
					@if (!is_null($result->ckList))
						data-name="{{ $result->ckList->getName() }}"
					@else
						data-name="Document ID# {{ $result->getCkListId() }}"
					@endif
					>View</button>
			</td>
		</tr>

	@endforeach

@else

	<tr>
		<td colspan="7" class="no-archive-documents">
			No Archive Documents
		</td>
	</tr>

@endif