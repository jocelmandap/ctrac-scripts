<div class="box box-warning">
	<div class="box-header with-border">
		<h3 class="box-title">Applicant Information</h3>
	</div>
	<div class="box-body">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				
			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingOne">
			        <h4 class="panel-title">
			            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Personal Information
			            </a>
			        </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			        <div class="panel-body form-horizontal">
			        	<div class="form-group">
			        	    <label for="name" class="col-md-4 control-label">Employee ID</label>

			        	    <div class="col-md-6">
			        	        <input id="jde_id" type="number" class="form-control" name="jde_id" value="{{ $user_details->getJDEId() }}">
			        	    </div>
			        	</div>
			        	<div class="form-group">
			        	    <label for="name" class="col-md-4 control-label">First Name</label>

			        	    <div class="col-md-6">
			        	        <input id="first_name" type="text" class="form-control" name="name" value="{{ $personal_data->getFirstName() }}">
			        	    </div>
			        	</div>
			        	<div class="form-group">
			        	    <label for="name" class="col-md-4 control-label">Middle Name</label>

			        	    <div class="col-md-6">
			        	        <input id="middle_name" type="text" class="form-control" name="name" value="{{ $personal_data->getMiddleName() }}">
			        	    </div>
			        	</div>
			        	<div class="form-group">
			        	    <label for="name" class="col-md-4 control-label">Last Name</label>

			        	    <div class="col-md-6">
			        	        <input id="last_name" type="text" class="form-control" name="name" value="{{ $personal_data->getLastName() }}">
			        	    </div>
			        	</div>
			        	<div class="form-group">
			        	    <label for="is_active" class="col-md-4 control-label">Gender</label>

			        	    <div class="col-md-6">
			        	        <select name="is_active" id="is_active" class="form-control">
			        	        	@if ($personal_data->getGender() == 1)
			        	        		<option value="1">Male</option>
			        	        		<option value="0">Female</option>
			        	        	@else
			        	        		<option value="0">Female</option>
			        	        		<option value="1">Male</option>
			        	        	@endif
			        	        </select>
			        	    </div>
			        	</div>
			        </div>
			    </div>
			</div>

			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			        <h4 class="panel-title">
			            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Current Home Address
			            </a>
			        </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			        <div class="panel-body">
			        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
			        	<code style="display: none;">
			        		INSERT INTO document_checklist_form_uploads SET user_id = , ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
			        	</code>
			        	<div style="text-align: center;">
			        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="">Add Entry</button>
			        	</div>
			        </div>
			    </div>
			</div>

			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			        <h4 class="panel-title">
			            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Permanent Home Address
			            </a>
			        </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			        <div class="panel-body">
			        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
			        	<code style="display: none;">
			        		INSERT INTO document_checklist_form_uploads SET user_id = , ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
			        	</code>
			        	<div style="text-align: center;">
			        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="">Add Entry</button>
			        	</div>
			        </div>
			    </div>
			</div>

			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			        <h4 class="panel-title">
			            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseTwo">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Parent Information
			            </a>
			        </h4>
			    </div>
			    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			        <div class="panel-body">
			        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
			        	<code style="display: none;">
			        		INSERT INTO document_checklist_form_uploads SET user_id = , ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
			        	</code>
			        	<div style="text-align: center;">
			        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="">Add Entry</button>
			        	</div>
			        </div>
			    </div>
			</div>

			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			        <h4 class="panel-title">
			            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseTwo">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Emergency Contact
			            </a>
			        </h4>
			    </div>
			    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			        <div class="panel-body">
			        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
			        	<code style="display: none;">
			        		INSERT INTO document_checklist_form_uploads SET user_id = , ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
			        	</code>
			        	<div style="text-align: center;">
			        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="">Add Entry</button>
			        	</div>
			        </div>
			    </div>
			</div>

			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			        <h4 class="panel-title">
			            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseTwo">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Beneficiary Information
			            </a>
			        </h4>
			    </div>
			    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			        <div class="panel-body">
			        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
			        	<code style="display: none;">
			        		INSERT INTO document_checklist_form_uploads SET user_id = , ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
			        	</code>
			        	<div style="text-align: center;">
			        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="">Add Entry</button>
			        	</div>
			        </div>
			    </div>
			</div>

			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			        <h4 class="panel-title">
			            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseTwo">
			                <i class="more-less glyphicon glyphicon-plus"></i>
			                Contact Information
			            </a>
			        </h4>
			    </div>
			    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			        <div class="panel-body">
			        	<p><i><b><i class="fa fa-info-circle"></i> How it works?</b> This will add an entry to Document Checklist Form Uploads.</i></p>	
			        	<code style="display: none;">
			        		INSERT INTO document_checklist_form_uploads SET user_id = , ck_list_id = 124, is_completed = 1, document_child_status_id = 0, doc_prev_status = 0, date_created = NOW(), date_modified = NOW(), created_by = '', modified_by = '', job_application_id = 661361, reviewed_ind = 0, triton_ind = 0, locked_agreement_ind = 0;
			        	</code>
			        	<div style="text-align: center;">
			        		<button id="submit_query_insert_form_uploads" class="btn btn-warning" data-user="">Add Entry</button>
			        	</div>
			        </div>
			    </div>
			</div>

		</div><!-- panel-group -->
	</div>
</div>