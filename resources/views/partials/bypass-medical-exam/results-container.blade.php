<div class="box box-danger">
	<div class="box-header with-border">
		<h3 class="box-title">Results</h3>
	</div>
	<div class="box-body @if ($result_status) bg-green @else bg-red @endif">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Message</th>
				</tr>
			</thead>
			<tbody>
				@if (!empty($result_message))
					@php
						$count = 1;
					@endphp
					@foreach ($result_message as $key => $value)

						<tr>
							<td>{{ $count }}</td>
							<td>{{ $value }}</td>
						</tr>

						@php
							$count++;
						@endphp

					@endforeach

				@endif
			</tbody>
		</table>
	</div>
</div>