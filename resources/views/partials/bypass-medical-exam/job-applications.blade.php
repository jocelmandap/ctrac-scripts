<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Job Applications</h3>
	</div>
	<div class="box-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Job App ID</th>
					<th>User ID</th>
					<th>Name</th>
					<th>Nationality</th>
					<th>Country</th>
					<th>Status Code</th>
					<th>Job Title</th>
					<th>Status</th>
					<th>Exam</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="job-application-container">
				@if (!is_null($job_applications) && !$job_applications->isEmpty())
					
					@foreach ($job_applications as $row)
						

						<tr>
							<td>{{ $row->getId() }}</td>
							<td>{{ $row->getUserId() }}</td>
							<td>{{ $user_details->personalData->getFullName() }}</td>
							<td><label title="{{ $user_details->country->getCountryName() }}" data-toggle="tooltip">{{ $user_details->country->getCountryCode() }}</label></td>
							<td><label title="{{ $user_details->personalData->country->getCountryName() }}" data-toggle="tooltip">{{ $user_details->personalData->country->getCountryCode() }}</label></td>
							<td>
									<label class="label label-success job-applications" title="@if (!is_null($row->status)) {{ $row->status->getName() }} @endif" data-toggle="tooltip">{{ $row->getStatusCodeId() }}</label>
							</td>
							<td style="text-align: left;">
								<strong>@if (!is_null($row->JobRequest)) {{ $row->JobRequest->getTitle() }} @else None @endif<strong>
							</td>
							<td>
								<strong>@if(!is_null($row->status)) {{ $row->status->statusGroup->getName() }} @else Registered @endif<strong>
							</td>
							<td>
								@if(!is_null($row->medicalExamAnswer)) {{ $row->medicalExamAnswer->getByPassedExam() }} @else No Answer @endif
							</td>
							<td>
								<button class="btn btn-info bypassed-exam-answer btn-xs" data-id="{{ $row->getId() }}">Bypassed Exam</button>
							</td>
						</tr>

					@endforeach					
				@else

					<tr>
						<td colspan="9" style="text-align: center;">No Job Application under Medical Group!</td>
					</tr>

				@endif
			</tbody>
		</table>
	</div>
</div>