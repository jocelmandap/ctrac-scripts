<!-- Modal -->
<div class="modal fade" id="role-form-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title role-form-title">Add New Role</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal role-form" id="role-form" method="POST" action="{{ route('admin_role_save') }}">
                    
                    {{ csrf_field() }}
                    <input type="hidden" name="role_id" id="role_id">

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="desc" class="col-md-4 control-label">Description</label>

                        <div class="col-md-6">
                            <textarea name="desc" id="desc" style="width: 100%; height: 100px; resize: none;"></textarea>
                        </div>
                    </div>

                    <div class="form-group hide">
                        <label for="is_active" class="col-md-4 control-label">Status</label>

                        <div class="col-md-6">
                            <select name="is_active" id="is_active" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="submit_role_form">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>