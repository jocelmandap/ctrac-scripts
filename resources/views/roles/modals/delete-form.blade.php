<div class="modal modal-danger fade" id="view-delete-role-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Delete Role</h4>
			</div>
			<div class="modal-body delete-role-container">
				<input type="hidden" id="delete-role-id" value="">
				<input type="hidden" id="delete-role-name" value="">
				<p>Are you sure you want to delete <span class="delete-role-name"></span> role?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
				<button type="button" id="submit_delete_role" class="btn btn-outline">Yes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>