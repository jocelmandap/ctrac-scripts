@extends('layouts.master')

@section('title', 'Role List | Roles')

@section('head')

	<link rel="stylesheet" href="/bower_components/datatables/media/css/dataTables.bootstrap.min.css">

@endsection

@section('header-content')

	<h1 class="page-title-header">
		Role List
		<small>Roles</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Role List</li>
	</ol>

@endsection

@section('main-content')
	
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Role List</h3>
					<button class="btn btn-xs btn-success pull-right" id="add_role_page">Add New Role</button>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="role-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th>#</th>
				                <th>Role Name</th>
				                <th>Description</th>
				                <th>Created At</th>
				                <th>Updated At</th>
				                <th>Actions</th>
				            </tr>
						</thead>
						<tbody>
							@if (!$role_list->isEmpty())
								@foreach($role_list as $role)
									<tr>
										<td>{{ $role->getId() }}</td>
										<td>{{ $role->getName() }}</td>
										<td>{{ $role->getDescription() }}</td>
										<td>{{ $role->getCreatedAt(false) }}</td>
										<td>{{ $role->getUpdatedAt(false) }}</td>
										<td>
											<button 
												data-id="{{ $role->getId() }}"
												data-name="{{ $role->getName() }}"
												data-desc="{{ $role->getDescription() }}"
												class="btn btn-xs btn-info edit-role-form">Edit</button>
											<button 
												data-id="{{ $role->getId() }}"
												data-name="{{ $role->getName() }}"
												class="btn btn-xs btn-danger remove-role">Delete</button>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="6" style="text-align: center;">No Available Role</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->

@endsection

@section('modal-content')
	
	@include('roles.modals.form')
	@include('roles.modals.delete-form')

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			var RoleHandler = {
				init : function () {
					$('#role-table-container').DataTable();
					RoleHandler.addNewModal();
					RoleHandler.editModal();
					RoleHandler.submitForm();
					RoleHandler.deleteModal();
					RoleHandler.deleteRole();
				},
				clearFormData: function () {
					$('#role_id').val('');
					$('#name').val('');
					$('#desc').val('');
				},
				addNewModal : function () {
					$(document).on('click', '#add_role_page', function () {
						RoleHandler.clearFormData();
						$('.role-form-title').text('Add New Role');
						$('#role-form-modal').modal('show');
					});
				},
				editModal : function () {
					$(document).on('click', '.edit-role-form', function () {
						RoleHandler.clearFormData();
						var self = $(this);

						$('.role-form-title').text('Edit ' + self.data('name') + ' Role');
						$('#role_id').val(self.data('id'));
						$('#name').val(self.data('name'));
						$('#desc').val(self.data('desc'));
						$('#role-form-modal').modal('show');
					});
				},
				submitForm : function () {
					$('#submit_role_form').click(function () {
						$('#role-form').submit();
					});
				},
				deleteModal : function () {
					$(document).on('click', '.remove-role', function () {
						var self = $(this);
						var role_id = self.data('id');
						var name = self.data('name');

						$('.delete-role-name').text(name);
						$('#delete-role-id').val(role_id);
						$('#delete-role-name').val(name);
						$('#view-delete-role-modal').modal('show');
					});
				},
				deleteRole : function () {
					$(document).on('click', '#submit_delete_role', function () {
						var role_id = $('#delete-role-id').val();
						var name = $('#delete-role-name').val();
						$.ajax({
							url: '{{ route('admin_delete_role') }}',
							type: 'POST',
							dataType: 'json',
							data: {
								role_id: role_id,
								name: name
							},
							beforeSend: function () {
								var content = `
									<div class="loader-bg-container">
										<div class="dv-loader-bg">
										 	<i class="fa fa-cog fa-spin fa-3x"></i>
										 	<p>Updating...</p>
										</div>
									</div>
								`;
								$('.delete-role-container').html(content);
							},
							success: function (response) {
								if (response.status == true) {
									$('#view-delete-role-modal').modal('hide');
									setTimeout(function () {
										location.reload();
									}, 700);
								} else {
									alert(response.message);
								}
							}
						});
					});
				}
			};

			RoleHandler.init();
		});
	</script>
@endsection