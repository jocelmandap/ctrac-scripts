<!-- Modal -->
<div class="modal fade" id="edit-user-form-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Edit <span class="edit-user-form-title">Name</span> Details</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST" id="edit-user-form" action="{{ route('admin_users_save') }}">
                    {{ csrf_field() }}
                    <input type="hidden" id="edit_user_id" name="user_id" value="">
                    <input type="hidden" id="form-type" name="form_type" value="edit">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="edit_name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="edit_name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="edit_role_id" class="col-md-4 control-label">Role</label>

                        <div class="col-md-6">
                            <select name="role_id" id="edit_role_id" class="form-control">
                                @if (!$user_roles->isEmpty())
                                    @foreach ($user_roles as $user_role)
                                        <option value="{{ $user_role->getId() }}">{{ $user_role->getName() }}</option>
                                    @endforeach
                                @else
                                    <option value="0">None</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit_is_active" class="col-md-4 control-label">Status</label>

                        <div class="col-md-6">
                            <select name="is_active" id="edit_is_active" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group deactivate-container" style="display: none;">
                        <div class="col-md-4 control-label"></div>
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="deactivate_status" value="1"> Send De-activate Account Email?
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="edit_email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="edit_email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="submit_edit_user_form">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>