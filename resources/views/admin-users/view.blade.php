@extends('layouts.master')

@section('title', 'User List | Admin Users')

@section('head')

	<link rel="stylesheet" href="/bower_components/datatables/media/css/dataTables.bootstrap.min.css">

@endsection

@section('header-content')

	<h1 class="page-title-header">
		User List
		<small>Admin User List</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">User List</li>
	</ol>

@endsection

@section('main-content')
	
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">User List</h3>
					<button class="btn btn-xs btn-success pull-right" id="add_new_user">Add New User</button>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="user-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th>#</th>
				                <th>Name</th>
				                <th>Email</th>
				                <th>Status</th>
				                <th>Role</th>
				                <th>Created At</th>
				                <th>Updated At</th>
				                <th>Actions</th>
				            </tr>
						</thead>
						<tbody>
							@if (!$user_list->isEmpty())
								@foreach($user_list as $user)
									<tr>
										<td>{{ $user->getId() }}</td>
										<td>{{ $user->getName() }}</td>
										<td>{{ $user->getEmail() }}</td>
										<td>
											@if ($user->getIsActive())
												<label class="label label-success">Active</label>
											@else
												<label class="label label-danger">Inactive</label>
											@endif
										</td>
										<td>
											@if (!is_null($user->roles()->first()))
												{{ $user->roles()->first()->getName() }}
												@php
													$user_role_id = $user->roles()->first()->getId();
												@endphp
											@else
												Empty Role
												@php
													$user_role_id = 0;
												@endphp
											@endif
										</td>
										<td>{{ $user->getCreatedAt(false) }}</td>
										<td>{{ $user->getUpdatedAt(false) }}</td>
										<td>
											<button
												data-id = "{{ $user->getId() }}" 
												data-name = "{{ $user->getName() }}"
												class="btn btn-xs btn-warning change-password-user">Change Password</button>
											<button
												data-id = "{{ $user->getId() }}" 
												data-name = "{{ $user->getName() }}"
												data-email = "{{ $user->getEmail() }}"
												data-status = "{{ $user->getIsActive() }}"
												data-role = "{{ $user_role_id }}"
												class="btn btn-xs btn-info edit-user">Edit</button>
											<button
												data-id = "{{ $user->getId() }}" 
												data-name = "{{ $user->getName() }}" 
												class="btn btn-xs btn-danger remove-user">Delete</button>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="8" style="text-align: center;">No Available Users</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->

@endsection

@section('modal-content')
	
	@include('admin-users.modals.delete-user')
	@include('admin-users.modals.form')
	@include('admin-users.modals.edit-form')
	@include('admin-users.modals.change-form')

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			var UserHandler = {
				init : function () {
					$('#user-table-container').DataTable();
					UserHandler.deleteModal();
					UserHandler.deleteUser();
					UserHandler.addNewUserModal();
					UserHandler.submitForm();
					UserHandler.editUserModal();
					UserHandler.submitEditForm();
					UserHandler.deactivateCheckbox();
					UserHandler.changePasswordModal();
					UserHandler.submitChangePasswordForm();
				},
				isValid : function (user_search_by, user_detail) {
					if (user_search_by == 'email') {
						var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
						return pattern.test(user_detail);
					} else {
						return $.isNumeric(user_detail);
					}
				},
				deleteModal : function () {
					$(document).on('click', '.remove-user', function () {
						var self = $(this);
						var user_id = self.data('id');
						var name = self.data('name');

						$('.delete-user-name').text(name);
						$('#delete-user-id').val(user_id);
						$('#delete-user-name').val(name);
						$('#view-delete-user-modal').modal('show');
					});
				},
				deleteUser : function () {
					$(document).on('click', '#submit_delete_user', function () {
						var user_id = $('#delete-user-id').val();
						var name = $('#delete-user-name').val();
						$.ajax({
							url: '{{ route('admin_users_delete') }}',
							type: 'POST',
							dataType: 'json',
							data: {
								user_id: user_id,
								name: name
							},
							beforeSend: function () {
								var content = `
									<div class="loader-bg-container">
										<div class="dv-loader-bg">
										 	<i class="fa fa-cog fa-spin fa-3x"></i>
										 	<p>Updating...</p>
										</div>
									</div>
								`;
								$('.delete-user-container').html(content);
							},
							success: function (response) {
								if (response.status == true) {
									$('#view-delete-user-modal').modal('hide');
									setTimeout(function () {
										location.reload();
									}, 700);
								} else {
									alert(response.message);
								}
							}
						});
					});
				},
				addNewUserModal : function () {
					$(document).on('click', '#add_new_user', function () {
						// PageHandler.clearFormData();
						$('.user-form-title').text('Add New User');
						$('#user-form-modal').modal('show');
					});
				},
				editUserModal : function () {
					$(document).on('click', '.edit-user', function () {
						var self = $(this);

						$('#edit_user_id').val(self.data('id'));
						$('#edit_name').val(self.data('name'));
						$('#edit_role_id').val(self.data('role'));
						$('#edit_is_active').val(self.data('status'));
						$('#edit_email').val(self.data('email'));

						$('.edit-user-form-title').text(self.data('name'));
						$('#edit-user-form-modal').modal('show');
					});
				},
				deactivateCheckbox : function () {
					$(document).on('change', '#edit_is_active', function () {
						if ($(this).val() == 0) {
							$('.deactivate-container').slideDown('400');
						} else {
							$('.deactivate-container').slideUp('400');
						}
					});
				},
				changePasswordModal : function () {
					$(document).on('click', '.change-password-user', function () {
						var self = $(this);

						$('#change_user_id').val(self.data('id'));
						$('#change_name').val(self.data('name'));
						$('.change-user-form-title').text(self.data('name'));
						$('#change-user-form-modal').modal('show');
					});
				},
				submitForm : function () {
					$('#submit_user_form').click(function () {
						$('#user-form').submit();
					});
				},
				submitEditForm : function () {
					$('#submit_edit_user_form').click(function () {
						$('#edit-user-form').submit();
					});
				},
				submitChangePasswordForm : function () {
					$('#submit_change_user_form').click(function () {
						$('#change-user-form').submit();
					});
				}
			};

			UserHandler.init();
		});
	</script>
@endsection