@extends('layouts.master')

@section('title', 'HRIS Requisition Uploads')

@section('header-content')

	<h1>
		HRIS Requisition Uploads
		<small>Requisition Uploads</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Requisition Uploads</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('requisitions_upload_save_id_list') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">
						
						<div class="col-md-12">
							<div class="form-group">
								<label for="upload_gateway_csv">Upload a CSV File</label>
								<input type="file" id="upload_req_csv" name="upload_req_csv">

								<p class="help-block">Please download the sample format <a href="{{ asset('uploads/requisitions/req_sample.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
							</div>
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
		@if (Session::has('history_logs'))
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Results: </h3>
					</div>
					<div class="box-body">

						<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
							<thead>
					            <tr>
					            	<th>#</th>
					                <th>Job Opening</th>
					                <th>Job Code</th>
					            </tr>
							</thead>
							<tbody>
								@php
									$count = 1;
								@endphp
								@foreach (Session::get('history_logs') as $log)
									
									<tr>
										<td>{{ $count }}</td>
										<td>{{ $log['job_opening'] }}</td>
										<td>{{ $log['job_code'] }}</td>
									</tr>
									
									@php
										$count++;
									@endphp

								@endforeach	
							</tbody>
						</table>
					</div>	
				</div>
			</div>
		@endif
	</div>
	<!-- /.row -->

@endsection