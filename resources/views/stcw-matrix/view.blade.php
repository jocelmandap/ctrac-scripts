@extends('layouts.master')

@section('title', 'CTRAC STCW')

@section('header-content')

	<h1>
		CTRAC STCW
		<small>CTRAC stcw</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">CTRAC STCW</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row stcw-matrix-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('stcw_update') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
							<label for="update_by" class="col-md-4 control-label">Update By:</label>

							<div class="col-md-6">
								<select id="update_by" name="update_by" class="form-control">
									<option value="jobcode">Job Code</option>
									<option value="certificates">Certificates</option>
								</select>
							</div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
							<label for="upload_stcw_csv" class="col-md-4 control-label">Upload a CSV File</label>

							<div class="col-md-6">
								<input type="file" id="upload_stcw_csv" name="upload_stcw_csv">

								<p class="help-block">Please download the sample format <a href="{{ asset('uploads/stcw-matrix/stcw_certificate_job_code_sample.csv') }}" id="sample_dl" target="_blank" rel="noopener noreferrer">here</a></p>
							</div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
							<label class="col-md-4 control-label">&nbsp;</label>

							<div class="col-md-6">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>

		<!-- ./col -->

		@if (Session::get('update_by') == 'certificates')
			
			@if (Session::has('history_logs') )
				<div class="col-md-6">
					
					@if (Session::has('history_logs'))
						<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">Success Results: </h3>
								<a 
									href="{{ route('stcw_matrix_result_logs_download') }}" 
									target="_blank" 
									class="btn btn-info btn-xs pull-right">Download</a>
							</div>
							<div class="box-body" style="overflow-y: auto; max-height: 400px;">
								@if (!empty(Session::get('history_logs')))
									<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>#</th>
												<th>Certificate ID</th>
												<th>Certificate Code</th>
												<th>Certificate Name</th>
												<th>Certificate Show Indicator</th>
												<th>Update Type</th>
											</tr>
										</thead>
										<tbody>
											@php
												$count = 1;
											@endphp
											@foreach (Session::get('history_logs') as $log)
												
												<tr>
													<td>{{ $count }}</td>
													<td>{{ $log['cert_type_id'] }}</td>
													<td>{{ $log['cert_type_code'] }}</td>
													<td>{{ $log['cert_type_name'] }}</td>
													<td>{{ $log['show_ind'] }}</td>
													<td>{{ $log['stcw_status'] }}</td>
												</tr>
												
												@php
													$count++;
												@endphp

											@endforeach	
										</tbody>
									</table>
								@endif
							</div>	
						</div>
					@endif

				</div>
			@endif

		@endif

		@if (Session::get('update_by') == 'jobcode')
			
			@if (Session::has('history_logs') )
				<div class="col-md-6">
					
					@if (Session::has('history_logs'))
						<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">Success Insert Results: </h3>
								<a 
									href="{{ route('stcw_matrix_inserted_job_logs_download') }}" 
									target="_blank" 
									class="btn btn-info btn-xs pull-right">Download</a>
							</div>
							<div class="box-body" style="overflow-y: auto; max-height: 400px;">
								@if (!empty(Session::get('history_logs')))
									<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>#</th>
												<th>ID</th>
												<th>Job Int ID</th>
												<th>Certificate ID</th>
												<th>Job Code</th>
												<th>Certificate Code</th>
												<th>Update Type</th>
											</tr>
										</thead>
										<tbody>
											@php
												$count = 1;
											@endphp
											@foreach (Session::get('history_logs') as $log)
												
												<tr>
													<td>{{ $count }}</td>
													<td>{{ $log['id'] }}</td>
													<td>{{ $log['job_int_id'] }}</td>
													<td>{{ $log['cert_type_id'] }}</td>
													<td>{{ $log['job_code'] }}</td>
													<td>{{ $log['cert_type_code'] }}</td>
													<td>{{ $log['stcw_status'] }}</td>
												</tr>
												
												@php
													$count++;
												@endphp

											@endforeach	
										</tbody>
									</table>
								@else
									<h1>No updates</h1>
								@endif
							</div>	
						</div>
					@endif

				</div>
			@endif

			@if (Session::has('delete_logs') )
				<div class="col-md-6">
				</div>
				<div class="col-md-6">
					
					@if (Session::has('delete_logs'))
						<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">Success Delete Results: </h3>
								<a 
									href="{{ route('stcw_matrix_deleted_job_logs_download') }}" 
									target="_blank" 
									class="btn btn-info btn-xs pull-right">Download</a>
							</div>
							<div class="box-body" style="overflow-y: auto; max-height: 400px;">
								@if (!empty(Session::get('delete')))
									<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>#</th>
												<th>ID</th>
												<th>Job Int ID</th>
												<th>Certificate ID</th>
												<th>Job Code</th>
												<th>Certificate Code</th>
												<th>Update Type</th>
											</tr>
										</thead>
										<tbody>
											@php
												$count = 1;
											@endphp
											@foreach (Session::get('delete_logs') as $log)
												
												<tr>
													<td>{{ $count }}</td>
													<td>{{ $log['id'] }}</td>
													<td>{{ $log['job_int_id'] }}</td>
													<td>{{ $log['cert_type_id'] }}</td>
													<td>{{ $log['job_code'] }}</td>
													<td>{{ $log['cert_type_code'] }}</td>
													<td>{{ $log['stcw_status'] }}</td>
												</tr>
												
												@php
													$count++;
												@endphp

											@endforeach	
										</tbody>
									</table>
								@else
									<h1>No updates</h1>
								@endif
							</div>	
						</div>
					@endif

				</div>
			@endif

		@endif

		<!-- ./col -->
	</div>
	<!-- /.row -->

@endsection

@section('foot')

	<script type="text/javascript">
		$(document).ready(function () {
			var STCWHandler = {
				init : function () {
					STCWHandler.changeSampleDownload();
				},
				changeSampleDownload : function () {
					$(document).on('change', '#update_by', function() {
						if ($(this).val() == 'jobcode') {
							$('#sample_dl').prop('href', '{{ asset('uploads/stcw-matrix/stcw_certificate_job_code_sample.csv') }}');
						}
						else {
							$('#sample_dl').prop('href', '{{ asset('uploads/stcw-matrix/stcw_certificate_sample.csv') }}');
						}
					});
				}
			};

			STCWHandler.init();
		});
	</script>

@endsection