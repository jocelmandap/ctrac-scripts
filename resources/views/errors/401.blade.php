<html>
	<title>401 Unauthorize Access | C-TRAC App</title>
	<head>
		<style>
			body {
				margin: 0;
				padding: 0;
			}

			.body-wrapper {
				display: table;
				background: url(/images/401-royal-image.jpg);
			    background-position: center;
			    background-repeat: no-repeat;
			    background-size: cover;
			    position: fixed;
			    width: 100%;
			    height: 100%;
			}

			.body-content {
				display: table-row;
				width: 100%;
			}

			.main-content {
				display: table-cell;
				vertical-align: middle;
				text-align: center;
			}

			.error-message {
				color: #fff;
			    max-width: 600px;
			    margin: 0 auto;
			    background: #0000006e;
			    padding: 20px;
			    font-size: 1.5em;
			    border-radius: 10px;
			}

			.error-message a {
				text-decoration: none;
			    background: #2f9780;
			    color: #fff;
			    padding: 10px;
			    font-size: 16px;
			    border-radius: 6px;
			}
		</style>
	</head>
	<body>
		<div class="body-wrapper">
			<div class="body-content">
				<div class="main-content">
					<div class="error-message">
						<p>oh no, you don't have access to this page, please click the link below to go back to dashboard</p>
						<a href="{{ route('main_dashboard') }}">Dashboard</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>