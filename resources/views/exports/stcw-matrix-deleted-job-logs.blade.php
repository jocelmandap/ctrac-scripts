<table>
	<thead>
        <tr>
            <th>ID</th>
            <th>Job Int ID</th>
            <th>Job Code</th>
            <th>Certificate ID</th>
            <th>Certificate Code</th>
            <th>Update Type</th>
        </tr>
	</thead>
	<tbody>
		@if (!empty($delete_cert_logs))
			@foreach ($delete_cert_logs as $log)
				
				<tr>
					<td>{{ $log['id'] }}</td>
					<td>{{ $log['job_int_id'] }}</td>
					<td>{{ $log['cert_type_id'] }}</td>
					<td>{{ $log['created_by'] }}</td>
					<td>{{ $log['created_on'] }}</td>
					<td>{{ $log['stcw_status'] }}</td>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="5">No Data</td>
			</tr>
		@endif
	</tbody>
</table>