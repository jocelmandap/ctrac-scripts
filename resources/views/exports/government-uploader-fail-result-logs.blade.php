<table>
	<thead>
        <tr>
            <th>Employee ID</th>
            <th>Remarks</th>
        </tr>
	</thead>
	<tbody>
		@php
			$count = 1;
		@endphp
		@if (!empty($history_logs_not_updated))
			@foreach ($history_logs_not_updated as $log)
				
				<tr>
					<td>{{ $log['jde_id'] }}</td>
					<th>{{ $log['remarks'] }}</th>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="13">No Data</td>
			</tr>
		@endif
	</tbody>
</table>