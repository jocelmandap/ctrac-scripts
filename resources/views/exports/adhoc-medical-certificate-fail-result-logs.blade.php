<table>
	<thead>
        <tr>
            <th>User ID</th>
            <th>Document ID Not Updated</th>
            <th>Remarks</th>
        </tr>
	</thead>
	<tbody>
		@php
			$count = 1;
		@endphp
		@if (!empty($history_logs_not_updated))
			@foreach ($history_logs_not_updated as $log)
				
				<tr>
					<td>{{ $log['user_id'] }}</td>
                    <th>{{ $log['ck_list_id'] }}</th>
                    <th>{{ $log['remarks'] }}</th>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="3">No Data</td>
			</tr>
		@endif
	</tbody>
</table>