<table>
	<thead>
		<tr>
			<th>Employee ID</th>
			<th>Full Name</th>
			<th>Nationality</th>
			<th>Job Code</th>
			<th>Email</th>
			<th>Name of Documents in Pending Documents Approval and Completed Status</th>
			<th>Status of Documents (Pending Document Approval/ Completed)</th>
			<th>Modified By</th>
			<th>Date Uploaded/Completed</th>
			<th>Joining Date</th>
			<th>Joining Vessel</th>
			<th>Recruiter Name</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($user_document_data))
			@foreach ($user_document_data as $user_document_detail)
				
				@if (!$user_document_detail->isEmpty())
					@foreach ($user_document_detail as $details)
						@php
							$user_detail = $details->user()->first();
							$user_personal_data = null;
							$user_assignment_details = null;
							$ship_details = null;
							if (!is_null($user_detail))
							{
								$user_personal_data = $user_detail->personalData()->first();
								$user_assignment_details = $user_detail->assignment()->first();

								if (!is_null($user_assignment_details))
								{
									$ship_details = $user_assignment_details->ship()->first();
								}
							}

							$job_application = $details->jobApplications()->first();
							$job_int_detail = null;
							$history_record_detail = null;
							$admin_detail = null;
							$admin_personal_data = null;
							if (!is_null($job_application))
							{
								$job_int_detail = $job_application->jobInt()->first();
								$history_record_detail = $job_application->historyRecords()
																		 ->whereIn('status_code_id', [2420 , 3420, 15000])
																		 ->where('active', 1)
																		 ->first();
								if (!is_null($history_record_detail))
								{
									$admin_detail = $history_record_detail->admin()->first();

									if (!is_null($admin_detail))
									{
										$admin_personal_data = $admin_detail->personalData()->first();
									}
								}
							}

							$document_type = $details->ckList()->first();
							$modified_by = $details->modifiedBy()->first();
							$modified_by_detail = null;
							if (!is_null($modified_by))
							{
								$modified_by_detail = $modified_by->personalData()->first();
							}
						@endphp
						<tr>
							<td>{{ (!is_null($user_detail)) ? $user_detail->getJDEId() : 'No Data' }}</td>
							<td>{{ (isset($user_personal_data) && !is_null($user_personal_data)) ? $user_personal_data->getFullName() : 'No Data' }}</td>
							<td>{{ (!is_null($user_detail)) ? $user_detail->getCountryCode() : 'No Data' }}</td>
							<td>{{ (isset($job_int_detail) && !is_null($job_int_detail)) ? $job_int_detail->getCode() : 'No Data' }}</td>
							<td>{{ (!is_null($user_detail)) ? $user_detail->getEmail() : 'No Data' }}</td>
							<td>{{ (!is_null($document_type)) ? $document_type->getName() : 'No Data' }}</td>
							<td>
								@switch($details->getIsCompleted())
								    @case(1)
								        Pending Documents Approval
								        @break

								    @case(2)
								        Completed
								        @break

								    @default
								        No data
								@endswitch
							</td>
							<td>{{ (!is_null($modified_by_detail)) ? $modified_by_detail->getFullName() : 'No Data' }}</td>
							<td>{{ $details->getDateModified() }}</td>
							<td>{{ (!is_null($user_assignment_details)) ? $user_assignment_details->getAssignmentDate() : 'No Data' }}</td>
							<td>{{ (!is_null($ship_details)) ? $ship_details->getShipName() : 'No Data' }}</td>
							<td>{{ (!is_null($admin_personal_data)) ? $admin_personal_data->getFullName() : 'No Data' }}</td>
						</tr>
					@endforeach
				@endif

			@endforeach

		@else
			<tr>
				<td colspan="13">No Data</td>
			</tr>
		@endif
	</tbody>
</table>