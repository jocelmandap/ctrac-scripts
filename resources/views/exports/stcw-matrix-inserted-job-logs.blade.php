<table>
	<thead>
        <tr>
            <th>Job Int ID</th>
            <th>Certificate ID</th>
            <th>Created On</th>
            <th>Created By</th>
            <th>Update Type</th>
        </tr>
	</thead>
	<tbody>
		@if (!empty($history_logs))
			@foreach ($history_logs as $log)
				
				<tr>
					<td>{{ $log['job_int_id'] }}</td>
					<td>{{ $log['cert_type_id'] }}</td>
					<td>{{ $log['created_on'] }}</td>
					<td>{{ $log['created_by'] }}</td>
					<td>{{ $log['stcw_status'] }}</td>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="5">No Data</td>
			</tr>
		@endif
	</tbody>
</table>