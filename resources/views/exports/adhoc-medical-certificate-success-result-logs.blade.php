<table>
	<thead>
        <tr>
            <th>User ID</th>
            <th>Document ID</th>
            <th>Document Entry ID</th>
            <th>Document Header ID</th>
            <th>Document Header Update Type</th>
        </tr>
	</thead>
	<tbody>
		@if (!empty($history_logs))
			@foreach ($history_logs as $log)
				
				<tr>
					<td>{{ $log['user_id'] }}</td>
                    <td>{{ $log['ck_list_id'] }}</td>
                    <td>{{ $log['document_entry_id'] }}</td>
                    <td>{{ $log['document_header'] }}</td>
                    <td>{{ $log['document_header_status'] }}</td>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="5">No Data</td>
			</tr>
		@endif
	</tbody>
</table>