<table>
	<thead>
        <tr>
            <th>Employee ID</th>
            <th>Document Entry Status</th>
            <th>Document Header Status</th>
            <th>Personal Data Status</th>
        </tr>
	</thead>
	<tbody>
		@php
			$count = 1;
		@endphp
		@if (!empty($history_logs))
			@foreach ($history_logs as $log)
				
				<tr>
					<td>{{ $log['jde_id'] }}</td>
					<td>{{ ($log['document_entry']) ? 'Updated' : 'Not Updated' }}</td>
					<td>{{ ($log['document_header']) ? 'Updated' : 'Not Updated' }}</td>
					<td>{{ ($log['personal_data']) ? 'Updated' : 'Not Updated' }}</td>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="13">No Data</td>
			</tr>
		@endif
	</tbody>
</table>