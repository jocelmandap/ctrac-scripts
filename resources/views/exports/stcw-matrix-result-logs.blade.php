<table>
	<thead>
        <tr>
            <th>Certificate ID</th>
            <th>Certificate Code</th>
            <th>Certificate Name</th>
            <th>Certificate Show Indicator</th>
            <th>Update Type</th>
        </tr>
	</thead>
	<tbody>
		@if (!empty($history_logs))
			@foreach ($history_logs as $log)
				
				<tr>
					<td>{{ $log['cert_type_id'] }}</td>
					<td>{{ $log['cert_type_code'] }}</td>
					<td>{{ $log['cert_type_name'] }}</td>
					<td>{{ $log['show_ind'] }}</td>
					<td>{{ $log['stcw_status'] }}</td>
				</tr>

			@endforeach	
		@else
			<tr>
				<td colspan="5">No Data</td>
			</tr>
		@endif
	</tbody>
</table>