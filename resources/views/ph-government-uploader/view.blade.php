@extends('layouts.master')

@section('title', 'PH Government Uploader')

@section('header-content')

	<h1>
		PH Government Document
		<small>Uploader</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">PH Government Document</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Update <i class="fa fa-refresh"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('ph_government_uploader_submit') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="update_by" class="col-md-4 control-label">Choose what to update:</label>

						    <div class="col-md-6">
						        <select id="ck_list_id" name="ck_list_id" class="form-control">
						        	<option value="180">Pag-Ibig</option>
						        	<option value="179">Philhealth</option>
						        	<option value="178">Social Security System</option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="upload_csv" class="col-md-4 control-label">Upload a CSV File</label>

						    <div class="col-md-6">
						        <input type="file" id="upload_csv" name="upload_csv">

						        <p class="help-block">Please download the sample format <a href="{{ asset('uploads/ph-documents/pagibig_sample.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label class="col-md-4 control-label">&nbsp;</label>

						    <div class="col-md-6">
						        <button type="submit" class="btn btn-primary">Submit</button>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>
		<!-- ./col -->

		@if (Session::has('history_logs') || Session::has('history_logs_not_updated'))
			<div class="col-md-6">
				
				@if (Session::has('history_logs'))
					<div class="box box-success">
						<div class="box-header with-border">
							<h3 class="box-title">Success Results: </h3>
							<a 
								href="{{ route('ph_government_uploader_success_result_logs_download') }}" 
								target="_blank" 
								class="btn btn-info btn-xs pull-right">Download</a>
						</div>
						<div class="box-body" style="overflow-y: auto; max-height: 400px;">
							<p>Total New / Updated: <label class="label label-info">{{ Session::get('total_updated') }}</label></p>
							@if (!empty(Session::get('history_logs')))
								<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
									<thead>
							            <tr>
							            	<th>#</th>
							                <th>Employee ID</th>
							                <th>Document Entry Status</th>
							                <th>Document Header Status</th>
							                <th>Personal Data Status</th>
							            </tr>
									</thead>
									<tbody>
										@php
											$count = 1;
										@endphp
										@foreach (Session::get('history_logs') as $log)
											
											<tr>
												<td>{{ $count }}</td>
												<td>{{ $log['jde_id'] }}</td>
												<td>{{ ($log['document_entry']) ? 'Updated' : 'Not Updated' }}</td>
												<td>{{ ($log['document_header']) ? 'Updated' : 'Not Updated' }}</td>
												<td>{{ ($log['personal_data']) ? 'Updated' : 'Not Updated' }}</td>
											</tr>
											
											@php
												$count++;
											@endphp

										@endforeach	
									</tbody>
								</table>
							@endif
						</div>	
					</div>
				@endif

				@if (Session::has('history_logs_not_updated'))
					<div class="box box-danger">
						<div class="box-header with-border">
							<h3 class="box-title">Not Updated Results: </h3>
							<a 
								href="{{ route('ph_government_uploader_fail_result_logs_download') }}" 
								target="_blank" 
								class="btn btn-info btn-xs pull-right">Download</a>
						</div>
						<div class="box-body" style="overflow-y: auto; max-height: 400px;">
							<p>Total of Not Updated: <label class="label label-info">{{ Session::get('total_not_updated') }}</label></p>
							@if (!empty(Session::get('history_logs_not_updated')))
								<table id="document-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
									<thead>
							            <tr>
							            	<th>#</th>
							                <th>Employee ID</th>
							                <th>Remarks</th>
							            </tr>
									</thead>
									<tbody>
										@php
											$count = 1;
										@endphp
										@foreach (Session::get('history_logs_not_updated') as $log)
											
											<tr>
												<td>{{ $count }}</td>
												<td>{{ $log['jde_id'] }}</td>
												<th>{{ $log['remarks'] }}</th>
											</tr>
											
											@php
												$count++;
											@endphp

										@endforeach	
									</tbody>
								</table>
							@endif
						</div>	
					</div>
				@endif
			</div>
		@endif

@endsection