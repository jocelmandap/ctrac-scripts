<!-- Modal -->
<div class="modal fade" id="page-form-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 700px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title page-form-title">Add New Page</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal page-form" id="page-form" method="POST" action="{{ route('admin_save_page') }}">
                    
                    {{ csrf_field() }}
                    <input type="hidden" name="page_id" id="page_id">

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="col-md-4 control-label">User Role</label>

                        <div class="col-md-6">
                            <select name="role_id[]" id="role_id" multiple="multiple">
                                @if (!$role_list->isEmpty())
                                    @foreach($role_list as $role)
                                        <option value="{{ $role->getId() }}">{{ $role->getName() }}</option>
                                    @endforeach
                                @else
                                    <option value="0">None</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="desc" class="col-md-4 control-label">Description</label>

                        <div class="col-md-6">
                            <textarea name="desc" id="desc" style="width: 100%; height: 100px; resize: none;"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="route_name" class="col-md-4 control-label">Route Name</label>

                        <div class="col-md-6">
                            <input id="route_name" type="text" class="form-control" name="route_name" value="">
                            <p class="help-block" style="font-size: 12px;">Note: Please add first a Route Name on your web.php route or 'none' if it is a Parent Page</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ordering" class="col-md-4 control-label">Ordering</label>

                        <div class="col-md-6">
                            <select name="ordering" id="ordering" class="form-control">
                                @for ($num = 0; $num <= 20; $num++)
                                    <option value="{{ $num }}">{{ $num }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="icon_class" class="col-md-4 control-label">Icon Class</label>

                        <div class="col-md-6">
                            <input id="icon_class" type="text" class="form-control" name="icon_class" value="">
                            <p class="help-block" style="font-size: 12px;">Note: Please select icon class <a href="https://adminlte.io/themes/AdminLTE/pages/UI/icons.html">here</a></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="bg_class" class="col-md-4 control-label">Background Color Class</label>

                        <div class="col-md-6">
                            <input id="bg_class" type="text" class="form-control" name="bg_class" value="">
                            <p class="help-block" style="font-size: 12px;">Note: Please select bacground class <a href="https://adminlte.io/themes/AdminLTE/pages/UI/general.html">here</a></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="is_active" class="col-md-4 control-label">Status</label>

                        <div class="col-md-6">
                            <select name="is_active" id="is_active" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="parent_id" class="col-md-4 control-label">Parent Page</label>

                        <div class="col-md-6">
                            <select name="parent_id" id="parent_id" class="form-control">
                                <option value="0">Yes</option>
                                @if (!$page_list->isEmpty())
                                    @foreach($page_list as $page)
                                        @if ($page->getParentId() == 0)
                                            <option value="{{ $page->getId() }}">{{ $page->getName() }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="submit_page_form">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>