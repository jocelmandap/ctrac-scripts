@extends('layouts.master')

@section('title', 'Page List | Pages')

@section('head')

	<link rel="stylesheet" href="/bower_components/datatables/media/css/dataTables.bootstrap.min.css">

@endsection

@section('header-content')

	<h1 class="page-title-header">
		Page List
		<small>Pages</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Page List</li>
	</ol>

@endsection

@section('main-content')
	
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Page List</h3>
					<button class="btn btn-xs btn-success pull-right" id="add_new_page">Add New Page</button>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="page-table-container" class="table table-striped table-bordered datatable-table" cellspacing="0" width="100%">
						<thead>
				            <tr>
				            	<th>#</th>
				                <th>Page Name</th>
				                <th>Description</th>
				                <th>User Role</th>
				                <th>Link</th>
				                <th>Ordering</th>
				                <th>Icon</th>
				                <th>Bacground Color Class</th>
				                <th>Status</th>
				                <th>Is Parent</th>
				                <th>Actions</th>
				            </tr>
						</thead>
						<tbody>
							@if (!$page_list->isEmpty())
								@foreach($page_list as $page)
									@php
										$role_ids = [];
									@endphp
									<tr>
										<td>{{ $page->getId() }}</td>
										<td>{{ $page->getName() }}</td>
										<td>{{ $page->getDescription() }}</td>
										<td>
											@if (!$page->roles->isEmpty())
												@foreach($page->roles as $role)
													<label class="label label-success">{{ $role->getName() }}</label><br>
													@php
														$role_ids[] = $role->getId();
													@endphp
												@endforeach
											@else
												<label class="label label-danger">None</label>
												@php
													$role_ids = [];
												@endphp
											@endif
										</td>
										<td>
											@if ($page->getRouteName() != 'none')
												@if (Route::has($page->getRouteName()))
													<a href="{{ route($page->getRouteName()) }}" target="_blank" rel="noopener noreferrer">Link</a>
												@else
													<a href="#" target="_blank" rel="noopener noreferrer">Not Available</a>
												@endif
											@else
												Parent page
											@endif
										</td>
										<td>{{ $page->getOrder() }}</td>
										<td><i class="fa {{ $page->getIconClass() }}"></i></td>
										<td>{{ $page->getBgClass() }}</td>
										<td>
											@if ($page->getIsActive())
												<label class="label label-success">Active</label>
											@else
												<label class="label label-danger">Inactive</label>
											@endif
										</td>
										<td>
											@if ($page->getParentId() == 0)
												Yes
											@else
												No
											@endif
										</td>
										<td>
											<button 
												data-id="{{ $page->getId() }}"
												data-name="{{ $page->getName() }}"
												data-desc="{{ $page->getDescription() }}"
												data-route="{{ $page->getRouteName() }}"
												data-ordering="{{ $page->getOrder() }}"
												data-icon="{{ $page->getIconClass() }}"
												data-bg="{{ $page->getBgClass() }}"
												data-status="{{ $page->getIsActive() }}"
												data-parent="{{ $page->getParentId() }}"
												data-roles="{{ implode($role_ids, ',') }}"
												class="btn btn-xs btn-info edit-page-form">Edit</button>
											<button 
												data-id="{{ $page->getId() }}"
												data-name="{{ $page->getName() }}"
												class="btn btn-xs btn-danger remove-page">Delete</button>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="10" style="text-align: center;">No Available Page</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->

@endsection

@section('modal-content')
	
	@include('pages.modals.form')
	@include('pages.modals.delete-form')

@endsection

@section('foot')
	<script src="/bower_components/datatables/media/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			var PageHandler = {
				init : function () {
					$('#page-table-container').DataTable();
					PageHandler.addNewModal();
					PageHandler.editModal();
					PageHandler.submitForm();
					PageHandler.deleteModal();
					PageHandler.deletePage();
				},
				clearFormData: function () {
					$('#page_id').val('');
					$('#name').val('');
					$('#desc').val('');
					$('#route_name').val('');
					$('#ordering').val(0);
					$('#icon_class').val('');
					$('#bg_class').val('');
					$('#is_active').val(1);
					$('#parent_id').val('0');
					$('#role_id').val(null).trigger('change');

					$("#role_id").select2({
					 	allowClear:true,
						placeholder: 'Select Roles'
					});
				},
				addNewModal : function () {
					$(document).on('click', '#add_new_page', function () {
						PageHandler.clearFormData();
						$('.page-form-title').text('Add New Page');
						$('#page-form-modal').modal('show');
					});
				},
				editModal : function () {
					$(document).on('click', '.edit-page-form', function () {
						PageHandler.clearFormData();
						var self = $(this);

						$('.page-form-title').text('Edit ' + self.data('name') + ' Page');
						$('#page_id').val(self.data('id'));
						$('#name').val(self.data('name'));
						$('#desc').val(self.data('desc'));
						$('#route_name').val(self.data('route'));
						$('#ordering').val(self.data('ordering'));
						$('#icon_class').val(self.data('icon'));
						$('#bg_class').val(self.data('bg'));
						$('#is_active').val(self.data('status'));
						$('#parent_id').val(self.data('parent'));
						$('#page-form-modal').modal('show');
						if ($.type(self.data('roles')) != 'number') {
							$('#role_id').val(self.data('roles').split(','));
						} else {
							$('#role_id').val(self.data('roles'));
						}
						
						$('#role_id').trigger('change');
					});
				},
				submitForm : function () {
					$('#submit_page_form').click(function () {
						$('#page-form').submit();
					});
				},
				deleteModal : function () {
					$(document).on('click', '.remove-page', function () {
						var self = $(this);
						var page_id = self.data('id');
						var name = self.data('name');

						$('.delete-page-name').text(name);
						$('#delete-page-id').val(page_id);
						$('#delete-page-name').val(name);
						$('#view-delete-page-modal').modal('show');
					});
				},
				deletePage : function () {
					$(document).on('click', '#submit_delete_page', function () {
						var page_id = $('#delete-page-id').val();
						var name = $('#delete-page-name').val();
						$.ajax({
							url: '{{ route('admin_delete_page') }}',
							type: 'POST',
							dataType: 'json',
							data: {
								page_id: page_id,
								name: name
							},
							beforeSend: function () {
								var content = `
									<div class="loader-bg-container">
										<div class="dv-loader-bg">
										 	<i class="fa fa-cog fa-spin fa-3x"></i>
										 	<p>Updating...</p>
										</div>
									</div>
								`;
								$('.delete-page-container').html(content);
							},
							success: function (response) {
								if (response.status == true) {
									$('#view-delete-page-modal').modal('hide');
									setTimeout(function () {
										location.reload();
									}, 700);
								} else {
									alert(response.message);
								}
							}
						});
					});
				}
			};

			PageHandler.init();
		});
	</script>
@endsection