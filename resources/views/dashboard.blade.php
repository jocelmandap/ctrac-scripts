@extends('layouts.master')

@section('title', 'Dashboard')

@section('header-content')

	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>

@endsection

@section('main-content')
	<!-- Small boxes (Stat box) -->
	<div class="row">
		
		@if (!$pages->isEmpty())
			
			@foreach ($parent_pages as $parent_page)
				
				@php
					$route_name = $parent_page->getRouteName();
					if (!Route::has($route_name))
					{
						$route_name = 'home';
					}
					
					$is_have_child = false;
					$child_pages_data = array();
					if (!empty($child_pages)) {
						foreach ($child_pages as $child_page) 
						{
							if ($parent_page->getId() == $child_page->getParentId()) {
								$is_have_child = true;
								$child_pages_data[] = $child_page;
							}
						}
					}

					if ($parent_page->getRouteName() == 'none')
					{
						$is_have_child = true;
					}
				@endphp
					
				@if ($is_have_child)

					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box {{ $parent_page->getBgClass() }}" style="position: relative;">
							<div class="inner">
								<h3>{{ count($child_pages_data) }}<sup style="font-size: 13px"> Pages</sup></h3>

								<p>{{ $parent_page->getName() }}</p>
							</div>
							<div class="icon">
								<i class="{{ $parent_page->getIconClass() }}" aria-hidden="true"></i>
							</div>
							<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

							@if (!empty($child_pages_data))
								<ul class="sub-menus-dashboard">
								@foreach ($child_pages_data as $child_page_data)

									@php
										$child_route_name = $child_page_data->getRouteName();
										if (!Route::has($child_route_name))
										{
											$child_route_name = 'home';
										}
									@endphp

									<li class="small-box {{ $child_page_data->getBgClass() }}">
										<div class="inner">
											<p>{{ $child_page_data->getName() }}</p>
										</div>
										<div class="icon sub-icon">
											<i class="{{ $child_page_data->getIconClass() }}" aria-hidden="true"></i>
										</div>
										<a 
											href="{{ route($child_route_name) }}" 
											class="small-box-footer ">More info <i class="fa fa-arrow-circle-right"></i></a>
									</li>

								@endforeach
								</ul>
							@endif
						</div>
					</div>

				@else

					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box {{ $parent_page->getBgClass() }}">
							<div class="inner">
								<h3>&nbsp;</h3>

								<p>{{ $parent_page->getName() }}</p>
							</div>
							<div class="icon">
								<i class="{{ $parent_page->getIconClass() }}" aria-hidden="true"></i>
							</div>
							<a href="{{ route($route_name) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					</div>
					<!-- ./col -->

				@endif

			@endforeach

		@else
			
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>&nbsp;</h3>

						<p>No Access to Pages</p>
					</div>
					<div class="icon">
						<i class="fa fa-address-book" aria-hidden="true"></i>
					</div>
					<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->

		@endif
	</div>
	<!-- /.row -->

@endsection