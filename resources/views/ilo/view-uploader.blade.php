@extends('layouts.master')

@section('title', 'ILO T&A Violation Data Uploader')

@section('header-content')

	<h1>
		ILO T&A Violation Data
		<small>Uploader</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('main_dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">ILO T&A Violation Data</li>
	</ol>

@endsection

@section('main-content')

	<!-- Small boxes (Stat box) -->
	<div class="row venue-skills-main">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Uploader <i class="fa fa-upload"></i></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="{{ route('ilo_tna_violation_data_uploader_submit') }}" role="form" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="box-body">
						
						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="period_of" class="col-md-4 control-label">Period of</label>

						    <div class="col-md-6">
						        <select name="period_of" id="period_of" class="form-control">
						        	<option value="1">1</option>
						        	<option value="2">2</option>
						        	<option value="3">3</option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="month_of" class="col-md-4 control-label">Month of</label>

						    <div class="col-md-6">
						        <select name="month_of" id="month_of" class="form-control">
						        	<option value="<?=date('m')?>"><?=date('F')?></option>
						        	<option value="1">January</option>
						        	<option value="2">February</option>
						        	<option value="3">March</option>
						        	<option value="4">April</option>
						        	<option value="5">May</option>
						        	<option value="6">June</option>
						        	<option value="7">July</option>
						        	<option value="8">August</option>
						        	<option value="9">September</option>
						        	<option value="10">October</option>
						        	<option value="11">November</option>
						        	<option value="12">December</option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="year_of" class="col-md-4 control-label">Year of</label>

						    <div class="col-md-6">
						        <select name="year_of" id="year_of" class="form-control">
						        	<option value="<?=date('Y')?>"><?=date('Y')?></option>
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="ship_id" class="col-md-4 control-label">Brand - Ship</label>

						    <div class="col-md-6">
						        <select name="ship" id="ship_id" class="form-control">
						        	@if (!$ships->isEmpty())
										@foreach ($ships as $ship)
											<option value="{{ $ship->getId() }}">{{ $ship->getBrand() . ' - ' . $ship->getName() . ' (' . $ship->getCode() . ')' }}</option>
										@endforeach
						        	@else
						        		<option value="">No Content</option>
						        	@endif
						        </select>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label for="upload_csv" class="col-md-4 control-label">Upload a CSV File</label>

						    <div class="col-md-6">
						        <input type="file" id="upload_csv" name="upload_csv" accept=".csv">

						        <p class="help-block">Please download the sample format <a href="{{ asset('uploads/salary-matrix/sample_ship_salary_matrix.csv') }}" target="_blank" rel="noopener noreferrer">here</a></p>
						    </div>
						</div>

						<div class="form-group" style="display: inline-block; width: 100%;">
						    <label class="col-md-4 control-label">&nbsp;</label>

						    <div class="col-md-6">
						        <button type="submit" class="btn btn-primary">Submit</button>
						    </div>
						</div>

					</div>
					<!-- /.box-body -->
				</form>
			</div>
		</div>
	</div>
	<!-- /.row -->

@endsection