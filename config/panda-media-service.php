<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Panda Media Service
	|--------------------------------------------------------------------------
	|
	| Media service for C-TRAC and other apps on HR IT
	|
	*/

	'host' => env('PANDA_HOST', 'http://10.27.80.57'),

	'host-test' => env('PANDA_HOST', 'http://10.27.80.57:7001'),

	'access-token' => env('PANDA_ACCESS_TOKEN', 'da39a3ee5e6b4b0d3255bfef95601890afd80709')
];