<?php

use Illuminate\Database\Seeder;
use CTRAC\Model\Scripts\CtracPage;
use CTRAC\Model\Scripts\CtracRole;

class CtracPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$super_admin = CtracRole::where('name', 'Super Admin')->first();
    	$admin  = CtracRole::where('name', 'Admin')->first();
    	$background_check_admin  = CtracRole::where('name', 'Background Check Admin')->first();
    	$archive_document_admin  = CtracRole::where('name', 'Archive Document Admin')->first();

        $pages = [
        	[
        		'name' => 'Background Checks',
        		'description' => 'View Background Check of a crew',
        		'route_name' => 'background_check_index',
        		'order' => 1,
        		'is_active' => 1,
        		'parent_id' => 0,
        		'icon_class' => 'fa fa-check'
        	],
        	[
        		'name' => 'Crisis Management',
        		'description' => 'adhoc functionality for Crisis Management',
        		'route_name' => 'crisis_management_index',
        		'order' => 2,
        		'is_active' => 1,
        		'parent_id' => 0,
        		'icon_class' => 'fa fa-list-alt'
        	],
        	[
        		'name' => 'CTRAC Gateways',
        		'description' => 'Upload CTRAC Gateways',
        		'route_name' => 'gateways_index_view',
        		'order' => 3,
        		'is_active' => 1,
        		'parent_id' => 0,
        		'icon_class' => 'fa fa-list-alt'
        	]
        ];

        foreach ($pages as $page) 
        {
        	$page_class = new CtracPage();
        	$page_class->name = $page['name'];
        	$page_class->description = $page['description'];
        	$page_class->route_name = $page['route_name'];
        	$page_class->order = $page['order'];
        	$page_class->is_active = $page['is_active'];
        	$page_class->parent_id = $page['parent_id'];
        	$page_class->icon_class = $page['icon_class'];
        	$page_class->save();
        	$page_role = array();

        	if ($page['name'] == 'Background Checks') {
        		$page_class->roles()->attach($super_admin);
        		$page_class->roles()->attach($admin);
        		$page_class->roles()->attach($background_check_admin);
        	}

        	if ($page['name'] == 'Crisis Management') {
        		$page_class->roles()->attach($super_admin);
        		$page_class->roles()->attach($admin);
        	}

        	if ($page['name'] == 'CTRAC Gateways') {
        		$page_class->roles()->attach($super_admin);
        		$page_class->roles()->attach($admin);
        	}

        	error_log('Page: ' . $page['name']);
        }
    }
}
