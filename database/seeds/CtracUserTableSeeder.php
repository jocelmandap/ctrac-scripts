<?php

use Illuminate\Database\Seeder;
use CTRAC\Model\Scripts\CtracUser;
use CTRAC\Model\Scripts\CtracRole;

class CtracUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$super_admin = CtracRole::where('name', 'Super Admin')->first();
    	$admin  = CtracRole::where('name', 'Admin')->first();
    	$background_check_admin  = CtracRole::where('name', 'Background Check Admin')->first();
    	$archive_document_admin  = CtracRole::where('name', 'Archive Document Admin')->first();

        $users = [
        	[
        		'name' => 'Super Admin',
        		'email' => 'admin@ctracdev.com',
        		'password' => bcrypt('ctracadmin123'),
        		'is_active' => 1
        	],
        	[
        		'name' => 'Admin',
        		'email' => 'admin@mail.com',
        		'password' => bcrypt('admin123'),
        		'is_active' => 1
        	],
        	[
        		'name' => 'Background Check Admin',
        		'email' => 'backgroundcheck@mail.com',
        		'password' => bcrypt('bc123'),
        		'is_active' => 1
        	],
        	[
        		'name' => 'Archive Document Admin',
        		'email' => 'archive@mail.com',
        		'password' => bcrypt('archive123'),
        		'is_active' => 1
        	]
        ];

        foreach ($users as $user) 
        {
        	$user_class = new CtracUser();
        	$user_class->name = $user['name'];
        	$user_class->email = $user['email'];
        	$user_class->password = $user['password'];
        	$user_class->is_active = $user['is_active'];
        	$user_class->save();
        	$role = array();

        	if ($user['name'] == 'Super Admin') {
        		$role = $super_admin;
        	}

        	if ($user['name'] == 'Admin') {
        		$role = $admin;
        	}

        	if ($user['name'] == 'Background Check Admin') {
        		$role = $background_check_admin;
        	}

        	if ($user['name'] == 'Archive Document Admin') {
        		$role = $archive_document_admin;
        	}

        	$user_class->roles()->attach($role);
        	error_log('User: ' . $user['name']);
        }
    }
}
