<?php

use Illuminate\Database\Seeder;
use CTRAC\Model\Scripts\CtracPage;
use CTRAC\Model\Scripts\CtracRole;

class ArchivePageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$super_admin = CtracRole::where('name', 'Super Admin')->first();
    	$admin  = CtracRole::where('name', 'Admin')->first();
    	$archive_document_admin  = CtracRole::where('name', 'Archive Document Admin')->first();

        $pages = [
        	[
        		'name' => 'Archive Documents',
        		'description' => 'View all archive documents',
        		'route_name' => 'archive_document_index',
        		'order' => 5,
        		'is_active' => 1,
        		'parent_id' => 0,
        		'icon_class' => 'fa fa-file-o'
        	]
        ];

        foreach ($pages as $page) 
        {
        	$page_class = new CtracPage();
        	$page_class->name = $page['name'];
        	$page_class->description = $page['description'];
        	$page_class->route_name = $page['route_name'];
        	$page_class->order = $page['order'];
        	$page_class->is_active = $page['is_active'];
        	$page_class->parent_id = $page['parent_id'];
        	$page_class->icon_class = $page['icon_class'];
        	$page_class->save();
        	$page_role = array();

        	$page_class->roles()->attach($super_admin);
        	$page_class->roles()->attach($admin);
        	$page_class->roles()->attach($archive_document_admin);

        	error_log('Page: ' . $page['name']);
        }
    }
}
