<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(CtracUserTableSeeder::class);
        // $this->call(CtracRoleTableSeeder::class);
        // $this->call(CtracPageTableSeeder::class);
        // $this->call(CtracPage2TableSeeder::class);
        // $this->call(ArchivePageTableSeeder::class);
        $this->call(ILOShipTableSeeder::class);
    }
}
