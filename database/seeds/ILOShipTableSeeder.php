<?php

use Illuminate\Database\Seeder;
use CTRAC\Helpers\DatabaseConnection;
use CTRAC\Model\ILO\Ship;

class ILOShipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ships = [
        	[
        		'name' => 'ADVENTURE OF THE SEAS',
        		'code' => 'AD',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'ALLURE OF THE SEAS',
        		'code' => 'AL',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'ANTHEM OF THE SEAS',
        		'code' => 'AN',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'BRILLIANCE OF THE SEAS',
        		'code' => 'BR',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'CELEBRITY CONSTELLATION',
        		'code' => 'CS',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'CELEBRITY ECLIPSE',
        		'code' => 'EC',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'CELEBRITY EDGE',
        		'code' => 'EG',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'ENCHANTMENT OF THE SEAS',
        		'code' => 'EN',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'CELEBRITY EQUINOX',
        		'code' => 'EQ',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'EXPLORER OF THE SEAS',
        		'code' => 'EX',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'FREEDOM OF THE SEAS',
        		'code' => 'FR',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'SKYSEA GOLDEN ERA',
        		'code' => 'GE',
        		'brand' => 'SKS'
        	],
        	[
        		'name' => 'GRANDEUR OF THE SEAS',
        		'code' => 'GR',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'HARMONY OF THE SEAS',
        		'code' => 'HM',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'INDEPENDENCE OF THE SEAS',
        		'code' => 'ID',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'CELEBRITY INFINITY',
        		'code' => 'IN',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'AZAMARA JOURNEY',
        		'code' => 'JR',
        		'brand' => 'ACC'
        	],
        	[
        		'name' => 'LIBERTY OF THE SEAS',
        		'code' => 'LB',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'MARINER OF THE SEAS',
        		'code' => 'MA',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'MAJESTY OF THE SEAS',
        		'code' => 'MJ',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'CELEBRITY MILLENNIUM',
        		'code' => 'ML',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'EMPRESS OF THE SEAS',
        		'code' => 'NE',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'NAVIGATOR OF THE SEAS',
        		'code' => 'NV',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'OASIS OF THE SEAS',
        		'code' => 'OA',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'OVATION OF THE SEAS',
        		'code' => 'OV',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'AZAMARA PURSUIT',
        		'code' => 'PR',
        		'brand' => 'ACC'
        	],
        	[
        		'name' => 'QUANTUM OF THE SEAS',
        		'code' => 'QN',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'AZAMARA QUEST',
        		'code' => 'QS',
        		'brand' => 'ACC'
        	],
        	[
        		'name' => 'RADIANCE OF THE SEAS',
        		'code' => 'RD',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'CELEBRITY REFLECTION',
        		'code' => 'RF',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'RHAPSODY OF THE SEAS',
        		'code' => 'RH',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'CELEBRITY SILHOUETTE',
        		'code' => 'SI',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'CELEBRITY SOLSTICE',
        		'code' => 'SL',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'CELEBRITY SUMMIT',
        		'code' => 'SM',
        		'brand' => 'CCI'
        	],
        	[
        		'name' => 'SERENADE OF THE SEAS',
        		'code' => 'SR',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'SYMPHONY OF THE SEAS',
        		'code' => 'SY',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'VISION OF THE SEAS',
        		'code' => 'VI',
        		'brand' => 'RCI'
        	],
        	[
        		'name' => 'VOYAGER OF THE SEAS',
        		'code' => 'VY',
        		'brand' => 'RCI'
        	]
        ];

        /**
         * Set DB Connection to Production
         */
        $db_connection = DatabaseConnection::CTRAC_QA;
        if (config('app.env') != 'local')
        {
            $db_connection = DatabaseConnection::CTRAC_PROD;
        }

        foreach ($ships as $ship) 
        {
        	$ship_class = new Ship();
        	$ship_class->setDBConnection($db_connection);
        	$ship_class->name = $ship['name'];
        	$ship_class->code = $ship['code'];
        	$ship_class->brand = $ship['brand'];
        	$ship_class->save();

        	error_log('Ship: ' . $ship['name']);
        }
    }
}
