<?php

use Illuminate\Database\Seeder;
use CTRAC\Model\Scripts\CtracRole;

class CtracRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	[
        		'name' => 'Super Admin',
        		'description' => 'Granted all access to all module'
        	],
        	[
        		'name' => 'Admin',
        		'description' => 'Granted limited access'
        	],
        	[
        		'name' => 'Background Check Admin',
        		'description' => 'Granted access only for Background check module'
        	],
        	[
        		'name' => 'Archive Document Admin',
        		'description' => 'Granted access only for archive document files module'
        	]
        ];

        foreach ($roles as $role) 
        {
        	$role_class = new CtracRole();
        	$role_class->name = $role['name'];
        	$role_class->description = $role['description'];
        	$role_class->save();

        	error_log('Role: ' . $role['name']);
        }
    }
}
