<?php

use Illuminate\Database\Seeder;
use CTRAC\Model\Scripts\CtracPage;
use CTRAC\Model\Scripts\CtracRole;

class CtracPage2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = CtracRole::where('name', 'Super Admin')->first();
    	$admin  = CtracRole::where('name', 'Admin')->first();
    	$background_check_admin  = CtracRole::where('name', 'Background Check Admin')->first();
    	$archive_document_admin  = CtracRole::where('name', 'Archive Document Admin')->first();

        $pages = [
        	[
        		'name' => 'CTRAC Reports',
        		'description' => 'CTRAC Reports',
        		'route_name' => 'None',
        		'order' => 4,
        		'is_active' => 1,
        		'parent_id' => 0,
        		'icon_class' => 'fa fa-bar-chart'
        	],
        	[
        		'name' => 'test sub page 1',
        		'description' => 'test sub page 1',
        		'route_name' => 'crisis_management_index',
        		'order' => 1,
        		'is_active' => 1,
        		'parent_id' => 9,
        		'icon_class' => 'fa fa-circle-o'
        	],
        	[
        		'name' => 'test sub page 2',
        		'description' => 'test sub page 2',
        		'route_name' => 'gateways_index_view',
        		'order' => 2,
        		'is_active' => 1,
        		'parent_id' => 9,
        		'icon_class' => 'fa fa-circle-o'
        	]
        ];

        foreach ($pages as $page) 
        {
        	$page_class = new CtracPage();
        	$page_class->name = $page['name'];
        	$page_class->description = $page['description'];
        	$page_class->route_name = $page['route_name'];
        	$page_class->order = $page['order'];
        	$page_class->is_active = $page['is_active'];
        	$page_class->parent_id = $page['parent_id'];
        	$page_class->icon_class = $page['icon_class'];
        	$page_class->save();
        	$page_role = array();

        	$page_class->roles()->attach($super_admin);
        	$page_class->roles()->attach($admin);

        	error_log('Page: ' . $page['name']);
        }
    }
}
